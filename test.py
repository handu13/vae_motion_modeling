import os
import numpy as np
from Config import Config
from mosi_dev_deepmotionmodeling.mosi_utils_anim import load_json_file, write_to_json_file
from mosi_dev_deepmotionmodeling.preprocessing.preprocessing import process_file
from src_final.network.Discriminator import HPDiscriminator, Bihmp_Discriminator, CNN_Discriminator
from src_final.network.Generator import HPGenerator,Bihmp_Generator, TCN_RNN_Generator
from src_final.model.HPGAN import HPGAN
from src_final.model.BIHMPGAN import BIHMPGAN
from src_final.model.motionGAN_recon import motionGAN_recon
from src_final.model.motionGAN_norecon import motionGAN_norecon
from src_final.utils import CMU_SKELETON, CMU_ANIMATED_JOINTS
import itertools
import copy
EPS = 1e-6
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
'''
evaluation:
    long-term
    short-term prediction
    discriminator output
    different actions?
'''

def preprocess(filename, window):
    clip = process_file(filename, window=window, window_step=window, sliding_window=True, animated_joints=CMU_ANIMATED_JOINTS,
                 body_plane_indices=[6,27,2], fid_l=[4,5], fid_r=[29,30])
    return clip


def test(generator, evaluator, config):
    training_data = np.load(config['data']['data_path'])
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    training_data = (training_data - mean_pose) / std_pose
    dataset_dir = 'data/training_data/preprocessed/input_data/preprocessed'
    testing_dataset = [os.path.join(dataset_dir, 'ART_6kmh.bvh'),
                       os.path.join(dataset_dir, 'Captury_6kmh.bvh'),
                       os.path.join(dataset_dir, 'OptiTrack_6kmh.bvh'),
                       os.path.join(dataset_dir, 'Vicon_6kmh.bvh')]
    training_quality = evaluator(training_data)
    if type(training_quality) is tuple:
        training_quality = np.mean(training_quality[0].numpy())
    else:
        training_quality = np.mean(training_quality.numpy())
    test_qualities = []
    for dataset in testing_dataset:
        test_data = preprocess(dataset, config['data']['input_sequence_length']+config['data']['output_sequence_length'])
        test_data = np.asarray(test_data, dtype=np.float32)
        mean_pose = test_data.mean(axis=(0, 1))
        std_pose = test_data.std(axis=(0, 1))
        std_pose[std_pose<EPS] = EPS
        test_data = (test_data - mean_pose) / std_pose
        test_quality = evaluator(test_data)
        if type(test_quality) is tuple:
            test_quality = np.mean(test_quality[0].numpy())
        else:
            test_quality = np.mean(test_quality.numpy())
        test_qualities.append(test_quality)
    test_qualities = np.asarray(test_qualities, dtype=np.float32)

    relative = (training_quality - test_qualities) - min(training_quality - test_qualities)

    return test_qualities, training_quality, relative


def main():
    config = load_json_file(r'config/motionGAN/config_10.json')
    name = "motionGAN_10"
    #output_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output')
    g = TCN_RNN_Generator(config["generator"])
    d = CNN_Discriminator(config["discriminator"])
    model = motionGAN_recon(g, d, name, debug=True)
    model.load(suffix=str(200))
    test_qualities, training_quality, relative = test(model.g, model.d, config)
    print("MotionGAN result:")
    print("training data qualities: ")
    print(training_quality)
    print("testing data qualities: ")
    print("ART  CAPTURY  OPTITRACK  VICON")
    print(test_qualities)
    print("relative quality:")
    print(relative)

    # #################HPGAN
    # config = load_json_file(r'config/HP-GAN/config_0.json')
    # name = "HP-GAN_0"
    # output_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output')
    # g = HPGenerator(config["generator"])
    # d = HPDiscriminator(config["discriminator"])
    # e = HPDiscriminator(config["evaluator"])
    # model = HPGAN(g, d, e, name, debug=True, output_dir=output_dir)
    # model.load(suffix=str(200))
    # test_qualities, training_quality, relative = test(g, d, config)
    # print("HP-GAN result:")
    # print("training data qualities: ")
    # print(training_quality)
    # print("testing data qualities: ")
    # print("ART  CAPTURY  OPTITRACK  VICON")
    # print(test_qualities)
    # print("relative quality:")
    # print(relative)

    ##############BihmpGAN
    # config = load_json_file(r'config/Bihmp-GAN/config_0.json')
    # name = "Bihmp-GAN_0"
    # output_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output')
    # g = Bihmp_Generator(config["generator"])
    # d = Bihmp_Discriminator(config["discriminator"])
    # e = Bihmp_Discriminator(config["evaluator"])
    # model = BIHMPGAN(g, d, None, name, debug=True, output_dir=output_dir)
    # model.load(suffix=str(200))
    # test_qualities, training_quality, relative = test(g, d, config)
    # print("Bihmp-GAN result:")
    # print("training data qualities: ")
    # print(training_quality)
    # print("testing data qualities: ")
    # print("ART  CAPTURY  OPTITRACK  VICON")
    # print(test_qualities)
    # print("relative quality:")
    # print(relative)


if __name__ == "__main__":
    filename = 'data/training_data/preprocessed/input_data/preprocessed/Vicon_6kmh.bvh'
    clip = process_file(filename, window=64, window_step=10, sliding_window=True, animated_joints=CMU_ANIMATED_JOINTS,
                 body_plane_indices=[6,27,2], fid_l=[4,5], fid_r=[29,30])
    clip = np.asarray(clip, dtype=np.float32)
    print(clip.shape)
    output = 'data/training_data/training_clips.npy'
    np.save(output, clip)
        
        