import os 
import sys
import numpy as np
import argparse
import logging
import tensorflow as tf
tf.get_logger().setLevel('ERROR')
dirname = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, os.path.join(dirname, '..'))
os.chdir(os.path.join(dirname, '..'))

from mosi_dev_deepmotionmodeling.mosi_utils_anim import load_json_file
from mosi_dev_deepmotionmodeling.mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder
from mosi_dev_deepmotionmodeling.models.frame_encoder import FrameEncoder
from preprocessing.preprocessing import process_bvhfile, process_file
from mosi_dev_deepmotionmodeling.utilities.utils import get_global_position_framewise, get_global_position, export_point_cloud_data_without_foot_contact
EPS = 1e-6


def get_training_data(name='h36m', data_type='angle'):
    data_path = os.path.join(dirname, r'..', r'data\training_data\processed_mocap_data', name)
    filename = '_'.join([name, data_type]) + '.npy'
    if not os.path.isfile(os.path.join(data_path, filename)):
        print("Cannot find " + os.path.join(data_path, filename))
    else:

        motion_data = np.load(os.path.join(data_path, filename))
        return motion_data


def main():
    parser = argparse.ArgumentParser("reconstruction motion from frame encoder")
    parser.add_argument("-test_file",
                        type=str,
                        default=r'E:\workspace\mocap_data\mk_cmu_retargeting_default_pose\h36m\S1\WalkTogether.bvh')
    parser.add_argument("-save_path",
                        type=str,
                        default='.')
    arg = parser.parse_args()
    dropout_rate = 0.1
    pretrained_model = r'data\models\frame_encoder1\frame_encoder-100-0.0001-0.1-0100.ckpt'
    encoder = FrameEncoder(dropout_rate=dropout_rate)
    encoder.load_weights(pretrained_model)

    ### load meta data
    h36m_data = get_training_data()
    print(h36m_data.shape)
    h36m_data = np.reshape(h36m_data, (h36m_data.shape[0] * h36m_data.shape[1], h36m_data.shape[2]))
    ### normalize data
    mean_value = h36m_data.mean(axis=0)[np.newaxis, :]
    std_value = h36m_data.std(axis=0)[np.newaxis, :]
    std_value[std_value<EPS] = EPS

    motion_data = process_bvhfile(arg.test_file, sliding_window=False)
    motion_data1 = process_file(arg.test_file, sliding_window=False)
    normalized_data = (motion_data - mean_value) / std_value    
    normalized_data1 = (motion_data1 - mean_value) / std_value
    enc_res = encoder(normalized_data)
    enc_res1 = encoder(normalized_data1)
    recon_motion = enc_res * std_value + mean_value
    recon_motion1 = enc_res1 * std_value + mean_value

    export_point_cloud_data_without_foot_contact(recon_motion.numpy(), os.path.join(arg.save_path, 'recon1.panim'))
    export_point_cloud_data_without_foot_contact(recon_motion1.numpy(), os.path.join(arg.save_path, 'recon2.panim'))


if __name__ == "__main__":
    main()
