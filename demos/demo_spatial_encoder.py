import os 
import sys
import numpy as np
import argparse
import logging
import tensorflow as tf
tf.get_logger().setLevel('ERROR')
dirname = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, os.path.join(dirname, '..'))
os.chdir(os.path.join(dirname, '..'))
from src_final.model.motionGAN_recon import motionGAN_recon
from src_final.network.Generator import TCN_RNN_Generator
from src_final.network.Discriminator import CNN_Discriminator
from mosi_dev_deepmotionmodeling.mosi_utils_anim import load_json_file
from mosi_dev_deepmotionmodeling.mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder
from src_final.network.PoseEmbedding import SpatialEncoder, SpatialDecoder
from src_final.utils import CMU_SKELETON, CMU_JOINT_LEVELS
from preprocessing.preprocessing import process_bvhfile, process_file
from mosi_dev_deepmotionmodeling.utilities.utils import get_global_position_framewise, get_global_position, export_point_cloud_data_without_foot_contact
EPS = 1e-6

    
def get_full_skeleton_data():
    training_data = np.load(os.path.join(dirname, '..', r'data/training_data/training_clips_h36m.npy'))
    return training_data



def test():
    training_data = r''



def main():
    parser = argparse.ArgumentParser("reconstruct motion from spatial encoder")
    parser.add_argument('-test_file',
                        type=str,
                        help="Provide the path of your test file.",
                        required=True)
    parser.add_argument('-save_path',
                        type=str,
                        default='.')       
                                    
    arg = parser.parse_args()

    # enc_file = os.path.join(dirname, '..', 'output/spatialEncoderNewNoGAN/spatialEncoder_new_nogan_SpatialEncoder_1000.ckpt')
    # dec_file = os.path.join(dirname, '..', 'output/spatialEncoderNewNoGAN/spatialEncoder_new_nogan_SpatialDecoder_1000.ckpt') 
    enc_file = os.path.join(dirname, '..', 'output/spatialEncoder/model/spatialEncoder_SpatialEncoder_150.ckpt')
    dec_file = os.path.join(dirname, '..', 'output/spatialEncoder/model/spatialEncoder_SpatialDecoder_150.ckpt') 
    spatialEncoder = SpatialEncoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
    spatialDecoder = SpatialDecoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)    
    spatialEncoder.load_weights(enc_file)
    spatialDecoder.load_weights(dec_file)

    ### load meta data
    h36m_data = get_full_skeleton_data()   ## the data is different from FrameEncoder
    mean_pose = h36m_data.mean(axis=(0, 1))
    std_pose = h36m_data.std(axis=(0, 1))
    std_pose[std_pose < EPS] = EPS    

    ### 
    bvhreader = BVHReader(arg.test_file)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader)
    animated_joints = skeleton.animated_joints
    motion_data = process_file(arg.test_file, animated_joints=animated_joints, sliding_window=False)
    
    ### normalized input file
    normalized_data = (motion_data - mean_pose) / std_pose

    enc_res = spatialEncoder(normalized_data)
    dec_res = spatialDecoder(enc_res)
    dec_res = dec_res.numpy()
    reconstruction = dec_res * std_pose + mean_pose 

    # export_to_bvh_file()
    filename = os.path.split(arg.test_file)[-1]

    export_point_cloud_data_without_foot_contact(reconstruction, os.path.join(arg.save_path, filename.replace('bvh', 'panim')),
                                                 scale_factor=5, skeleton=CMU_SKELETON)




if __name__ == "__main__":
    # main()
    test()
