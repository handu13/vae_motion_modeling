import numpy as np
import tensorflow as tf
import model_utils as nn

class SequenceToSequenceGenerator(object):
    '''
    GAN generator network that predict future skeleton poses using sequernce to sequence network.
    '''
    def __init__(self, inputs, inputs_depth, z, input_sequence_length, output_sequence_length, 
                 cell_type='gru', project_to_rnn_output=False, reverse_input=False,
                 use_attention=False, use_residual=False,
                 bias_initializer=tf.compat.v1.constant_initializer(0.), kernel_initializer=tf.compat.v1.truncated_normal_initializer(stddev=0.001),
                 reuse=False):
        '''
        Initialize the generative network.

        Args:
            inputs(tf.placeholder): The input variable containing current data.
            inputs_depth(int): input embed size.
            z(tf.placeholder, optional): A random generated input vector used as input.
            input_sequence_length(int): the length of the input sequence.
            output_sequence_length(int): the length of the resulted sequence.
            cell_type(str): The type of cell to use for the encode and decoder.
            project_to_rnn_output(bool): project the input to the number of hidden unit in the RNN.
            reverse_input(bool): reverse the input sequence before feeding it to the network.
            use_attention(bool): true to use attention instead of the last state of the encoder.
            use_residual(bool): use resent like structure for the recurrent.
            bias_initializer: initializer for the bias value.
            kernel_initializer: initializer for the `W` parameters.            
            reuse(bool): True to reuse model parameters from a previously created model.
        '''
        self._reuse = reuse
        self._batch_size = tf.shape(input=inputs)[0] # batch_size
        self._input_sequence_length = input_sequence_length
        self._output_sequence_length = output_sequence_length
        self._inputs_depth = inputs_depth
        self._inputs_shape = inputs.shape
        self._element_shape = inputs.shape[2:].as_list()
        self._output = None
        self._parameters = []
        self._weights = []
        self._num_neurons = 1024
        self._num_layers = 2
        self._num_nn_layers = 2
        self._cell_type = cell_type
        self._bias_initializer = bias_initializer
        self._kernel_initializer = kernel_initializer
        self._reccurent_bias_initializer = None
        self._reccurent_kernel_initializer = None
        self._project_to_rnn_output = project_to_rnn_output
        self._use_attention = use_attention
        self._use_residual = use_residual

        if self._use_residual:
            self._project_to_rnn_output = True

        # Similar to tf.zeros but support variable batch size.
        if self._project_to_rnn_output:
            self._zeros_input = tf.fill(tf.stack([tf.shape(input=inputs)[0], self._num_neurons]), 0.0)
        else:
            self._zeros_input = tf.fill(tf.stack([tf.shape(input=inputs)[0], self._inputs_depth]), 0.0)

        if reverse_input:
            inputs = tf.reverse(inputs, axis=[1])
        self.create_model(inputs, z)

    @property
    def output(self):
        return self._output

    @property
    def parameters(self):
        ''' All trainable parameters '''
        return self._parameters

    @property
    def weights(self):
        ''' Weights only parameters for regularization '''
        return self._weights

    def create_model(self, inputs, z):
        '''
        Construct a generative model.

        Args:
            inputs(tf.placeholder): The input variable containing current data.
            z(tf.placeholder): A vector containss the randomly generated latent data.
        '''
        with tf.compat.v1.variable_scope(self.__class__.__name__, reuse=self._reuse) as vs:

            outputs, encoder_state = self._build_encoder(inputs, z)

            first_input = outputs[:, -1, :] # [batch, sequence, elements]
            if self._use_attention:
                encoder_state = nn.attention(outputs, 
                                             kernel_initializer=self._kernel_initializer,
                                             bias_initializer=self._bias_initializer)
            self._output = self._build_decoder(first_input, z, encoder_state)

            self._parameters = tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.TRAINABLE_VARIABLES, scope=vs.name+'/')
            self._weights = [v for v in self._parameters if (v.name.endswith('Wi:0') or \
                                                             v.name.endswith('weights:0') or \
                                                             v.name.endswith('Wo:0') or \
                                                             v.name.endswith('Wsi:0') or \
                                                             ('Wzi' in v.name) or \
                                                             ('Wzci' in v.name) or \
                                                             ('Wzhi' in v.name))]

    def _create_rnn_model(self):
        ''' Create RNN model '''
        return nn.create_rnn_model(self._num_layers, 
                                   self._cell_type, 
                                   self._num_neurons, 
                                   use_residual=self._use_residual)

    def _input_projection(self, inputs):
        ''' Project each skeleton pose to the encoder. '''

        inputs = tf.reshape(inputs, shape=[-1, inputs.shape[1].value]+[np.prod(inputs.shape[2:].as_list())]) #[batch,sequence,96]
        if self._project_to_rnn_output:
            net = inputs
            layer_index = 0
            num_neurons = self._num_neurons // (self._num_nn_layers+1) #num_neurons // 3???
            for i in range(self._num_nn_layers):
                net = tf.compat.v1.layers.dense(inputs=net, 
                                      units=(i+1)*num_neurons, #increasing hidden neurons
                                      kernel_initializer=self._kernel_initializer,
                                      bias_initializer=self._bias_initializer,
                                      activation=tf.nn.relu,
                                      name="fc{}".format(layer_index+1))
                # net = tf.layers.dropout(inputs=net, rate=0.5)
                layer_index += 1

            encoder_inputs = tf.compat.v1.layers.dense(inputs=net, 
                                             units=self._num_neurons,
                                             kernel_initializer=self._kernel_initializer,
                                             bias_initializer=self._bias_initializer,
                                             activation=None, 
                                             name="fc{}".format(layer_index+1))
            encoder_inputs.set_shape([inputs.shape[0].value, inputs.shape[1].value, self._num_neurons])
        else:
            Wi = tf.compat.v1.get_variable("Wi", shape=[np.prod(self._element_shape), self._inputs_depth], initializer=self._kernel_initializer)
            bi = tf.compat.v1.get_variable("bi", shape=[self._inputs_depth], initializer=self._bias_initializer)

            encoder_inputs = tf.tensordot(inputs, Wi, axes=[[2], [0]])
            encoder_inputs.set_shape([inputs.shape[0].value, inputs.shape[1].value, self._inputs_depth]) # https://github.com/tensorflow/tensorflow/issues/6682
            encoder_inputs = encoder_inputs + bi

        return encoder_inputs

    def _output_projection(self, outputs):
        ''' Project each decoder output back to skeleton pose. '''

        if self._project_to_rnn_output:
            net = outputs
            layer_index = 0
            for i in range(self._num_nn_layers):
                net = tf.compat.v1.layers.dense(inputs=net, 
                                      units=int(self._num_neurons/(i+1)),
                                      kernel_initializer=self._kernel_initializer,
                                      bias_initializer=self._bias_initializer,
                                      activation=tf.nn.relu, 
                                      name="fc{}".format(layer_index+1))
                # net = tf.layers.dropout(inputs=net, rate=0.5)
                layer_index += 1

            pred = tf.compat.v1.layers.dense(inputs=net, 
                                   units=np.prod(self._element_shape),
                                   kernel_initializer=self._kernel_initializer,
                                   bias_initializer=self._bias_initializer,
                                   activation=None, 
                                   name="fc{}".format(layer_index+1))

            pred.set_shape([outputs.shape[0].value, self._output_sequence_length, np.prod(self._element_shape)])
            pred = tf.reshape(pred, shape=[-1, pred.shape[1].value] + self._inputs_shape[2:].as_list())
        else:
            Wo = tf.compat.v1.get_variable("Wo", shape=[self._num_neurons, np.prod(self._element_shape)], initializer=self._kernel_initializer)
            bo = tf.compat.v1.get_variable("bo", shape=[np.prod(self._element_shape)], initializer=self._bias_initializer)

            pred = tf.tensordot(outputs, Wo, axes=[[2], [0]])
            pred.set_shape([outputs.shape[0].value, self._output_sequence_length, np.prod(self._element_shape)]) # https://github.com/tensorflow/tensorflow/issues/6682
            pred = pred + bo
            pred = tf.reshape(pred, shape=[-1, pred.shape[1].value] + self._inputs_shape[2:].as_list())

        return pred

    def _build_encoder(self, inputs, z):
        ''' Build the encoder part of the generative mode. '''
        with tf.compat.v1.variable_scope("encoder", reuse=self._reuse):
            encoder_inputs = self._input_projection(inputs)
            cell = self._create_rnn_model()
            outputs, state = tf.compat.v1.nn.dynamic_rnn(cell, encoder_inputs, dtype=tf.float32)

            return outputs, state

    def _build_decoder(self, first_input, z, encoder_state):
        '''
        Build the decoder part of the generative mode. It can decode based on the initial state without
        the need of future_inputs.

        Args:
            first_input(tf.placeholder, optional): each cell takes input form the output of the previous cell,
                                                   except first cell. first_input is used for the first cell.
            z(tf.placeholder, optional): random vector in order to sample multiple predictions from the 
                                         same input.
            encoder_state(cell state): the last state of the encoder.

        Return:
            The output of the network.
        '''
        with tf.compat.v1.variable_scope("decoder", reuse=self._reuse):
            cell = self._create_rnn_model()
            outputs, _ = self._dynamic_rnn_decoder(cell, first_input, z, encoder_state, self._output_sequence_length)
            return self._output_projection(outputs)

    def _dynamic_rnn_decoder(self, cell, first_input, z, encoder_state, sequence_length, time_major=False, dtype=tf.float32):
        ''' Unroll the RNN decoder '''
        if not self._project_to_rnn_output:
            # From output state to input embed.
            Wsi = tf.compat.v1.get_variable("Wsi", 
                                  shape=[self._num_neurons, self._inputs_depth], 
                                  initializer=self._kernel_initializer)

        if first_input is None:
            first_input = self._zeros_input

        first_input = first_input if self._project_to_rnn_output else tf.matmul(first_input, Wsi)
        #if _project_to_rnn_output, the first input shape is [,num_neuron], else [,inputs_depth]

        if z is not None:
            is_tuple = isinstance(encoder_state[0], tf.nn.rnn_cell.LSTMStateTuple) if (self._num_layers > 1) else isinstance(encoder_state, tf.nn.rnn_cell.LSTMStateTuple)
            if is_tuple:
                if self._num_layers > 1:
                    states = []
                    for i in range(self._num_layers):
                        Wzhi = tf.compat.v1.get_variable("Wzhi{}".format(i), shape=[z.shape.as_list()[-1], encoder_state[i].h.shape.as_list()[-1]], initializer=self._kernel_initializer)
                        Wzci = tf.compat.v1.get_variable("Wzci{}".format(i), shape=[z.shape.as_list()[-1], encoder_state[i].c.shape.as_list()[-1]], initializer=self._kernel_initializer)                        
                        states.append(tf.nn.rnn_cell.LSTMStateTuple(encoder_state[i].c + tf.matmul(z, Wzci), encoder_state[i].h + tf.matmul(z, Wzhi)))
                    encoder_state = tuple(states)
                else:
                    Wzhi = tf.compat.v1.get_variable("Wzhi", shape=[z.shape.as_list()[-1], encoder_state.h.shape.as_list()[-1]], initializer=self._kernel_initializer)
                    Wzci = tf.compat.v1.get_variable("Wzci", shape=[z.shape.as_list()[-1], encoder_state.c.shape.as_list()[-1]], initializer=self._kernel_initializer)      
                    encoder_state = tf.nn.rnn_cell.LSTMStateTuple(encoder_state.c + tf.matmul(z, Wzci), encoder_state.h + tf.matmul(z, Wzhi))
            #not is_tuple, means not from lstmencoder?
            else:
                if self._num_layers > 1:
                    states = []
                    for i in range(self._num_layers):
                        Wzi = tf.compat.v1.get_variable("Wzi{}".format(i), shape=[z.shape.as_list()[-1], encoder_state[i].shape.as_list()[-1]], initializer=self._kernel_initializer)
                        states.append(encoder_state[i] + tf.matmul(z, Wzi))
                    encoder_state = tuple(states)
                else:
                    Wzi = tf.compat.v1.get_variable("Wzi", shape=[z.shape.as_list()[-1], encoder_state.shape.as_list()[-1]], initializer=self._kernel_initializer)
                    encoder_state = encoder_state + tf.matmul(z, Wzi)

        def loop_fn_init(time):
            elements_finished = (sequence_length <= 0)
            next_input = first_input
            next_cell_state = encoder_state
            emit_output = None
            next_loop_state = None

            return (elements_finished, next_input, next_cell_state, emit_output, next_loop_state)

        def loop_fn_next(time, cell_output, cell_state, loop_state):
            emit_output = cell_output
            next_cell_state = cell_state

            elements_finished = (time >= sequence_length)
            finished = tf.reduce_all(input_tensor=elements_finished) #reduce all batches
            next_input = tf.cond(
                pred=finished,
                true_fn=lambda: self._zeros_input,
                false_fn=lambda: cell_output if self._project_to_rnn_output else tf.matmul(cell_output, Wsi))
            next_loop_state = None
            return (elements_finished, next_input, next_cell_state, emit_output, next_loop_state)

        def loop_fn(time, cell_output, cell_state, loop_state):
            if cell_output is None:
                return loop_fn_init(time)
            else:
                return loop_fn_next(time, cell_output, cell_state, loop_state)
        #details about loop function for rnn:https://www.tensorflow.org/api_docs/python/tf/nn/raw_rnn?hl=en 
        outputs_ta, final_state, _ = tf.compat.v1.nn.raw_rnn(cell, loop_fn)
        outputs = outputs_ta.stack()

        if not time_major:
            outputs = tf.transpose(a=outputs, perm=[1, 0, 2])

        return outputs, final_state

    def train(self, input_data, learning_rate=1e-5, batch_size=256, step_save=1000):
        pass
        
    def predict(self):
        pass

    def save_model(self):
        pass

    def load_model(self):
        pass
