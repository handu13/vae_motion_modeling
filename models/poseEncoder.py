import os, time, sys, traceback
import utils
import numpy as np
import tensorflow as tf
from collections import OrderedDict

class PoseEncoder(object):
    def __init__(self,num_joints,num_params_per_joint,num_zdim,sess=None):

        self.input_x = tf.compat.v1.placeholder(tf.float32, shape=[None, num_joints, num_params_per_joint], name='input_x')
        self.input_z = tf.compat.v1.placeholder(tf.float32, shape=[None, num_zdim], name='input_z')
        self.num_zdim = num_zdim
        self.num_params_per_joint = num_params_per_joint
        
        if sess is not None:
            self.sess = sess
        else:
            config = tf.compat.v1.ConfigProto()
            self.sess = tf.compat.v1.InteractiveSession(config=config)

        self.joint_names = [   
            "Hips", "LowerBack", "Spine", "Spine1","Neck", "Neck1", "Head", #7
            "RightShoulder", "RightArm", "RightForeArm", "RightHand", "RightFingerBase", "RightHandFinger1", "RThumb", #14
            "LeftShoulder", "LeftArm", "LeftForeArm", "LeftHand", "LeftFingerBase", "LeftHandFinger1", "LThumb", #21
            "RHipJoint","RightUpLeg", "RightLeg", "RightFoot", "RightToeBase", #26
            "LHipJoint","LeftUpLeg", "LeftLeg", "LeftFoot", "LeftToeBase" #31
        ] #31 joints

        self.encoder_joints_dict ={
            'r_arm': ["RightShoulder", "RightArm", "RightForeArm", "RightHand", "RightFingerBase", "RightHandFinger1", "RThumb"],
            'l_arm': ["LeftShoulder", "LeftArm", "LeftForeArm", "LeftHand", "LeftFingerBase", "LeftHandFinger1", "LThumb"],

            'r_leg': ["RHipJoint", "RightUpLeg", "RightLeg", "RightFoot", "RightToeBase"],
            'l_leg': ["LHipsJoint", "LeftUpLeg", "LeftLeg", "LeftFoot", "LeftToeBase"],

            'torso': ["LowerBack", "Spine", "Spine1", "Neck", "Neck1", "Head"],

            'torso_r_arm': ['torso_ft', 'r_arm_ft'],
            'torso_l_arm': ['torso_ft', 'l_arm_ft'],
            'torso_r_leg': ['torso_ft', 'r_leg_ft'],
            'torso_l_leg': ['torso_ft', 'l_leg_ft'],

            'upper_body': ['torso_r_arm_ft', 'torso_l_arm_ft'],
            'lower_body': ['torso_r_leg_ft', 'torso_l_leg_ft'],
            
            'full_body': ['Hips_ft','upper_body_ft', 'lower_body_ft']
        }
        self.decoder_joints_dict = self._inverse_graph(self.encoder_joints_dict)

    def _inverse_graph(self,graph_dict):
        inverse_graph_dict = {joint_name: [] for joint_name in self.joint_names}
        for u_node, edges in graph_dict.items():
            for i, v_node in enumerate(edges):
                inverse_graph_dict.setdefault(v_node, [])
                inverse_graph_dict[v_node].append((i, u_node))
        return inverse_graph_dict

    def _encoderNet(self,encode_data,name='encoder_net'):
        with tf.compat.v1.variable_scope(name,reuse=tf.compat.v1.AUTO_REUSE):
            encoder_net = OrderedDict()
            ### individual joints ###
            for i,joint_name in enumerate(self.joint_names):
                encoder_net[joint_name] = encode_data[:,i,:]
            ### L1 group ###
            # joint -> concate -> joint_group -> fc layer -> joint_group_ft
            for joint_group in ['l_arm','r_arm','r_leg','l_leg','torso']:
                joint_group_ft = joint_group + '_ft'
                encoder_net[joint_group] = tf.concat([encoder_net[sub_part] for sub_part in self.encoder_joints_dict[joint_group]],axis=1)
                encoder_net[joint_group_ft] = tf.contrib.layers.fully_connected(encoder_net[joint_group],num_output=64,activation_fn=tf.nn.tanh,scope=joint_group_ft)
            ### L2 group ###
            for joint_group in ['torso_l_arm','torso_r_arm','torso_l_leg','torso_r_leg']:
                joint_group_ft = joint_group + '_ft'
                encoder_net[joint_group] = tf.concat([encoder_net[sub_part] for sub_part in self.encoder_joints_dict[joint_group]],axis=1)
                encoder_net[joint_group_ft] = tf.contrib.layers.fully_connected(encoder_net[joint_group],num_output=128,activation_fn=tf.nn.tanh,scope=joint_group_ft)
            ### L3 group ###
            for joint_group in ['upper_body','lower_body']:
                joint_group_ft = joint_group + '_ft'
                encoder_net[joint_group] = tf.concat([encoder_net[sub_part] for sub_part in self.encoder_joints_dict[joint_group]],axis=1)
                encoder_net[joint_group_ft] = tf.contrib.layers.fully_connected(encoder_net[joint_group],num_output=256,activation_fn=tf.nn.tanh,scope=joint_group_ft)
            ### L4 group ###
            encoder_net['Hips_ft'] = tf.contrib.layers.fully_connected(encoder_net['Hips'],num_output=256,activation_fn=tf.nn.tanh,scope='Hip_ft')
            for joint_group in ['full_body']:
                joint_group_ft = joint_group + '_ft'
                encoder_net[joint_group] = tf.concat([encoder_net[sub_part] for sub_part in self.encoder_joints_dict[joint_group]],axis=1)
                encoder_net[joint_group_ft] = tf.contrib.layers.fully_connected(encoder_net[joint_group],num_output=512,activation_fn=tf.nn.tanh,scope=joint_group_ft)
            encoder_net['full_body_ft2'] = tf.contrib.layers.fully_connected(encoder_net['full_body_ft'],num_output=512,activation_fn=tf.nn.tanh,scope='full_body_ft2')
            encoder_net['z_joints'] = tf.contrib.layers.fully_connected(encoder_net['full_body_ft2'],num_output=self.num_zdim,activation_fn=tf.nn.tanh,scope='full_body_ft2')
            
        return encoder_net

    def _decoderNet(self,decode_data,name='decoder_net'):
        with tf.compat.v1.variable_scope(name,reuse=tf.compat.v1.AUTO_REUSE):
            decoder_net = OrderedDict()
            decoder_net['z_joints'] = decode_data
            ### inverse L4 group
            decoder_net['full_body_ft2'] = tf.contrib.layers.fully_connected(decoder_net['z_joints'],num_outputs=512,activation_fn=tf.nn.tanh,scope='full_body_ft2')
            decoder_net['full_body_ft'] = tf.contrib.layers.fully_connected(decoder_net['full_body_ft2'],num_outputs=512,activation_fn=tf.nn.tanh,scope='full_body_ft')
            decoder_net['full_body'] = tf.contrib.layers.fully_connected(decoder_net['full_body_ft'],num_outputs=512,activation_fn=tf.nn.tanh,scope='full_body')
            ### inverse L3 group ###
            for joint_group in ['upper_body','lower_body']:
                n_units = 256
                joint_group_ft = joint_group + '_ft'
                super_group_layers = [decoder_net[super_part][:, i * n_units: (i + 1) * n_units] for i, super_part in self.decoder_joints_dict[joint_group_ft]]
                decoder_net[joint_group_ft] = tf.nn.relu(tf.add_n(super_group_layers))
                decoder_net[joint_group] = tf.contrib.layers.fully_connected(decoder_net[joint_group_ft],num_outputs=n_units,activation_fn=tf.nn.tanh,scope=joint_group)
            ### inverse L2 group ###
            for joint_group in ['torso_l_arm','torso_r_arm','torso_l_leg','torso_r_leg']:
                n_units = 128
                joint_group_ft = joint_group + '_ft'
                super_group_layers = [decoder_net[super_part][:, i * n_units: (i + 1) * n_units] for i, super_part in self.decoder_joints_dict[joint_group_ft]]
                decoder_net[joint_group_ft] = tf.nn.relu(tf.add_n(super_group_layers))
                decoder_net[joint_group] = tf.contrib.layers.fully_connected(decoder_net[joint_group_ft],num_outputs=n_units,activation_fn=tf.nn.tanh,scope=joint_group)
            ### inverse L1 group ###
            for joint_group in ['l_arm','r_arm','r_leg','l_leg','torso']:
                n_units = 64
                joint_group_ft = joint_group + '_ft'
                super_group_layers = [decoder_net[super_part][:, i * n_units: (i + 1) * n_units] for i, super_part in self.decoder_joints_dict[joint_group_ft]]
                decoder_net[joint_group_ft] = tf.nn.relu(tf.add_n(super_group_layers))
                joint_group_ft_units = self.num_params_per_joint * len(self.encoder_joints_dict[joint_group])
                decoder_net[joint_group] = tf.contrib.layers.fully_connected(decoder_net[joint_group_ft],num_outputs=joint_group_ft_units,activation_fn=None,scope=joint_group)
            ### individual joints ###
            for joint in self.joint_names[1:]:
                n_units = self.num_params_per_joint
                super_group_layers = [decoder_net[super_part][:, i * n_units: (i + 1) * n_units] for i, super_part in self.decoder_joints_dict[joint_group_ft]]
                decoder_net[joint] = tf.add_n(super_group_layers)
            decoder_net['Hips_ft'] = tf.nn.relu(decoder_net['full_body'][:,:256])
            decoder_net['Hips'] =  tf.contrib.layers.fully_connected(decoder_net['Hips_ft'],num_outputs=3,activation_fn=None,scope='Hips')
            ### concate individual joints ###
            full_body_x = tf.concat([tf.expand_dims(decoder_net[joint_name], axis=1) for joint_name in self.joint_names], axis=1)
            norms = tf.norm(tensor=full_body_x, axis=2, keepdims=True)
            decoder_net['full_body_x'] = full_body_x/(norms+tf.constant(1e-9, dtype=tf.float32))
        return decoder_net

    def create_model(self):
        ### x-z-x cycle ###
        x_real = self.input_x
        encoder_real = self._encoderNet(x_real)
        z_real = encoder_real['z_joints']
        decoder_real = self._decoderNet(z_real)
        x_recon = decoder_real['full_body_x']
        ### z-x-z cycle ###
        z_rand = self.input_z
        decoder_fake = self._decoderNet(z_rand)
        x_fake = decoder_fake['full_body_x']
        encoder_fake = self._encoderNet(x_fake)
        z_recon = encoder_fake['z_joints']
        ### cyclic loss ###
        tensor_x_loss = tf.reduce_mean(input_tensor=(x_recon - x_real)**2,axis=[1,2])
        tensor_z_loss = tf.reduce_mean(input_tensor=(z_rand - z_recon)**2,axis=1)
        self.loss_cyclic = tf.reduce_mean(input_tensor=tensor_x_loss) + tf.reduce_mean(input_tensor=tensor_z_loss)

    def train(self, input_data, lr_encoder=2e-6, lr_decoder=2e-6, batch_size=256, step_save=1000):
        self.sess.run(tf.compat.v1.global_variables_initializer())
        self.param_encoder = tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.GLOBAL_VARIABLES, scope='encoder_net')
        self.param_decoder = tf.compat.v1.get_collection(tf.compat.v1.GraphKeys.GLOBAL_VARIABLES, scope='decoder_net')
        

    def predict(self):
        pass

    def save_model(self):
        pass

    def load_model(self):
        pass

        
