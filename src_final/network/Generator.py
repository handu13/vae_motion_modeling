import tensorflow as tf
import numpy as np
from .PoseEmbedding import SpatialEncoder, SpatialDecoder
from ..utils import CMU_SKELETON, CMU_JOINT_LEVELS

'''BiHMP-GAN Generator'''

'''config parameters
output_length, output_dims, hidden_units=512, z_dims=8, cell="lstm", use_residual=True, enc_file='', dec_file=''
'''
class Bihmp_Generator(tf.keras.Model):
    def __init__(self, config):
        super(Bihmp_Generator, self).__init__()
        self.enc = bihmp_Encoder(config["hidden_units"], cell=config["cell"])
        self.z_dims = config["z_dims"]
        self.dec = bihmp_Decoder(config["hidden_units"] + config["z_dims"], config["output_dims"], cell=config["cell"])
        self.output_length = config["output_length"]
        self.cell = config["cell"]
        self.use_residual = config["use_residual"]
        self.spatialencoder = SpatialEncoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
        self.spatialencoder.load_weights(config["enc_file"])
        self.spatialencoder.trainable = False
        self.spatialdecoder = SpatialDecoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
        self.spatialdecoder.load_weights(config["dec_file"])
        self.spatialdecoder.trainable = False

    def call(self, x, z):
        n_samples, len_seq, dims = x.shape
        x = tf.reshape(x, [n_samples*len_seq, -1])
        x = self.spatialencoder(x)
        x = tf.reshape(x, [n_samples, len_seq, -1])
        enc_output, enc_state = self.enc(x)
        dec_hidden = enc_state
        last_x = x[:,-1,:]
        if self.cell == "lstm":
            hidden_h, hidden_c = dec_hidden
            dec_hidden = [tf.concat([hidden_h, z],axis=-1), tf.concat([hidden_c, z],axis=-1)]
            input_com = tf.concat([hidden_h, hidden_c, z],axis=-1)
        elif self.cell == "gru":
            dec_hidden = tf.concat([dec_hidden, z], axis=-1)
            input_com = dec_hidden
        next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1),axis=1)
        emit_output = []
        for t in range(self.output_length):
            dec_output, dec_hidden = self.dec(next_input, dec_hidden)
            if self.use_residual:
                last_x = last_x + tf.squeeze(dec_output)
                emit_output.append(tf.expand_dims(last_x,axis=1))
                next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1), axis=1)
            else:
                emit_output.append(dec_output)
                next_input = tf.expand_dims(tf.concat([tf.squeeze(dec_output),input_com],axis=-1),axis=1)
        output = tf.concat(emit_output, axis=1)
        output = tf.reshape(output, [n_samples*self.output_length, -1])
        output = self.spatialdecoder(output)
        output = tf.reshape(output, [n_samples, self.output_length, -1])
        return output

class bihmp_Encoder(tf.keras.Model):
    def __init__(self, enc_units, cell="lstm"):
        super(bihmp_Encoder, self).__init__()
        self.enc_units = enc_units
        self.cell = cell
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            self.rnn = tf.keras.layers.LSTM(self.enc_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_state=True, unroll=True)
        if self.cell == "gru":
            self.rnn = tf.keras.layers.GRU(self.enc_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_state=True, unroll=True)
    def call(self, x):
        if self.cell == "lstm":
            enc_output, enc_h, enc_c = self.rnn(x)
            enc_state = [enc_h, enc_c]
        elif self.cell == "gru":
            enc_output, enc_state = self.rnn(x)
        return enc_output, enc_state

class bihmp_Decoder(tf.keras.Model):
    def __init__(self, dec_units, out_dims, cell="lstm"):
        super(bihmp_Decoder, self).__init__()
        self.dec_units = dec_units
        self.cell = cell
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            self.rnn = tf.keras.layers.LSTM(self.dec_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_sequences=True, return_state=True, unroll=True)
        if self.cell == "gru":
            self.rnn = tf.keras.layers.GRU(self.dec_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_sequences=True, return_state=True, unroll=True)
        self.fc = tf.keras.layers.Dense(out_dims)
    def call(self, x, hidden=None):
        if self.cell == "lstm":
            dec_output, dec_state_h, dec_state_c = self.rnn(x, hidden)
            dec_state = [dec_state_h, dec_state_c]
        elif self.cell == "gru":
            dec_output, dec_state = self.rnn(x, hidden)
        output = self.fc(dec_output)
        return output, dec_state


'''HPGAN Generator'''

"""config parameters
output_length, output_dims, hidden_units=1024, z_dims=128, embed_shape=128, num_layers=2, 
cell="gru"
"""
class HPGenerator(tf.keras.Model):

    def __init__(self, config):
        super(HPGenerator, self).__init__()
        self.hidden_units = config["hidden_units"]
        self.embed_shape = config["embed_shape"]
        self.num_layers = config["num_layers"]
        self.output_length = config["output_length"]
        self.cell = config["cell"]
        self.output_dims = config["output_dims"]
        self.enc = hp_Encoder(self.hidden_units, self.embed_shape, self.num_layers, cell=self.cell)
        self.dec = hp_Decoder(self.hidden_units, self.num_layers, cell=self.cell)
        self.z_fc = []
        self.z_dims = config["z_dims"]
        for _ in range(self.num_layers):
            if self.cell == 'lstm':
                self.z_fc.append([tf.keras.layers.Dense(self.hidden_units, use_bias=False),tf.keras.layers.Dense(self.hidden_units, use_bias=False)])
            elif self.cell == 'gru':
                self.z_fc.append(tf.keras.layers.Dense(self.hidden_units, use_bias=False))
        self.decinp_fc = tf.keras.layers.Dense(self.embed_shape, use_bias=False)
        self.fc = tf.keras.layers.Dense(self.output_dims)

    def call(self, x, z):
        enc_output, enc_state = self.enc(x)
        dec_hidden = enc_state
        last_x = tf.expand_dims(enc_output[:,-1,:], axis=1)
        if self.cell == "lstm":
            for i in range(self.num_layers):
                dec_hidden[i][0] = tf.add(dec_hidden[i][0], self.z_fc[i][0](z))
                dec_hidden[i][1] = tf.add(dec_hidden[i][1], self.z_fc[i][1](z))
        elif self.cell == "gru":
            for i in range(self.num_layers):
                dec_hidden[i] = tf.add(dec_hidden[i], self.z_fc[i](z))
        next_input = self.decinp_fc(last_x)
        emit_output = []
        for t in range(self.output_length):
            dec_output, dec_hidden = self.dec(next_input, dec_hidden)
            dec_output += last_x
            last_x = dec_output
            emit_output.append(dec_output)
            next_input = self.decinp_fc(dec_output)
        output = tf.concat(emit_output, axis=1)
        output = self.fc(output)
        return output

class hp_Encoder(tf.keras.Model):
    def __init__(self, enc_units=1024, embed_shape=128, num_layers=2, cell="gru"):
        super(hp_Encoder, self).__init__()
        self.enc_units = enc_units
        self.embed_shape = embed_shape
        self.num_layers = num_layers
        self.cell = cell
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        self.projection = tf.keras.layers.Dense(embed_shape)
        if self.cell == "lstm":
            rnncell = [tf.keras.layers.LSTMCell(self.enc_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init) for _ in range(self.num_layers)]
        if self.cell == "gru":
            rnncell = [tf.keras.layers.GRUCell(self.enc_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init) for _ in range(self.num_layers)]
        self.rnn = tf.keras.layers.RNN(rnncell, return_sequences=True, return_state=True, unroll=True)
    def call(self, x):
        input_proj = self.projection(x)
        output = self.rnn(input_proj)
        enc_output = output[0]
        enc_state = output[1:]
        return enc_output, enc_state

class hp_Decoder(tf.keras.Model):
    def __init__(self, dec_units=1024, num_layers=2, cell="gru"):
        super(hp_Decoder, self).__init__()
        self.dec_units = dec_units
        self.num_layers = num_layers
        self.cell = cell
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            cells = [tf.keras.layers.LSTMCell(self.dec_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init) for _ in range(self.num_layers)]
        if self.cell == "gru":
            cells = [tf.keras.layers.GRUCell(self.dec_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init) for _ in range(self.num_layers)]
        self.rnn = tf.keras.layers.RNN(cells, return_sequences=True, return_state=True, unroll=True)
    def call(self, x, hidden):
        output = self.rnn(x, hidden)
        dec_output = output[0]
        dec_state = output[1:]
        return dec_output, dec_state

'''TCN Generator'''
class TCN_Generator(tf.keras.Model): #sequential generation
    def __init__(self, n_filters, f_sizes, dilation_rates, output_dims, output_length, use_residual=False, spatialenc=None, spatialdec=None):
        super(TCN_Generator, self).__init__()
        self.model = tf.keras.Sequential()
        for i in range(len(n_filters)):
            self.model.add(tcn_Block(n_filters[i], f_sizes[i], dilation_rates[i], use_residual))
        self.model.add(tf.keras.layers.Conv1D(output_dims, 1, padding='same'))
        self.output_length = output_length
        self.spatialencoder = spatialenc
        if spatialenc is not None:
            self.spatialencoder.trainable = False
        self.spatialdecoder = spatialdec
        if spatialdec is not None:
            self.spatialdecoder.trainable = False
    def call(self, x, training=False):
        if self.spatialencoder and self.spatialdecoder:
            n_samples, len_seq, dims = x.shape
            x = tf.reshape(x, [n_samples*len_seq, -1])
            x = self.spatialencoder(x)
            x = tf.reshape(x, [n_samples, len_seq, -1])
        emit_output = []
        for _ in range(self.output_length):
            pred = self.model(x, training=training)
            new = tf.keras.layers.Add()([pred[:,-1,:],x[:,-1,:]]) 
            new = tf.expand_dims(new, axis=1)
            emit_output.append(new)
            x = tf.concat([x, new], axis=1)[:,1:,:]
        output = tf.concat(emit_output, axis=1)
        if self.spatialencoder and self.spatialdecoder:
            output = tf.reshape(output, [n_samples*self.out_length, -1])
            output = self.spatialdecoder(output)
            output = tf.reshape(output, [n_samples, self.out_length, -1])
        return output
    
class tcn_Block(tf.keras.Model):
    def __init__(self, n_filter, f_size, dilation_rate, use_residual=False):
        super(tcn_Block, self).__init__()
        self.use_residual = use_residual
        self.block = tf.keras.Sequential([
            tf.keras.layers.Conv1D(n_filter, f_size, dilation_rate=dilation_rate, padding='causal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.ReLU(),
            tf.keras.layers.SpatialDropout1D(0.5)
            ])
        if self.use_residual:
            self.fc = tf.keras.layers.Conv1D(n_filter, 1, padding='same')
            self.add = tf.keras.layers.Add()
    def call(self, x, training=False):
        y = self.block(x, training=training)
        if self.use_residual:
            origin_x = self.fc(x)
            y = self.add([y, origin_x])
        return y

'''TCN_encoder-decoder generator'''
class ED_TCN_Generator(tf.keras.Model):
    def __init__(self, n_filters, f_sizes, dilation_rates, hidden_dims, output_dims, z_dims=None, use_residual=False, spatialenc=None, spatialdec=None):
        super(ED_TCN_Generator, self).__init__()
        self.encoder = tf.keras.Sequential()
        for i in range(len(n_filters)):
            self.encoder.add(tcn_Enc_Block(n_filters[i], f_sizes[i], dilation_rates[i]))
        self.encoder.add(tf.keras.layers.Dense(hidden_dims, activation='tanh'))
        self.decoder = tf.keras.Sequential()
        for i in range(len(n_filters)):
            self.decoder.add(tcn_Dec_Block(n_filters[-1-i], f_sizes[-1-i], dilation_rates[-1-i]))
        self.decoder.add(tf.keras.layers.Dense(output_dims))
        self.z_dims = z_dims
        self.spatialencoder = spatialenc
        if spatialenc is not None:
            self.spatialencoder.trainable = False
        self.spatialdecoder = spatialdec
        if spatialdec is not None:
            self.spatialdecoder.trainable = False
    def call(self, x, z=None, training=False):
        if self.spatialencoder and self.spatialdecoder:
            n_samples, len_seq, dims = x.shape
            x = tf.reshape(x, [n_samples*len_seq, -1])
            x = self.spatialencoder(x)
            x = tf.reshape(x, [n_samples, len_seq, -1])
        x = self.encoder(x, training=training)
        output = self.decoder(x, training=training)
        if self.spatialencoder and self.spatialdecoder:
            output = tf.reshape(output, [n_samples*self.out_length, -1])
            output = self.spatialdecoder(output)
            output = tf.reshape(output, [n_samples, self.out_length, -1])
        return output

class tcn_Enc_Block(tf.keras.Model):
    def __init__(self, n_filter, f_size, dilation_rate):
        super(tcn_Enc_Block, self).__init__()
        self.block = tf.keras.Sequential([
            tf.keras.layers.Conv1D(n_filter, f_size, dilation_rate=dilation_rate, padding='causal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.ReLU(),
            tf.keras.layers.MaxPool1D(2)
        ])
    def call(self, x, training=False):
        return self.block(x, training=training)

class tcn_Dec_Block(tf.keras.Model):
    def __init__(self, n_filter, f_size, dilation_rate):
        super(tcn_Dec_Block, self).__init__()
        self.block = tf.keras.Sequential([
            tf.keras.layers.UpSampling1D(2),
            tf.keras.layers.Conv1D(n_filter, f_size, dilation_rate=dilation_rate, padding='causal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.ReLU()
        ])
    def call(self, x, training=False):
        return self.block(x, training=training)

'''TCN_RNN decoder'''
class dilated_cnn_Block(tf.keras.Model):
    def __init__(self, n_filter, f_size, dilation_rate, use_residual=False):
        super(dilated_cnn_Block, self).__init__()
        self.use_residual = use_residual
        self.block = tf.keras.Sequential([
            tf.keras.layers.Conv1D(n_filter, f_size, dilation_rate=dilation_rate, padding='valid'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.ReLU(),
            tf.keras.layers.SpatialDropout1D(0.5)
            ])
        if self.use_residual:
            self.fc = tf.keras.layers.Conv1D(n_filter, 1, padding='same')
            self.add = tf.keras.layers.Add()
        
    def call(self, x, training=True):
        y = self.block(x, training=training)
        if self.use_residual:
            origin_x = self.fc(x)
            y = self.add([y, origin_x])
        return y

'''
n_filters=[128,128,128,128], f_sizes=[3,3,3,3], dilation_rates=[1,2,4,4], 
mid_units=128, hidden_units=128, output_dims=72, output_length=32, z_dims=128, use_residual=True, cell='lstm',
use_spatial=True, enc_file = '', dec_file=''
'''

class TCN_RNN_Generator(tf.keras.Model):
    def __init__(self, config):
        super(TCN_RNN_Generator, self).__init__()
        self.encoder = tf.keras.Sequential()
        for i in range(len(config["n_filters"])):
            self.encoder.add(dilated_cnn_Block(config["n_filters"][i], config["f_sizes"][i], config["dilation_rates"][i]))
        self.encoder.add(tf.keras.layers.Flatten())
        self.encoder.add(tf.keras.layers.Dense(config["mid_units"], activation=tf.nn.leaky_relu))
        self.z_dims = config["z_dims"]
        if self.z_dims:
            self.dec = bihmp_Decoder(config["hidden_units"] + config["z_dims"], config["output_dims"], cell=config["cell"])
        else:
            self.dec = bihmp_Decoder(config["hidden_units"], config["output_dims"], cell=config["cell"])
        self.output_length = config["output_length"]
        self.cell = config["cell"]
        self.use_residual = config["use_residual"]
        if config["use_spatial"]:
            self.spatialencoder = SpatialEncoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
            self.spatialencoder.load_weights(config["enc_file"])
            self.spatialencoder.trainable = False
            self.spatialdecoder = SpatialDecoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
            self.spatialdecoder.load_weights(config["dec_file"])
            self.spatialdecoder.trainable = False

     
    def call(self, x, z=None): # z: batchsize * z_dims
        if hasattr(self, "spatialencoder") and hasattr(self, "spatialdecoder"):
            n_samples, len_seq, dims = x.shape
            x = tf.reshape(x, [n_samples*len_seq, -1])
            x = self.spatialencoder(x)
            x = tf.reshape(x, [n_samples, len_seq, -1])
        long_state = self.encoder(x) # batchsize * hidden_dims
        if z is not None:
            last_x = x[:,-1,:]
            input_com = tf.concat([long_state, z],axis=-1)
            next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1),axis=1)
            emit_output = []
            for t in range(self.output_length):
                if t==0:
                    dec_output, dec_hidden = self.dec(next_input)
                else:
                    dec_output, dec_hidden = self.dec(next_input, dec_hidden)
                if self.use_residual:
                    last_x = last_x + tf.squeeze(dec_output)
                    emit_output.append(tf.expand_dims(last_x,axis=1))
                    next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1), axis=1)
                else:
                    emit_output.append(dec_output)
                    next_input = tf.expand_dims(tf.concat([tf.squeeze(dec_output),input_com],axis=-1),axis=1)
        else:
            last_x = x[:, -1, :]
            input_com = long_state
            emit_output = []
            next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1),axis=1)
            for t in range(self.output_length):
                if t==0:
                    dec_output, dec_hidden = self.dec(next_input)
                else:
                    dec_output, dec_hidden = self.dec(next_input, dec_hidden)
                if self.use_residual:
                    last_x = last_x + tf.squeeze(dec_output)
                    emit_output.append(tf.expand_dims(last_x,axis=1))
                    next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1), axis=1)
                else:
                    emit_output.append(dec_output)
                    next_input = tf.expand_dims(tf.concat([tf.squeeze(dec_output),input_com],axis=-1),axis=1)
        output = tf.concat(emit_output, axis=1)
        if hasattr(self, "spatialencoder") and hasattr(self, "spatialdecoder"):
            output = tf.reshape(output, [n_samples*self.output_length, -1])
            output = self.spatialdecoder(output)
            output = tf.reshape(output, [n_samples, self.output_length, -1])
        return output

class TCN_RNN_init_Generator(tf.keras.Model):
    def __init__(self, config):
        super(TCN_RNN_init_Generator, self).__init__()
        self.encoder = tf.keras.Sequential()
        for i in range(len(config["n_filters"])):
            self.encoder.add(dilated_cnn_Block(config["n_filters"][i], config["f_sizes"][i], config["dilation_rates"][i]))
        self.encoder.add(tf.keras.layers.Flatten())
        self.encoder.add(tf.keras.layers.Dense(config["mid_units"], activation=tf.nn.leaky_relu))
        self.z_dims = config["z_dims"]
        self.cell = config["cell"]
        if self.cell == 'lstm':
            self.state_c_layer = tf.keras.layers.Dense(config["hidden_units"])
            self.state_h_layer = tf.keras.layers.Dense(config["hidden_units"])
            #dec_state = [dec_state_h, dec_state_c]
        elif self.cell == 'gru':
            self.state_layer = tf.keras.layers.Dense(config["hidden_units"])
            #dec_state = decstate
        self.dec = bihmp_Decoder(config["hidden_units"], config["output_dims"], cell=config["cell"])
        # if self.z_dims:
        #     self.dec = bihmp_Decoder(config["hidden_units"] + config["z_dims"], config["output_dims"], cell=config["cell"])
        # else:
        #     self.dec = bihmp_Decoder(config["hidden_units"], config["output_dims"], cell=config["cell"])
        self.output_length = config["output_length"]
        self.cell = config["cell"]
        self.use_residual = config["use_residual"]
        if config["use_spatial"]:
            self.spatialencoder = SpatialEncoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
            self.spatialencoder.load_weights(config["enc_file"])
            self.spatialencoder.trainable = False
            self.spatialdecoder = SpatialDecoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
            self.spatialdecoder.load_weights(config["dec_file"])
            self.spatialdecoder.trainable = False

     
    def call(self, x, z=None): # z: batchsize * z_dims
        if hasattr(self, "spatialencoder") and hasattr(self, "spatialdecoder"):
            n_samples, len_seq, dims = x.shape
            x = tf.reshape(x, [n_samples*len_seq, -1])
            x = self.spatialencoder(x)
            x = tf.reshape(x, [n_samples, len_seq, -1])
        long_state = self.encoder(x) # batchsize * hidden_dims
        if z is not None:
            last_x = x[:,-1,:]
            input_com = tf.concat([long_state, z],axis=-1)
            next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1),axis=1)
            emit_output = []
            for t in range(self.output_length):
                if t==0:
                    if self.cell == 'lstm':
                        dec_state_h = self.state_h_layer(long_state)
                        dec_state_c = self.state_c_layer(long_state)
                        init_state = [dec_state_h, dec_state_c]
                    elif self.cell == 'gru':
                        init_state = self.state_layer(long_state)
                    dec_output, dec_hidden = self.dec(next_input, init_state)
                else:
                    dec_output, dec_hidden = self.dec(next_input, dec_hidden)
                if self.use_residual:
                    last_x = last_x + tf.squeeze(dec_output)
                    emit_output.append(tf.expand_dims(last_x,axis=1))
                    next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1), axis=1)
                else:
                    emit_output.append(dec_output)
                    next_input = tf.expand_dims(tf.concat([tf.squeeze(dec_output),input_com],axis=-1),axis=1)
        else:
            last_x = x[:, -1, :]
            input_com = long_state
            emit_output = []
            next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1),axis=1)
            for t in range(self.output_length):
                if t==0:
                    dec_output, dec_hidden = self.dec(next_input)
                else:
                    dec_output, dec_hidden = self.dec(next_input, dec_hidden)
                if self.use_residual:
                    last_x = last_x + tf.squeeze(dec_output)
                    emit_output.append(tf.expand_dims(last_x,axis=1))
                    next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1), axis=1)
                else:
                    emit_output.append(dec_output)
                    next_input = tf.expand_dims(tf.concat([tf.squeeze(dec_output),input_com],axis=-1),axis=1)
        output = tf.concat(emit_output, axis=1)
        if hasattr(self, "spatialencoder") and hasattr(self, "spatialdecoder"):
            output = tf.reshape(output, [n_samples*self.output_length, -1])
            output = self.spatialdecoder(output)
            output = tf.reshape(output, [n_samples, self.output_length, -1])
        return output


