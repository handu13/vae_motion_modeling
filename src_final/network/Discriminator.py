import tensorflow as tf
import tensorflow_addons as tfa
from .PoseEmbedding import SpatialEncoder
from  ..utils import CMU_SKELETON, CMU_JOINT_LEVELS

'''Bihmp-gan discriminator, bidirectional RNN'''

'''config parameters
hidden_size=512, input_length, output_length, z_dims=8, cell="lstm", bidirectional=True, use_multi_states=True, output_activation=None, enc_file=''
'''
class Bihmp_Discriminator(tf.keras.Model):
    def __init__(self, config):
        super(Bihmp_Discriminator, self).__init__()
        self.cell = config["cell"]
        self.bidirectional = config["bidirectional"]
        self.input_length = config["input_length"]
        self.output_length = config["output_length"]
        self.z_dims = config["z_dims"]
        self.use_multi_states = config["use_multi_states"]
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            self.rnn = tf.keras.layers.LSTM(config["hidden_size"], return_sequences=True, unroll=True)
        elif self.cell == "gru":
            self.rnn = tf.keras.layers.GRU(config["hidden_size"], kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_sequences=True, unroll=True)
        if self.bidirectional:
            self.disc = tf.keras.layers.Bidirectional(self.rnn, merge_mode=None)
        else:
            self.disc = self.rnn
        self.output_fc = tf.keras.layers.Dense(1, activation=config["output_activation"])
        if self.z_dims:
            self.z_fc = tf.keras.layers.Dense(self.z_dims)
        self.spatialencoder = SpatialEncoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
        self.spatialencoder.load_weights(config["enc_file"])
        self.spatialencoder.trainable = False

    def call(self, x):
        n_samples, len_seq, dims = x.shape
        x = tf.reshape(x, [n_samples*len_seq, -1])
        x = self.spatialencoder(x)
        x = tf.reshape(x, [n_samples, len_seq, -1])
        output = self.disc(x)
        if self.use_multi_states:
            if self.bidirectional:
                states = [output[0][:,self.input_length,:], output[0][:,-1,:], 
                          output[1][:,self.output_length,: ], output[1][:,-1,:]] 
                final_state = tf.add_n(states)
            else:
                final_state = tf.add_n([output[:,self.input_length,:],output[:,-1,:]])
        else:
            if self.bidirectional:
                final_state = tf.add_n([output[0][:,-1,:], output[1][:,-1,:]])
            else:
                final_state = output[:,-1,:]
        output = self.output_fc(final_state)
        if self.z_dims:
            recon_z = self.z_fc(final_state)
            return output, recon_z
        else:
            return output

"""
hidden_size=512, embed_shape=128, num_layers=3, output_activation=None
"""


'''hpgan discriminator, FCN'''
class HPDiscriminator(tf.keras.Model):
    def __init__(self, config):
        super(HPDiscriminator, self).__init__()
        self.hidden_size = config["hidden_size"]
        self.embed_shape = config["embed_shape"]
        self.num_layers = config["num_layers"]
        self.regularizer = tf.keras.regularizers.l2(0.001)
        self.projection = tf.keras.layers.Dense(self.embed_shape, use_bias=False, kernel_regularizer=self.regularizer)
        self.flatten = tf.keras.layers.Flatten()
        self.net = []
        for i in range(self.num_layers):
            self.net.append(tf.keras.layers.Dense(self.hidden_size, activation=tf.nn.relu, kernel_regularizer=self.regularizer))
        self.output_fc = tf.keras.layers.Dense(1, activation=config["output_activation"], kernel_regularizer=self.regularizer)
    
    def call(self, x):
        x = self.projection(x)
        x = self.flatten(x)
        for i in range(self.num_layers):
            x = self.net[i](x)
        output = self.output_fc(x)
        return output


'''CNN discriminator'''

'''
n_filters=[128,128,128,128], f_sizes=[3,3,3,3], dilation_rates=[1,2,4,8], base_dim, multiscale, z_dims, guided
'''
class CNN_Discriminator(tf.keras.Model):
    def __init__(self, config):
        super(CNN_Discriminator, self).__init__()
        self.multiscale = config["multiscale"]
        self.z_dims = config["z_dims"]
        self.guided = config["guided"]
        self.layer1 = cnn_Block(config["n_filters"][0], config["f_sizes"][0], config["dilation_rates"][0], 4)
        self.layer2 = cnn_Block(config["n_filters"][1], config["f_sizes"][1], config["dilation_rates"][1], 4)
        self.layer3 = cnn_Block(config["n_filters"][2], config["f_sizes"][2], config["dilation_rates"][2], 4)
        # self.layer1 = dilated_cnn_Block(config["n_filters"][0], config["f_sizes"][0], config["dilation_rates"][0])
        # self.layer2 = dilated_cnn_Block(config["n_filters"][1], config["f_sizes"][1], config["dilation_rates"][1])
        # self.layer3 = dilated_cnn_Block(config["n_filters"][2], config["f_sizes"][2], config["dilation_rates"][2])
        if self.multiscale:
            self.scale1 = tf.keras.layers.Dense(8,activation='relu')
            self.scale1d = tf.keras.layers.Dense(config["base_dim"],activation='relu')
            self.scale2 = tf.keras.layers.Dense(config["base_dim"],activation='relu')
        else:
            self.base = tf.keras.layers.Dense(config["base_dim"],activation='relu')
        #self.dropout = tf.keras.layers.Dropout(0.5)

        self.output_layer = tf.keras.layers.Dense(1, activation=config["output_activation"])
        if self.z_dims:
            self.z_fc = tf.keras.layers.Dense(self.z_dims)
        if self.guided:
            self.guide_fc = tf.keras.layers.Dense(1)
    def call(self, x, training=True):
        recon_z = None
        guided_output = None
        l1 = self.layer1(x, training=training)
        l2 = self.layer2(l1, training=training)
        l3 = self.layer3(l2, training=training)
        #l4 = self.layer4(l3, training=training)
        if self.multiscale:
            s1 = self.scale1(l1)
            s1 = tf.keras.layers.Flatten()(s1)
            s1 = self.scale1d(s1)
            l3 = tf.keras.layers.Flatten()(l3)
            s2 = self.scale2(l3)
            base = tf.concat([s1, s2], axis=-1)
        else:
            l3 = tf.keras.layers.Flatten()(l3)
            base = self.base(l3)
        output = self.output_layer(base)
        if self.z_dims:
            recon_z = self.z_fc(base)
        if self.guided:
            guided_output = self.guide_fc(base)
        return output, recon_z, guided_output

class cnn_Block(tf.keras.Model):
    def __init__(self, n_filter, f_size, dilation_rate, pool_size, use_residual=False):
        super(cnn_Block, self).__init__()
        self.use_residual = use_residual
        self.block = tf.keras.Sequential([
            tf.keras.layers.Conv1D(n_filter, f_size, dilation_rate=dilation_rate, padding='same'),
            tfa.layers.InstanceNormalization(axis=-1),
            tf.keras.layers.ReLU(),
            tf.keras.layers.SpatialDropout1D(0.5),
            tf.keras.layers.MaxPool1D(pool_size)
            ])
        if self.use_residual:
            self.fc = tf.keras.layers.Conv1D(n_filter, 1, padding='same')
            self.add = tf.keras.layers.Add()
    def call(self, x, training=True):
        y = self.block(x, training=training)
        if self.use_residual:
            origin_x = self.fc(x)
            y = self.add([y, origin_x])
        return y

class dilated_cnn_Block(tf.keras.Model):
    def __init__(self, n_filter, f_size, dilation_rate, use_residual=False):
        super(dilated_cnn_Block, self).__init__()
        self.use_residual = use_residual
        self.block = tf.keras.Sequential([
            tf.keras.layers.Conv1D(n_filter, f_size, dilation_rate=dilation_rate, padding='valid'),
            #tf.keras.layers.BatchNormalization(),
            tfa.layers.InstanceNormalization(axis=-1),
            tf.keras.layers.ReLU(),
            tf.keras.layers.SpatialDropout1D(0.5)
            ])
        if self.use_residual:
            self.fc = tf.keras.layers.Conv1D(n_filter, 1, padding='same')
            self.add = tf.keras.layers.Add()
        
    def call(self, x, training=True):
        y = self.block(x, training=training)
        if self.use_residual:
            origin_x = self.fc(x)
            y = self.add([y, origin_x])
        return y




'''motion GAN bihmpdisc'''
class Bihmp_Discriminator_mg(tf.keras.Model):
    def __init__(self, config):
        super(Bihmp_Discriminator_mg, self).__init__()
        self.cell = config["cell"]
        self.bidirectional = config["bidirectional"]
        self.input_length = config["input_length"]
        self.output_length = config["output_length"]
        self.z_dims = config["z_dims"]
        self.use_multi_states = config["use_multi_states"]
        self.guided = config["guided"]
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            self.rnn = tf.keras.layers.LSTM(config["hidden_size"], return_sequences=True, unroll=True)
        elif self.cell == "gru":
            self.rnn = tf.keras.layers.GRU(config["hidden_size"], kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_sequences=True, unroll=True)
        if self.bidirectional:
            self.disc = tf.keras.layers.Bidirectional(self.rnn, merge_mode=None)
        else:
            self.disc = self.rnn
        self.output_fc = tf.keras.layers.Dense(1, activation=config["output_activation"])
        if self.z_dims:
            self.z_fc = tf.keras.layers.Dense(self.z_dims)
        if self.guided:
            self.guide_fc = tf.keras.layers.Dense(1)
        self.spatialencoder = SpatialEncoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
        self.spatialencoder.load_weights(config["enc_file"])
        self.spatialencoder.trainable = False

    def call(self, x):
        n_samples, len_seq, dims = x.shape
        x = tf.reshape(x, [n_samples*len_seq, -1])
        x = self.spatialencoder(x)
        x = tf.reshape(x, [n_samples, len_seq, -1])
        output = self.disc(x)
        recon_z = None
        guided_output = None
        if self.use_multi_states:
            if self.bidirectional:
                states = [output[0][:,self.input_length,:], output[0][:,-1,:], 
                          output[1][:,self.output_length,: ], output[1][:,-1,:]] 
                final_state = tf.add_n(states)
            else:
                final_state = tf.add_n([output[:,self.input_length,:],output[:,-1,:]])
        else:
            if self.bidirectional:
                final_state = tf.add_n([output[0][:,-1,:], output[1][:,-1,:]])
            else:
                final_state = output[:,-1,:]
        output = self.output_fc(final_state)
        if self.z_dims:
            recon_z = self.z_fc(final_state)
        if self.guided:
            guided_output = self.guide_fc(final_state)
        return output, recon_z, guided_output

    
class HPDiscriminator_mg(tf.keras.Model):
    def __init__(self, config):
        super(HPDiscriminator, self).__init__()
        self.hidden_size = config["hidden_size"]
        self.embed_shape = config["embed_shape"]
        self.num_layers = config["num_layers"]
        self.guided = config["guided"]
        self.regularizer = tf.keras.regularizers.l2(0.001)
        self.projection = tf.keras.layers.Dense(self.embed_shape, use_bias=False, kernel_regularizer=self.regularizer)
        self.flatten = tf.keras.layers.Flatten()
        self.net = []
        for i in range(self.num_layers):
            self.net.append(tf.keras.layers.Dense(self.hidden_size, activation=tf.nn.relu, kernel_regularizer=self.regularizer))
        self.output_fc = tf.keras.layers.Dense(1, activation=config["output_activation"], kernel_regularizer=self.regularizer)
        if self.z_dims:
            self.z_fc = tf.keras.layers.Dense(self.z_dims)
        if self.guided:
            self.guide_fc = tf.keras.layers.Dense(1)
    
    def call(self, x):
        x = self.projection(x)
        x = self.flatten(x)
        for i in range(self.num_layers):
            x = self.net[i](x)
        output = self.output_fc(x)
        if self.z_dims:
            recon_z = self.z_fc(x)
        if self.guided:
            guided_output = self.guide_fc(x)
        return output, recon_z, guided_output


################################################################
'''
1. discriminator, trained with WGAN-GP
2. discriminator + sigmoid, trained with WGAN-GP / GAN
3. instancenormalization + outputlayer + sigmoid, trained with WGAN-GP / GAN
4. batchnormalization + outputlayer + sigmoid, trained with WGAN-GP / GAN
5. outputlayer + batchnormalization + sigmoid, trained with WGAN-GP / GAN
'''
class CNN_Discriminator_test1(tf.keras.Model):  #outputlayer + normalization + sigmoid
    def __init__(self, config):
        super(CNN_Discriminator_test1, self).__init__()
        self.multiscale = config["multiscale"]
        self.z_dims = config["z_dims"]
        self.guided = config["guided"]
        self.layer1 = cnn_Block(config["n_filters"][0], config["f_sizes"][0], config["dilation_rates"][0], 4)
        self.layer2 = cnn_Block(config["n_filters"][1], config["f_sizes"][1], config["dilation_rates"][1], 4)
        self.layer3 = cnn_Block(config["n_filters"][2], config["f_sizes"][2], config["dilation_rates"][2], 4)
        # self.layer1 = dilated_cnn_Block(config["n_filters"][0], config["f_sizes"][0], config["dilation_rates"][0])
        # self.layer2 = dilated_cnn_Block(config["n_filters"][1], config["f_sizes"][1], config["dilation_rates"][1])
        # self.layer3 = dilated_cnn_Block(config["n_filters"][2], config["f_sizes"][2], config["dilation_rates"][2])
        if self.multiscale:
            self.scale1 = tf.keras.layers.Dense(8,activation='relu')
            self.scale1d = tf.keras.layers.Dense(config["base_dim"],activation='relu')
            self.scale2 = tf.keras.layers.Dense(config["base_dim"],activation='relu')
        else:
            self.base = tf.keras.layers.Dense(config["base_dim"],activation='relu')
        #self.dropout = tf.keras.layers.Dropout(0.5)
        self.norm = tf.keras.layers.BatchNormalization()
        self.output_layer = tf.keras.layers.Dense(1)
        if self.z_dims:
            self.z_fc = tf.keras.layers.Dense(self.z_dims)
        if self.guided:
            self.guide_fc = tf.keras.layers.Dense(1)
    def call(self, x, training=True):
        recon_z = None
        guided_output = None
        l1 = self.layer1(x, training=training)
        l2 = self.layer2(l1, training=training)
        l3 = self.layer3(l2, training=training)
        #l4 = self.layer4(l3, training=training)
        if self.multiscale:
            s1 = self.scale1(l1)
            s1 = tf.keras.layers.Flatten()(s1)
            s1 = self.scale1d(s1)
            l3 = tf.keras.layers.Flatten()(l3)
            s2 = self.scale2(l3)
            base = tf.concat([s1, s2], axis=-1)
        else:
            l3 = tf.keras.layers.Flatten()(l3)
            base = self.base(l3)
        output = self.output_layer(base)
        output = self.norm(output)
        if self.z_dims:
            recon_z = self.z_fc(base)
        if self.guided:
            guided_output = self.guide_fc(base)
        return output, recon_z, guided_output


class CNN_Discriminator_test2(tf.keras.Model):  #outputlayer + normalization + sigmoid
    def __init__(self, config):
        super(CNN_Discriminator_test2, self).__init__()
        self.multiscale = config["multiscale"]
        self.z_dims = config["z_dims"]
        self.guided = config["guided"]
        self.layer1 = cnn_Block(config["n_filters"][0], config["f_sizes"][0], config["dilation_rates"][0], 4)
        self.layer2 = cnn_Block(config["n_filters"][1], config["f_sizes"][1], config["dilation_rates"][1], 4)
        self.layer3 = cnn_Block(config["n_filters"][2], config["f_sizes"][2], config["dilation_rates"][2], 4)
        # self.layer1 = dilated_cnn_Block(config["n_filters"][0], config["f_sizes"][0], config["dilation_rates"][0])
        # self.layer2 = dilated_cnn_Block(config["n_filters"][1], config["f_sizes"][1], config["dilation_rates"][1])
        # self.layer3 = dilated_cnn_Block(config["n_filters"][2], config["f_sizes"][2], config["dilation_rates"][2])
        if self.multiscale:
            self.scale1 = tf.keras.layers.Dense(8,activation='relu')
            self.scale1d = tf.keras.layers.Dense(config["base_dim"],activation='relu')
            self.scale2 = tf.keras.layers.Dense(config["base_dim"],activation='relu')
        else:
            self.base = tf.keras.layers.Dense(config["base_dim"],activation='relu')
        #self.dropout = tf.keras.layers.Dropout(0.5)
        self.norm = tf.keras.layers.BatchNormalization()
        self.output_layer = tf.keras.layers.Dense(1)
        if self.z_dims:
            self.z_fc = tf.keras.layers.Dense(self.z_dims)
        if self.guided:
            self.guide_fc = tf.keras.layers.Dense(1)
    def call(self, x, training=True):
        recon_z = None
        guided_output = None
        l1 = self.layer1(x, training=training)
        l2 = self.layer2(l1, training=training)
        l3 = self.layer3(l2, training=training)
        #l4 = self.layer4(l3, training=training)
        if self.multiscale:
            s1 = self.scale1(l1)
            s1 = tf.keras.layers.Flatten()(s1)
            s1 = self.scale1d(s1)
            l3 = tf.keras.layers.Flatten()(l3)
            s2 = self.scale2(l3)
            base = tf.concat([s1, s2], axis=-1)
        else:
            l3 = tf.keras.layers.Flatten()(l3)
            base = self.base(l3)
        output = self.output_layer(base)
        output = tf.keras.activations.sigmoid(self.norm(output))
        if self.z_dims:
            recon_z = self.z_fc(base)
        if self.guided:
            guided_output = self.guide_fc(base)
        return output, recon_z, guided_output

class CNN_Discriminator_test3(tf.keras.Model):  #normalization + outputlayer + sigmoid
    def __init__(self, config):
        super(CNN_Discriminator_test3, self).__init__()
        self.multiscale = config["multiscale"]
        self.z_dims = config["z_dims"]
        self.guided = config["guided"]
        self.layer1 = cnn_Block(config["n_filters"][0], config["f_sizes"][0], config["dilation_rates"][0], 4)
        self.layer2 = cnn_Block(config["n_filters"][1], config["f_sizes"][1], config["dilation_rates"][1], 4)
        self.layer3 = cnn_Block(config["n_filters"][2], config["f_sizes"][2], config["dilation_rates"][2], 4)
        # self.layer1 = dilated_cnn_Block(config["n_filters"][0], config["f_sizes"][0], config["dilation_rates"][0])
        # self.layer2 = dilated_cnn_Block(config["n_filters"][1], config["f_sizes"][1], config["dilation_rates"][1])
        # self.layer3 = dilated_cnn_Block(config["n_filters"][2], config["f_sizes"][2], config["dilation_rates"][2])
        if self.multiscale:
            self.scale1 = tf.keras.layers.Dense(8,activation='relu')
            self.scale1d = tf.keras.layers.Dense(config["base_dim"],activation='relu')
            self.scale2 = tf.keras.layers.Dense(config["base_dim"],activation='relu')
        else:
            self.base = tf.keras.layers.Dense(config["base_dim"],activation='relu')
        #self.dropout = tf.keras.layers.Dropout(0.5)
        self.norm = tf.keras.layers.BatchNormalization()
        self.output_layer = tf.keras.layers.Dense(1, activation=config["output_activation"])
        if self.z_dims:
            self.z_fc = tf.keras.layers.Dense(self.z_dims)
        if self.guided:
            self.guide_fc = tf.keras.layers.Dense(1)
    def call(self, x, training=True):
        recon_z = None
        guided_output = None
        l1 = self.layer1(x, training=training)
        l2 = self.layer2(l1, training=training)
        l3 = self.layer3(l2, training=training)
        #l4 = self.layer4(l3, training=training)
        if self.multiscale:
            s1 = self.scale1(l1)
            s1 = tf.keras.layers.Flatten()(s1)
            s1 = self.scale1d(s1)
            l3 = tf.keras.layers.Flatten()(l3)
            s2 = self.scale2(l3)
            base = tf.concat([s1, s2], axis=-1)
        else:
            l3 = tf.keras.layers.Flatten()(l3)
            base = self.base(l3)
        normedbase = self.norm(base)
        output = self.output_layer(normedbase)
        if self.z_dims:
            recon_z = self.z_fc(base)
        if self.guided:
            guided_output = self.guide_fc(base)
        return output, recon_z, guided_output