import os, sys
import numpy as np
import tensorflow as tf
import copy



class SpatialEncDecoder(tf.keras.Model):
    
    def __init__(self, encoder, decoder):
        self.encoder = encoder
        self.decoder = decoder
    
    def call(self, inputs, training=False):

        hidden_value = self.encoder(inputs)
        return self.decoder(hidden_value)
    
    def save(self, save_path):
        self.encoder.save(save_path)
        self.decoder.save(save_path)

    def load(self, model_dir):
        self.encoder.load(model_dir)
        self.decoder.load(model_dir)


class SpatialEncoder(tf.keras.Model):
    '''
    Define the encoder network for encoding a pose.
    '''
    def __init__(self, num_params_per_joint, num_zdim, joint_dict, levels):
        '''
        param num_params_per_joint: int, dof of a joint
        param num_zdim: int, dimensions of the embedding space
        param joint_dict: dictionary, joints and joints' parent and index
        param levels: list, combination of parts in each level. 
        '''
        super(SpatialEncoder, self).__init__()
        self.num_params_per_joint = num_params_per_joint 
        self.num_zdim = num_zdim 
        self.joint_dict = joint_dict 
        self.levels = copy.deepcopy(levels)
        
        for i in range(len(self.levels[0])):
            for j in range(len(self.levels[0][i])):
                self.levels[0][i][j] = self.joint_dict[self.levels[0][i][j]]['index']
        self.level1_layers = []
        for group in self.levels[0]:
            self.level1_layers.append(tf.keras.layers.Dense(units=64, activation='tanh'))
        self.level2_layers = []
        for group in self.levels[1]:
            self.level2_layers.append(tf.keras.layers.Dense(units=128, activation='tanh'))
        self.level3_layers = []
        for group in self.levels[2]:
            self.level3_layers.append(tf.keras.layers.Dense(units=256, activation='tanh'))
        self.root_layer = tf.keras.layers.Dense(units=64, activation='tanh')
        self.level4_layers = tf.keras.layers.Dense(units=512, activation='tanh')
        self.fc1 = tf.keras.layers.Dense(units=512, activation='tanh')
        self.out = tf.keras.layers.Dense(units=self.num_zdim, activation='tanh')


    def call(self, x):
        '''
        param x: [batchsize, pose]
        '''
        self.features = [[],[],[],[]]
        for i in range(len(self.levels[0])):
            group  = self.levels[0][i]
            group_input = tf.concat([x[:, part*self.num_params_per_joint:(part+1)*self.num_params_per_joint] for part in group],axis=1)
            group_ft = self.level1_layers[i](group_input)
            self.features[0].append(group_ft)
        for i in range(len(self.levels[1])):
            group = self.levels[1][i]
            group_input = tf.concat([self.features[0][part] for part in group], axis=1)
            group_ft = self.level2_layers[i](group_input)
            self.features[1].append(group_ft)
        for i in range(len(self.levels[2])):
            group = self.levels[2][i]
            group_input = tf.concat([self.features[1][part] for part in group], axis=1)
            group_ft = self.level3_layers[i](group_input)
            self.features[2].append(group_ft)
        root_inp = tf.concat([x[:, :self.num_params_per_joint], x[:, -3:]],axis=1)
        root_ft = self.root_layer(root_inp)
        self.features[2].append(root_ft)
        level4_inp = tf.concat(self.features[2], axis=1)
        level4_ft = self.level4_layers(level4_inp)
        self.features[3].append(level4_ft)
        ft = self.fc1(self.features[3][0])
        z = self.out(ft)
        return z
        
class SpatialDecoder(tf.keras.Model):
    def __init__(self, num_params_per_joint, num_zdim, joint_dict, levels):
        '''
        param num_params_per_joint: int, dof of a joint
        param num_zdim: int, dimensions of the embedding space
        param joint_dict: dictionary, joints and joints' parent and index
        param levels: list, combination of parts in each level. 
        '''
        super(SpatialDecoder, self).__init__()
        self.num_params_per_joint = num_params_per_joint 
        self.num_zdim = num_zdim 
        self.joint_dict = joint_dict 
        self.levels = copy.deepcopy(levels)
        self.index_dict = {}
        for i in range(len(self.levels[0])):
            for j in range(len(self.levels[0][i])):
                self.index_dict[self.joint_dict[self.levels[0][i][j]]['index']] = (i,j)
        self.fc1 = tf.keras.layers.Dense(units=512, activation=tf.nn.leaky_relu)
        self.fc2 = tf.keras.layers.Dense(units=512, activation=tf.nn.leaky_relu)
        self.level4_layers = tf.keras.layers.Dense(units=256*2+64, activation=tf.nn.leaky_relu)
        self.root_layer = tf.keras.layers.Dense(units=6, activation=None)
        self.level3_layers = []
        for group in self.levels[2]:
            self.level3_layers.append(tf.keras.layers.Dense(units=256, activation=tf.nn.leaky_relu))
        self.level2_layers = []
        for group in self.levels[1]:
            self.level2_layers.append(tf.keras.layers.Dense(units=128, activation=tf.nn.leaky_relu))
        self.level1_layers = []
        for group in self.levels[0]:
            self.level1_layers.append(tf.keras.layers.Dense(units=self.num_params_per_joint*len(group), activation=None))

    def call(self, z):
        '''
        param z: [batchsize, zdim]
        '''        
        self.features = [[],[],[],[]]
        ft1_temp = []
        for i in range(len(self.levels[0])):
            ft1_temp.append([])
        ft2_temp = []
        for i in range(len(self.levels[1])):
            ft2_temp.append([])
        z = self.fc1(z)
        z = self.fc2(z)
        level4_ft = self.level4_layers(z)
        self.features[3].append(level4_ft[:, :256])
        self.features[3].append(level4_ft[:, 256:512])
        self.features[3].append(level4_ft[:, 512:])
        for i in range(len(self.levels[2])):
            group_inp = self.features[3][i]
            group_ft = self.level3_layers[i](group_inp)
            ft2_temp[self.levels[2][i][0]].append(group_ft[:, :128])
            ft2_temp[self.levels[2][i][1]].append(group_ft[:, 128:])
        for i in range(len(ft2_temp)):
            self.features[2].append(tf.add_n(ft2_temp[i]))
        for i in range(len(self.levels[1])):
            group_inp = self.features[2][i]
            group_ft = self.level2_layers[i](group_inp)
            ft1_temp[self.levels[1][i][0]].append(group_ft[:, :64])
            ft1_temp[self.levels[1][i][1]].append(group_ft[:, 64:])
        for i in range(len(ft1_temp)):
            self.features[1].append(tf.add_n(ft1_temp[i]))
        for i in range(len(self.levels[0])):
            group_inp = self.features[1][i]
            group_ft = self.level1_layers[i](group_inp)
            self.features[0].append(group_ft)
        root = self.root_layer(self.features[3][2])
        pose = [root[:, :3]]
        for i in range(1, len(self.joint_dict)):
            index_i, index_j = self.index_dict[i]
            pose.append(self.features[0][index_i][:, index_j*self.num_params_per_joint:(index_j+1)*self.num_params_per_joint])
        pose.append(root[:, 3:])
        return tf.concat(pose, axis=1)

class SpatialDiscriminator(tf.keras.Model):
    def __init__(self, num_params_per_joint, num_zdim, joint_dict, levels):
        super(SpatialDiscriminator, self).__init__()
        self.num_params_per_joint = num_params_per_joint
        self.num_zdim = num_zdim
        self.joint_dict = joint_dict
        self.levels = copy.deepcopy(levels)
        for i in range(len(self.levels[0])):
            for j in range(len(self.levels[0][i])):
                self.levels[0][i][j] = self.joint_dict[self.levels[0][i][j]]['index']
        self.layer_dict = {}
        self.layer_dict['level1'] = []
        for group in self.levels[0]:
            self.layer_dict['level1'].append([tf.keras.layers.Dense(units=32, activation=tf.nn.leaky_relu),
                                tf.keras.layers.Dense(units=1, activation=tf.nn.leaky_relu)])
        self.layer_dict['level2'] = []
        for group in self.levels[1]:
            self.layer_dict['level2'].append([tf.keras.layers.Dense(units=64, activation=tf.nn.leaky_relu),
                               tf.keras.layers.Dense(units=1, activation=tf.nn.leaky_relu)])
        self.root_layers1 = tf.keras.layers.Dense(units=32, activation=tf.nn.leaky_relu)
        self.root_layers2 = tf.keras.layers.Dense(units=1, activation=tf.nn.leaky_relu)
        self.fc1 = tf.keras.layers.Dense(units=256, activation=tf.nn.leaky_relu)
        self.fc2 = tf.keras.layers.Dense(units=1, activation=tf.nn.leaky_relu)
        self.final = tf.keras.layers.Dense(units=128, activation=tf.nn.leaky_relu)
        self.out = tf.keras.layers.Dense(units=1)

    def call(self, x):
        self.features = [[],[],[],[]]
        for i in range(len(self.levels[0])):
            group  = self.levels[0][i]
            group_input = tf.concat([x[:, part*self.num_params_per_joint:(part+1)*self.num_params_per_joint] for part in group],axis=1)
            group_ft_1 = self.layer_dict['level1'][i][0](group_input)
            self.features[0].append(group_ft_1)
            group_ft_2 = self.layer_dict['level1'][i][1](group_ft_1)
            self.features[1].append(group_ft_2)
        root_inp = tf.concat([x[:, :self.num_params_per_joint], x[:, -3:]],axis=1)
        root_ft = self.root_layers1(root_inp)
        root_ft = self.root_layers2(root_ft)
        for i in range(len(self.levels[1])):
            group = self.levels[1][i]
            group_input = tf.concat([self.features[0][part] for part in group], axis=1)
            group_ft_1 = self.layer_dict['level2'][i][0](group_input)
            self.features[2].append(group_ft_1)
            group_ft_2 = self.layer_dict['level2'][i][1](group_ft_1)
            self.features[3].append(group_ft_2)
        fusion_ft = []
        for ft in self.features[2]:
            fusion_ft.append(ft)
        fusion_ft = tf.concat(fusion_ft, axis=1)
        fusion_ft = self.fc1(fusion_ft)
        fusion_ft = self.fc2(fusion_ft)
        final_ft = []
        for ft in self.features[1]:
            final_ft.append(ft)
        for ft in self.features[3]:
            final_ft.append(ft)
        final_ft.append(fusion_ft)
        final_ft.append(root_ft)
        final_ft = tf.concat(final_ft, axis=1)
        final_ft = self.final(final_ft)
        out = self.out(final_ft)
        return out
        
        



