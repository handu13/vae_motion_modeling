import tensorflow as tf
import collections

CMU_SKELETON = collections.OrderedDict(
    [
        ('Hips', {'parent': None, 'index': 0}),
        ('LHipJoint', {'parent':'Hips', 'index':1}),
        ('LeftUpLeg', {'parent': 'LHipJoint', 'index': 2}),
        ('LeftLeg', {'parent': 'LeftUpLeg', 'index': 3}),
        ('LeftFoot', {'parent': 'LeftLeg', 'index': 4}),
        ('LeftToeBase', {'parent': 'LeftFoot', 'index': 5}),
        ('LowerBack', {'parent': 'Hips', 'index': 6}),
        ('Spine', {'parent': 'LowerBack', 'index': 7}),
        ('Spine1', {'parent': 'Spine', 'index': 8}),
        ('LeftShoulder', {'parent': 'Spine1', 'index': 9}),
        ('LeftArm', {'parent': 'LeftShoulder', 'index': 10}),
        ('LeftForeArm', {'parent': 'LeftArm', 'index': 11}),
        ('LeftHand', {'parent': 'LeftForeArm', 'index': 12}),
        ('LThumb', {'parent': 'LeftHand', 'index': 13}),
        ('LeftFingerBase', {'parent': 'LeftHand', 'index': 14}),
        ('LeftHandFinger1', {'parent': 'LeftFingerBase', 'index': 15}),
        ('Neck', {'parent': 'Spine1', 'index': 16}),
        ('Neck1', {'parent': 'Neck', 'index': 17}),
        ('Head', {'parent': 'Neck1', 'index': 18}),
        ('RightShoulder', {'parent': 'Spine1', 'index': 19}),
        ('RightArm', {'parent': 'RightShoulder', 'index': 20}),
        ('RightForeArm', {'parent': 'RightArm', 'index': 21}),
        ('RightHand', {'parent': 'RightForeArm', 'index': 22}),
        ('RThumb', {'parent': 'RightHand', 'index': 23}),
        ('RightFingerBase', {'parent': 'RightHand', 'index': 24}),
        ('RightHandFinger1', {'parent': 'RightFingerBase', 'index': 25}),
        ('RHipJoint', {'parent': 'Hips', 'index': 26}),
        ('RightUpLeg', {'parent': 'RHipJoint', 'index': 27}),
        ('RightLeg', {'parent': 'RightUpLeg', 'index': 28}),
        ('RightFoot', {'parent': 'RightLeg', 'index': 29}),
        ('RightToeBase', {'parent': 'RightFoot', 'index': 30})
    ]
)

CMU_ANIMATED_JOINTS = ['Hips','LHipJoint','LeftUpLeg','LeftLeg','LeftFoot','LeftToeBase','LowerBack',
                       'Spine','Spine1','LeftShoulder','LeftArm','LeftForeArm','LeftHand','LThumb','LeftFingerBase','LeftHandFinger1',
                       'Neck','Neck1','Head','RightShoulder','RightArm','RightForeArm',
                       'RightHand','RThumb','RightFingerBase','RightHandFinger1',
                       'RHipJoint','RightUpLeg', 'RightLeg','RightFoot','RightToeBase']

CMU_JOINT_LEVELS = [[['RightShoulder', 'RightArm', 'RightForeArm', 'RightHand', 'RThumb', 'RightFingerBase', 'RightHandFinger1'], 
                       ['LeftShoulder', 'LeftArm', 'LeftForeArm', 'LeftHand', 'LThumb', 'LeftFingerBase', 'LeftHandFinger1'],
                       ['RHipJoint', 'RightUpLeg', 'RightLeg', 'RightFoot', 'RightToeBase'], 
                       ['LHipJoint', 'LeftUpLeg', 'LeftLeg', 'LeftFoot', 'LeftToeBase'], 
                       ['LowerBack', 'Spine', 'Spine1', 'Neck', 'Neck1', 'Head']
                      ],
                     [[0, 4],[1, 4],[2, 4],[3, 4]],
                     [[0, 1],[2, 3]]]
                
def bonelength(x, skeleton):
    '''
    input:
        x: one frame, [batchsize, num_joints, 3]
        skeleton: skeleton dict
    output:
        length: [batchs, num_bones] 
    '''
    length = []
    childs = []
    parents = []
    for joint in skeleton:
        if skeleton[joint]["parent"]:
            childs.append(tf.expand_dims(x[:, skeleton[joint]["index"], :], axis=1))
            parents.append(tf.expand_dims(x[:, skeleton[skeleton[joint]["parent"]]["index"], :],axis=1))
    childs = tf.concat(childs, axis=1)
    parents = tf.concat(parents, axis=1) # batchsize, num_bones, 3
    return tf.math.sqrt(tf.reduce_sum(tf.math.squared_difference(childs, parents), axis=-1))

def bone_loss(ref_frame, gen_seq, skeleton):
    '''
    inputs:
        ref_frame: the last frame of input sequence, [batchsize, num_joints, 3]
        gen_seq: the generated sequence, [batchsize, seq_len, num_joints, 3]
    '''
    bone_length_ref = bonelength(ref_frame, skeleton) # [batchsize, num_bones]
    loss = 0
    batchsize, seq_len = gen_seq.shape[:2]
    for i in range(seq_len):
        bone_length = bonelength(gen_seq[:, i, :, :], skeleton)
        loss += tf.reduce_mean(tf.abs(bone_length_ref - bone_length) / (bone_length_ref + 0.00001))
    return loss / (seq_len*batchsize)

def smoothness(data):
    g_next = data[:, 1:, :]
    g_prev = data[:, :-1, :]
    return tf.reduce_mean(tf.norm(g_next-g_prev, ord=2, axis=-1), axis=-1)
    
def smoothness_loss(data):
    s = smoothness(data)
    return tf.maximum(0.0001, tf.reduce_mean(s))
