"""train a frame encoder with cyclic loss
"""
import os 
import sys
import numpy as np
import collections
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), '../..'))
from network.PoseEmbedding import SpatialEncoder, SpatialDecoder, SpatialDiscriminator, SpatialEncDecoder
from utils import CMU_SKELETON, CMU_JOINT_LEVELS, CMU_ANIMATED_JOINTS
from mosi_dev_deepmotionmodeling.preprocessing.preprocessing import process_bvhfile, process_file
from mosi_dev_deepmotionmodeling.mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder
from mosi_dev_deepmotionmodeling.utilities.utils import export_point_cloud_data_without_foot_contact
from mosi_dev_deepmotionmodeling.utilities.skeleton_def import MH_CMU_ANIMATED_JOINTS


CMU_SKELETON_29 = collections.OrderedDict(
    [
        ('Hips', {'parent': None, 'index': 0}),
        ('LeftUpLeg', {'parent': 'LHipJoint', 'index': 1}),
        ('LeftLeg', {'parent': 'LeftUpLeg', 'index': 2}),
        ('LeftFoot', {'parent': 'LeftLeg', 'index': 3}),
        ('LeftToeBase', {'parent': 'LeftFoot', 'index': 4}),
        ('LowerBack', {'parent': 'Hips', 'index': 5}),
        ('Spine', {'parent': 'LowerBack', 'index': 6}),
        ('Spine1', {'parent': 'Spine', 'index': 7}),
        ('LeftShoulder', {'parent': 'Spine1', 'index': 8}),
        ('LeftArm', {'parent': 'LeftShoulder', 'index': 9}),
        ('LeftForeArm', {'parent': 'LeftArm', 'index': 10}),
        ('LeftHand', {'parent': 'LeftForeArm', 'index': 11}),
        ('LThumb', {'parent': 'LeftHand', 'index': 12}),
        ('LeftFingerBase', {'parent': 'LeftHand', 'index': 13}),
        ('LeftHandFinger1', {'parent': 'LeftFingerBase', 'index': 14}),
        ('Neck', {'parent': 'Spine1', 'index': 15}),
        ('Neck1', {'parent': 'Neck', 'index': 16}),
        ('Head', {'parent': 'Neck1', 'index': 17}),
        ('RightShoulder', {'parent': 'Spine1', 'index': 18}),
        ('RightArm', {'parent': 'RightShoulder', 'index': 19}),
        ('RightForeArm', {'parent': 'RightArm', 'index': 20}),
        ('RightHand', {'parent': 'RightForeArm', 'index': 21}),
        ('RThumb', {'parent': 'RightHand', 'index': 22}),
        ('RightFingerBase', {'parent': 'RightHand', 'index': 23}),
        ('RightHandFinger1', {'parent': 'RightFingerBase', 'index': 24}),
        ('RightUpLeg', {'parent': 'RHipJoint', 'index': 25}),
        ('RightLeg', {'parent': 'RightUpLeg', 'index': 26}),
        ('RightFoot', {'parent': 'RightLeg', 'index': 27}),
        ('RightToeBase', {'parent': 'RightFoot', 'index': 28})
    ]
)
CMU_JOINT_LEVELS_29 = [[['RightShoulder', 'RightArm', 'RightForeArm', 'RightHand', 'RThumb', 'RightFingerBase', 'RightHandFinger1'], 
                       ['LeftShoulder', 'LeftArm', 'LeftForeArm', 'LeftHand', 'LThumb', 'LeftFingerBase', 'LeftHandFinger1'],
                       ['RightUpLeg', 'RightLeg', 'RightFoot', 'RightToeBase'], 
                       ['LeftUpLeg', 'LeftLeg', 'LeftFoot', 'LeftToeBase'], 
                       ['LowerBack', 'Spine', 'Spine1', 'Neck', 'Neck1', 'Head']
                      ],
                     [[0, 4],[1, 4],[2, 4],[3, 4]],
                     [[0, 1],[2, 3]]]
def train_spatialEncoder():
    training_data = r''
    encoder = SpatialEncoder(num_params_per_joint=3, num_zdim=10, joint_dict=CMU_SKELETON_29, levels=CMU_JOINT_LEVELS_29)
    decoder = SpatialDecoder()
    encoder = SpatialEncDecoder()


def test_spatial_encoder():
    encoder = SpatialEncoder(num_params_per_joint=3, num_zdim=10, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
    input_data = np.random.rand(32, 3*31)
    input_bvhfile = r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\mk_cmu_skeleton\h36m\S1\WalkTogether.bvh'
    bvhreader = BVHReader(input_bvhfile)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader)
    animated_joints = skeleton.animated_joints

    # def preprocess(filename, window):
    #     clip = process_file(filename, window=window, window_step=window, sliding_window=True, animated_joints=CMU_ANIMATED_JOINTS,
    #                  body_plane_indices=[6,27,2], fid_l=[4,5], fid_r=[29,30])
    #     return clip
    # body_plane_indice=[2, 17, 13]

    # def process_file(filename, window=240, window_step=120, sliding_window=True, animated_joints=MH_CMU_ANIMATED_JOINTS,
    #                  body_plane_indices=[7, 25, 1], fid_l=[3, 4], fid_r=[27, 28]):
    #     """ Compute joint positions for animated joints """

    #     print(filename)
    #     bvhreader = BVHReader(filename)
    #     ref_dir = np.array([0, 0, 1])
    #     up_axis = np.array([0, 1, 0])

    processed_data = process_file(input_bvhfile, sliding_window=False)
    print(processed_data.shape)
    
    # export_point_cloud_data_without_foot_contact(processed_data, 'WalkTogether.panim', scale_factor=5)
    body_plane_indices=[6,27,2]
    fid_l=[4,5]
    fid_r=[29,30]

    body_plane_joints = [MH_CMU_ANIMATED_JOINTS[index] for index in body_plane_indices]
    fid_l_joints = [MH_CMU_ANIMATED_JOINTS[index] for index in fid_l]
    fid_r_joints = [MH_CMU_ANIMATED_JOINTS[index] for index in fid_r]
    print("body plain joints: {}".format(body_plane_joints))
    print("fid_l joints: {}".format(fid_l_joints))
    print("fid_r joints: {}".format(fid_r_joints))
    body_plane_indices_new = [animated_joints.index(joint) for joint in body_plane_joints]
    fid_l_new = [animated_joints.index(joint) for joint in fid_l_joints]
    print("new body plain is: ", body_plane_indices_new)
    print("new left joints")
    # processed_data = process_file(input_bvhfile, sliding_window=False, animated_joints=CMU_SKELETON)
    # print(processed_data.shape)
    res = encoder(input_data)
    print(res.shape)

    decoder = SpatialDecoder(num_params_per_joint=3, num_zdim=10, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
    res_dec = decoder(res)
    print(res_dec.shape)




if __name__ == "__main__":
    test_spatial_encoder()