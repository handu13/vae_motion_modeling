import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + r'/../..')
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + r'/..')
sys.path.append("..")
os.chdir(os.path.dirname(os.path.abspath(__file__)))
import tensorflow as tf
import numpy as np
import collections
from mosi_dev_deepmotionmodeling.utilities.utils import get_files, export_point_cloud_data_without_foot_contact, write_to_json_file
from mosi_dev_deepmotionmodeling.mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder, panim
from network.PoseEmbedding import SpatialEncoder, SpatialDecoder, SpatialDiscriminator
from model.PoseGAN import PoseGAN
from utils import CMU_SKELETON, CMU_JOINT_LEVELS
EPS = 1e-6


def train():
    training_data = np.load(os.path.dirname(os.path.abspath(__file__)) + r'/../../data/training_data/training_clips_h36m.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    reshaped_training_data = np.reshape(training_data, (-1, dims))
    train_data = reshaped_training_data[:int(0.9*n_samples)]
    val_data = reshaped_training_data[int(0.9*n_samples):]
    num_zdim = 32
    num_params_per_joint = 3
    joint_dict = CMU_SKELETON
    joint_levels = CMU_JOINT_LEVELS
    T1 = 200
    T2 = 300
    batchsize = 32
    model_name = 'spatialEncoder_new'
    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/model'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/log'
    enc = SpatialEncoder(num_params_per_joint, num_zdim, joint_dict, joint_levels)
    dec = SpatialDecoder(num_params_per_joint, num_zdim, joint_dict, joint_levels)
    disc = SpatialDiscriminator(num_params_per_joint, num_zdim, joint_dict, joint_levels)
    model = PoseGAN(model_name, enc, dec, disc, num_zdim)
    model.train(train_data, T1, T2, batchsize, g_iter=1, d_iter=5, learning_rate=1e-5, save_every_epochs=10, save_path=save_path, val_data=val_data, val_every_epochs=5, log_path=log_path)


def test():
    training_data = np.load(os.path.dirname(os.path.abspath(__file__)) + r'/../../data/training_data/training_clips_h36m.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    test_data = training_data[int(0.9*n_samples):]
    test_samples = test_data.shape[0]
    num_zdim = 32
    num_params_per_joint = 3
    joint_dict = CMU_SKELETON
    joint_levels = CMU_JOINT_LEVELS
    load_epoch = 300
    batchsize = 32
    model_name = 'spatialEncoder_new'
    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/model'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/log'
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/sample'
    sample_dir = os.path.join(sample_path, str(load_epoch))
    if not os.path.exists(sample_dir):
        os.makedirs(sample_dir)
    enc = SpatialEncoder(num_params_per_joint, num_zdim, joint_dict, joint_levels)
    dec = SpatialDecoder(num_params_per_joint, num_zdim, joint_dict, joint_levels)
    disc = SpatialDiscriminator(num_params_per_joint, num_zdim, joint_dict, joint_levels)
    model = PoseGAN(model_name, enc, dec, disc, num_zdim)
    model.load(save_path, load_epoch, 'encoder')
    model.load(save_path, load_epoch, 'decoder')
    x_recon_loss, x_recon = model.test(test_data, batchsize)
    x_recon = x_recon.numpy()
    print("Testing reconstruction loss: ", x_recon_loss)
    x_recon = x_recon * std_pose + mean_pose
    test_data = test_data * std_pose + mean_pose
    skeleton_bvh = os.path.dirname(os.path.abspath(__file__)) + r'/../../data/training_data/preprocessed/input_data/preprocessed/Vicon_6kmh.bvh'  ### provide skeleton example
    skeleton_bvh = BVHReader(skeleton_bvh)
    skeleton = SkeletonBuilder().load_from_bvh(skeleton_bvh)
    animated_joints = skeleton.generate_bone_list_description() 
    if isinstance(animated_joints, list):
        animated_joints = collections.OrderedDict([(d['name'], {'parent':d['parent'], 'index':d['index']}) for d in animated_joints])
    
    for i in range(x_recon.shape[0]):
        origin_filename = os.path.join(sample_dir, 'origin_' + str(i) + '.panim')
        export_point_cloud_data_without_foot_contact(test_data[i], origin_filename, skeleton=animated_joints)
        sample_filename = os.path.join(sample_dir, 'generated_' + str(i) + '.panim')
        export_point_cloud_data_without_foot_contact(x_recon[i], sample_filename, skeleton=animated_joints)

if __name__ == "__main__":
    train()
    # test()



