
import os
import sys
dirname = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(dirname, '../..'))
import shutil
import numpy as np
from Config import Config
from mosi_dev_deepmotionmodeling.mosi_utils_anim import load_json_file, write_to_json_file
from mosi_dev_deepmotionmodeling.preprocessing.preprocessing import process_file
from src_final.network.Discriminator import HPDiscriminator, Bihmp_Discriminator, CNN_Discriminator, CNN_Discriminator_test1, CNN_Discriminator_test2, CNN_Discriminator_test3
from src_final.network.Generator import HPGenerator,Bihmp_Generator, TCN_RNN_Generator
from src_final.model.HPGAN import HPGAN
from src_final.model.BIHMPGAN import BIHMPGAN
from src_final.model.motionGAN_recon import motionGAN_recon
from src_final.model.motionGAN_norecon import motionGAN_norecon
from src_final.utils import CMU_SKELETON, CMU_ANIMATED_JOINTS
import itertools
import copy
EPS = 1e-6



def test_spatial_encoder():
    training_data_path = os.path.join(dirname, '../..', 'data/training_data/dancing/dancing_data.npy')
    training_data = np.load(training_data_path)
    print(training_data.shape)    


def train_motionGAN():

    training_data_path = os.path.join(dirname, '../..', 'data/training_data/dancing/dancing_data.npy')
    training_data = np.load(training_data_path)
    print(training_data.shape)

    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS

    # config = load_json_file(os.path.join(dirname, '../..', 'config/motionGAN_norecon.json'))
    config = load_json_file(r'D:\workspace\my_git_repos\vae_motion_modeling\config\motionGAN_norecon\config_29.json')

    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    train_set = training_data[:int(0.9*n_samples)] 
    test_set = training_data[int(0.9*n_samples):]    

    input_sequence_length = 120
    output_sequence_length = 120

    input_sequence = train_set[:, :input_sequence_length, :]
    output_sequence = train_set[:, input_sequence_length:, :]    
    test_sequence = test_set[:, :input_sequence_length, :]   
    name = 'dancing_motionGAN'
    output_dir = os.path.join(dirname, '../..', r'data/models', name)
    if not os.path.join(output_dir):
        os.makedirs(output_dir)
    ### model initialization
    g = TCN_RNN_Generator(config["generator"])
    d = CNN_Discriminator(config["discriminator"])
    model = motionGAN_norecon(g, d, name, debug=True, output_dir=output_dir)
    model.train(input_sequence, output_sequence, test_sequence, test_set, std_pose, mean_pose, config["train"])


def test1():
    config_file = r'D:\workspace\my_git_repos\vae_motion_modeling\config\motionGAN_norecon\config_29.json'
    config = load_json_file(config_file)
    name = 'motionGAN'
    output_dir = os.path.join('output', name)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    ## load training data
    # training_data = np.load(config['data']['data_path'])
    training_data_path = os.path.join(dirname, '../..', 'data/training_data/dancing/dancing_data.npy')
    training_data = np.load(training_data_path)
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    train_set = training_data[:int(0.9*n_samples)] 
    test_set = training_data[int(0.9*n_samples):]      

    input_sequence_length = config['data']['input_sequence_length']
    output_sequence_length = config['data']['output_sequence_length']
    input_sequence = train_set[:, :input_sequence_length, :]
    output_sequence = train_set[:, input_sequence_length:, :]    
    test_sequence = test_set[:, :input_sequence_length, :]        


    ## model initialization
    g = TCN_RNN_Generator(config["generator"])
    d = CNN_Discriminator(config["discriminator"])
    model = motionGAN_norecon(g, d, name, debug=True, output_dir=output_dir)
    model.train(input_sequence, output_sequence, test_sequence, test_set, std_pose, mean_pose, config["train"])
    # shutil.move(filename, output_dir)


if __name__ == "__main__":
    # train_motionGAN()
    test1()