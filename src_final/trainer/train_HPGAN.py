import os
import sys
dirname = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(dirname, '../..'))
import numpy as np
from Config import Config
from mosi_dev_deepmotionmodeling.mosi_utils_anim import load_json_file, write_to_json_file
from mosi_dev_deepmotionmodeling.preprocessing.preprocessing import process_file
from src_final.network.Discriminator import HPDiscriminator, Bihmp_Discriminator, CNN_Discriminator, CNN_Discriminator_test1, CNN_Discriminator_test2, CNN_Discriminator_test3
from src_final.network.Generator import HPGenerator,Bihmp_Generator, TCN_RNN_Generator
from src_final.model.HPGAN import HPGAN
from src_final.model.BIHMPGAN import BIHMPGAN
from src_final.model.motionGAN_recon import motionGAN_recon
from src_final.model.motionGAN_norecon import motionGAN_norecon
from src_final.utils import CMU_SKELETON, CMU_ANIMATED_JOINTS
from mosi_dev_deepmotionmodeling.mosi_utils_anim.utilities.io_helper_functions import load_json_file
import itertools
import copy
EPS = 1e-6


def train_HPGAN():
    config_file = r'D:\workspace\my_git_repos\vae_motion_modeling\config\HP-GAN\config_0.json'
    config = load_json_file(config_file)
    ## load training data
    name =  'hpgan'
    training_data = np.load(config['data']['data_path'])
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    train_set = training_data[:int(0.9*n_samples)] 
    test_set = training_data[int(0.9*n_samples):]      

    input_sequence_length = config['data']['input_sequence_length']
    output_sequence_length = config['data']['output_sequence_length']
    input_sequence = train_set[:, :input_sequence_length, :]
    output_sequence = train_set[:, input_sequence_length:, :]    
    test_sequence = test_set[:, :input_sequence_length, :]        
    
    output_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output')
    ## model initialization
    g = HPGenerator(config["generator"])
    d = HPDiscriminator(config["discriminator"])
    e = HPDiscriminator(config["evaluator"])
    model = HPGAN(g, d, e, name, debug=True, output_dir=output_dir)
    model.train(input_sequence, output_sequence, test_sequence, test_set, std_pose, mean_pose, config["train"])


if __name__ == "__main__":
    train_HPGAN()