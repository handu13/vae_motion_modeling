import h5py
import numpy as np

def h5tonpy(h5path,npypath): 
    with h5py.File(h5path, 'r') as h5f:
        poses = h5f['3D_positions'][:].T #[num_frames,96]
        num_frames = poses.shape[0]
        poses = poses.reshape(num_frames,-1,3) #[num_frames,num_joints,3]
    np.save(npypath,poses)
	
def npytoh5(npypath,h5path):
    poses = np.load(npypath)
    with h5py.File(h5path, 'w') as h5f:
        num_frames = poses.shape[0]
        num_joints = poses.shape[1]
        poses = poses.reshape(num_frames, num_joints*poses.shape[2]).T
        h5f.create_dataset('3D_positions',data=poses)


if __name__ == "__main__":
    h5tonpy(r'H:\hpgan-master\human36m\S1\Walking.h5',r'H:\hpgan-master\test.npy')
    npytoh5(r'H:\hpgan-master\test.npy',r'H:\hpgan-master\test.h5')