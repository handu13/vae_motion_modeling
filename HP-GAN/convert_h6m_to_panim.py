import numpy as np
from morphablegraphs.python_src.morphablegraphs.animation_data.quaternion import Quaternion
import os
from morphablegraphs.python_src.morphablegraphs.utilities import write_to_json_file

### H6M skeleton joint definition
Unknown = -1  # The type isn't available
Hip = 0
HipRight = 1
KneeRight = 2
FootRight = 3
ToeBaseRight = 4
Site1 = 5
HipLeft = 6
KneeLeft = 7
FootLeft = 8
ToeBaseLeft = 9
Site2 = 10
Spine1 = 11
Spine2 = 12
Neck = 13
Head = 14
Site3 = 15
ShoulderLeft = 16
ElbowLeft = 17
WristLeft = 18
HandLeft = 19
HandThumbLeft = 20
Site4 = 21
WristEndLeft = 22
Site5 = 23
ShoulderRight = 24
ElbowRight = 25
WristRight = 26
HandRight = 27
HandThumbRight = 28
Site6 = 29
WristEndRight = 30
Site7 = 31


h6M_SKELETON = [
    {'name': 'Hips', 'parent': None, 'index': 0},
    {'name': 'HipRight', 'parent': 'Hips', 'index': 1},
    {'name': 'KneeRight', 'parent': 'HipRight', 'index': 2},
    {'name': 'FootRight', 'parent': 'KneeRight', 'index': 3},
    {'name': 'ToeBaseRight', 'parent': 'FootRight', 'index': 4},
    {'name': 'Site1', 'parent': 'ToeBaseRight', 'index': 5},
    {'name': 'HipLeft', 'parent': 'Hips', 'index': 6},
    {'name': 'KneeLeft', 'parent': 'HipLeft', 'index': 7},
    {'name': 'FootLeft', 'parent': 'KneeLeft', 'index': 8},
    {'name': 'ToeBaseLeft', 'parent': 'FootLeft', 'index': 9},
    {'name': 'Site2', 'parent': 'ToeBaseLeft', 'index': 10},
    {'name': 'Spine1', 'parent': 'Hips', 'index': 11},
    {'name': 'Spine2', 'parent': 'Spine1', 'index': 12},
    {'name': 'Neck', 'parent': 'Spine2', 'index': 13},
    {'name': 'Head', 'parent': 'Neck', 'index': 14},
    {'name': 'Site3', 'parent': 'Head', 'index': 15},
    {'name': 'ShoulderLeft', 'parent': 'Neck', 'index': 16},
    {'name': 'ElbowLeft', 'parent': 'ShoulderLeft', 'index': 17},
    {'name': 'WristLeft', 'parent': 'ElbowLeft', 'index': 18},
    {'name': 'HandLeft', 'parent': 'WristLeft', 'index': 19},
    {'name': 'HandThumbLeft', 'parent': 'HandLeft', 'index': 20},
    {'name': 'Site4', 'parent': 'HandThumbLeft', 'index': 21},
    {'name': 'WristEndLeft', 'parent': 'HandLeft', 'index': 22},
    {'name': 'Site5', 'parent': 'WristEndLeft', 'index': 23},
    {'name': 'ShoulderRight', 'parent': 'Neck', 'index': 24},
    {'name': 'ElbowRight', 'parent': 'ShoulderRight', 'index': 25},
    {'name': 'WristRight', 'parent': 'ElbowRight', 'index': 26},
    {'name': 'HandRight', 'parent': 'WristRight', 'index': 27},
    {'name': 'HandThumbRight', 'parent': 'HandRight', 'index': 28},
    {'name': 'Site6', 'parent': 'HandThumbRight', 'index': 29},
    {'name': 'WristEndRight', 'parent': 'HandRight', 'index': 30},
    {'name': 'Site7', 'parent': 'WristEndRight', 'index': 31},
]

def convert_batch_pd_to_panim(input_file, output_folder):
    '''
    convert the motions generated from HP-GAN to panim format

    Args:
        input_file(str): numpy file contains point cloud data, batchsize * n_frames * n_joints * 3
        output_folder(str): path to save generated file for each sample in the batch

    Returns:
        None
    '''
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    batch_data = np.load(input_file)
    n_samples, n_frames, n_joints, _ = batch_data.shape
    ### hard coded parameters for H6m data processing
    scaling_factor = 10.0
    rotation_angle = -90
    rotation_axis = np.array([1, 0, 0])
    r_q = Quaternion.fromAngleAxis(np.deg2rad(rotation_angle), rotation_axis)

    batch_data = batch_data / scaling_factor
    for idx, motion in enumerate(batch_data):
        rotated_data = np.zeros(motion.shape)
        for i in range(n_frames):
            for j in range(n_joints):
                rotated_data[i, j] = r_q * motion[i, j]
        output_data = {
            "has_skeleton": True, 'motion_data': rotated_data.tolist(), 'skeleton': h6M_SKELETON
        }
        write_to_json_file(os.path.join(output_folder, str(idx) + '.panim'), output_data)


def convert_pd_to_panim(input_file, output_file):
    '''

    Args:
        input_file (str): path to input file, input_file should be a .npy file containing point cloud data which has
        the motion data matrix n_frames * n_joint *3
        output_file (str): path to output file. The filename should end with extension .panim

    Returns:
        None
    '''

    motion_data = np.load(input_file)
    ### hard coded parameters for H6m data processing
    scaling_factor = 10.0
    rotation_angle = -90
    rotation_axis = np.array([1, 0, 0])
    motion_data = motion_data / scaling_factor
    r_q = Quaternion.fromAngleAxis(np.deg2rad(rotation_angle), rotation_axis)
    n_frames, n_joints, n_dims = motion_data.shape
    rotated_data = np.zeros(motion_data.shape)
    for i in range(n_frames):
        for j in range(n_joints):
            rotated_data[i, j] = r_q * motion_data[i, j]
    output_data = {
        "has_skeleton": True, 'motion_data': rotated_data.tolist(), 'skeleton': h6M_SKELETON
    }
    write_to_json_file(output_file, output_data)


def demo():
    # filename = r'E:\gits\vae_motion_modeling\test data\test.npy'
    # panim_data = np.load(r'E:\workspace\projects\retargeting_experiments\panim_from_mk_retargeting\walking.npy')
    filename = r'E:\gits\vae_motion_modeling\data\HPGAN_results\test\pred_7_skeleton.npy'
    save_filename = r'E:\gits\vae_motion_modeling\data\HPGAN_results\panim_output'
    # convert_pd_to_panim(filename, save_filename)
    convert_batch_pd_to_panim(filename, save_filename)


def data_format_test():
    output_path = r'E:\gits\vae_motion_modeling\test data'
    input_data = r'E:\gits\vae_motion_modeling\test data\test.npy'
    loaded_data = np.load(input_data)
    print(loaded_data.shape)
    save_data = loaded_data / 100.0
    np.save(os.path.join(output_path, 'rescaled.npy'), save_data)


if __name__ == "__main__":
    demo()
    # data_format_test()