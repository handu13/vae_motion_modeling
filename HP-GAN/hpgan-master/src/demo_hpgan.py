import argparse
import numpy as np
import os
from braniac import nn as nn
from braniac.format import SourceFactory
from braniac.utils import DataPreprocessing, NormalizationMode
from braniac.viz import Skeleton2D
from braniac.readers.body import SequenceBodyReader
from braniac.models.body import RNNDiscriminator, NNDiscriminator, SequenceToSequenceGenerator
from test_hpgan import run_hpgan
from train_hpgan import main
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()


def demo_hpgan():
    '''

    Returns:

    '''
    parser = argparse.ArgumentParser()

    parser.add_argument("-test",
                        "--test_file",
                        type=str,
                        default=r'D:\workspace\my_git_repos\vae_motion_modeling\HP-GAN\hpgan-master\dataset/test_map.csv',
                        help="Provide the path of your test file.",
                        required=False)

    parser.add_argument("-model",
                        "--model_file",
                        type=str,
                        default=r'D:\workspace\my_git_repos\vae_motion_modeling\data\HPGAN_results\models\models-295',
                        help="Provide the path of the model checkpoint file",
                        required=False)

    parser.add_argument("-dnf",
                        "--data_normalization_file",
                        default=r'D:\workspace\my_git_repos\vae_motion_modeling\HP-GAN\hpgan-master\dataset/data_statistics.h5',
                        type=str,
                        help="Provide the path to your data normalization file. Which should contain mean and standard deviation of the input dataset.")

    parser.add_argument("-out",
                        "--output_folder",
                        type=str,
                        default=r'../../../data/HPGAN_results',
                        help="Provide the path of your output folder.",
                        required=False)

    parser.add_argument("-dataset",
                        "--dataset_name",
                        type=str,
                        help="Provide the name of the dataset (nturgbd or human36m).",
                        default="human36m")

    parser.add_argument("-ccf",
                        "--camera_calibration_file",
                        type=str,
                        default=r'D:\workspace\my_git_repos\vae_motion_modeling\HP-GAN\hpgan-master\dataset\cameras.h5',
                        help="Provide the path to your camera calibration file.")
    args = parser.parse_args()
    run_hpgan(args)



def train_hpgan():
    parser = argparse.ArgumentParser()
    parser.add_argument("-train",
                        '--train_file',
                        type=str,
                        default=r'D:\workspace\my_git_repos\vae_motion_modeling\HP-GAN\hpgan-master\dataset/train_map.csv',
                        help="Provide the path of your train file.",
                        required=False)

    parser.add_argument("-test",
                        "--test_file",
                        type=str,
                        default=r'D:\workspace\my_git_repos\vae_motion_modeling\HP-GAN\hpgan-master\dataset/test_map.csv',
                        help="Provide the path of your test file.",
                        required=False)

    parser.add_argument("-ccf",
                        "--camera_calibration_file",
                        type=str,
                        default=r'D:\workspace\my_git_repos\vae_motion_modeling\HP-GAN\hpgan-master\dataset\cameras.h5',
                        help="Provide the path to your camera calibration file.")

    parser.add_argument("-dnf",
                        "--data_normalization_file",
                        default=r'D:\workspace\my_git_repos\vae_motion_modeling\HP-GAN\hpgan-master\dataset/data_statistics.h5',
                        type=str,
                        help="Provide the path to your data normalization file. Which should contain mean and standard deviation of the input dataset.")

    parser.add_argument("-out",
                        "--output_folder",
                        type=str,
                        default=r'../../../data/HPGAN_results',
                        help="Provide the path of your output folder.",
                        required=False)

    parser.add_argument("-dataset",
                        "--dataset_name",
                        type=str,
                        help="Provide the name of the dataset (nturgbd or human36m).",
                        default="human36m")

    parser.add_argument("-epochs",
                        "--max_epochs",
                        type=int,
                        help="Maximum number of epochs (default 300).",
                        default=300)

    parser.add_argument("-clip",
                        "--record_clip",
                        default=False)

    args = parser.parse_args()
    main(args)


if __name__ == "__main__":
    demo_hpgan()
    # train_hpgan()
