import sys
import time
import os
import math
import csv
import argparse
import numpy as np
import logging
import random as rnd
import h5py
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'

import tensorflow as tf

from braniac import nn as nn
from braniac.format import SourceFactory
from braniac.utils import DataPreprocessing, NormalizationMode
from braniac.viz import Skeleton2D
from braniac.readers.body import SequenceBodyReader
from braniac.models.body import RNNDiscriminator, NNDiscriminator, SequenceToSequenceGenerator


def run_hpgan(args):
    '''
    Test GAN with test data.

    Args:
        args: arg parser object, contains all arguments provided by the user.
    '''
    base_folder = args.output_folder
    output_path = os.path.join(base_folder, R'test')
    output_test_folder = os.path.join(output_path, "")
    if not os.path.exists(output_test_folder):
        os.makedirs(output_test_folder)

    dataset = args.dataset_name

    source = SourceFactory(dataset, args.camera_calibration_file)
    sensor = source.create_sensor()
    body_inf = source.create_body()

    input_sequence_length = 10
    output_sequence_length = 20
    sequence_length = input_sequence_length + output_sequence_length
    z_size = 128
    inputs_depth = 128
    n_random_samples = 10
    output_folder = r'../../../data/HPGAN_results/results'
    logging.info("Loading data...")
    if args.data_normalization_file is not None:
        data_preprocessing = DataPreprocessing(args.data_normalization_file,
                                               normalization_mode=NormalizationMode.MeanAndStd2)

    if dataset == 'nturgbd':
        test_data_reader = SequenceBodyReader(args.test_file, 
                                               sequence_length,
                                               dataset,
                                               skip_frame=0,
                                               data_preprocessing=data_preprocessing,
                                               random_sequence=False)
    elif dataset == 'human36m':
        test_data_reader = SequenceBodyReader(args.test_file, 
                                               sequence_length,
                                               dataset,
                                               skip_frame=1,
                                               data_preprocessing=data_preprocessing,
                                               random_sequence=True)
    else:
        raise ValueError("Invalid dataset value.")

    g_inputs = tf.compat.v1.placeholder(dtype=tf.float32, shape=[None, input_sequence_length] + list(test_data_reader.element_shape), name="g_inputs") #[None, input_sequence_length, 32, 3]
    d_inputs = tf.compat.v1.placeholder(dtype=tf.float32, shape=[None, sequence_length] + list(test_data_reader.element_shape), name="d_inputs") #[None, sequence_length, 32,3]
    g_z = tf.compat.v1.placeholder(dtype=tf.float32, shape=[None, z_size], name="g_z")
    # Defining the model.
    d_real = NNDiscriminator(d_inputs, inputs_depth, sequence_length)
    
    g = SequenceToSequenceGenerator(g_inputs,
                                    inputs_depth,
                                    g_z, 
                                    input_sequence_length, 
                                    output_sequence_length,
                                    reverse_input=False)
    
    d_fake_inputs = tf.concat([g_inputs, g.output], axis=1)
    d_fake = NNDiscriminator(d_fake_inputs, inputs_depth, sequence_length, reuse=True)
    d_real_prob = NNDiscriminator(d_inputs, inputs_depth, sequence_length, scope="prob")
    d_fake_prob = NNDiscriminator(d_fake_inputs, inputs_depth, sequence_length, reuse=True, scope="prob")

    batch_size = 1

    z_rand_type = 'uniform'
    z_rand_params = {'low':-0.1, 'high':0.1, 'mean':0.0, 'std':0.2}
    z_data_p = []
    for _ in range(n_random_samples):
        z_data_p.append(nn.generate_random(z_rand_type, z_rand_params, shape=[batch_size, z_size]))

    g_prev = g_inputs[:, input_sequence_length-1:input_sequence_length, :, :]
    if output_sequence_length > 1:
        g_prev = tf.concat([g_prev, g.output[:, 0:output_sequence_length-1, :, :]], axis=1)
    g_next = g.output

    g_consistency_loss = tf.maximum(0.0001, tf.norm(tensor=g_next-g_prev, ord=2) / (batch_size*output_sequence_length))

    g_bone_loss = (nn.bone_loss(g_prev, g_next, body_inf) / (batch_size*output_sequence_length))


    # Gradient penalty
    def gradient_penalty():
        alpha = tf.random.uniform([], 0.0, 1.0)
        d_inputs_hat = alpha * d_inputs + (1 - alpha) * d_fake_inputs
        d_outputs_hat = NNDiscriminator(d_inputs_hat, inputs_depth, sequence_length, reuse=True).output
        gradients = tf.gradients(ys=d_outputs_hat, xs=d_inputs_hat)[0]
        gradients_l2 = tf.sqrt(tf.reduce_sum(input_tensor=tf.square(gradients), axis=[2,3]))
        return tf.reduce_mean(input_tensor=tf.square(gradients_l2 - 1.))

    gradient_penalty_loss = 10.0 * gradient_penalty()

    # Discriminator and generative loss function.
    d_loss = tf.reduce_mean(input_tensor=d_fake.output - d_real.output) + gradient_penalty_loss
    g_gan_loss = -tf.reduce_mean(input_tensor=d_fake.output)

    d_loss_prob = -tf.reduce_mean(input_tensor=tf.math.log(d_real_prob.prob) + tf.math.log(1. - d_fake_prob.prob))

    d_loss += 0.001 * tf.add_n([tf.nn.l2_loss(p) for p in d_real.weights])

    g_loss = g_gan_loss + 0.001*g_consistency_loss + 0.01*g_bone_loss

    d_loss_prob += 0.001 * tf.add_n([tf.nn.l2_loss(p) for p in d_real_prob.weights])

    saver = tf.compat.v1.train.Saver()

    with tf.compat.v1.Session() as sess:
        generate_skeletons = nn.generate_skeletons_with_prob(sess, g, d_real_prob, d_fake_prob, data_preprocessing, 
                                                             d_inputs, g_inputs, g_z)
        # test_data_reader.reset()
        # d_test_loss = 0.
        # g_test_loss = 0.
        # d_test_loss_prob = 0.
        # d_is_sequence_probs = []

        # k = 0

        saver.restore(sess, args.model_file)
        input_data, _, current_batch_size, activities, subjects = test_data_reader.next_minibatch(batch_size)

        input_past_data = input_data[:, 0:input_sequence_length, :, :]
        skeleton_data, d_is_sequence_probs = generate_skeletons(input_data, input_past_data, z_data_p)
        subject_id = subjects[0]
        skeleton2D = Skeleton2D(sensor, body_inf)
        skeleton2D.draw_to_file(skeleton_data, subject_id, image_path=os.path.join(output_folder, "prediction.png"))
        # while test_data_reader.has_more():
        #     input_data, _, current_batch_size, activities, subjects = test_data_reader.next_minibatch(batch_size)
        #     input_past_data = input_data[:, 0:input_sequence_length, :, :]

            
        #     skeleton_data, d_is_sequence_probs = generate_skeletons(input_data, input_past_data, z_data_p)
        #     np.save(os.path.join(output_test_folder,"pred_{}_skeleton.npy".format(k)),np.array(skeleton_data))

        #     z_data = nn.generate_random(z_rand_type, z_rand_params, shape=[batch_size, z_size])

        #     d_loss_val = sess.run(d_loss, feed_dict={d_inputs:input_data, g_inputs:input_past_data, g_z:z_data})  
        #     d_loss_val_prob = sess.run(d_loss_prob, feed_dict={d_inputs:input_data, g_inputs:input_past_data, g_z:z_data})
        #     g_loss_val = sess.run(g_loss, feed_dict={g_inputs:input_past_data, d_inputs:input_data, g_z:z_data})

        #     d_test_loss += d_loss_val
        #     g_test_loss += g_loss_val
        #     d_test_loss_prob += d_loss_val_prob
        #     k += 1

        # print("d_test_loss:", d_test_loss/(k*batch_size))
        # print("g_test_loss:", g_test_loss/(k*batch_size))
        # print("d_test_loss_prob:", d_test_loss_prob/(k*batch_size))




if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-test",
                        "--test_file",
                        type=str,
                        help="Provide the path of your test file.",
                        required=True)
    
    parser.add_argument("-model",
                        "--model_file",
                        type=str,
                        help="Provide the path of the model checkpoint file",
                        required=True)

    parser.add_argument("-dnf",
                        "--data_normalization_file",
                        type=str,
                        help="Provide the path to your data normalization file. Which should contain mean and standard deviation of the input dataset.")

    parser.add_argument("-out",
                        "--output_folder",
                        type=str,
                        help="Provide the path of your output folder.",
                        required=True)

    parser.add_argument("-dataset",
                        "--dataset_name",
                        type=str,
                        help="Provide the name of the dataset (nturgbd or human36m).",
                        default="human36m")   

    args = parser.parse_args()
    run_hpgan(args)