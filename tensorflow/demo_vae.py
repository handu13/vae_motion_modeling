from deepMotionSim.models.conv_autoencoder import ConvAutoEncoder
from deepMotionSim.models.vae_encoder import MotionVaeEncoder
import tensorflow as tf
import numpy as np
from preprocessing import process_bvhfile
from deepMotionSim.utils.utilities import export_point_cloud_data_without_foot_contact, \
    retarget_panim_data_to_game_engine_skeleton, get_joint_position_data_from_anim, get_encoded_data
from morphablegraphs.python_src.morphablegraphs.animation_data import BVHReader, SkeletonBuilder
import os


def demo_vae():
    # encoded_motion_data = np.load(r'E:\tmp\training_data\walk\leftStance_game_engine_skeleton_smoothed_grounded\encoded_motion.npz')['encoded_motion']
    elementary_action = 'walk'
    motion_primitive = 'rightStance_game_engine_skeleton_smoothed_grounded'
    # encoded_motion_data = get_encoded_data(elementary_action, motion_primitive)
    # print(encoded_motion_data.shape)
    # min_value = np.min(encoded_motion_data)
    # max_value = np.max(encoded_motion_data)
    # print('min value: ', min_value)
    # print('max_value: ', max_value)
    sess = tf.InteractiveSession()
    # n_samples, n_dims, n_frames = encoded_motion_data.shape
    # reshaped_motion_data = np.reshape(encoded_motion_data, (n_samples, n_dims * n_frames))
    n_epoches = 250
    batchsize = 10
    learning_rate = 0.001
    npc = 32
    n_dims = 128
    n_frames = 30
    # print(n_frames)
    # print(n_dims)
    input_dims = n_dims * n_frames
    model_name = '_'.join([elementary_action, motion_primitive, 'vae'])
    # model_name = r'E:\workspace\tensorflow_results\tmp\walk_beginRightStance_game_engine_skeleton_smoothed_grounded_vae_0.001_500_10_32.ckpt'
    c_m = MotionVaeEncoder(npc=npc, input_dims=input_dims, name=model_name, encoder_activation=tf.nn.tanh,
                           decoder_activation=tf.nn.tanh, n_random_samples=1, sess=sess)
    # model_file = r'data/vae_small_kl_' + str(learning_rate) + '_' + str(n_epoches) + '_' + str(batchsize) + '_' + str(npc) + '.ckpt'
    model_filename = '_'.join([elementary_action, motion_primitive, 'vae', str(learning_rate), str(n_epoches),
                               str(batchsize), str(npc)]) + '.ckpt'
    # model_filename = r'E:\workspace\tensorflow_results\tmp\walk_beginLeftStance_game_engine_skeleton_smoothed_grounded_vae_0.001_500_10_32.ckpt'
    save_path = r'../data/training_data/cvae_models'
    model_file = os.path.join(save_path, model_filename)
    c_m.create_model()
    # c_m.train(reshaped_motion_data, learning_rate=learning_rate)
    # c_m.save_model(model_file)
    c_m.load_model(model_file)
    # reconstructed_data = c_m(reshaped_motion_data)
    # reconstructed_data = np.reshape(reconstructed_data, (n_samples, n_dims, n_frames))
    # print('reconstruction error is: ', np.mean((reconstructed_data - encoded_motion_data)**2))


    #### evaluate encoded mean and variance
    # mean_value = c_m.get_mean(reshaped_motion_data)
    # print(mean_value)
    #
    # variance_value = c_m.get_variance(reshaped_motion_data)
    # print(variance_value)
    skeleton_file = r'../data/example_skeletons/game_engine_target.bvh'
    bvhreader = BVHReader(skeleton_file)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader)

    n_new_samples = 100
    new_samples = c_m.generate_new_samples(n_new_samples)

    new_samples = np.reshape(new_samples, (n_new_samples, n_dims, n_frames))
    #### load variational autoencoder model
    n_dims = 66
    n_frames = 60
    kernel_size = 9
    encode_activation = tf.nn.relu
    decode_activation = None
    hidden_units = 128
    batchsize = 10
    meta_data = np.load(r'../data/training_data/conv_autoencoder_models/preprocess_meta_locomotion_small_window.npz')
    Xmean = meta_data['Xmean']
    Xstd = meta_data['Xstd']
    # model_filename = r'E:\tensorflow\tags\tag_models\conv_autoencoder_small_window_ulm_style_1e-05_250_10_128.ckpt'
    model_filename = r'../data/training_data/conv_autoencoder_models/conv_autoencoder_small_window_1e-05_250_10_128.ckpt'
    m = ConvAutoEncoder(name='motion_autoencoder', n_frames=n_frames, n_dims=n_dims, kernel_size=kernel_size,
                        encode_activation=encode_activation, decode_activation=decode_activation,
                        hidden_units=hidden_units, pooling='average', batch_norm=False, sess=sess)
    m.create_model(batchsize)
    m.load_model(model_filename)

    # #### export origin motions
    # reconstructed_motion = []
    # for i in range(len(reconstructed_data)):
    #     reconstructed_motion.append(m.decode_data(reconstructed_data[i][np.newaxis, :, :])[0])
    #
    # reconstructed_motion = np.swapaxes(reconstructed_motion, 1, 2)
    # reconstructed_motion = (reconstructed_motion * Xstd) + Xmean
    #
    # save_folder = r'E:\tmp\autoencoder_reconstruction'
    # save_folder = os.path.join(save_folder, elementary_action, motion_primitive)
    # for i in range(len(reconstructed_motion)):
    #     export_motion = np.reshape(reconstructed_motion[i], (n_frames, n_joints, 3))
    #     export_joint_position_to_cloud_data(export_motion, os.path.join(save_folder, filename_list[i][:-4]+'.panim'))

    new_motions = []
    for i in range(n_new_samples):
        new_motions.append(m.decode_data(new_samples[i][np.newaxis, :, :])[0])
    new_motions = np.asarray(new_motions)
    new_motions = np.swapaxes(new_motions, 1, 2)

    new_motions = (new_motions * Xstd) + Xmean
    print(new_motions.shape)
    save_path = r'../data/generated_motion'
    save_path = os.path.join(save_path, elementary_action, motion_primitive)
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    for i in range(n_new_samples):

        filename = 'new_sample_' + str(i) + '.panim'
        export_point_cloud_data_without_foot_contact(new_motions[i], os.path.join(save_path, filename))
        joint_pos = get_joint_position_data_from_anim(new_motions[i])
        motion_vector = retarget_panim_data_to_game_engine_skeleton(joint_pos, skeleton_file)
        motion_vector.export(skeleton, os.path.join(save_path, filename.replace('.panim', '.bvh')))


if __name__ == "__main__":
    demo_vae()