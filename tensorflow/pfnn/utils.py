import numpy as np



def vector_lerp(vec1, vec2, scale):
    '''
    perform linear intepolation between two vector
    :param vec1: numpy.array
    :param vec2: numpy.array
    :param scale: float
    :return:
    '''
    return (1 - scale) * vec1 + scale * vec2


def getRelativePositionTo(point, to):
    '''
    convert point to relative point to target point
    :param to: referenece point
    :return:
    '''
    return point - to


def getRelativeDirectionTo(direction, to):
    '''
    convert direction to relative direction
    :param direction:
    :param to:
    :return:
    '''
    return direction - to