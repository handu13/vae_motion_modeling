from deepMotionSim.models.vae_encoder import MotionVaeEncoder
from deepMotionSim.utils.utilities import get_encoded_data
import numpy as np
import os
import tensorflow as tf


def train_vae():
    elementary_actions = ['walk']
    motion_primitives = [
        'beginLeftStance_game_engine_skeleton_smoothed_grounded',
        'beginRightStance_game_engine_skeleton_smoothed_grounded',
        'leftStance_game_engine_skeleton_smoothed_grounded',
        'rightStance_game_engine_skeleton_smoothed_grounded',
        'endLeftStance_game_engine_skeleton_smoothed_grounded',
        'endRightStance_game_engine_skeleton_smoothed_grounded'
    ]
    for action in elementary_actions:
        for mm in motion_primitives:
            encoded_motion_data = get_encoded_data(action, mm)
            n_samples, n_dims, n_frames = encoded_motion_data.shape
            reshaped_motion_data = np.reshape(encoded_motion_data, (n_samples, n_dims * n_frames))
            n_epoches = 250
            batchsize = 10
            learning_rate = 0.001
            npc = 32
            model_filename = '_'.join([action, mm, 'vae', str(learning_rate), str(n_epoches),
                                       str(batchsize), str(npc)]) + '.ckpt'
            input_dims = n_dims * n_frames
            model_name = '_'.join([action, mm, 'vae'])
            c_m = MotionVaeEncoder(npc=npc, input_dims=input_dims, name=model_name, encoder_activation=tf.nn.tanh,
                                   decoder_activation=tf.nn.tanh, n_random_samples=1)
            save_path = r'E:\workspace\tensorflow_results\tmp'
            model_file = os.path.join(save_path, model_filename)
            c_m.create_model()
            c_m.train(reshaped_motion_data, learning_rate=learning_rate, n_epoches=n_epoches, batchsize=batchsize)
            c_m.save_model(model_file)
            c_m.sess.close()


def train_running_vae():
    pass

if __name__ == "__main__":
    train_vae()