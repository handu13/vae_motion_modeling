import tensorflow as tf
import numpy as np
import os
from pathlib import Path
import sys 
os.chdir(os.path.dirname(os.path.abspath(__file__)))

from preprocessing import process_bvhfile, process_file
from deepMotionSim.models.test_models import motion_decoder_channel_first, motion_encoder_channel_first, \
    motion_encoder_without_pooling, motion_decoder_without_pooling
from deepMotionSim.utils import export_point_cloud_data_without_foot_contact


def demo_motion_encoder_single_file():
    # preprocess = np.load(r'E:\workspace\tensorflow_results\data\preprocessed_core_channel_first.npz')
    preprocess = np.load(r'E:\workspace\tensorflow_results\tags\tag_data\preprocess_meta_locomotion_small_window.npz')
    Xmean = np.swapaxes(preprocess['Xmean'], 1, 2)
    Xstd = np.swapaxes(preprocess['Xstd'], 1, 2)
    # test_file = r'C:\Users\hadu01\Downloads\fix-screws-by-hand\fix-screws-by-hand_007-snapPoseSkeleton.bvh'
    # test_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\ACCAD\Female1_bvh\Female1_A08_CrouchToLie.bvh'
    test_file = r'E:\workspace\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\edin\edin_locomotion\locomotion_jog_000_000.bvh'
    # test_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\edin\edin_locomotion\locomotion_walk_001_000.bvh'
    # test_file = r'C:\repo\data\1 - MoCap\4 - Alignment\elementary_action_walk\turnRightLeftStance_game_engine_skeleton_smoothed_grounded\walk_turnwalk_013_4_turnRight_204_347.bvh'
    # test_file = r'E:\tmp\style_transfer_motion.panim'
    filename = os.path.split(test_file)[-1]
    # test_data = process_file(test_file, sliding_window=False, with_game_engine=False, body_plane_indice=[2, 17, 13])
    # test_data = process_file(test_file, sliding_window=False)
    test_data = process_bvhfile(test_file, sliding_window=False, body_plane_indices=[2, 17, 13])
    # test_data = process_panim_data(test_file, sliding_window=False)
    print(test_data.shape)

    test_data = np.swapaxes(test_data, 0, 1)[np.newaxis, :, :]

    if test_data.shape[2] % 2 != 0:
        test_data = test_data[:, :, :-1]

    n_samples, n_dims, n_frames = test_data.shape
    normalized_data = (test_data - Xmean) / Xstd
    input = tf.placeholder(tf.float32, shape=[1, n_dims, n_frames])
    encoder_op = motion_encoder_channel_first(input, name='encoder', hidden_units=256, pooling='average')
    decoder_op = motion_decoder_channel_first(encoder_op, n_dims, name='decoder', unpool='average')
    # encoder_op = motion_encoder_channel_first(input, name='encoder', hidden_units=256, pooling='spectrum')
    # decoder_op = motion_decoder_channel_first(encoder_op, n_dims, name='decoder', unpool='spectrum')
    # print(encoder_op.shape)
    # print(decoder_op.shape)
    saver = tf.train.Saver()
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        # saver.restore(sess, 'data/core_network_64_hidden_0.0001_250.ckpt')
        # saver.restore(sess, 'data/core_network_spectrum_pooling_250_0.00001.ckpt')
        saver.restore(sess, r'E:\workspace\tensorflow_results\data\core_network_average_pooling_300.ckpt')
        encoded_motion = sess.run(encoder_op, feed_dict={input: normalized_data})
        print('encoded motion shape: ', encoded_motion.shape)
        reconstructed_motion = sess.run(decoder_op, feed_dict={input: normalized_data})
        # denoised_motion = sess.run(decoder_op, feed_dict={input: reconstructed_motion})
    reconstructed_motion = (reconstructed_motion * Xstd) + Xmean
        # export_point_cloud_data(reconstructed_motion, os.path.join(r'E:\tmp', 'denoised_motion.panim'))
        # denoised_motion = (denoised_motion * preprocess['Xstd']) + preprocess['Xmean']

    print(reconstructed_motion.shape)
        # export_point_cloud_data(np.swapaxes(reconstructed_motion[0], 0, 1), os.path.join(r'E:\workspace\tmp', filename[:-4]+'_average_pooling.panim'))
        # export_point_cloud_data(denoised_motion, os.path.join(r'E:\tmp', 'denoised_motion.panim'))


if __name__ == "__main__":
    demo_motion_encoder_single_file()