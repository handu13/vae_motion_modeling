# encoding: UTF-8
import tensorflow as tf
import numpy as np
from deepMotionSim.models.test_models import motion_encoder_channel_first, motion_decoder_channel_first, create_style_transfer_model, \
    spectrum_style_transfer, motion_encoder_multilayers1, motion_decoder_multilayers1, create_style_transfer_hidden_model
import sys
from deepMotionSim.utils import export_point_cloud_data, convert_bvh_to_latent_space, backproject_to_motion_space, gram_matrix
from datetime import datetime
from preprocessing import process_file
import os


def train_spectrum_style_transfer():
    accad_data = np.load(r'../theano/data/training_data/processed_accad_data.npz')['clips']
    cmu_data = np.load(r'../theano/data/training_data/processed_cmu_data.npz')['clips']
    stylized_data = np.load(r'../theano/data/training_data/processed_stylized_data.npz')['clips']
    ulm_data = np.load(r'../theano/data/training_data/processed_ulm_locomotion_data.npz')['clips']
    edin_data = np.load(r'../theano/data/training_data/processed_edin_data.npz')['clips']
    rng = np.random.RandomState(23456)
    # X = np.concatenate([accad_data, cmu_data, edin_data, ulm_data], axis=0)
    X = edin_data
    X = np.swapaxes(X, 1, 2)
    n_samples, n_dims, n_frames = X.shape

    autoencoder_file = r'data/core_network_channel_first.ckpt'
    preprocess_file = r'preprocessed_core_channel_first.npz'
    preprocess = np.load(preprocess_file)
    X = (X - preprocess['Xmean']) / preprocess['Xstd']
    I = np.arange(len(X))
    rng.shuffle(I)
    X = X[I]
    print(X.shape)
    style_motion_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\sexy\sexy_normalwalking_17.bvh'
    # training_data = X[:, :-7, :]
    style_amount = 0.0
    content_amount = 1.0
    s, c = style_amount / (style_amount + content_amount), content_amount / (style_amount + content_amount)
    style_H = convert_bvh_to_latent_space(style_motion_file, autoencoder_file, preprocess_file)


    input = tf.placeholder(dtype=tf.float32, shape=[1, n_dims, n_frames])
    encoder_op = motion_encoder_channel_first(input, name='encoder', reuse=True)
    decoder_op = motion_decoder_channel_first(encoder_op, n_dims, name='decoder', reuse=False)
    encoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='encoder')
    decoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='decoder')
    batchsize = 1
    n_epochs = 1
    learning_rate = 0.000001
    logs_path = r'C:\tmp'
    model_name = 'spectrum_style_transfer'
    style_transfer_op = spectrum_style_transfer(input, name=model_name, reuse=False)
    style_G = gram_matrix(style_H)
    style_transfer_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=model_name)
    content_loss_op = c * tf.reduce_mean(tf.pow(style_transfer_op - encoder_op, 2))
    style_loss_op = s * tf.reduce_sum(tf.pow(gram_matrix(style_transfer_op) - style_G, 2))
    loss_op = content_loss_op + style_loss_op    ## train loss_op
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train_op = optimizer.minimize(loss_op, var_list=[style_transfer_params])
    encoder_saver = tf.train.Saver(encoder_params + decoder_params)
    style_model_saver = tf.train.Saver(style_transfer_params)
    model_file = 'data/spectrum_style_tranfer_network.ckpt'
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        summary_writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())
        sess.run(tf.global_variables_initializer())
        last_mean = 0
        encoder_saver.restore(sess, autoencoder_file)

        for epoch in range(n_epochs):
            batchinds = np.arange(n_samples // batchsize)
            rng.shuffle(batchinds)
            c = []
            for idx, batch_idx in enumerate(batchinds):

                sess.run(train_op, feed_dict={input: X[batch_idx*batchsize: (batch_idx+1)*batchsize]})
                c.append(sess.run(loss_op, feed_dict={input: X[batch_idx*batchsize: (batch_idx+1)*batchsize]}))
                if np.isnan(c[-1]): return
                if idx % (int(len(batchinds) / 1000) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(idx)/len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
            curr_mean = np.mean(c)
            diff_mean, last_mean = curr_mean-last_mean, curr_mean
            print('\r[Epoch %i] 100.0%% mean %.5f diff %.5f %s' %
                (epoch, curr_mean, diff_mean, str(datetime.now())[11:19]))
            # sys.stdout.flush()
        if model_file is not None:
            save_path = style_model_saver.save(sess, model_file)
            print("Model saved in file: %s" % save_path)


def fft_hidden_layer(hidden_layer, N=256):
    ### padding and fft
    input_shape = hidden_layer.get_shape().as_list()
    N = tf.convert_to_tensor(N)
    zero_padding = tf.zeros((input_shape[0], input_shape[1], N - input_shape[2]))
    extended_hidden_layer = tf.concat([hidden_layer, zero_padding], axis=-1)
    return tf.fft(tf.complex(extended_hidden_layer, extended_hidden_layer * 0.0))


def run_train_style_transfer(fine_tuning=False):
    accad_data = np.load(r'../theano/data/training_data/processed_accad_data.npz')['clips']
    cmu_data = np.load(r'../theano/data/training_data/processed_cmu_data.npz')['clips']
    stylized_data = np.load(r'../theano/data/training_data/processed_stylized_data.npz')['clips']
    ulm_data = np.load(r'../theano/data/training_data/processed_ulm_locomotion_data.npz')['clips']
    edin_data = np.load(r'../theano/data/training_data/processed_edin_data.npz')['clips']
    rng = np.random.RandomState(23456)
    # X = np.concatenate([stylized_data, edin_data], axis=0)
    X = edin_data
    X = np.swapaxes(X, 1, 2)
    n_samples, n_dims, n_frames = X.shape

    autoencoder_file = r'data/core_network_average_pooling_500.ckpt'
    # autoencoder_file = r'data/core_network_average_pooling_300.ckpt'
    preprocess_file = r'preprocessed_core_channel_first.npz'
    preprocess = np.load(preprocess_file)
    X = (X - preprocess['Xmean']) / preprocess['Xstd']
    I = np.arange(len(X))
    rng.shuffle(I)
    X = X[I]
    print(X.shape)

    s = 1000
    c = 1
    ### process style motion data
    # style_motion_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\childlike\childlike_normalwalking_5.bvh'
    style_motion_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\sexy\sexy_normalwalking_16.bvh'
    style_motion_data = process_file(style_motion_file, sliding_window=False)
    style_motion_data = np.swapaxes(style_motion_data, 0, 1)[np.newaxis, :, :]
    if style_motion_data.shape[2]%2 != 0:
        style_motion_data = style_motion_data[:, :, :-1]
    normalized_style_data = (style_motion_data - preprocess['Xmean']) / preprocess['Xstd']
    style_input = tf.placeholder(dtype=tf.float32, shape=normalized_style_data.shape)

    style_H = motion_encoder_channel_first(style_input, name='encoder', hidden_units=256, pooling='average')
    # style_H = motion_encoder_multilayers1(style_input, name='encoder')

    # fft_style_H = fft_hidden_layer(style_H)
    style_G = gram_matrix(style_H)
    # style_G = gram_matrix(fft_style_H)
    input = tf.placeholder(dtype=tf.float32, shape=[1, n_dims, n_frames])

    encoder_op = motion_encoder_channel_first(input, name='encoder', reuse=True, pooling='average')
    decoder_op = motion_decoder_channel_first(encoder_op, n_dims, name='decoder')
    # encoder_op = motion_encoder_multilayers1(input, name='encoder', reuse=True)
    # decoder_op = motion_decoder_multilayers1(encoder_op, n_dims, name='decoder')
    encoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='encoder')
    decoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='decoder')

    batchsize = 1
    n_epochs = 100
    learning_rate = 0.00001
    # model_name = 'style_transfer_model'
    # style_transfer_op = create_style_transfer_model(input, n_dims, name=model_name)  ## the style transfer operator take a motion input to
    #                                                         # produce a hidden representation
    model_name = 'style_transfer_hidden_model'
    stylized_encoder_op = create_style_transfer_hidden_model(input, name=model_name)
    # stylized_encoder_op = motion_encoder_channel_first(style_transfer_op, name='encoder', reuse=True, pooling='average')
    # stylized_encoder_op = motion_encoder_multilayers1(style_transfer_op, name='encoder', reuse=True)

    style_transfer_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=model_name)

    content_loss_op = c * tf.reduce_mean(tf.pow(stylized_encoder_op - encoder_op, 2))
    # fft_stylized_encoder_op = fft_hidden_layer(stylized_encoder_op)
    # style_loss_op = s * tf.abs(tf.reduce_mean(tf.pow(gram_matrix(fft_stylized_encoder_op) - style_G, 2)))
    style_loss_op = s * tf.reduce_mean(tf.pow(gram_matrix(stylized_encoder_op) - style_G, 2))
    loss_op = content_loss_op + style_loss_op    ## train loss_op
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train_op = optimizer.minimize(loss_op, var_list=[style_transfer_params])
    save_dir = 'data'


    encoder_saver = tf.train.Saver(encoder_params + decoder_params)
    style_model_saver = tf.train.Saver(style_transfer_params)
    if fine_tuning:
        style_model_origin_saver = tf.train.Saver(style_transfer_params)
        origin_style_transfer_model_file = r'data/style_transfer_hidden_0.0001_250.ckpt'
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    #
    # print('train style transfer models with average over gram matrix, content weight 0: ')

    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        last_mean = 0
        encoder_saver.restore(sess, autoencoder_file)
        if fine_tuning:
            style_model_origin_saver.restore(sess, origin_style_transfer_model_file)
            model_file_name = '_'.join(['style_transfer_hidden_fine_tuning', str(learning_rate), str(n_epochs)]) + '.ckpt'
            model_file = os.path.join(save_dir, model_file_name)
        else:
            model_file_name = '_'.join(['style_transfer_hidden', str(learning_rate), str(n_epochs)]) + '.ckpt'
            model_file = os.path.join(save_dir, model_file_name)
        # style_loss = sess.run(style_loss_op, feed_dict={input: X[20:21], style_input: normalized_style_data})
        # print("style loss: ", style_loss)
        # content_loss = sess.run(content_loss_op, feed_dict={input: X[20:21]})
        # print(content_loss)

        for epoch in range(n_epochs):
            batchinds = np.arange(n_samples // batchsize)
            rng.shuffle(batchinds)
            c = []
            for idx, batch_idx in enumerate(batchinds):

                sess.run(train_op, feed_dict={input: X[batch_idx*batchsize: (batch_idx+1)*batchsize],
                                              style_input: normalized_style_data})
                c.append(sess.run(loss_op, feed_dict={input: X[batch_idx*batchsize: (batch_idx+1)*batchsize],
                                                      style_input: normalized_style_data}))
                if np.isnan(c[-1]): return
                if idx % (int(len(batchinds) / 1000) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(idx)/len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
            curr_mean = np.mean(c)
            diff_mean, last_mean = curr_mean-last_mean, curr_mean
            print('\r[Epoch %i] 100.0%% mean %.5f diff %.5f %s' %
                (epoch, curr_mean, diff_mean, str(datetime.now())[11:19]))
            # sys.stdout.flush()
        if model_file is not None:
            save_path = style_model_saver.save(sess, model_file)
            print("Model saved in file: %s" % save_path)


def test():
    style_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\angry\angry_normalwalking_0.bvh'
    model_file = r'data\core_network_channel_first'
    preprocess_file = 'preprocessed_core_channel_first.npz'
    H = convert_bvh_to_latent_space(style_file, model_file, preprocess_file)
    print(H.shape)


if __name__ == "__main__":
    # style_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\angry\angry_normalwalking_0.bvh'
    # model_file = r'data\core_network_channel_first'
    # preprocess_file = 'preprocessed_core_channel_first.npz'
    # H = convert_bvh_to_latent_space(style_file, model_file, preprocess_file)
    # print(H.shape)

    # motion_data = backproject_to_motion_space(H, model_file, preprocess_file, n_input=70)
    # # motion_data = np.swapaxes(motion_data, 1, 2)
    # print(motion_data.shape)
    # export_point_cloud_data(motion_data, r'E:\tmp\angry_normalwalking_0.panim')
    run_train_style_transfer(fine_tuning=True)
    # train_spectrum_style_transfer()