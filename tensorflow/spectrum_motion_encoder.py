# encoding: UTF-8
from deepMotionSim.models.test_models import spectrum_motion_encoder
import numpy as np
import tensorflow as tf
import sys
from datetime import datetime
from deepMotionSim.utils import export_point_cloud_data


def train_spectrum_motion_encoder():
    accad_data = np.load(r'../theano/data/training_data/processed_accad_data.npz')['clips']
    cmu_data = np.load(r'../theano/data/training_data/processed_cmu_data.npz')['clips']
    stylized_data = np.load(r'../theano/data/training_data/processed_stylized_data.npz')['clips']
    ulm_data = np.load(r'../theano/data/training_data/processed_ulm_locomotion_data.npz')['clips']
    edin_data = np.load(r'../theano/data/training_data/processed_edin_data.npz')['clips']
    rng = np.random.RandomState(23456)
    X = np.concatenate([accad_data, cmu_data, stylized_data, edin_data, ulm_data], axis=0)
    X = np.swapaxes(X, 1, 2)
    Xmean = X.mean(axis=2).mean(axis=0)[np.newaxis,:,np.newaxis]
    Xmean[:,-7:-4] = 0.0
    Xmean[:,-4:]   = 0.5
    Xstd = np.array([[[X.std()]]]).repeat(X.shape[1], axis=1)

    Xstd[:,-7:-5] = 0.9 * X[:,-7:-5].std()
    Xstd[:,-5:-4] = 0.9 * X[:,-5:-4].std()
    Xstd[:,-4:] = 0.5
    np.savez_compressed('data/preprocessed_core_channels_first.npz', Xmean=Xmean, Xstd=Xstd)
    X = (X - Xmean) / Xstd
    I = np.arange(len(X))
    rng.shuffle(I)
    X = X[I]
    batchsize = 1
    n_epochs = 250
    learning_rate = 0.0001

    n_samples, n_dims, n_frames = X.shape
    input = tf.placeholder(tf.float32, shape=[batchsize, n_dims, n_frames])
    output = spectrum_motion_encoder(input)
    print(output.shape)
    real_part = tf.real(output)
    print(real_part.shape)
    imag_part = tf.imag(output)
    print(imag_part.shape)
    loss_op = tf.reduce_mean(tf.pow(input - output, 2))
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train_op = optimizer.minimize(loss_op)
    init = tf.global_variables_initializer()
    saver = tf.train.Saver()
    model_file = 'data/core_network_spectrum'
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        res = sess.run(output, feed_dict={input: X[0: batchsize]})
    # print(res.shape)
    # print(res[0, 0, :])
        last_mean = 0
        for epoch in range(n_epochs):
            batchinds = np.arange(n_samples // batchsize)
            rng.shuffle(batchinds)
            c = []

            for bii, bi in enumerate(batchinds):
                sess.run(train_op, feed_dict={input: X[bi*batchsize: (bi+1)*batchsize]})
                c.append(sess.run(loss_op, feed_dict={input: X[bi*batchsize: (bi+1)*batchsize]}))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 1000) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii)/len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
            curr_mean = np.mean(c)
            diff_mean, last_mean = curr_mean-last_mean, curr_mean
            print('\r[Epoch %i] 100.0%% mean %.5f diff %.5f %s' %
                (epoch, curr_mean, diff_mean, str(datetime.now())[11:19]))
            # sys.stdout.flush()
        if model_file is not None:
            save_path = saver.save(sess, model_file)
            print("Model saved in file: %s" % save_path)


def test_spectrum_motion_encoder():
    data = np.load(r'../theano/data/training_data/processed_edin_data.npz')['clips']
    data = np.swapaxes(data, 1, 2)
    preprocess = np.load(r'preprocessed_core_channel_first.npz')

    normalized_data = (data - preprocess['Xmean']) / preprocess['Xstd']
    n_samples, n_dims, n_frames = data.shape
    input = tf.placeholder(dtype=tf.float32, shape=[None, n_dims, n_frames])
    fft_conv_op = spectrum_motion_encoder(input, n_dims)
    init = tf.global_variables_initializer()
    saver = tf.train.Saver()
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        sess.run(init)
        saver.restore(sess, 'data/core_network_spectrum')
        reconstructed_motion = sess.run(fft_conv_op, feed_dict={input: normalized_data[13:14]})
        reconstructed_motion = (reconstructed_motion * preprocess['Xstd']) + preprocess['Xmean']
        # print(reconstructed_motion.shape)
        export_point_cloud_data(reconstructed_motion, 'data/fft_generated_motion.panim')


if __name__ == "__main__":
    train_spectrum_motion_encoder()
    # test_spectrum_motion_encoder()