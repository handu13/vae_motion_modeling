import tensorflow as tf
from deepMotionSim.models.conv_autoencoder import ConvAutoEncoder
import numpy as np
from morphablegraphs.python_src.morphablegraphs.animation_data import BVHReader
from deepMotionSim.utils import export_point_cloud_data_without_foot_contact, retarget_panim_data_to_game_engine_skeleton, \
    get_joint_position_data_from_anim, combine_motion_clips, export_anim_to_joint_position, export_joint_position_to_cloud_data
from preprocessing import process_bvhfile


def demo_conv_autoencoder():
    ### parameter for general model
    n_dims = 66
    n_frames = 60
    kernel_size = 9
    encode_activation = tf.nn.relu
    decode_activation = None
    hidden_units = 128
    n_epoches = 250
    batchsize = 10
    # preprocessing_data = np.load(r'E:\workspace\tensorflow_results\data\training_data\customized_small_window\preprocess_meta_customized_ulm.npz')
    # Xmean = preprocessing_data['Xmean']
    # Xstd = preprocessing_data['Xstd']
    sess = tf.InteractiveSession()
    # model_filename = r'E:\workspace\tensorflow_results\tmp/conv_autoencoder_small_window_costomized_locomotion_' + str(learning_rate) + '_' + str(n_epoches) + '_' + str(
    #     batchsize) + '_' + str(hidden_units) + '.ckpt'
    # m = ConvAutoEncoder(name='locomotion_autoencoder', n_frames=n_frames, n_dims=n_dims, kernel_size=kernel_size,
    #                     encode_activation=encode_activation, decode_activation=decode_activation,
    #                     hidden_units=hidden_units, pooling='average', batch_norm=False, sess=sess)
    # m.create_model_joint_pos_loss(batchsize, Xmean, Xstd)
    meta_data = np.load(r'E:\workspace\tensorflow_results\tags\tag_data\preprocess_meta_locomotion_small_window.npz')
    Xmean = meta_data['Xmean']
    Xstd = meta_data['Xstd']
    model_filename = r'E:\workspace\tensorflow_results\tags\tag_models\conv_autoencoder_small_window_1e-05_250_10_128.ckpt'
    m = ConvAutoEncoder(name='motion_autoencoder', n_frames=n_frames, n_dims=n_dims, kernel_size=kernel_size,
                        encode_activation=encode_activation, decode_activation=decode_activation,
                        hidden_units=hidden_units, pooling='average', batch_norm=False, sess=sess)
    m.create_model(batchsize)
    m.load_model(model_filename)
    ### process input file
    test_file = r'E:\workspace\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\ulm_locomotion\Take_walk\walk_003_1.bvh'
    # test_data = np.asarray(process_file(test_file, sliding_window=True, with_game_engine=False, body_plane_indice=[2, 17, 13]))[:, :, :-4]
    bvhreader = BVHReader(test_file)
    n_frames = len(bvhreader.frames)
    test_data = process_bvhfile(test_file, window=60, window_step=30, body_plane_indices=[2, 17, 13], sliding_window=True)
    # test_data = np.swapaxes(test_data, 0, 1)[np.newaxis, :, :]
    print(test_data.shape)
    n_clips, window_size, n_dims = test_data.shape
    normalized_data = (test_data - Xmean)/Xstd
    normalized_data = np.swapaxes(normalized_data, 1, 2)
    reconstructed_clips = np.zeros(normalized_data.shape)
    # zeros = np.zeros(normalized_data.shape)
    # normalized_data = np.concatenate((normalized_data, zeros), axis=0)
    # res = m(normalized_data)
    # reconstructed_clips = res[:5]
    batch_idx = n_clips // batchsize
    # for i in range(batch_idx-1):
    #     reconstructed_clips[i*batchsize: (i+1)*batchsize] = m(normalized_data[i*batchsize: (i+1)*batchsize])
    for i in range(n_clips):
        reconstructed_clips[i: i+1] = m(normalized_data[i: i+1])
    reconstructed_clips = np.swapaxes(reconstructed_clips, 1, 2)
    reconstructed_clips = reconstructed_clips * Xstd + Xmean
    print(reconstructed_clips.shape)

    long_clip = combine_motion_clips(reconstructed_clips, motion_len=n_frames, window_step=30)
    print(long_clip.shape)
    joint_pos = export_anim_to_joint_position(long_clip)
    export_joint_position_to_cloud_data(joint_pos, r'E:\workspace\tensorflow_results\example\result.panim')



def test():
    datafile1 = np.load(r'E:\workspace\tensorflow_results\preprocess_meta_locomotion.npz')
    # encoded1 = np.load(r'E:\gits\vae_motion_modeling\data\training_data\locomotion\walk\beginLeftStance_game_engine_skeleton_smoothed_grounded\encoded_motion.npz')
    print(datafile1['Xmean'].shape)
    # print(encoded1['encoded_motion'].shape)
    #
    datafile2 = np.load(r'E:\workspace\tensorflow_results\tags\tag_data\preprocess_meta_ulm_style_small_window.npz')
    # encoded2 = np.load(r'E:\workspace\tmp\training_data\angryWalk\leftStance\encoded_motion.npz')
    print(datafile2['Xmean'].shape)
    # print(encoded2['encoded_motion'].shape)




if __name__ == "__main__":
    # demo_conv_autoencoder()
    test()