import tensorflow as tf
from pfnn.pfnn import PFNN
import numpy as np
import os
from deepMotionSim.utils import export_point_cloud_Edin_data, Edin_skeleton


def train_pfnn():
    training_data = np.load(r'../data\training_data\pfnn\vanilla\database.npz')
    X = training_data['Xun']
    Y = training_data['Yun']
    P = training_data['Pun']

    # load mean and standard derivation
    param_folder = r'../data\training_data\pfnn\vanilla'
    Xmean = np.fromfile(os.path.join(param_folder, 'Xmean.bin'), dtype=np.float32)
    Xstd = np.fromfile(os.path.join(param_folder, 'Xstd.bin'), dtype=np.float32)
    Ymean = np.fromfile(os.path.join(param_folder, 'Ymean.bin'), dtype=np.float32)
    Ystd = np.fromfile(os.path.join(param_folder, 'Ystd.bin'), dtype=np.float32)

    X = (X - Xmean) / Xstd
    Y = (Y - Ymean) / Ystd
    ### using CPU
    # os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    # os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    input_data = np.concatenate((X, P[...,np.newaxis]), axis=-1)
    print(input_data.shape)
    print(Y.shape)
    model = PFNN(4, 343, 311, 0.7, 32)
    model.create_model()
    model.train(input_data, Y, n_epoches=10)


def demo_pfnn():
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    param_folder = r'../data\training_data\pfnn\vanilla'
    training_data = os.path.join(param_folder, 'database.npz')
    model_params_database = np.load(r'../data/network_parameters\pfnn\vanilla\network.npz')

    training_data = np.load(training_data)
    X = training_data['Xun']
    P = training_data['Pun']

    Xmean = np.fromfile(os.path.join(param_folder, 'Xmean.bin'), dtype=np.float32)
    Xstd = np.fromfile(os.path.join(param_folder, 'Xstd.bin'), dtype=np.float32)

    Ymean = np.fromfile(os.path.join(param_folder, 'Ymean.bin'), dtype=np.float32)
    Ystd = np.fromfile(os.path.join(param_folder, 'Ystd.bin'), dtype=np.float32)

    X = (X - Xmean) / Xstd

    model = PFNN(4, 343, 311, 0.7, 32)
    model.create_model()
    model.load_params_from_theano(model_params_database)
    batchsize = 200

    input_data = np.concatenate([X[0:batchsize], P[0:batchsize][...,np.newaxis]], axis=-1)
    output = model(input_data)
    output = output * Ystd + Ymean
    print(output.shape)

    # local_position = output[:, 32:32 + 93]
    # print(local_position[0])
    export_filename = 'result.panim'
    visualize_output_data(output, export_filename)



def visualize_output_data(output_data, filename):
    '''

    :param output_data: numpy array, n_frames * 311

    :return:
    '''
    root_velocity = output_data[:, :2]
    root_rvelocity = output_data[:, 2:3]
    # input_joint_pos -> world space
    local_position = output_data[:, 32:32+93] # out joint pos
    local_velocity = output_data[:, 32+93:32+93*2]
    # out joint pos -> world space
    # out joint vel
    # ((input_joint_pos + joint vel) + out joint pos) / 2
    anim_data = np.concatenate([local_position, local_velocity, root_velocity, root_rvelocity], axis=-1)
    export_point_cloud_Edin_data(anim_data, filename, Edin_skeleton)


if __name__ == "__main__":
    # train_pfnn()
    demo_pfnn()