# encoding: UTF-8
import numpy as np
from morphablegraphs.python_src.morphablegraphs.utilities import write_to_json_file
from morphablegraphs.python_src.morphablegraphs.animation_data.quaternions import Quaternions
import collections
import tensorflow as tf
from deepMotionSim.models.test_models import motion_encoder_channel_first, motion_decoder_channel_first
from preprocessing import process_file
from morphablegraphs.python_src.morphablegraphs.utilities import get_aligned_data_folder
from morphablegraphs.python_src.morphablegraphs.animation_data import BVHReader, SkeletonBuilder, MotionVector
from morphablegraphs.python_src.morphablegraphs.animation_data.skeleton_models import SKELETON_MODELS
from morphablegraphs.python_src.morphablegraphs.animation_data.retargeting.analytical import generate_joint_map
from morphablegraphs.python_src.morphablegraphs.animation_data.retargeting.point_cloud_retargeting import \
    PointCloudRetargeting
import os
import glob
from morphablegraphs.python_src.morphablegraphs.animation_data.utils import convert_euler_frames_to_cartesian_frames, \
    get_rotation_angle, \
    rotate_cartesian_frames_to_ref_dir, shift_quat_frames_to_ground
import copy
from morphablegraphs.python_src.morphablegraphs.animation_data.quaternion import Quaternion
from morphablegraphs.python_src.morphablegraphs.motion_analysis.motion_plane import Plane
from morphablegraphs.python_src.morphablegraphs.external.transformations import euler_matrix
import matplotlib.pyplot as plt

GAME_ENGINE_SKELETON = collections.OrderedDict(
    [
        ('Root', {'parent': None, 'index': 0}),  # -1
        ('pelvis', {'parent': 'Root', 'index': 1}),  # 0
        ('spine_03', {'parent': 'pelvis', 'index': 2}),  # 1
        ('clavicle_l', {'parent': 'spine_03', 'index': 3}),  # 2
        ('upperarm_l', {'parent': 'clavicle_l', 'index': 4}),  # 3
        ('lowerarm_l', {'parent': 'upperarm_l', 'index': 5}),  # 4
        ('hand_l', {'parent': 'lowerarm_l', 'index': 6}),  # 5
        ('clavicle_r', {'parent': 'spine_03', 'index': 7}),  # 2
        ('upperarm_r', {'parent': 'clavicle_r', 'index': 8}),  # 7
        ('lowerarm_r', {'parent': 'upperarm_r', 'index': 9}),  # 8
        ('hand_r', {'parent': 'lowerarm_r', 'index': 10}),
        ('neck_01', {'parent': 'spine_03', 'index': 11}),
        ('head', {'parent': 'neck_01', 'index': 12}),
        ('thigh_l', {'parent': 'pelvis', 'index': 13}),
        ('calf_l', {'parent': 'thigh_l', 'index': 14}),
        ('foot_l', {'parent': 'calf_l', 'index': 15}),
        ('ball_l', {'parent': 'foot_l', 'index': 16}),
        ('thigh_r', {'parent': 'pelvis', 'index': 17}),
        ('calf_r', {'parent': 'thigh_r', 'index': 18}),
        ('foot_r', {'parent': 'calf_r', 'index': 19}),
        ('ball_r', {'parent': 'foot_r', 'index': 20})
    ]
)

Edin_skeleton = collections.OrderedDict(
    [
        ('Hips', {'parent': None, 'index': 0}),  # -1
        ('LHipJoint', {'parent': 'Hips', 'index': 1}),  # 0
        ('LeftUpLeg', {'parent': 'LHipJoint', 'index': 2}),  # 1
        ('LeftLeg', {'parent': 'LeftUpLeg', 'index': 3}),  # 2
        ('LeftFoot', {'parent': 'LeftLeg', 'index': 4}),  # 3
        ('LeftToeBase', {'parent': 'LeftFoot', 'index': 5}),  # 4
        ('RHipJoint', {'parent': 'Hips', 'index': 6}),  # 5
        ('RightUpLeg', {'parent': 'RHipJoint', 'index': 7}),  # 2
        ('RightLeg', {'parent': 'RightUpLeg', 'index': 8}),  # 7
        ('RightFoot', {'parent': 'RightLeg', 'index': 9}),  # 8
        ('RightToeBase', {'parent': 'RightFoot', 'index': 10}),
        ('LowerBack', {'parent': 'Hips', 'index': 11}),
        ('Spine', {'parent': 'LowerBack', 'index': 12}),
        ('Spine1', {'parent': 'Spine', 'index': 13}),
        ('Neck', {'parent': 'Spine1', 'index': 14}),
        ('Neck1', {'parent': 'Neck', 'index': 15}),
        ('Head', {'parent': 'Neck1', 'index': 16}),
        ('LeftShoulder', {'parent': 'Neck', 'index': 17}),
        ('LeftArm', {'parent': 'LeftShoulder', 'index': 18}),
        ('LeftForeArm', {'parent': 'LeftArm', 'index': 19}),
        ('LeftHand', {'parent': 'LeftForeArm', 'index': 20}),
        ('LeftFingerBase', {'parent': 'LeftHand', 'index': 21}),
        ('LeftHandIndex1', {'parent': 'LeftFingerBase', 'index': 22}),
        ('LThumb', {'parent': 'LeftHand', 'index': 23}),
        ('RightShoulder', {'parent': 'Neck', 'index': 24}),
        ('RightArm', {'parent': 'RightShoulder', 'index': 25}),
        ('RightForeArm', {'parent': 'RightArm', 'index': 26}),
        ('RightHand', {'parent': 'RightForeArm', 'index': 27}),
        ('RightFingerBase', {'parent': 'RightHand', 'index': 28}),
        ('RightHandIndex1', {'parent': 'RightFingerBase', 'index': 29}),
        ('RThumb', {'parent': 'RightHand', 'index': 30})
    ]
)

GAME_ENGINE_ANIMATED_JOINTS = ['Root', 'pelvis', 'spine_03', 'clavicle_l', 'upperarm_l', 'lowerarm_l',
                               'hand_l', 'clavicle_r',
                               'upperarm_r', 'lowerarm_r', 'hand_r', 'neck_01', 'head', 'thigh_l', 'calf_l', 'foot_l',
                               'ball_l', 'thigh_r', 'calf_r', 'foot_r', 'ball_r']


def export_point_cloud_data(anim_data, filename):
    '''
    expected animation data shape is 1*n_channels*n_frames
    :param anim_data:
    :param filename:
    :return:
    '''

    # anim = np.swapaxes(anim_data[0].copy(), 0, 1)

    anim = anim_data.copy()
    print(anim.shape)
    joints, root_x, root_z, root_r = anim[:, :-7], anim[:, -7], anim[:, -6], anim[:, -5]
    joints = joints.reshape((len(joints), -1, 3))  ### reshape 2D matrix to (n_frames, n_joints, n_dims)
    rotation = Quaternions.id(1)
    translation = np.array([[0, 0, 0]])
    ref_dir = np.array([0, 0, 1])
    for i in range(len(joints)):
        joints[i, :, :] = rotation * joints[i]
        joints[i, :, 0] = joints[i, :, 0] + translation[0, 0]
        joints[i, :, 2] = joints[i, :, 2] + translation[0, 2]
        rotation = Quaternions.from_angle_axis(-root_r[i], np.array([0, 1, 0])) * rotation
        # offsets.append(rotation * ref_dir)
        translation = translation + rotation * np.array([root_x[i], 0, root_z[i]])

    save_data = {'motion_data': joints.tolist(), 'has_skeleton': True, 'skeleton': GAME_ENGINE_SKELETON}
    write_to_json_file(filename, save_data)


def softmax(x, **kw):
    softness = kw.pop('softness', 1.0)
    maxi, mini = np.max(x, **kw), np.min(x, **kw)
    return maxi + np.log(softness + np.exp(mini - maxi))


def softmin(x, **kw):
    return -softmax(-x, **kw)


def rotate_cartesian_frame(cartesian_frame, q):
    '''
    rotate a cartesian frame by given quaternion q
    :param cartesian_frame: ndarray (n_joints * 3)
    :param q: Quaternion
    :return:
    '''
    new_cartesian_frame = np.zeros(cartesian_frame.shape)
    for i in range(len(cartesian_frame)):
        new_cartesian_frame[i] = q * cartesian_frame[i]
    return new_cartesian_frame


def get_rotation_to_ref_direction(dir_vecs, ref_dir):
    rotations = []
    for dir_vec in dir_vecs:
        rotations.append(Quaternion.between(dir_vec, ref_dir))
    return rotations


def convert_cartesian_frame_to_velocity_data_general(cartesian_frames, body_plane_index):
    '''
    A general case: no directional joint, do not remove joint
    :param cartesian_frames:
    :return:
    '''
    cartesian_frames = np.concatenate((cartesian_frames[0:1], cartesian_frames), axis=0)
    n_frames = len(cartesian_frames)
    forward = []
    for i in range(len(cartesian_frames)):
        forward.append(cartesian_pose_orientation(cartesian_frames[i], body_plane_index))
    forward = np.asarray(forward)
    ref_dir = np.array([0, 0, 1])
    rotations = get_rotation_to_ref_direction(forward, ref_dir=ref_dir)

    # cartesian_frames = cartesian_frames[:, 1:, :]  ## remove 'Game_engine' joint

    """ Put on Floor """
    fid_l, fid_r = np.array([15, 16]), np.array([19, 20])

    foot_heights = np.minimum(cartesian_frames[:, fid_l, 1], cartesian_frames[:, fid_r, 1]).min(axis=1)
    floor_height = softmin(foot_heights, softness=0.5, axis=0)
    # print(floor_height)
    cartesian_frames = cartesian_frames - floor_height

    """ Get Root Velocity """
    velocity = (cartesian_frames[1:, 0:1] - cartesian_frames[:-1, 0:1]).copy()
    """ Remove Translation """
    cartesian_frames[:, :, 0] = cartesian_frames[:, :, 0] - cartesian_frames[:, 0:1, 0]
    cartesian_frames[:, :, 2] = cartesian_frames[:, :, 2] - cartesian_frames[:, 0:1, 2]
    """ Remove Y Rotation """
    for i in range(n_frames):
        cartesian_frames[i] = rotate_cartesian_frame(cartesian_frames[i], rotations[i])
    # save_data = {'motion_data': cartesian_frames.tolist(), 'has_skeleton': True, 'skeleton': GAME_ENGINE_SKELETON}
    # write_to_json_file(r'E:\experiment data\tmp\remove_y_rotation.panim', save_data)

    """ Rotate Velocity """
    for i in range(n_frames - 1):
        # print(rotations[i+1])

        velocity[i, 0] = rotations[i + 1] * velocity[i, 0]
    """ Get Rotation Velocity """
    r_v = np.zeros(n_frames - 1)
    for i in range(n_frames - 1):
        q = rotations[i + 1] * (-rotations[i])
        r_v[i] = Quaternion.get_angle_from_quaternion(q, ref_dir)

    """ Add Velocity, RVelocity """
    cartesian_frames = cartesian_frames[:-1]
    cartesian_frames = cartesian_frames.reshape(len(cartesian_frames), -1)
    cartesian_frames = np.concatenate([cartesian_frames, velocity[:, :, 0]], axis=-1)
    cartesian_frames = np.concatenate([cartesian_frames, velocity[:, :, 2]], axis=-1)
    cartesian_frames = np.concatenate([cartesian_frames, r_v[:, np.newaxis]], axis=-1)
    return cartesian_frames


def rotate_cartesian_frame_to_ref_dir(cartesian_frames, ref_dir, body_plane_indices, up_axis):
    '''
    rotation is assumed only about vertical axis
    :param cartesian_frames: n_frames * n_joints * 3
    :param ref_dir: 3d array
    :param body_plane_indices: 3d array (indices of three joints)
    :param up_axis: 3d array
    :return:
    '''
    n_frames, n_joints, n_dims = cartesian_frames.shape
    forward_dir = cartesian_pose_orientation(cartesian_frames[0], body_plane_indices, up_axis)
    axis_indice = np.where(up_axis == 0)
    angle = np.deg2rad(get_rotation_angle(forward_dir[axis_indice], ref_dir[axis_indice]))
    rotmat = euler_matrix(0, -angle, 0, 'rxyz')
    ones = np.ones((n_frames, n_joints, 1))
    extended_cartesian_frames = np.concatenate((cartesian_frames, ones), axis=-1)
    extended_cartesian_frames = np.transpose(extended_cartesian_frames, (0, 2, 1))
    rotated_cartesian_frames = np.matmul(rotmat, extended_cartesian_frames)
    return np.transpose(rotated_cartesian_frames, (0, 2, 1))[:, :, :-1]


def cartesian_pose_orientation(cartesian_pose, body_plane_index, up_axis):
    points = cartesian_pose[body_plane_index, :]
    body_plane = Plane(points)
    normal_vec = body_plane.normal_vector
    normal_vec[np.where(up_axis == 1)] = 0  ### only consider forward direction on the ground
    return normal_vec / np.linalg.norm(normal_vec)


def rotation_cartesian_frames(cartesian_frames, angles):
    '''
    :param cartesian_frames: n_frames * n_joints * 3
    :param angles: n_frames * 1
    :return:
    '''
    n_frames, n_joints, n_dims = cartesian_frames.shape
    sin_theta = np.sin(angles)
    cos_theta = np.cos(angles)
    rotmat = np.array([cos_theta, np.zeros(n_frames), sin_theta, np.zeros(n_frames), np.zeros(n_frames),
                       np.ones(n_frames), np.zeros(n_frames), np.zeros(n_frames), -sin_theta, np.zeros(n_frames),
                       cos_theta, np.zeros(n_frames), np.zeros(n_frames), np.zeros(n_frames), np.zeros(n_frames),
                       np.ones(n_frames)]).T
    rotmat = np.reshape(rotmat, (n_frames, 4, 4))
    ones = np.ones((n_frames, n_joints, 1))
    extended_cartesian_frames = np.concatenate((cartesian_frames, ones), axis=-1)
    swapped_cartesian_frames = np.transpose(extended_cartesian_frames, (0, 2, 1))
    rotated_cartesian_frames = np.matmul(rotmat, swapped_cartesian_frames)
    return np.transpose(rotated_cartesian_frames, (0, 2, 1))[:, :, :-1]


def get_rotation_angles_for_vectors(dir_vecs, ref_dir, up_axis):
    '''

    :param dir_vecs: nd array N*3
    :param ref_dir: 3d array
    :param up_axis: 3d array
    :return:
    '''
    axis_indice = np.where(up_axis == 0)
    angles = []
    for i in range(len(dir_vecs)):
        angles.append(get_rotation_angle(dir_vecs[i][axis_indice], ref_dir[axis_indice]))
    return np.deg2rad(np.asarray(angles))


# def convert_cartesian_frames_to_anim_beta(cartesian_frames, body_plane_indices):
#     '''
#
#     :param cartesian_frames: n_frames * n_joint * 3
#     :return:
#
#     Steps:
#     1. rotate all frames so that the start frame heading to the reference direction
#     2. rotate each frame to face reference direction
#     3. translate each frame to starting position
#     4. append translation speed to motion data
#     5. append rotation speed to motion data
#     '''
#     ref_dir = np.array([0, 0, 1])
#     up_axis = np.array([0, 1, 0])
#     cartesian_frames = rotate_cartesian_frames_to_ref_dir(cartesian_frames, ref_dir, body_plane_indices, up_axis)
#     #### duplicate the first cartesian frame to make sure the output frame length is the same as input
#     cartesian_frames = np.concatenate((cartesian_frames[0:1], cartesian_frames), axis=0)
#     """ Compute forward direction for each frame """
#     forward = []
#     for i in range(len(cartesian_frames)):
#         forward.append(cartesian_pose_orientation(cartesian_frames[i], body_plane_indices, up_axis))
#     forward = np.asarray(forward)
#     rotation_angles = get_rotation_angles_for_vectors(forward, ref_dir, up_axis)
#     delta_angles = rotation_angles[1:] - rotation_angles[:-1]
#     """ Get Root Velocity """
#     velocity = (cartesian_frames[1:, 0:1] - cartesian_frames[:-1, 0:1]).copy()
#     """ Remove Translation """
#     cartesian_frames[:, :, 0] = cartesian_frames[:, :, 0] - cartesian_frames[:, 0:1, 0]
#     cartesian_frames[:, :, 2] = cartesian_frames[:, :, 2] - cartesian_frames[:, 0:1, 2]
#     """ Remove Y Rotation """
#     cartesian_frames = rotation_cartesian_frames(cartesian_frames, -rotation_angles)
#     """ Rotate speed vectory"""
#     n_frames = len(rotation_angles) - 1
#     print(velocity.shape)
#     velocity[:, :, 1] = 0
#     ones = np.ones((velocity.shape[0], 1, 1))
#     velocity = np.concatenate((velocity, ones), axis=-1)
#     angles = -rotation_angles[1:]
#     sin_theta = np.sin(angles)
#     cos_theta = np.cos(angles)
#     rotmat = np.array([cos_theta, np.zeros(n_frames), sin_theta, np.zeros(n_frames), np.zeros(n_frames),
#                        np.ones(n_frames), np.zeros(n_frames), np.zeros(n_frames), -sin_theta, np.zeros(n_frames),
#                        cos_theta, np.zeros(n_frames), np.zeros(n_frames), np.zeros(n_frames), np.zeros(n_frames),
#                        np.ones(n_frames)]).T
#     swapped_v_mat = np.transpose(velocity, (0, 2, 1))
#     rotmat = np.reshape(rotmat, (n_frames, 4, 4))
#     rotated_v = np.matmul(rotmat, swapped_v_mat)
#     rotated_v = np.transpose(rotated_v, (0, 2, 1))
#     """ Add Velocity, RVelocity """
#     cartesian_frames = cartesian_frames[:-1]
#     cartesian_frames = cartesian_frames.reshape(len(cartesian_frames), -1)
#     cartesian_frames = np.concatenate([cartesian_frames, rotated_v[:, :, 0]], axis=-1)
#     cartesian_frames = np.concatenate([cartesian_frames, rotated_v[:, :, 2]], axis=-1)
#     cartesian_frames = np.concatenate([cartesian_frames, delta_angles[:, np.newaxis]], axis=-1)
#     return cartesian_frames


def convert_cartesian_frames_to_volecoty_data(cartesian_frames):
    '''

    :param cartesian_frames: n_frames * n_joints * n_dims
    :return:
    '''
    print('warning: only works for game engine skeleton with Game_engine joint')
    cartesian_frames = np.concatenate((cartesian_frames[0:1], cartesian_frames), axis=0)
    n_frames = len(cartesian_frames)
    forward = cartesian_frames[:, 0, :] - cartesian_frames[:, 1, :]  ###this is special for game engine skeleton
    forward[:, 1] = 0.0  #### set the vertical direction to 0
    forward = forward / np.linalg.norm(forward, axis=-1)[:, np.newaxis]
    ref_dir = np.array([0, 0, 1])
    cartesian_frames = cartesian_frames[:, 1:, :]  ## remove 'Game_engine' joint
    rotations = get_rotation_to_ref_direction(forward, ref_dir=ref_dir)
    # cartesian_frames = cartesian_frames[:, 1:, :]  ## remove 'Game_engine' joint

    """ Put on Floor """
    fid_l, fid_r = np.array([15, 16]), np.array([19, 20])

    foot_heights = np.minimum(cartesian_frames[:, fid_l, 1], cartesian_frames[:, fid_r, 1]).min(axis=1)
    floor_height = softmin(foot_heights, softness=0.5, axis=0)
    # print(floor_height)
    cartesian_frames = cartesian_frames - floor_height

    """ Get Root Velocity """
    velocity = (cartesian_frames[1:, 0:1] - cartesian_frames[:-1, 0:1]).copy()
    """ Remove Translation """
    cartesian_frames[:, :, 0] = cartesian_frames[:, :, 0] - cartesian_frames[:, 0:1, 0]
    cartesian_frames[:, :, 2] = cartesian_frames[:, :, 2] - cartesian_frames[:, 0:1, 2]
    """ Remove Y Rotation """
    for i in range(n_frames):
        cartesian_frames[i] = rotate_cartesian_frame(cartesian_frames[i], rotations[i])

    """ Rotate Velocity """
    for i in range(n_frames - 1):
        # print(rotations[i+1])

        velocity[i, 0] = rotations[i + 1] * velocity[i, 0]
    """ Get Rotation Velocity """
    r_v = np.zeros(n_frames - 1)
    for i in range(n_frames - 1):
        q = rotations[i + 1] * (-rotations[i])
        r_v[i] = Quaternion.get_angle_from_quaternion(q, ref_dir)

    """ Add Velocity, RVelocity """
    cartesian_frames = cartesian_frames[:-1]
    cartesian_frames = cartesian_frames.reshape(len(cartesian_frames), -1)
    cartesian_frames = np.concatenate([cartesian_frames, velocity[:, :, 0]], axis=-1)
    cartesian_frames = np.concatenate([cartesian_frames, velocity[:, :, 2]], axis=-1)
    cartesian_frames = np.concatenate([cartesian_frames, r_v[:, np.newaxis]], axis=-1)
    return cartesian_frames


def get_joint_position_data_from_anim(anim):
    '''

    :param anim:  n_frames * n_channels (66, the last three must be x, z translation and rotation)
    :return:
    '''
    anim = copy.deepcopy(anim)
    joints, root_x, root_z, root_r = anim[:, :-3], anim[:, -3], anim[:, -2], anim[:, -1]
    joints = joints.reshape((len(joints), -1, 3))  ### reshape 2D matrix to (n_frames, n_joints, n_dims)
    rotation = Quaternions.id(1)
    translation = np.array([[0, 0, 0]])

    for i in range(len(joints)):
        joints[i, :, :] = rotation * joints[i]
        joints[i, :, 0] = joints[i, :, 0] + translation[0, 0]
        joints[i, :, 2] = joints[i, :, 2] + translation[0, 2]
        rotation = Quaternions.from_angle_axis(-root_r[i], np.array([0, 1, 0])) * rotation
        # offsets.append(rotation * ref_dir)
        translation = translation + rotation * np.array([root_x[i], 0, root_z[i]])

    return joints


def export_point_cloud_data_without_foot_contact(motion_data, filename):
    '''

    :param motion_data: n_frames * n_dims
    :param filename:
    :return:
    '''
    motion_data = copy.deepcopy(motion_data)
    joints, root_x, root_z, root_r = motion_data[:, :-3], motion_data[:, -3], motion_data[:, -2], motion_data[:, -1]
    joints = joints.reshape((len(joints), -1, 3))  ### reshape 2D matrix to (n_frames, n_joints, n_dims)
    rotation = Quaternions.id(1)
    translation = np.array([[0, 0, 0]])
    # ref_dir = np.array([0, 0, 1])
    for i in range(len(joints)):
        joints[i, :, :] = rotation * joints[i]
        joints[i, :, 0] = joints[i, :, 0] + translation[0, 0]
        joints[i, :, 2] = joints[i, :, 2] + translation[0, 2]
        rotation = Quaternions.from_angle_axis(-root_r[i], np.array([0, 1, 0])) * rotation
        # offsets.append(rotation * ref_dir)
        translation = translation + rotation * np.array([root_x[i], 0, root_z[i]])

    save_data = {'motion_data': joints.tolist(), 'has_skeleton': True, 'skeleton': GAME_ENGINE_SKELETON}
    write_to_json_file(filename, save_data)


def get_encoded_data(elementary_action, motion_primitive):
    data_folder = r'E:\workspace\tmp\training_data'
    filepath = os.path.join(data_folder, elementary_action, motion_primitive, 'encoded_motion.npz')
    if not os.path.exists(filepath):
        raise IOError('cannot find encoded data!')
    else:
        return np.load(filepath)['encoded_motion']


def export_joint_position_to_cloud_data(joint_position, filename):
    '''

    :param joint_position: n_frames * n_joints *3 (cartesian x y z)
    :param filename:
    :return:
    '''
    save_data = {'motion_data': joint_position.tolist(), 'has_skeleton': True, 'skeleton': GAME_ENGINE_SKELETON}
    write_to_json_file(filename, save_data)


def export_anim_to_joint_position(anim):
    '''
    Fast implementation of converting relative pose to global joint position
    :param anim:
    :return:
    '''
    joints, v_x, v_z, v_r = anim[:, :-3], anim[:, -3], anim[:, -2], anim[:, -1]
    joints = joints.reshape((len(joints), -1, 3))

    v_r = np.cumsum(v_r)
    """ Rotate motion about Y axis """
    rotated_joints = rotation_cartesian_frames(joints, v_r)

    v_x = np.reshape(v_x, (-1, 1, 1))
    v_z = np.reshape(v_z, (-1, 1, 1))
    trans_mat = np.concatenate((v_x, np.zeros(v_x.shape), v_z, np.ones(v_x.shape)), axis=-1)

    angles = v_r
    n_frames = len(v_r)
    sin_theta = np.sin(angles)
    cos_theta = np.cos(angles)
    rotmat = np.array([cos_theta, np.zeros(n_frames), sin_theta, np.zeros(n_frames), np.zeros(n_frames),
                       np.ones(n_frames), np.zeros(n_frames), np.zeros(n_frames), -sin_theta, np.zeros(n_frames),
                       cos_theta, np.zeros(n_frames), np.zeros(n_frames), np.zeros(n_frames), np.zeros(n_frames),
                       np.ones(n_frames)]).T
    swapped_trans_mat = np.transpose(trans_mat, (0, 2, 1))
    rotmat = np.reshape(rotmat, (n_frames, 4, 4))
    rotated_v = np.matmul(rotmat, swapped_trans_mat)
    rotated_v = np.transpose(rotated_v, (0, 2, 1))
    v_x = rotated_v[:, :, 0]
    v_z = rotated_v[:, :, 2]
    v_x, v_z = np.cumsum(v_x, axis=0), np.cumsum(v_z, axis=0)
    # print(rotated_joints[:, :, 0].shape)
    # print(v_x.shape)

    rotated_joints[:, :, 0] += v_x
    rotated_joints[:, :, 2] += v_z
    return rotated_joints


def export_speed_data(speed_data):
    '''
    compute global position on the ground from speed data
    :param speed_data: n_frames * 3
    :return:
    '''
    n_frames = len(speed_data)
    v_x, v_z, v_r = speed_data[:, 0], speed_data[:, 1], speed_data[:, 2]
    joints = np.zeros((n_frames, 1, 3))
    v_x = np.reshape(v_x, (-1, 1, 1))
    v_z = np.reshape(v_z, (-1, 1, 1))
    trans_mat = np.concatenate((v_x, np.zeros(v_x.shape), v_z, np.ones(v_x.shape)), axis=-1)
    angles = v_r
    sin_theta = np.sin(angles)
    cos_theta = np.cos(angles)
    rotmat = np.array([cos_theta, np.zeros(n_frames), sin_theta, np.zeros(n_frames), np.zeros(n_frames),
                       np.ones(n_frames), np.zeros(n_frames), np.zeros(n_frames), -sin_theta, np.zeros(n_frames),
                       cos_theta, np.zeros(n_frames), np.zeros(n_frames), np.zeros(n_frames), np.zeros(n_frames),
                       np.ones(n_frames)]).T
    swapped_trans_mat = np.transpose(trans_mat, (0, 2, 1))
    rotmat = np.reshape(rotmat, (n_frames, 4, 4))
    swapped_trans_mat = np.transpose(trans_mat, (0, 2, 1))
    rotmat = np.reshape(rotmat, (n_frames, 4, 4))
    rotated_v = np.matmul(rotmat, swapped_trans_mat)
    rotated_v = np.transpose(rotated_v, (0, 2, 1))
    v_x = rotated_v[:, :, 0]
    v_z = rotated_v[:, :, 2]
    v_x, v_z = np.cumsum(v_x, axis=0), np.cumsum(v_z, axis=0)
    joints[:, :, 0] += v_x
    joints[:, :, 2] += v_z
    return joints


def convert_anim_to_point_cloud_tf(anim):
    '''

    :param anim:
    :return:
    '''
    joints, v_x, v_z, v_r = anim[:, :-3], anim[:, -3], anim[:, -2], anim[:, -1]
    joints = tf.reshape(joints, (joints.shape[0], -1, 3))
    n_frames, n_joints = joints.shape[0], joints.shape[1]

    v_r = tf.reshape(tf.cumsum(v_r), (n_frames, 1))
    """ Rotate motion about Y axis """
    v_x = tf.reshape(v_x, (n_frames, 1))
    v_z = tf.reshape(v_z, (n_frames, 1))
    #### create rotation matrix
    sin_theta = tf.sin(v_r)
    cos_theta = tf.cos(v_r)
    rotmat = tf.concat((cos_theta, tf.zeros((n_frames, 1)), sin_theta, tf.zeros((n_frames, 1)), tf.zeros((n_frames, 1)),
                        tf.ones((n_frames, 1)), tf.zeros((n_frames, 1)), tf.zeros((n_frames, 1)), -sin_theta,
                        tf.zeros((n_frames, 1)), cos_theta, tf.zeros((n_frames, 1)), tf.zeros((n_frames, 1)),
                        tf.zeros((n_frames, 1)), tf.zeros((n_frames, 1)), tf.ones((n_frames, 1))), axis=-1)
    rotmat = tf.reshape(rotmat, (n_frames, 4, 4))

    ones = tf.ones((n_frames, n_joints, 1))
    extended_joints = tf.concat((joints, ones), axis=-1)
    swapped_joints = tf.transpose(extended_joints, (0, 2, 1))
    # print('swapped joints shape: ', swapped_joints.shape)
    rotated_joints = tf.matmul(rotmat, swapped_joints)
    rotated_joints = tf.transpose(rotated_joints, (0, 2, 1))[:, :, :-1]
    """ Rotate Velocity"""
    trans = tf.concat((v_x, tf.zeros((n_frames, 1)), v_z, tf.ones((n_frames, 1))), axis=-1)
    trans = tf.expand_dims(trans, 1)
    swapped_trans = tf.transpose(trans, (0, 2, 1))
    rotated_trans = tf.matmul(rotmat, swapped_trans)
    rotated_trans = tf.transpose(rotated_trans, (0, 2, 1))
    v_x = rotated_trans[:, :, 0]
    v_z = rotated_trans[:, :, 2]
    v_x, v_z = tf.cumsum(v_x, axis=0), tf.cumsum(v_z, axis=0)
    rotated_trans = tf.concat((v_x, tf.zeros((n_frames, 1)), v_z), axis=-1)
    rotated_trans = tf.expand_dims(rotated_trans, 1)
    export_joints = rotated_joints + rotated_trans
    return export_joints


# def convert_anim_to_point_cloud_tf(anim):
#
#     '''
#
#     :param anim: Tensor, n_frames * n_dims
#     :return:
#     '''
#     joints, root_x, root_z, root_r = anim[:, :-3], anim[:, -3], anim[:, -2], anim[:, -1]
#     output_frames = []
#     joints = tf.reshape(joints, (joints.shape[0], -1, 3))
#     rotation = TFQuaternion()
#     translation = tf.zeros(3)
#     for i in range(joints.shape[0]):
#         rotated_joints = []
#         for j in range(joints.shape[1]):
#             new_p = rotation * joints[i, j]
#             new_p = new_p + translation
#             new_p = tf.reshape(new_p, (1, 3))
#             rotated_joints.append(new_p)
#         output_frame = tf.expand_dims(tf.concat(rotated_joints, axis=0), 0)
#
#         rotation = TFQuaternion.fromAngleAxis(-root_r[i], np.array([0, 1, 0])) * rotation
#         translation = translation + rotation * tf.stack((root_x[i], 0, root_z[i]))
#
#         output_frames.append(output_frame)
#     return tf.concat(output_frames, axis=0)


def retarget_panim_data(animated_points):
    # target_filename = r'C:\repo\data\1 - MoCap\4 - Alignment\elementary_action_proudWalk\leftStance\walk_proud_normalwalking_23_leftStance_81_160.bvh'
    # target_filename = r'C:\repo\data\1 - MoCap\1 - Rawdata\Take_walk\walk_001_1.bvh'
    target_filename = r'E:\workspace\experiment data\cutted_holden_data_walking\LocomotionFlat01_000.bvh'
    src_skeleton_model = SKELETON_MODELS["game_engine_subset"]
    target_skeleton_model = SKELETON_MODELS["cmu"]
    src_scale = 1
    target_scale = 1
    joint_map = generate_joint_map(src_skeleton_model, target_skeleton_model)
    src_joints = GAME_ENGINE_SKELETON
    target_skeleton = load_skeleton(target_filename, target_skeleton_model, scale=target_scale)
    retargeting = PointCloudRetargeting(src_joints, src_skeleton_model, target_skeleton, joint_map, src_scale,
                                        additional_rotation_map=None, place_on_ground=True)
    new_frames = retargeting.run(animated_points, frame_range=None)
    target_motion = MotionVector()
    target_motion.frames = new_frames
    target_motion.n_frames = len(new_frames)
    return target_motion


def retarget_panim_data_to_edin_skeleton(animated_points, skeleton_file):
    # target_filename = r'../../game_engine_target_large.bvh'
    src_skeleton_model = SKELETON_MODELS["game_engine"]
    target_skeleton_model = SKELETON_MODELS["game_engine"]
    src_scale = 1
    target_scale = 1
    joint_map = generate_joint_map(src_skeleton_model, target_skeleton_model)
    src_joints = GAME_ENGINE_SKELETON
    target_skeleton = load_skeleton(skeleton_file, target_skeleton_model, scale=target_scale)
    retargeting = PointCloudRetargeting(src_joints, src_skeleton_model, target_skeleton, joint_map, src_scale,
                                        additional_rotation_map=None, place_on_ground=True)
    new_frames = retargeting.run(animated_points, frame_range=None)
    target_motion = MotionVector()
    target_motion.frames = new_frames
    target_motion.n_frames = len(new_frames)
    return target_motion


def retarget_panim_data_to_game_engine_skeleton(animated_points, skeleton_file):
    # target_filename = r'../../game_engine_target_large.bvh'
    src_skeleton_model = SKELETON_MODELS["game_engine"]
    target_skeleton_model = SKELETON_MODELS["game_engine"]
    src_scale = 1
    target_scale = 1
    joint_map = generate_joint_map(src_skeleton_model, target_skeleton_model)
    src_joints = GAME_ENGINE_SKELETON
    target_skeleton = load_skeleton(skeleton_file, target_skeleton_model, scale=target_scale)
    retargeting = PointCloudRetargeting(src_joints, src_skeleton_model, target_skeleton, joint_map, src_scale,
                                        additional_rotation_map=None, place_on_ground=True)
    new_frames = retargeting.run(animated_points, frame_range=None)
    target_motion = MotionVector()
    target_motion.frames = new_frames
    target_motion.n_frames = len(new_frames)
    return target_motion


def load_skeleton(filename, skeleton_model, scale=1.0):
    bvh_reader = BVHReader(filename)
    animated_joints = list(bvh_reader.get_animated_joints())
    target_skeleton = SkeletonBuilder().load_from_bvh(bvh_reader, animated_joints)
    target_skeleton.skeleton_model = skeleton_model
    target_skeleton.scale(scale)
    return target_skeleton


def convert_anim_data_to_point_cloud(anim_data):
    anim = copy.deepcopy(anim_data)
    joints, root_x, root_z, root_r = anim[:, :-3], anim[:, -3], anim[:, -2], anim[:, -1]
    joints = joints.reshape((len(joints), -1, 3))
    rotation = Quaternions.id(1)
    translation = np.array([[0, 0, 0]])
    for i in range(len(joints)):
        joints[i, :, :] = rotation * joints[i]
        joints[i, :, 0] = joints[i, :, 0] + translation[0, 0]
        joints[i, :, 2] = joints[i, :, 2] + translation[0, 2]
        rotation = Quaternions.from_angle_axis(-root_r[i], np.array([0, 1, 0])) * rotation
        translation = translation + rotation * np.array([root_x[i], 0, root_z[i]])
    return joints


def convert_anim_data_to_point_cloud_with_foot_contact(anim_data):
    anim = copy.deepcopy(anim_data)
    joints, root_x, root_z, root_r = anim[:, :-7], anim[:, -7], anim[:, -6], anim[:, -5]
    joints = joints.reshape((len(joints), -1, 3))
    rotation = Quaternions.id(1)
    translation = np.array([[0, 0, 0]])
    for i in range(len(joints)):
        joints[i, :, :] = rotation * joints[i]
        joints[i, :, 0] = joints[i, :, 0] + translation[0, 0]
        joints[i, :, 2] = joints[i, :, 2] + translation[0, 2]
        rotation = Quaternions.from_angle_axis(-root_r[i], np.array([0, 1, 0])) * rotation
        translation = translation + rotation * np.array([root_x[i], 0, root_z[i]])
    return joints


def convert_bvh_to_cmu_skeleton(bvhfile, save_path, cmu_skeleton_file=None):
    '''

    :param bvhfile:
    :return:
    '''
    cartesian_pos = get_joint_position_from_bvh(bvhfile, animated_joints=GAME_ENGINE_ANIMATED_JOINTS)
    if cmu_skeleton_file is None:
        cmu_skeleton_file = r'E:\workspace\mocap_data\cmu mocap data\02\02_03.bvh'
    bvhreader = BVHReader(cmu_skeleton_file)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader)
    panim_data = np.reshape(cartesian_pos[0], (cartesian_pos.shape[1], 21, 3))
    mv = retarget_panim_data(panim_data)
    ground_contact_joints = ['lFoot_EndSite', 'rFoot_EndSite']
    filename = os.path.split(bvhfile)[-1]
    mv.frames = shift_quat_frames_to_ground(mv.frames, ground_contact_joints, skeleton)
    mv.export(skeleton, os.path.join(save_path, filename))


def convert_bvh_to_latent_space(style_motion_file, autoencoder_file, preprocess_file):
    motion_data = process_file(style_motion_file, sliding_window=False)
    motion_data = motion_data[np.newaxis, :, :]
    motion_data = np.swapaxes(motion_data, 1, 2)
    if motion_data.shape[2] % 2 != 0:
        motion_data = motion_data[:, :, :-1]
    preprocess = np.load(preprocess_file)
    normalized_data = (motion_data - preprocess['Xmean']) / preprocess['Xstd']
    input = tf.placeholder(dtype=tf.float32, shape=[None, motion_data.shape[1], motion_data.shape[2]])
    encoder_op = motion_encoder_channel_first(input, name='encoder')
    # init = tf.global_variables_initializer()
    encoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='encoder')
    saver = tf.train.Saver(encoder_params)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        # sess.run(init)
        saver.restore(sess, autoencoder_file)
        H = sess.run(encoder_op, feed_dict={input: normalized_data})
    return H


def backproject_to_motion_space(hidden_data, autoencoder_file, preprocess_file, n_input):
    input = tf.placeholder(dtype=tf.float32, shape=[None, hidden_data.shape[1], hidden_data.shape[2]])
    decoder_op = motion_decoder_channel_first(input, n_input)
    preprocess = np.load(preprocess_file)
    init = tf.global_variables_initializer()
    saver = tf.train.Saver()
    config = tf.ConfigProto()
    with tf.Session(config=config) as sess:
        sess.run(init)
        saver.restore(sess, autoencoder_file)
        reconstructed_motion_data = sess.run(decoder_op, feed_dict={input: hidden_data})
    reconstructed_motion_data = (reconstructed_motion_data * preprocess['Xstd']) + preprocess['Xmean']
    return reconstructed_motion_data


def combine_motion_clips(clips, motion_len, window_step):
    '''

    :param clips:
    :param motion_len:
    :param window_step:
    :return:
    '''
    clips = np.asarray(clips)
    n_clips, window_size, n_dims = clips.shape

    ## case 1: motion length is smaller than window_step
    if motion_len <= window_step:
        assert n_clips == 1
        left_index = (window_size - motion_len) // 2 + (window_size - motion_len) % 2
        right_index = window_size - (window_size - motion_len) // 2
        return clips[0][left_index: right_index]

    ## case 2: motion length is larger than window_step and smaller than window
    if motion_len > window_step and motion_len <= window_size:
        assert n_clips == 2
        left_index = (window_size - motion_len) // 2 + (window_size - motion_len) % 2
        right_index = window_size - (window_size - motion_len) // 2
        return clips[0][left_index: right_index]

    residue_frames = motion_len % window_step
    print('residue_frames: ', residue_frames)
    ## case 3: residue frames is smaller than window step
    if residue_frames <= window_step:
        residue_frames += window_step
        combined_frames = np.concatenate(clips[0:n_clips - 2, :window_step], axis=0)
        left_index = (window_size - residue_frames) // 2 + (window_size - residue_frames) % 2
        right_index = window_size - (window_size - residue_frames) // 2
        combined_frames = np.concatenate((combined_frames, clips[-2, left_index:right_index]), axis=0)
        return combined_frames


def gram_matrix(X):
    '''
    X shape: n_batches * n_dims * n_frames
    :param X:
    :return: gram_matrix n_batches * n_dims * n_dims, sum over n_frames
    '''
    return tf.reduce_mean(tf.expand_dims(X, 2) * tf.expand_dims(X, 1), axis=3)


def get_input_data(data_type):
    if data_type == 'expmap':
        test_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data_raw\angry_fast walking_146.bvh'

        bvhreader = BVHReader(test_file)
        skeleton = SkeletonBuilder().load_from_bvh(bvhreader, animated_joints=GAME_ENGINE_ANIMATED_JOINTS)

        stylized_exp_data = np.load(r'../data/training_data/processed_stylized_data_expmap.npz')
        training_data = None
        for filename in stylized_exp_data.files:
            if training_data is None:
                training_data = stylized_exp_data[filename]
            else:
                print(stylized_exp_data[filename].shape)
                training_data = np.concatenate((training_data, stylized_exp_data[filename]), axis=0)
        X = np.swapaxes(training_data, 2, 1)
        return X
        # Xmean = X.mean(axis=2).mean(axis=0)[np.newaxis, :, np.newaxis]
        # print(Xmean.shape)
        # # Xstd = X.std(axis=2)[:, :, np.newaxis] + 1e-6
        # Xstd = X.std(axis=2).std(axis=0)[np.newaxis, :, np.newaxis] + 1e-6
        # print(Xstd.shape)
    else:
        raise NotImplementedError


def get_joint_position_data(elementary_action,
                            motion_primitive):
    data_folder = get_aligned_data_folder(elementary_action,
                                          motion_primitive)
    bvhfiles = glob.glob(os.path.join(data_folder, '*.bvh'))
    motion_data = []
    filename_list = []
    for bvhfile in bvhfiles:
        bvhreader = BVHReader(bvhfile)
        filename = os.path.split(bvhfile)[-1]
        skeleton = SkeletonBuilder().load_from_bvh(bvhreader)
        euler_frames = bvhreader.frames
        cartesian_frames = convert_euler_frames_to_cartesian_frames(skeleton, euler_frames,
                                                                    animated_joints=GAME_ENGINE_ANIMATED_JOINTS)
        cartesian_frames = np.reshape(cartesian_frames,
                                      (cartesian_frames.shape[0], np.prod(cartesian_frames.shape[1:])))
        motion_data.append(cartesian_frames)
        filename_list.append(filename)

    return np.asarray(motion_data), filename_list


def get_joint_position_from_bvh(bvhfile, animated_joints=None):
    bvhreader = BVHReader(bvhfile)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader)
    if animated_joints is None:
        animated_joints = bvhreader.animated_joints
    cartesian_frames = convert_euler_frames_to_cartesian_frames(skeleton, bvhreader.frames,
                                                                animated_joints=animated_joints)
    cartesian_frames = np.reshape(cartesian_frames, (cartesian_frames.shape[0], np.prod(cartesian_frames.shape[1:])))
    return cartesian_frames[np.newaxis, :, :]


def get_files(path):
    bvhfiles = []
    for root, dirs, files in os.walk(path):
        for filemane in [f for f in files if f.endswith(".bvh") and f != 'rest.bvh']:
            bvhfiles.append(os.path.join(root, filemane))
    return bvhfiles


def plot_foot_contact(foot_contact):
    '''

    :param foot_contact: numpy array, 4 * n_frames
    :return:
    '''

    fig, axes = plt.subplots(4, 1)
    axes[0].plot(foot_contact[0, :])
    axes[1].plot(foot_contact[1, :])
    axes[2].plot(foot_contact[2, :])
    axes[3].plot(foot_contact[3, :])
    plt.show()


def export_point_cloud_Edin_data(motion_data, filename, skeleton):
    import copy
    '''

    :param motion_data: n_frames * n_dims 
    :param filename: 
    :return: 
    '''
    motion_data = copy.deepcopy(motion_data)
    joints, vel, root_x, root_z, root_r = motion_data[:, :93], motion_data[:, 93:93 * 2], motion_data[:,
                                                                                          -3], motion_data[:,
                                                                                               -2], motion_data[:, -1]

    joints = joints.reshape((len(joints), -1, 3))  ### reshape 2D matrix to (n_frames, n_joints, n_dims)
    vel = vel.reshape((len(vel), -1, 3))
    rotation = Quaternions.id(1)
    translation = np.array([[0, 0, 0]])
    last_joints = np.array([0, 0, 0] * len(joints))
    # ref_dir = np.array([0, 0, 1])

    for i in range(len(joints)):
        joints[i, :, :] = rotation * joints[i]
        joints[i, :, 0] = joints[i, :, 0] + translation[0, 0]
        joints[i, :, 2] = joints[i, :, 2] + translation[0, 2]
        if i == 0:
            last_joints = joints[i]
        else:
            vel[i, :, :] = rotation * vel[i]
            joints[i, :, :] = ((last_joints + vel[i, :, :]) + joints[i, :, :]) / 2
            last_joints = joints[i]
        rotation = Quaternions.from_angle_axis(-root_r[i], np.array([0, 1, 0])) * rotation
        translation = translation + rotation * np.array([root_x[i], 0, root_z[i]])

    save_data = {'motion_data': joints.tolist(), 'has_skeleton': True, 'skeleton': skeleton}
    write_to_json_file(filename, save_data)