# encoding: UTF-8
import tensorflow as tf
import numpy as np
from datetime import datetime
import sys
sys.path.append('../')
from models import motion_encoder_channel_first, motion_decoder_channel_first
rng = np.random.RandomState(23456)


class MotionEncoderSingleLayer(object):
    def __init__(self, npc, input_dims, n_epochs, name, batchsize, encoder_activation=None, decoder_activation=None,
                 n_random_samples=1):
        self.npc = npc
        # self.n_dims = n_dims
        # self.n_frames = n_frames
        self.input_dims = input_dims
        self.n_epochs = n_epochs
        self.name = name
        self.batchsize = batchsize
        self.encoder_activation = encoder_activation
        self.decoder_activation = decoder_activation
        self.n_random_samples = n_random_samples
        self.sess = tf.InteractiveSession()

    @staticmethod
    def fully_connected_layer(input, hidden_num, name='fully_connected_layer', activation=None,
                              reuse=tf.AUTO_REUSE):
        with tf.variable_scope(name, reuse=reuse):
            layer_output = tf.layers.dense(input, hidden_num)
            if activation is None:
                layer_output = tf.contrib.layers.batch_norm(layer_output,
                                                            center=True,
                                                            scale=True,
                                                            is_training=True,
                                                            scope='bn')
                return layer_output
            else:
                layer_output = tf.contrib.layers.batch_norm(layer_output,
                                                            center=True,
                                                            scale=True,
                                                            is_training=True,
                                                            scope='bn')
                return activation(layer_output)
    
    def encode(self, input, reuse):
        encoder_layer1 = MotionEncoderSingleLayer.fully_connected_layer(input,
                                                                        hidden_num=1024,
                                                                        name='encoder_layer1',
                                                                        activation=self.encoder_activation,
                                                                        reuse=reuse)

        encoder_layer2 = MotionEncoderSingleLayer.fully_connected_layer(encoder_layer1,
                                                                        hidden_num=self.npc,
                                                                        name='encoder_layer2',
                                                                        activation=self.encoder_activation,
                                                                        reuse=reuse)
        return encoder_layer2
    
    def decode(self, input, reuse):
        decoder_2 = MotionEncoderSingleLayer.fully_connected_layer(input,
                                                                   hidden_num=1024,
                                                                   name='decoder_2',
                                                                   activation=self.decoder_activation,
                                                                   reuse=reuse)

        decoder_1 = MotionEncoderSingleLayer.fully_connected_layer(decoder_2,
                                                                   hidden_num=self.input_dims,
                                                                   name='decoder_1',
                                                                   activation=None,
                                                                   reuse=reuse)
        return decoder_1
        
    def create_model(self, reuse=tf.AUTO_REUSE):
        with tf.variable_scope(self.name, reuse=tf.AUTO_REUSE):

            self.input = tf.placeholder(dtype=tf.float32, shape=(None, self.input_dims), name='input')
            self.latent_input = tf.placeholder(dtype=tf.float32, shape=(None, self.npc), name='latent_input')
            ### initialize encoder
            


            # mean_encoder = MotioEncoderSingleLayer.fully_connected_layer(encoder,
            #                                                              hidden_num=self.npc,
            #                                                              name='mean_encoder',
            #                                                              activation=None)
            #
            # log_sigma_encoder = MotioEncoderSingleLayer.fully_connected_layer(encoder,
            #                                                                   hidden_num=self.npc,
            #                                                                   name='log_sigma_encoder',
            #                                                                   activation=None)
            self.encoder_op = self.encode(self.input, reuse)

            decoder_1 = self.decode(self.encoder_op, reuse)
            # # Sampler: Normal (gaussian) random distribution
            # eps = tf.random_normal(tf.shape(mean_encoder), dtype=tf.float32, mean=0., stddev=1.0,
            #                        name='epsilon')
            # z_mean = mean_encoder
            # z_log_sigma = log_sigma_encoder
            # self.z_op = z_mean + tf.exp(z_log_sigma / 2) * eps

            ### initialize decoder



            ### loss is the sum or reconstruction error and KL distance between normal distribution and mapped distribution
            reconstruction_loss = tf.reduce_mean(tf.pow(decoder_1 - self.input, 2))
    
            # kl_divergence_loss = -0.5 * tf.reduce_mean(1 + z_log_sigma - tf.square(z_mean) - tf.exp(z_log_sigma), 1)
            self.cost = reconstruction_loss
    
            self.output_op = self.decode(self.latent_input, reuse)
    
            self.model_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.name)
    
            self.saver = tf.train.Saver(self.model_params)

    def get_params(self):
        return self.model_params

    def __call__(self, input_data):
        z_value = self.sess.run(self.encoder_op, feed_dict={self.input: input_data})
        return self.sess.run(self.output_op, feed_dict={self.latent_input: z_value})

    def encode_data(self, input_data):
        return self.sess.run(self.encoder_op, feed_dict={self.input: input_data})

    def train(self, training_data, learning_rate=0.01):
        self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        train_op = self.optimizer.minimize(self.cost)
        self.sess.run(tf.global_variables_initializer())
        n_samples, input_dims = training_data.shape
        last_mean = 0
        for epoch in range(self.n_epochs):
            batchinds = np.arange(n_samples // self.batchsize)
            rng.shuffle(batchinds)
            c = []
            for bii, bi in enumerate(batchinds):
                for i in range(self.n_random_samples):
                    z_value = self.sess.run(self.encoder_op, feed_dict={
                        self.input: training_data[bi * self.batchsize: (bi + 1) * self.batchsize]})

                    self.sess.run(train_op, feed_dict={self.latent_input: z_value,
                                                       self.input: training_data[
                                                                   bi * self.batchsize: (bi + 1) * self.batchsize]})
                    c.append(self.sess.run(self.cost, feed_dict={
                        self.input: training_data[bi * self.batchsize: (bi + 1) * self.batchsize],
                        self.latent_input: z_value}))
                    if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii) / len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
            curr_mean = np.mean(c)
            diff_mean, last_mean = curr_mean - last_mean, curr_mean
            print('\r[Epoch %i] 100.0%% mean %.5f diff %.5f %s' %
                  (epoch, curr_mean, diff_mean, str(datetime.now())[11:19]))

    def load_model(self, model_file):
        self.saver.restore(self.sess, model_file)

    def save_model(self, model_file):
        save_path = self.saver.save(self.sess, model_file)
        print("Model saved in file: %s " % save_path)

    def generate_new_samples(self, n_samples):

        new_samples = np.random.rand(n_samples, self.npc)
        res = self.sess.run(self.output_op, feed_dict={self.latent_input: new_samples})
        return res
