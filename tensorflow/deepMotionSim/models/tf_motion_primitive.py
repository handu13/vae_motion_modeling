# encoding: UTF-8
import numpy as np
from .vae_encoder import MotionVaeEncoder
from .conv_autoencoder import ConvAutoEncoder
import tensorflow as tf


class TFMotionPrimitive(object):
    
    def __init__(self, elementary_action, motion_primitive, sess, npc, batchsize=10):
        self.elementary_action = elementary_action
        self.motion_primitive = motion_primitive
        self.sess = sess
        self.npc = npc
        self.batchsize = batchsize
        
    def load_model(self, model_file, model_meta_data):
        '''

        :param model_file: string filepath
        :param model_meta_data: dict: hyper-parameters for variational autoencoder
        :return:
        '''
        self.feature_dims = model_meta_data['feature_dims']
        self.feature_frames = model_meta_data['feature_frames']
        input_dims = self.feature_dims * self.feature_frames
        model_name = '_'.join([self.elementary_action, self.motion_primitive, 'vae'])
        self.vae_model = MotionVaeEncoder(npc=self.npc, input_dims=input_dims, name=model_name,
                                          encoder_activation=tf.nn.tanh, decoder_activation=tf.nn.tanh,
                                          n_random_samples=1, sess=self.sess)
        self.vae_model.create_model()
        self.vae_model.load_model(model_file)
    
    def load_motion_autoencoder(self, autoencoder_file, autoencoder_meta_data):
        '''

        :param autoencoder_file: string filepath
        :param autoencoder_meta_data: dict: hyperparameters for Autoencoder
        :return:
        '''
        hidden_units = autoencoder_meta_data['hidden_units']
        kernel_size = autoencoder_meta_data['kernel_size']
        self.n_frames = autoencoder_meta_data['n_frames']
        self.n_dims = autoencoder_meta_data['n_dims']
        self.mean = autoencoder_meta_data['mean']
        self.std = autoencoder_meta_data['std']
        self.conv_model = ConvAutoEncoder(name='motion_autoencoder', n_frames=self.n_frames, n_dims=self.n_dims,
                                          kernel_size=kernel_size, encode_activation=tf.nn.relu, decode_activation=None,
                                          hidden_units=hidden_units, pooling='average', batch_norm=False,
                                          sess=self.sess)
        self.conv_model.create_model()
        self.conv_model.load_model(autoencoder_file)
    
    def train_model(self):
        pass
    
    def sample(self, n_samples):

        if n_samples > self.batchsize:  ### in case the number of samples is too large
            n_batches = n_samples // self.batchsize
            motions = np.zeros((n_samples, self.n_frames, self.n_dims))
            for idx in range(n_batches):
                new_samples = self.vae_model.generate_new_samples(self.batchsize)
                new_samples = np.reshape(new_samples, (self.batchsize, self.feature_dims, self.feature_frames))
                new_batch_motions = self.conv_model.decode_data(new_samples)
                new_batch_motions = np.swapaxes(new_batch_motions, 1, 2) * self.std + self.mean
                motions[idx*self.batchsize: (idx+1)*self.batchsize] = new_batch_motions
        else:
            new_samples = self.vae_model.generate_new_samples(n_samples)
            new_samples = np.reshape(new_samples, (n_samples, self.feature_dims, self.feature_frames))
            motions = self.conv_model.decode_data(new_samples)
            motions = np.swapaxes(motions, 1, 2) * self.std + self.mean
        return motions