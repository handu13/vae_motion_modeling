# encoding: UTF-8

import tensorflow as tf
import numpy as np
from datetime import datetime
import sys
rng = np.random.RandomState(23456)

class RawAutoencoder(object):
    
    def __init__(self, npc, input_dim, n_epochs, name, batchsize=128, encoder_activation=None, decoder_activation=None):
        self.npc = npc
        self.input_dim = input_dim
        self.n_epochs = n_epochs
        self.name = name
        self.batchsize = batchsize
        self.sess = tf.InteractiveSession()
        self.encoder_activation = encoder_activation
        self.decoder_activation = decoder_activation
        
        self.n_h1 = 1024
        self.n_h2 = 512
        self.n_h3 = 256
        self.n_h4 = 128
        self.n_h5 = 64
        self.n_h6 = 32
        
        self.weights = {'encoder_h1': tf.Variable(initial_value=tf.random_normal([self.input_dim, self.n_h1])),
                        'encoder_h2': tf.Variable(initial_value=tf.random_normal([self.n_h1, self.n_h2])),
                        'encoder_h3': tf.Variable(initial_value=tf.random_normal([self.n_h2, self.n_h3])),
                        'encoder_h4': tf.Variable(initial_value=tf.random_normal([self.n_h3, self.n_h4])),
                        'encoder_h5': tf.Variable(initial_value=tf.random_normal([self.n_h4, self.n_h5])),
                        'encoder_h6': tf.Variable(initial_value=tf.random_normal([self.n_h5, self.n_h6])),
                        'encoder_h7': tf.Variable(initial_value=tf.random_normal([self.n_h6, self.npc]))}
        self.biases = {'encoder_b1': tf.Variable(initial_value=tf.random_normal([self.n_h1])),
                       'encoder_b2': tf.Variable(initial_value=tf.random_normal([self.n_h2])),
                       'encoder_b3': tf.Variable(initial_value=tf.random_normal([self.n_h3])),
                       'encoder_b4': tf.Variable(initial_value=tf.random_normal([self.n_h4])),
                       'encoder_b5': tf.Variable(initial_value=tf.random_normal([self.n_h5])),
                       'encoder_b6': tf.Variable(initial_value=tf.random_normal([self.n_h6])),
                       'encoder_b7': tf.Variable(initial_value=tf.random_normal([self.npc])),
                       'decoder_b7': tf.Variable(initial_value=tf.random_normal([self.n_h6])),
                       'decoder_b6': tf.Variable(initial_value=tf.random_normal([self.n_h5])),
                       'decoder_b5': tf.Variable(initial_value=tf.random_normal([self.n_h4])),
                       'decoder_b4': tf.Variable(initial_value=tf.random_normal([self.n_h3])),
                       'decoder_b3': tf.Variable(initial_value=tf.random_normal([self.n_h2])),
                       'decoder_b2': tf.Variable(initial_value=tf.random_normal([self.n_h1])),
                       'decoder_b1': tf.Variable(initial_value=tf.random_normal([self.input_dim]))}
        
    @staticmethod
    def fully_connected_layer(input, weight, bias, name, activation=None, reuse=tf.AUTO_REUSE):
        with tf.variable_scope(name, reuse=reuse):
            layer_output = tf.add(tf.matmul(input, weight), bias)
            if activation is None:
                layer_output = tf.contrib.layers.batch_norm(layer_output,
                                                            center=True,
                                                            scale=True,
                                                            is_training=True,
                                                            scope='bn')
                return layer_output

            else:
                layer_output = tf.contrib.layers.batch_norm(layer_output,
                                                            center=True,
                                                            scale=True,
                                                            is_training=True,
                                                            scope='bn')
                return activation(layer_output)
            
    def encode(self, input, reuse):
        encoder_layer1 = RawAutoencoder.fully_connected_layer(input, self.weights['encoder_h1'],
                                                              self.biases['encoder_b1'],
                                                              name='encoder_layer1',
                                                              activation=self.encoder_activation,
                                                              reuse=reuse)
        encoder_layer2 = RawAutoencoder.fully_connected_layer(encoder_layer1, self.weights['encoder_h2'],
                                                              self.biases['encoder_b2'],
                                                              name='encoder_layer2',
                                                              activation=self.encoder_activation,
                                                              reuse=reuse)
        encoder_layer3 = RawAutoencoder.fully_connected_layer(encoder_layer2, self.weights['encoder_h3'],
                                                              self.biases['encoder_b3'],
                                                              name='encoder_layer3',
                                                              activation=self.encoder_activation,
                                                              reuse=reuse)
        encoder_layer4 = RawAutoencoder.fully_connected_layer(encoder_layer3, self.weights['encoder_h4'],
                                                              self.biases['encoder_b4'],
                                                              name='encoder_layer4',
                                                              activation=self.encoder_activation,
                                                              reuse=reuse)
        encoder_layer5 = RawAutoencoder.fully_connected_layer(encoder_layer4, self.weights['encoder_h5'],
                                                              self.biases['encoder_b5'],
                                                              name='encoder_layer5',
                                                              activation=self.encoder_activation,
                                                              reuse=reuse)
        encoder_layer6 = RawAutoencoder.fully_connected_layer(encoder_layer5, self.weights['encoder_h6'],
                                                              self.biases['encoder_b6'],
                                                              name='encoder_layer6',
                                                              activation=self.encoder_activation,
                                                              reuse=reuse)
        encoder_layer7 = RawAutoencoder.fully_connected_layer(encoder_layer6, self.weights['encoder_h7'],
                                                              self.biases['encoder_b7'],
                                                              name='encoder_layer7',
                                                              activation=self.encoder_activation,
                                                              reuse=reuse)
        return encoder_layer7
        
    def decode(self, input, reuse):
        decoder_layer7 = RawAutoencoder.fully_connected_layer(input,
                                                              tf.transpose(self.weights['encoder_h7']),
                                                              self.biases['decoder_b7'],
                                                              name='decoder_layer7',
                                                              activation=self.decoder_activation,
                                                              reuse=reuse)
        decoder_layer6 = RawAutoencoder.fully_connected_layer(decoder_layer7,
                                                              tf.transpose(self.weights['encoder_h6']),
                                                              self.biases['decoder_b6'],
                                                              name='decoder_layer6',
                                                              activation=self.decoder_activation,
                                                              reuse=reuse)
        decoder_layer5 = RawAutoencoder.fully_connected_layer(decoder_layer6,
                                                              tf.transpose(self.weights['encoder_h5']),
                                                              self.biases['decoder_b5'],
                                                              name='decoder_layer5',
                                                              activation=self.decoder_activation,
                                                              reuse=reuse)
        decoder_layer4 = RawAutoencoder.fully_connected_layer(decoder_layer5,
                                                              tf.transpose(self.weights['encoder_h4']),
                                                              self.biases['decoder_b4'],
                                                              name='decoder_layer4',
                                                              activation=self.decoder_activation,
                                                              reuse=reuse)
        decoder_layer3 = RawAutoencoder.fully_connected_layer(decoder_layer4,
                                                              tf.transpose(self.weights['encoder_h3']),
                                                              self.biases['decoder_b3'],
                                                              name='decoder_layer3',
                                                              activation=self.decoder_activation,
                                                              reuse=reuse)
        decoder_layer2 = RawAutoencoder.fully_connected_layer(decoder_layer3,
                                                              tf.transpose(self.weights['encoder_h2']),
                                                              self.biases['decoder_b2'],
                                                              name='decoder_layer2',
                                                              activation=self.decoder_activation,
                                                              reuse=reuse)
        decoder_layer1 = RawAutoencoder.fully_connected_layer(decoder_layer2,
                                                              tf.transpose(self.weights['encoder_h1']),
                                                              self.biases['decoder_b1'],
                                                              name='decoder_layer1',
                                                              activation=None,
                                                              reuse=reuse)
        return decoder_layer1
    
    def create_model(self, reuse=tf.AUTO_REUSE):
        with tf.variable_scope(self.name, reuse=reuse):
            self.input = tf.placeholder(dtype=tf.float32, shape=(None, self.input_dim))
            self.latent_input = tf.placeholder(dtype=tf.float32, shape=(None, self.npc))
            self.encoder_op = self.encode(self.input, reuse)
            decoder_layer1 = self.decode(self.encoder_op, reuse)
            

            self.decoder_op = self.decode(self.latent_input, reuse)


            self.loss_op = self.input_dim * tf.reduce_mean(tf.pow(self.input - decoder_layer1, 2))
            
            self.model_params = list(self.weights.values()) + list(self.biases.values()) + tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.name)
            
            self.saver = tf.train.Saver(self.model_params)
        
    def __call__(self, input_data):
        encodered_motion = self.sess.run(self.encoder_op, feed_dict={self.input: input_data})
        return self.sess.run(self.decoder_op, feed_dict={self.latent_input: encodered_motion})
    
    def get_params(self):
        return self.model_params

    def encode_data(self, input_data):
        return self.sess.run(self.encoder_op, feed_dict={self.input: input_data})

    def decode_data(self, input_data):
        return self.sess.run(self.decoder_op, feed_dict={self.latent_input: input_data})

    def pre_train(self, training_data):
        n_samples, input_dims = training_data.shape

        with tf.variable_scope(self.name, reuse=tf.AUTO_REUSE):
            layer1_input = tf.placeholder(dtype=tf.float32, shape=(None, input_dims))
            encoder_layer1_output = RawAutoencoder.fully_connected_layer(layer1_input,
                                                                         self.weights['encoder_h1'],
                                                                         self.biases['encoder_b1'],
                                                                         name='encoder_layer1',
                                                                         activation=self.encoder_activation)
            layer1_output = RawAutoencoder.fully_connected_layer(encoder_layer1_output,
                                                                 tf.transpose(self.weights['encoder_h1']),
                                                                 self.biases['decoder_b1'],
                                                                 name='decoder_layer1',
                                                                 activation=None)
            layer1_loss = tf.reduce_mean(tf.pow(layer1_input - layer1_output, 2))
            layer1_trainer = self.optimizer.minimize(layer1_loss)

            layer2_input = tf.placeholder(dtype=tf.float32, shape=(None, self.n_h1))
            encoder_layer2_output = RawAutoencoder.fully_connected_layer(layer2_input,
                                                                         self.weights['encoder_h2'],
                                                                         self.biases['encoder_b2'],
                                                                         name='encoder_layer2',
                                                                         activation=self.encoder_activation)
            layer2_output = RawAutoencoder.fully_connected_layer(encoder_layer2_output,
                                                                 tf.transpose(self.weights['encoder_h2']),
                                                                 self.biases['decoder_b2'],
                                                                 name='decoder_layer2',
                                                                 activation=self.decoder_activation)
            layer2_loss = tf.reduce_mean(tf.pow(layer2_input - layer2_output, 2))
            layer2_trainer = self.optimizer.minimize(layer2_loss)

            layer3_input = tf.placeholder(dtype=tf.float32, shape=(None, self.n_h2))
            encoder_layer3_output = RawAutoencoder.fully_connected_layer(layer3_input,
                                                                         self.weights['encoder_h3'],
                                                                         self.biases['encoder_b3'],
                                                                         name='encoder_layer3',
                                                                         activation=self.encoder_activation)
            layer3_output = RawAutoencoder.fully_connected_layer(encoder_layer3_output,
                                                                 tf.transpose(self.weights['encoder_h3']),
                                                                 self.biases['decoder_b3'],
                                                                 name='decoder_layer3',
                                                                 activation=self.decoder_activation)
            layer3_loss = tf.reduce_mean(tf.pow(layer3_input - layer3_output, 2))
            layer3_trainer = self.optimizer.minimize(layer3_loss)

            layer4_input = tf.placeholder(dtype=tf.float32, shape=(None, self.n_h3))
            encoder_layer4_output = RawAutoencoder.fully_connected_layer(layer4_input,
                                                                         self.weights['encoder_h4'],
                                                                         self.biases['encoder_b4'],
                                                                         name='encoder_layer4',
                                                                         activation=self.encoder_activation)
            layer4_output = RawAutoencoder.fully_connected_layer(encoder_layer4_output,
                                                                 tf.transpose(self.weights['encoder_h4']),
                                                                 self.biases['decoder_b4'],
                                                                 name='decoder_layer4',
                                                                 activation=self.decoder_activation)
            layer4_loss = tf.reduce_mean(tf.pow(layer4_input - layer4_output, 2))
            layer4_trainer = self.optimizer.minimize(layer4_loss)

            layer5_input = tf.placeholder(dtype=tf.float32, shape=(None, self.n_h4))
            encoder_layer5_output = RawAutoencoder.fully_connected_layer(layer5_input,
                                                                         self.weights['encoder_h5'],
                                                                         self.biases['encoder_b5'],
                                                                         name='encoder_layer5',
                                                                         activation=self.encoder_activation)
            layer5_output = RawAutoencoder.fully_connected_layer(encoder_layer5_output,
                                                                 tf.transpose(self.weights['encoder_h5']),
                                                                 self.biases['decoder_b5'],
                                                                 name='decoder_layer5',
                                                                 activation=self.decoder_activation)
            layer5_loss = tf.reduce_mean(tf.pow(layer5_input - layer5_output, 2))
            layer5_trainer = self.optimizer.minimize(layer5_loss)

            layer6_input = tf.placeholder(dtype=tf.float32, shape=(None, self.n_h5))
            encoder_layer6_output = RawAutoencoder.fully_connected_layer(layer6_input,
                                                                         self.weights['encoder_h6'],
                                                                         self.biases['encoder_b6'],
                                                                         name='encoder_layer6',
                                                                         activation=self.encoder_activation)
            layer6_output = RawAutoencoder.fully_connected_layer(encoder_layer6_output,
                                                                 tf.transpose(self.weights['encoder_h6']),
                                                                 self.biases['decoder_b6'],
                                                                 name='decoder_layer6',
                                                                 activation=self.decoder_activation)
            layer6_loss = tf.reduce_mean(tf.pow(layer6_input - layer6_output, 2))
            layer6_trainer = self.optimizer.minimize(layer6_loss)

            layer7_input = tf.placeholder(dtype=tf.float32, shape=(None, self.n_h6))
            encoder_layer7_output = RawAutoencoder.fully_connected_layer(layer7_input,
                                                                         self.weights['encoder_h7'],
                                                                         self.biases['encoder_b7'],
                                                                         name='encoder_layer7',
                                                                         activation=self.encoder_activation)
            layer7_output = RawAutoencoder.fully_connected_layer(encoder_layer7_output,
                                                                 tf.transpose(self.weights['encoder_h7']),
                                                                 self.biases['decoder_b7'],
                                                                 name='decoder_layer7',
                                                                 activation=self.decoder_activation)
            layer7_loss = tf.reduce_mean(tf.pow(layer7_input - layer7_output, 2))
            layer7_trainer = self.optimizer.minimize(layer7_loss)
            
        ### initilize parameters
        self.sess.run(tf.global_variables_initializer())
        print('pre-train layer1')
        for epoch in range(self.n_epochs):
            batchinds = np.arange(n_samples // self.batchsize)
            rng.shuffle(batchinds)
            c = []
            for bii, bi in enumerate(batchinds):
                self.sess.run(layer1_trainer, feed_dict={layer1_input: training_data[bi*self.batchsize: (bi+1)*self.batchsize]})
                c.append(self.sess.run(layer1_loss, feed_dict={layer1_input: training_data[bi*self.batchsize: (bi+1)*self.batchsize]}))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:

                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii) / len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
        print('')
        layer1_res = self.sess.run(encoder_layer1_output, feed_dict={layer1_input: training_data})
        print('pre-train layer2')
        for epoch in range(self.n_epochs):
            batchinds = np.arange(n_samples // self.batchsize)
            rng.shuffle(batchinds)
            c = []
            for bii, bi in enumerate(batchinds):
                self.sess.run(layer2_trainer, feed_dict={layer2_input: layer1_res[bi*self.batchsize: (bi+1)*self.batchsize]})
                c.append(self.sess.run(layer2_loss, feed_dict={layer2_input: layer1_res[bi*self.batchsize: (bi+1)*self.batchsize]}))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii) / len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
        print('')
        layer2_res = self.sess.run(encoder_layer2_output, feed_dict={layer2_input: layer1_res})
        print('pre-train layer3')
        for epoch in range(self.n_epochs):
            batchinds = np.arange(n_samples // self.batchsize)
            rng.shuffle(batchinds)
            c = []
            for bii, bi in enumerate(batchinds):
                self.sess.run(layer3_trainer, feed_dict={layer3_input: layer2_res[bi*self.batchsize: (bi+1)*self.batchsize]})
                c.append(self.sess.run(layer3_loss, feed_dict={layer3_input: layer2_res[bi*self.batchsize: (bi+1)*self.batchsize]}))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii) / len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
        print('')
        layer3_res = self.sess.run(encoder_layer3_output, feed_dict={layer3_input: layer2_res})
        print('pre-train layer4')
        for epoch in range(self.n_epochs):
            batchinds = np.arange(n_samples // self.batchsize)
            rng.shuffle(batchinds)
            c = []
            for bii, bi in enumerate(batchinds):
                self.sess.run(layer4_trainer, feed_dict={layer4_input: layer3_res[bi*self.batchsize: (bi+1)*self.batchsize]})
                c.append(self.sess.run(layer4_loss, feed_dict={layer4_input: layer3_res[bi*self.batchsize: (bi+1)*self.batchsize]}))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii) / len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
        print('')
        layer4_res = self.sess.run(encoder_layer4_output, feed_dict={layer4_input: layer3_res})
        print('pre-train layer5')
        for epoch in range(self.n_epochs):
            batchinds = np.arange(n_samples // self.batchsize)
            rng.shuffle(batchinds)
            c = []
            for bii, bi in enumerate(batchinds):
                self.sess.run(layer5_trainer, feed_dict={layer5_input: layer4_res[bi*self.batchsize: (bi+1)*self.batchsize]})
                c.append(self.sess.run(layer5_loss, feed_dict={layer5_input: layer4_res[bi*self.batchsize: (bi+1)*self.batchsize]}))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii) / len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
        print('')
        layer5_res = self.sess.run(encoder_layer5_output, feed_dict={layer5_input: layer4_res})
        print('pre-train layer6')
        for epoch in range(self.n_epochs):
            batchinds = np.arange(n_samples // self.batchsize)
            rng.shuffle(batchinds)
            c = []
            for bii, bi in enumerate(batchinds):
                self.sess.run(layer6_trainer, feed_dict={layer6_input: layer5_res[bi*self.batchsize: (bi+1)*self.batchsize]})
                c.append(self.sess.run(layer6_loss, feed_dict={layer6_input: layer5_res[bi*self.batchsize: (bi+1)*self.batchsize]}))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii) / len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
        print('')
        layer6_res = self.sess.run(encoder_layer6_output, feed_dict={layer6_input: layer5_res})
        print('pre-train layer7')
        for epoch in range(self.n_epochs):
            batchinds = np.arange(n_samples // self.batchsize)
            rng.shuffle(batchinds)
            c = []
            for bii, bi in enumerate(batchinds):
                self.sess.run(layer7_trainer, feed_dict={layer7_input: layer6_res[bi*self.batchsize: (bi+1)*self.batchsize]})
                c.append(self.sess.run(layer7_loss, feed_dict={layer7_input: layer6_res[bi*self.batchsize: (bi+1)*self.batchsize]}))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii) / len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
        print('')

    def train(self, training_data, learning_rate=0.01, pre_train=True):

        self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        train_op = self.optimizer.minimize(self.loss_op)
        if pre_train:
            self.pre_train(training_data)
        else:
            self.sess.run(tf.global_variables_initializer())

        n_samples, input_dims = training_data.shape
        last_mean = 0
        for epoch in range(self.n_epochs):
            batchinds = np.arange(n_samples // self.batchsize)
            rng.shuffle(batchinds)
            c = []
            for bii, bi in enumerate(batchinds):

                self.sess.run(train_op, feed_dict={self.input: training_data[bi*self.batchsize: (bi+1)*self.batchsize]})
                c.append(self.sess.run(self.loss_op, feed_dict={self.input: training_data[bi*self.batchsize: (bi+1)*self.batchsize],
                                                                }))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 0,
                                                                          np.mean(c)))
                    sys.stdout.flush()
            curr_mean = np.mean(c)
            diff_mean, last_mean = curr_mean-last_mean, curr_mean
            print('\r[Epoch %i] 100.0%% mean %.5f diff %.5f %s' %
                (epoch, curr_mean, diff_mean, str(datetime.now())[11:19]))
            
    def load_model(self, model_file):
        self.saver.restore(self.sess, model_file)

    def save_model(self, model_file):
        save_path = self.saver.save(self.sess, model_file)
        print("Model saved in file: %s " % save_path)