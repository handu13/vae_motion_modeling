# encoding: UTF-8
import tensorflow as tf
import numpy as np
from datetime import datetime
import sys
rng = np.random.RandomState(23456)
sys.path.append(r'../')
from nn.pooling import average_pooling_1d


class CNNRegressionModel(object):

    def __init__(self, name, input_dims, output_dims, n_frames, sess=None):
        self.name = name
        self.input_dims = input_dims
        self.output_dims = output_dims
        self.n_frames = n_frames
        if sess is not None:
            self.sess = sess
        else:
            self.sess = tf.InteractiveSession()

    # def create_model(self, reuse=tf.AUTO_REUSE):
    #     with tf.variable_scope(self.name, reuse=reuse):
    #         self.input = tf.placeholder(dtype=tf.float32, shape=(None, self.input_dims, self.n_frames))
    #         self.output = tf.placeholder(dtype=tf.float32, shape=(None, self.output_dims, self.n_frames))
    #
    #         self.conv1 = tf.layers.conv1d(self.input, 64, 15, padding='same', activation=tf.nn.relu,
    #                                       data_format='channels_first')
    #         self.dropout1 = tf.layers.dropout(self.conv1, rate=0.25)
    #
    #         self.conv2 = tf.layers.conv1d(self.dropout1, 128, 15, padding='same', activation=tf.nn.relu,
    #                                       data_format='channels_first')
    #         self.dropout2 = tf.layers.dropout(self.conv2, rate=0.25)
    #         self.conv3 = tf.layers.conv1d(self.dropout2, self.output_dims, 15, padding='same', activation=None,
    #                                       data_format='channels_first')
    #         self.model_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.name)
    #         self.saver = tf.train.Saver(self.model_params)
    #         self.loss = tf.reduce_mean(tf.pow(self.conv3 - self.output, 2))

    def create_model(self, reuse=tf.AUTO_REUSE, train=True):
        '''
        Three layer cnn regression models
        :param reuse: 
        :return: 
        '''
        with tf.variable_scope(self, reuse=tf.AUTO_REUSE):
            self.input = tf.placeholder(dtype=tf.float32, shape=(None, self.input_dims, self.n_frames))
            self.output = tf.placeholder(dtype=tf.float32, shape=(None, self.output_dims, self.n_frames//2))
            if train:
                self.dropout1 = tf.layers.dropout(self.input, rate=0.25)
            else:
                self.dropout1 = self.input
            self.conv1 = tf.layers.conv1d(self.dropout1, 64, 15, padding='same', activation=tf.nn.relu,
                                          data_format='channels_first')
            if train:
                self.dropout2 = tf.layers.dropout(self.conv1, rate=0.25)
            else:
                self.dropout2 = self.conv1
            self.conv2 = tf.layers.conv1d(self.dropout2, 128, 11, padding='same', activation=tf.nn.relu,
                                          data_format='channels_first')
            if train:
                self.dropout3 = tf.layers.dropout(self.conv2, rate=0.25)
            else:
                self.dropout3 = self.conv2
            self.conv3 = tf.layers.conv1d(self.dropout3, self.output_dims, 9, padding='same', activation=None,
                                          data_format='channels_first')
            self.pooling = tf.layers.average_pooling1d(self.conv3, 2, strides=2, data_format='channels_first')
            self.model_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.name)
            self.output_op = self.pooling
            self.saver = tf.train.Saver(self.model_params)
            self.loss = tf.reduce_mean(tf.pow(self.output_op - self.output, 2))

    def create_model_channels_last(self, reuse=tf.AUTO_REUSE, train=True):
        with tf.variable_scope(self.name, reuse=reuse):
            self.input = tf.placeholder(dtype=tf.float32, shape=(None, self.n_frames, self.input_dims))
            self.output = tf.placeholder(dtype=tf.float32, shape=(None, self.n_frames//2, self.output_dims))
            if train:
                self.dropout1 = tf.layers.dropout(self.input, rate=0.25)
            else:
                self.dropout1 = self.input
            self.conv1 = tf.layers.conv1d(self.dropout1, 64, 15, padding='same', activation=tf.nn.relu,
                                          data_format='channels_last')
            if train:
                self.dropout2 = tf.layers.dropout(self.conv1, rate=0.25)
            else:
                self.dropout2 = self.conv1
            self.conv2 = tf.layers.conv1d(self.dropout2, 128, 11, padding='same', activation=tf.nn.relu,
                                          data_format='channels_last')
            if train:

                self.dropout3 = tf.layers.dropout(self.conv2, rate=0.25)
            else:
                self.dropout3 = self.conv2
            self.conv3 = tf.layers.conv1d(self.dropout3, self.output_dims, 9, padding='same', activation=None,
                                          data_format='channels_last')
            # self.pooling = tf.layers.average_pooling1d(self.conv3, 2, strides=2, data_format='channels_last')
            self.pooling = average_pooling_1d(self.conv3, 2, 2, 'SAME', data_format='channels_last')
            self.model_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.name)
            self.output_op = self.pooling
            self.saver = tf.train.Saver(self.model_params)
            self.loss = tf.reduce_mean(tf.pow(self.output_op - self.output, 2))
            
    def create_footstepper_model(self, reuse=tf.AUTO_REUSE):
        with tf.variable_scope(self.name, reuse=reuse):
            self.input = tf.placeholder(dtype=tf.float32, shape=(None, self.input_dims, self.n_frames))
            self.output = tf.placeholder(dtype=tf.float32, shape=(None, self.output_dims, self.n_frames))
            self.dropout1 = tf.layers.dropout(self.input, rate=0.25)
            self.conv1 = tf.layers.conv1d(self.dropout1, 64, 15, padding='same', activation=tf.nn.relu,
                                          data_format='channels_first')
            self.dropout2 = tf.layers.dropout(self.conv1, rate=0.25)

            self.conv2 = tf.layers.conv1d(self.dropout2, 128, 11, padding='same', activation=tf.nn.relu,
                                          data_format='channels_first')
            self.dropout3 = tf.layers.dropout(self.conv2, rate=0.25)
            self.conv3 = tf.layers.conv1d(self.dropout2, self.output_dims, 9, padding='same', activation=None,
                                          data_format='channels_first')
            self.model_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.name)
            self.output_op = self.conv3
            self.saver = tf.train.Saver(self.model_params)
            self.loss = tf.reduce_mean(tf.pow(self.output_op - self.output, 2))

    def train(self, X, Y, n_epoches, batchsize, learning_rate=0.001):
        self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        train_op = self.optimizer.minimize(self.loss)
        self.sess.run(tf.global_variables_initializer())
        n_samples, n_features, n_frames = X.shape
        last_mean = 0
        for epoch in range(n_epoches):
            batchinds = np.arange(n_samples // batchsize)
            rng.shuffle(batchinds)
            c = []
            for bii, bi in enumerate(batchinds):

                self.sess.run(train_op,
                              feed_dict={self.input: X[bi * batchsize: (bi + 1) * batchsize],
                                         self.output: Y[bi * batchsize: (bi + 1) * batchsize]})
                c.append(self.sess.run(self.loss, feed_dict={self.input: X[bi * batchsize: (bi + 1) * batchsize],
                                                             self.output: Y[bi * batchsize: (bi + 1) * batchsize]}))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii) / len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
            curr_mean = np.mean(c)
            diff_mean, last_mean = curr_mean - last_mean, curr_mean
            print('\r[Epoch %i] 100.0%% mean %.5f diff %.5f %s' %
                  (epoch, curr_mean, diff_mean, str(datetime.now())[11:19]))

    def __call__(self, input_data):
        return self.sess.run(self.output_op, feed_dict={self.input: input_data})

    def load_model(self, model_file):
        self.saver.restore(self.sess, model_file)

    def save_model(self, model_file):
        save_path = self.saver.save(self.sess, model_file)
        print("Model saved in file: %s " % save_path)

    def fine_tuning(self, X, Y, n_epoches, batchsize, learning_rate, model_filename):
        self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        train_op = self.optimizer.minimize(self.loss)
        # self.sess.run(tf.global_variables_initializer())
        self.sess.run(tf.variables_initializer(self.optimizer.variables()))
        self.load_model(model_filename)
        n_samples, n_features, n_frames = X.shape
        last_mean = 0
        for epoch in range(n_epoches):
            batchinds = np.arange(n_samples // batchsize)
            rng.shuffle(batchinds)
            c = []
            for bii, bi in enumerate(batchinds):

                self.sess.run(train_op,
                              feed_dict={self.input: X[bi * batchsize: (bi + 1) * batchsize],
                                         self.output: Y[bi * batchsize: (bi + 1) * batchsize]})
                c.append(self.sess.run(self.loss, feed_dict={self.input: X[bi * batchsize: (bi + 1) * batchsize],
                                                             self.output: Y[bi * batchsize: (bi + 1) * batchsize]}))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii) / len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
            curr_mean = np.mean(c)
            diff_mean, last_mean = curr_mean - last_mean, curr_mean
            print('\r[Epoch %i] 100.0%% mean %.5f diff %.5f %s' %
                  (epoch, curr_mean, diff_mean, str(datetime.now())[11:19]))