# encoding: UTF-8
import tensorflow as tf
import numpy as np
from datetime import datetime
import sys

rng = np.random.RandomState(23456)



class MotionVaeEncoderSingleLayer(object):
    def __init__(self, npc, input_dims, n_epochs, name, batchsize, encoder_activation=None, decoder_activation=None,
                 n_random_samples=1):
        self.npc = npc
        self.input_dims = input_dims
        self.n_epochs = n_epochs
        self.name = name
        self.batchsize = batchsize
        self.encoder_activation = encoder_activation
        self.decoder_activation = decoder_activation
        self.n_random_samples = n_random_samples
        self.sess = tf.InteractiveSession()

    @staticmethod
    def fully_connected_layer(input, hidden_num, name='fully_connected_layer', activation=None,
                              reuse=tf.AUTO_REUSE, batchnorm=True):
        with tf.variable_scope(name, reuse=reuse):
            layer_output = tf.layers.dense(input, hidden_num)
            if activation is None and batchnorm:
                layer_output = tf.contrib.layers.batch_norm(layer_output,
                                                            center=True,
                                                            scale=True,
                                                            is_training=True,
                                                            scope='bn')
                return layer_output
            elif activation is None and not batchnorm:
                return layer_output
            else:
                layer_output = tf.contrib.layers.batch_norm(layer_output,
                                                            center=True,
                                                            scale=True,
                                                            is_training=True,
                                                            scope='bn')
                return activation(layer_output)

    def encode(self, input, reuse):
        encoder = MotionVaeEncoderSingleLayer.fully_connected_layer(input,
                                                                    hidden_num=1024,
                                                                    name='encoder',
                                                                    activation=self.encoder_activation,
                                                                    reuse=reuse)

        mean_encoder = MotionVaeEncoderSingleLayer.fully_connected_layer(encoder,
                                                                         hidden_num=self.npc,
                                                                         name='mean_encoder',
                                                                         activation=None,
                                                                         reuse=reuse)

        log_sigma_encoder = MotionVaeEncoderSingleLayer.fully_connected_layer(encoder,
                                                                              hidden_num=self.npc,
                                                                              name='log_sigma_encoder',
                                                                              activation=None,
                                                                              reuse=reuse)
        return mean_encoder, log_sigma_encoder

    def create_sample(self, mu, sigma):
        eps = tf.random_normal(tf.shape(mu), dtype=tf.float32, mean=0., stddev=1.0, name='epsilon')
        sample = mu + tf.exp(sigma/2) * eps * 0
        return sample

    def decode(self, latent_input, reuse):
        decoder_2 = MotionVaeEncoderSingleLayer.fully_connected_layer(latent_input,
                                                                      hidden_num=1024,
                                                                      name='decoder_2',
                                                                      activation=self.decoder_activation,
                                                                      reuse=reuse)

        decoder_1 = MotionVaeEncoderSingleLayer.fully_connected_layer(decoder_2,
                                                                      hidden_num=self.input_dims,
                                                                      name='decoder_1',
                                                                      activation=None,
                                                                      reuse=reuse,
                                                                      batchnorm=False)
        return decoder_1

    def create_model(self, reuse=tf.AUTO_REUSE):
        with tf.variable_scope(self.name, reuse=reuse):
            self.input = tf.placeholder(dtype=tf.float32, shape=(None, self.input_dims), name='input')
            self.latent_input = tf.placeholder(dtype=tf.float32, shape=(None, self.npc), name='latent_input')
            ### initialize encoder
            mean_encoder, log_sigma_encoder = self.encode(self.input, reuse=reuse)
            # Sampler: Normal (gaussian) random distribution

            self.z_op = self.create_sample(mean_encoder, log_sigma_encoder)
            ### initialize decoder
            decoder_1 = self.decode(self.z_op, reuse=reuse)

        ### loss is the sum or reconstruction error and KL distance between normal distribution and mapped distribution
        reconstruction_loss = self.input_dims * tf.reduce_mean(tf.pow(decoder_1 - self.input, 2))
        # reconstruction_loss = 0.1 * tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(logits=decoder_1, labels=self.input), 1)
        kl_divergence_loss = -0.5 * tf.reduce_sum(1 + log_sigma_encoder - tf.square(mean_encoder) - tf.exp(log_sigma_encoder), 1)
        # self.cost = tf.reduce_mean(reconstruction_loss + kl_divergence_loss)
        self.reconstruction_loss = tf.reduce_mean(reconstruction_loss)
        self.kl_divergence_loss = tf.reduce_mean(kl_divergence_loss)
        self.cost = self.reconstruction_loss
        # self.output_op = tf.nn.sigmoid(self.decode_data(self.latent_input, reuse=reuse))
        self.output_op = self.decode(self.latent_input, reuse=reuse)
        self.model_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.name)

        self.saver = tf.train.Saver(self.model_params)

        self.mean_op = mean_encoder

        self.variance_op = log_sigma_encoder

    def get_params(self):
        return self.model_params

    def get_mean(self, input_data):
        return self.sess.run(self.mean_op, feed_dict={self.input: input_data})

    def get_variance(self, input_data):
        return self.sess.run(self.variance_op, feed_dict={self.input: input_data})

    def __call__(self, input_data):
        z_value = self.sess.run(self.z_op, feed_dict={self.input: input_data})
        return self.sess.run(self.output_op, feed_dict={self.latent_input: z_value})

    def pre_train(self, training_data):
        n_samples, input_dims = training_data.shape
        with tf.variable_scope(self.name, reuse=tf.AUTO_REUSE):
            layer1_input = tf.placeholder(dtype=tf.float32, shape=(None, input_dims))

            encoder_layer1_output = MotionVaeEncoderSingleLayer.fully_connected_layer(layer1_input,
                                                                                      hidden_num=1024,
                                                                                      name='encoder',
                                                                                      activation=self.encoder_activation)
            layer1_output = MotionVaeEncoderSingleLayer.fully_connected_layer(encoder_layer1_output,
                                                                              hidden_num=input_dims,
                                                                              name='decoder_1',
                                                                              activation=None)
            layer1_loss = tf.reduce_mean(tf.pow(layer1_output - layer1_input, 2))
            layer1_trainer = self.optimizer.minimize(layer1_loss)

            # layer2_input = tf.placeholder(dtype=tf.float32, shape=(None, 1024))
            # mean_output = MotionVaeEncoderSingleLayer.fully_connected_layer(layer2_input,
            #                                                                 hidden_num=self.npc,
            #                                                                 name='mean_encoder',
            #                                                                 activation=None)
            # # var_output = MotionVaeEncoderSingleLayer.fully_connected_layer(layer2_input,
            # #                                                                hidden_num=self.npc,
            # #                                                                name='log_sigma_encoder')
            # layer2_output = MotionVaeEncoderSingleLayer.fully_connected_layer(mean_output,
            #                                                                   hidden_num=1024,
            #                                                                   name='decoder_2',
            #                                                                   activation=self.decoder_activation)
            # layer2_loss = tf.reduce_mean(tf.pow(layer2_output - layer2_input, 2))
            # layer2_trainer = self.optimizer.minimize(layer2_loss)

            
        self.sess.run(tf.global_variables_initializer())
        print('pre-train layer1')
        for epoch in range(self.n_epochs):
            self.sess.run(layer1_trainer, feed_dict={layer1_input: training_data})
            err = self.sess.run(layer1_loss, feed_dict={layer1_input: training_data})
            sys.stdout.write('\r[Epoch %i] error %.5f' % (epoch, err))
        print('')

        # layer1_res = self.sess.run(encoder_layer1_output, feed_dict={layer1_input: training_data})
        # print('pre-train layer2')
        # for epoch in range(self.n_epochs):
        #     self.sess.run(layer2_trainer, feed_dict={layer2_input: layer1_res})
        #     err = self.sess.run(layer2_loss, feed_dict={layer2_input: layer1_res})
        #     sys.stdout.write('\r[Epoch %i] error %.5f' % (epoch, err))
        # print('')

    def train(self, training_data, learning_rate=0.01, pre_train=True):
        self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        train_op = self.optimizer.minimize(self.cost)
        if pre_train:
            self.pre_train(training_data)
        else:
            self.sess.run(tf.global_variables_initializer())

        n_samples, input_dims = training_data.shape
        last_mean = 0
        for epoch in range(self.n_epochs):
            batchinds = np.arange(n_samples // self.batchsize)
            rng.shuffle(batchinds)
            c = []
            c_reconstruction = []
            c_kl = []
            for bii, bi in enumerate(batchinds):
                for i in range(self.n_random_samples):
                    # z_value = self.sess.run(self.z_op, feed_dict={
                    #     self.input: training_data[bi * self.batchsize: (bi + 1) * self.batchsize]})

                    self.sess.run(train_op, feed_dict={self.input: training_data[
                                                                   bi * self.batchsize: (bi + 1) * self.batchsize]})
                    c.append(self.sess.run(self.cost, feed_dict={
                        self.input: training_data[bi * self.batchsize: (bi + 1) * self.batchsize]}))
                    c_reconstruction.append(self.sess.run(self.reconstruction_loss, feed_dict={
                        self.input: training_data[bi * self.batchsize: (bi + 1) * self.batchsize]
                    }))
                    c_kl.append(self.sess.run(self.kl_divergence_loss, feed_dict={
                        self.input: training_data[bi * self.batchsize: (bi + 1) * self.batchsize]
                    }))
                    if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 10) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii) / len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
            curr_mean = np.mean(c)
            diff_mean, last_mean = curr_mean - last_mean, curr_mean
            print('\r[Epoch %i] 100.0%% mean %.5f diff %.5f %s' %
                  (epoch, curr_mean, diff_mean, str(datetime.now())[11:19]))
            print()
            print('reconstruction error is: ', np.mean(c_reconstruction))

            print()
            print('KL divergenct error is: ', np.mean(c_kl))

            # print()


    def load_model(self, model_file):
        self.saver.restore(self.sess, model_file)

    def save_model(self, model_file):
        save_path = self.saver.save(self.sess, model_file)
        print("Model saved in file: %s " % save_path)

    def generate_new_samples(self, n_samples):

        new_samples = np.random.randn(n_samples, self.npc) 
        # print(new_samples)
        res = self.sess.run(self.output_op, feed_dict={self.latent_input: new_samples})
        return res
    
    def backproject(self, input_data):
        return self.sess.run(self.output_op, feed_dict={self.latent_input: input_data})