# encoding: UTF-8

import os
import sys
import tensorflow as tf
import numpy as np
from deepMotionSim.utils import get_joint_position_data, export_joint_position_to_cloud_data
from deepMotionSim.models.test_models import motion_encoder_channel_first, motion_decoder_channel_first
rng = np.random.RandomState(23456)
from datetime import datetime


def apply_convolutional_autoencoder_for_cartesian_data():
    '''

    :return:
    '''

    ### load data
    elementary_action = 'walk'
    motion_primitive = 'leftStance_game_engine_skeleton_new_grounded'
    data_filename = '_'.join([elementary_action, motion_primitive, 'cartesian_data_with_filenamelist.npz'])
    data_filename = os.path.join('data', data_filename)
    if not os.path.exists(data_filename):

        cartesian_motion, filename_list = get_joint_position_data(elementary_action,
                                                                  motion_primitive)

        n_samples, n_frames, n_dims = cartesian_motion.shape
        # export_data = joint_position_data[0]
        # n_joints = export_data.shape[1] // 3
        # export_data = np.reshape(export_data, (export_data.shape[0], n_joints, 3))
        # export_joint_position_to_cloud_data(export_data, r'E:\tmp\joint_position.panim')
        Xmean = cartesian_motion.mean(axis=1).mean(axis=0)[np.newaxis, np.newaxis, :]
        Xstd = np.array([[[cartesian_motion.std()]]]).repeat(n_dims, axis=2)

        np.savez_compressed(data_filename, Motion_data=cartesian_motion, Xmean=Xmean, Xstd=Xstd,
                            filename_list=filename_list)
    else:
        motion_data = np.load(data_filename)
        cartesian_motion = motion_data['Motion_data']
        filename_list = motion_data['filename_list']
        Xmean = motion_data['Xmean']
        Xstd = motion_data['Xstd']
    n_samples, n_frames, n_dims = cartesian_motion.shape
    print(cartesian_motion.shape)
    input_dims = n_frames * n_dims
    normalized_data = (cartesian_motion - Xmean) / Xstd
    print(normalized_data.shape)

    X = np.swapaxes(normalized_data, 1, 2)
    batchsize = 10
    n_epochs = 250
    learning_rate = 0.00001
    input = tf.placeholder(tf.float32, shape=[None, n_dims, n_frames])
    encoder_op = motion_encoder_channel_first(input, name='cnn_encoder', hidden_units=128, pooling='average',
                                              kernel_size=15, batch_normalization=True)

    decoder_op = motion_decoder_channel_first(encoder_op, n_dims, name='cnn_decoder', unpool='average', kernel_size=15)
    loss_op = tf.reduce_mean(tf.pow(input - decoder_op, 2))
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train_op = optimizer.minimize(loss_op)
    encoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='cnn_encoder')
    decoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='cnn_decoder')
    saver = tf.train.Saver(encoder_params + decoder_params)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        last_mean = 0
        for epoch in range(n_epochs):
            batchinds = np.arange(n_samples // batchsize)
            rng.shuffle(batchinds)
            c = []

            for bii, bi in enumerate(batchinds):
                # sess.run(train_op, feed_dict={input: X[bi*batchsize: (bi+1)*batchsize],
                #                               output: Y[bi*batchsize: (bi+1)*batchsize]})
                # c.append(sess.run(loss_op, feed_dict={input: X[bi*batchsize: (bi+1)*batchsize],
                #                                       output: Y[bi * batchsize: (bi + 1) * batchsize]}))
                sess.run(train_op, feed_dict={input: X[bi*batchsize: (bi+1)*batchsize]})
                c.append(sess.run(loss_op, feed_dict={input: X[bi*batchsize: (bi+1)*batchsize]}))
                if np.isnan(c[-1]): return
                if bii % (int(len(batchinds) / 1000) + 1) == 0:
                    sys.stdout.write('\r[Epoch %i]  %0.1f%% mean %.5f' % (epoch, 100 * float(bii)/len(batchinds),
                                                                          np.mean(c)))
                    sys.stdout.flush()
            curr_mean = np.mean(c)
            diff_mean, last_mean = curr_mean-last_mean, curr_mean
            print('\r[Epoch %i] 100.0%% mean %.5f diff %.5f %s' %
                (epoch, curr_mean, diff_mean, str(datetime.now())[11:19]))

        decoded_motion = sess.run(decoder_op, feed_dict={input: X})
        decoded_motion = np.swapaxes(decoded_motion, 1, 2)
        decoded_motion = (decoded_motion * Xstd) + Xmean
        reconstruction_err = np.mean((decoded_motion - cartesian_motion)**2)
        print('reconstruction error is: ', reconstruction_err)


if __name__ == "__main__":
    apply_convolutional_autoencoder_for_cartesian_data()