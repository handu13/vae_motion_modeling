# encoding: UTF-8

import tensorflow as tf
import numpy as np
import os
from deepMotionSim.models.test_models import motion_decoder_channel_first, motion_encoder_channel_first, \
    motion_encoder_without_pooling, motion_decoder_without_pooling
from deepMotionSim.utils import export_point_cloud_data, GAME_ENGINE_SKELETON, combine_motion_clips, get_input_data, get_encoded_data
from morphablegraphs.python_src.morphablegraphs.animation_data import BVHReader, SkeletonBuilder
from deepMotionSim.utils import export_point_cloud_data_without_foot_contact, retarget_panim_data_to_game_engine_skeleton, get_joint_position_data_from_anim
from deepMotionSim.models.conditional_vae_autoencoder import ConditionalMotionVaeEncoder
from deepMotionSim.models.conv_autoencoder import ConvAutoEncoder
rng = np.random.RandomState(23456)


def demo_cvae():
    style_elementary_actions = ['oldWalk', 'sexyWalk', 'childlikeWalk', 'depressedWalk', 'angryWalk', 'proudWalk']
    motion_primitives = ['leftStance', 'rightStance', 'beginLeftStance', 'beginRightStance']

    style_elementary_action = style_elementary_actions[0]
    motion_primitive_name = motion_primitives[0]
    ### load conditional motion variational autoencoder

    n_epoches = 250
    batchsize = 10
    learning_rate = 0.001
    npc = 32
    # npc = 64
    n_frames = 30
    n_dims = 128
    model_filename = '_'.join([style_elementary_action, motion_primitive_name, 'cvae', str(learning_rate),
                               str(n_epoches), str(batchsize), str(npc)]) + '.ckpt'
    model_path = r'../data/training_data/cvae_models'
    model_file = os.path.join(model_path, model_filename)
    # model_file = r'E:\workspace\projects\variational_style_simulation\trained_networks\oldWalk_leftStance_cvae_0.001_500_10_64.ckpt'
    sess = tf.InteractiveSession()
    cvae_model_name = '_'.join([style_elementary_action, motion_primitive_name, 'cvae'])
    c_m = ConditionalMotionVaeEncoder(npc=npc, n_frames=n_frames, n_dims=n_dims, n_epoches=n_epoches,
                                      name=cvae_model_name, batchsize=batchsize,
                                      encoder_activation=tf.nn.tanh, decoder_activation=tf.nn.tanh,
                                      n_random_samples=1, sess=sess)
    c_m.create_model()
    c_m.load_model(model_file)

    #### create random samples from the model
    N = 10
    new_samples = c_m.generate_new_samples(N)

    #### decode motion

    n_dims = 66
    n_frames = 60
    kernel_size = 9
    encode_activation = tf.nn.relu
    decode_activation = None
    hidden_units = 128
    n_epoches = 250
    batchsize = 10

    meta_data = np.load(r'E:\workspace\tensorflow_results\tags\tag_data\preprocess_meta_locomotion_small_window.npz')
    Xmean = meta_data['Xmean']
    Xstd = meta_data['Xstd']
    model_filename = r'E:\workspace\tensorflow_results\tags\tag_models\conv_autoencoder_small_window_1e-05_250_10_128.ckpt'
    m = ConvAutoEncoder(name='motion_autoencoder', n_frames=n_frames, n_dims=n_dims, kernel_size=kernel_size,
                        encode_activation=encode_activation, decode_activation=decode_activation,
                        hidden_units=hidden_units, pooling='average', batch_norm=False, sess=sess)
    m.create_model(batchsize)
    m.load_model(model_filename)
    new_motions = []
    for i in range(N):
        new_motions.append(m.decode_data(new_samples[i][np.newaxis, :, :])[0])
    new_motions = np.asarray(new_motions)
    new_motions = np.swapaxes(new_motions, 1, 2)
    new_motions = (new_motions * Xstd) + Xmean
    save_folder = r'../data/generated_motion'
    save_folder = os.path.join(save_folder, style_elementary_action, motion_primitive_name)
    if not os.path.exists(save_folder):
        os.makedirs(save_folder)
    skeleton_file = r'../data/example_skeletons/game_engine_target.bvh'
    bvhreader = BVHReader(skeleton_file)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader)
    for i in range(N):
        filename = 'new_sample_' + str(i) + '.panim'
        export_point_cloud_data_without_foot_contact(new_motions[i], os.path.join(save_folder, filename))
        joint_pos = get_joint_position_data_from_anim(new_motions[i])
        motion_vector = retarget_panim_data_to_game_engine_skeleton(joint_pos, skeleton_file)
        motion_vector.export(skeleton, os.path.join(save_folder, filename.replace('.panim', '.bvh')))
    m.sess.close()


if __name__ == "__main__":
    demo_cvae()