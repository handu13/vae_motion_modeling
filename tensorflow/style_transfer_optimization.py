# encoding: UTF-8
import numpy as np
from preprocessing import process_file, process_motion_vector, GAME_ENGINE_ANIMATED_JOINTS
import tensorflow as tf
from deepMotionSim.models.test_models import motion_encoder_channel_first, motion_decoder_channel_first
from deepMotionSim.utils import gram_matrix, export_point_cloud_data, convert_anim_data_to_point_cloud, retarget_panim_data, \
    export_joint_position_to_cloud_data, combine_motion_clips, export_point_cloud_data_without_foot_contact
from constraints import constrain, multiconstraint, foot_sliding, joint_lengths, trajectory
from morphablegraphs.python_src.morphablegraphs.animation_data import BVHReader, SkeletonBuilder, MotionVector, BVHWriter
import os
from scipy.ndimage.filters import gaussian_filter1d
from morphablegraphs.python_src.morphablegraphs.construction.preprocessing.motion_spline_smoothing import adjust_mv_framelegnth



def cut_frames(frames, window=240, window_step=120):
    windows = []
    if len(frames) % window_step == 0:
        n_clips = (len(frames) - len(frames) % window_step) // window_step
    else:
        n_clips = (len(frames) - len(frames) % window_step) // window_step + 1
    for j in range(0, n_clips):
        """ If slice too small pad out by repeating start and end poses """
        slice = frames[j * window_step: j * window_step + window]
        if len(slice) < window:
            left = slice[:1].repeat((window - len(slice)) // 2 + (window - len(slice)) % 2, axis=0)
            right = slice[-1:].repeat((window - len(slice)) // 2, axis=0)
            slice = np.concatenate([left, slice, right], axis=0)
        if len(slice) != window: raise Exception()

        windows.append(slice)
    return np.asarray(windows)


def convert_motion_into_clips(bvhfile):
    bvhreader = BVHReader(bvhfile)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader)
    clips = cut_frames(bvhreader.frames)
    save_folder = r'E:\workspace\tmp'
    for i in range(len(clips)):
        filename = os.path.split(bvhfile)[-1][:-4] + '_' + str(i) + '.bvh'
        BVHWriter(os.path.join(save_folder, filename), skeleton, clips[i], frame_time=skeleton.frame_time,
                  is_quaternion=False)


def style_transfer_piece_wise(content_mv, style_mv):
    preprocess = np.load(r'E:\workspace\tensorflow_results\data\preprocessed_core_channel_first.npz')
    n_cnontent_frames = len(content_mv.frames)

    cnt_motion_clips = process_motion_vector(content_mv)
    cnt_motion_clips = np.swapaxes(cnt_motion_clips, 1, 2)
    n_content_clips = len(cnt_motion_clips)
    n_frames = 240
    n_dims = 70
    # style_motion_clips = process_motion_vector(style_mv, sliding_window=True)
    # style_motion_clips = np.swapaxes(style_motion_clips, 1, 2)
    style_motion_data = process_motion_vector(style_mv, sliding_window=False)
    if len(style_motion_data) % 2 != 0:
        style_motion_data = style_motion_data[:-1]
    # n_style_clips = len(style_motion_clips)
    normalized_motion_clips = (cnt_motion_clips - preprocess['Xmean']) / preprocess['Xstd']
    # normalized_style_clips = (style_motion_clips - preprocess['Xmean']) / preprocess['Xstd']
    n_style_frames, n_dims = style_motion_data.shape

    style_motion_data = np.swapaxes(style_motion_data, 0, 1)[np.newaxis, :, :]
    normalized_style_motion_data = (style_motion_data - preprocess['Xmean']) / preprocess['Xstd']
    """ Computational Graph Construction """

    """ Encode content motion """
    content_input = tf.placeholder(tf.float32, shape=(1, n_dims, n_frames))
    content_encoder_op = motion_encoder_channel_first(content_input, name='encoder', hidden_units=256, pooling='average')
    """ Encode style motion """
    # style_input = tf.placeholder(tf.float32, shape=(n_style_clips, n_dims, n_frames))
    style_input = tf.placeholder(tf.float32, shape=(1, n_dims, n_style_frames))
    style_encoder_op = motion_encoder_channel_first(style_input, name='encoder', hidden_units=256, pooling='average')

    style_gram_op = gram_matrix(style_encoder_op)
    # style_gram_op = tf.expand_dims(tf.reduce_mean(style_gram_op, axis=0), 0)
    # print(style_gram_op.shape)

    n_hidden_units = 256
    H = tf.Variable(initial_value=tf.random_normal(shape=[1, n_hidden_units, int(n_frames / 2)]), dtype=tf.float32)
    H_decoder = motion_decoder_channel_first(H, n_dims, name='decoder')
    # print('H decoder shape: ', H_decoder.shape)
    H_decoder_motion = (H_decoder * preprocess['Xstd']) + preprocess['Xmean']
    s = 500.0
    c = 1.0
    loss_op = c * tf.reduce_mean(tf.pow(H - content_encoder_op, 2)) + s * tf.reduce_mean(
        tf.pow(gram_matrix(H) - style_gram_op, 2))
    optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
    train_op = optimizer.minimize(loss_op, var_list=[H])
    encoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='encoder')
    decoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='decoder')
    encoder_saver = tf.train.Saver(encoder_params + decoder_params)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.InteractiveSession(config=config)
    encoder_saver.restore(sess, r'E:\workspace\tensorflow_results\data\core_network_average_pooling_500.ckpt')
    sess.run(tf.variables_initializer(var_list=optimizer.variables()))
    epoches = 250
    reconstructed_clips = []

    for i in range(n_content_clips):
        encoded_clip_value = sess.run(content_encoder_op, feed_dict={content_input: normalized_motion_clips[i:i+1]})
        assign_op = H.assign(encoded_clip_value)
        sess.run(assign_op)
        for epoch in range(epoches):
            sess.run(train_op, feed_dict={style_input: normalized_style_motion_data,
                                          content_input: normalized_motion_clips[i:i+1]})
            # print(sess.run(loss_op, feed_dict={style_input: normalized_style_clips,
            #                                    content_input: normalized_motion_clips[i:i + 1]}))
        trsf_motion = sess.run(H_decoder_motion)
        trsfvel = np.mean(np.sqrt(trsf_motion[:, -7:-6] ** 2 + trsf_motion[:, -6:-5] ** 2), axis=2)[:, :, np.newaxis]
        cntvel = np.mean(np.sqrt(cnt_motion_clips[i: i+1, -7:-6] ** 2 + cnt_motion_clips[i: i+1, -6:-5] ** 2), axis=2)[:, :, np.newaxis]
        labels = trsf_motion[:, -4:, :]
        traj_value = trsfvel * (cnt_motion_clips[i: i+1, -7:-4, :]/cntvel)
        traj_value[:, -1, :] = cnt_motion_clips[i: i+1, -5, :]
        # constrain_op = multiconstraint(foot_sliding(labels), joint_lengths(), trajectory(traj_value))(H_decoder_motion)
        constrain_op = multiconstraint(joint_lengths(), foot_sliding(labels))(H_decoder_motion)
        constrain_train_op = optimizer.minimize(constrain_op, var_list=[H])
        for epoch in range(epoches):
            sess.run(constrain_train_op)
        reconstructed_motion = sess.run(H_decoder)
        # print('reconstructed motion shape: ', reconstructed_motion.shape)
        reconstructed_motion = (reconstructed_motion * preprocess['Xstd']) + preprocess['Xmean']
        # reconstructed_motion[:, -7:-4, :] = traj_value
        #
        # print('############################')
        # print(reconstructed_motion.shape)
        # """ Smooth data """
        # sigma = 2.5
        # for i in range(63):
        #     reconstructed_motion[0, i, :] = gaussian_filter1d(reconstructed_motion[0, i, :], sigma)
        reconstructed_motion = np.swapaxes(reconstructed_motion[0], 0, 1)
        # export_point_cloud_data_without_foot_contact(reconstructed_motion[:, :-4], os.path.join(save_dir, str(i) + '.panim'))
        reconstructed_clips.append(reconstructed_motion)
    sess.close()
    long_clip = combine_motion_clips(reconstructed_clips, motion_len=n_cnontent_frames, window_step=120)
    sigma = 2.5
    for i in range(63):
        long_clip[:, i] = gaussian_filter1d(long_clip[:, i], sigma)

    # export_point_cloud_data_without_foot_contact(long_clip[:, :-4],
    #                                              r'E:\workspace\tensorflow_results\example\result1.panim')
    return long_clip


def style_transfer_clean(content_mv, style_mv):
    preprocess = np.load(r'E:\workspace\tensorflow_results\data\preprocessed_core_channel_first.npz')
    cnt_motion_data = process_motion_vector(content_mv, sliding_window=False)
    if len(cnt_motion_data) % 2 != 0:
        cnt_motion_data = cnt_motion_data[:-1]
        n_cnt_frames, n_dims = cnt_motion_data.shape
    cnt_motion_data = np.swapaxes(cnt_motion_data, 0, 1)[np.newaxis, :, :]
    normalized_cnt_motion_data = (cnt_motion_data - preprocess['Xmean']) / preprocess['Xstd']
    style_motion_data = process_motion_vector(style_mv, sliding_window=False)
    if len(style_motion_data) % 2 != 0:
        style_motion_data = style_motion_data[:-1]
    n_style_frames, n_dims = style_motion_data.shape
    style_motion_data = np.swapaxes(style_motion_data, 0, 1)[np.newaxis, :, :]
    normalized_style_motion_data = (style_motion_data - preprocess['Xmean']) / preprocess['Xstd']
    sess = tf.InteractiveSession()
    batchsize = 1
    ## create content encoder
    content_input = tf.placeholder(tf.float32, shape=[batchsize, n_dims, n_cnt_frames])
    content_encoder_op = motion_encoder_channel_first(content_input, name='encoder', hidden_units=256,
                                                      pooling='average')
    ## create style encdoer
    style_input = tf.placeholder(tf.float32, shape=[batchsize, n_dims, n_style_frames])
    style_encoder_op = motion_encoder_channel_first(style_input, name='encoder', hidden_units=256, pooling='average')
    ## create style gram matrix
    style_gram_op = gram_matrix(style_encoder_op)
    ## initilize new motion
    n_hidden_units = 256
    H = tf.Variable(initial_value=tf.random_normal(shape=[batchsize, n_hidden_units, int(n_cnt_frames/2)]),
                    dtype=tf.float32)
    H_decoder = motion_decoder_channel_first(H, n_dims, name='decoder')
    H_decoder_motion = (H_decoder * preprocess['Xstd']) + preprocess['Xmean']
    encoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='encoder')
    decoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='decoder')
    encoder_saver = tf.train.Saver(encoder_params + decoder_params)
    encoder_saver.restore(sess, r'E:\workspace\tensorflow_results\data\core_network_average_pooling_500.ckpt')
    optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
    s = 100.0
    c = 1.0
    n_epoches = 250
    loss_op = c * tf.reduce_mean(tf.pow(H - content_encoder_op, 2)) + s * tf.reduce_mean(
        tf.pow(gram_matrix(H) - style_gram_op, 2))
    train_op = optimizer.minimize(loss_op, var_list=[H])
    # sess.run(tf.variables_initializer(var_list=optimizer.variables()))
    sess.run(tf.global_variables_initializer())
    encoder_saver.restore(sess, r'E:\workspace\tensorflow_results\data\core_network_average_pooling_500.ckpt')
    content_encoded_value = sess.run(content_encoder_op, feed_dict={
        content_input: normalized_cnt_motion_data})
    assign_op = H.assign(content_encoded_value)
    sess.run(assign_op)

    for epoch in range(n_epoches):
        sess.run(train_op, feed_dict={style_input: normalized_style_motion_data,
                                      content_input: normalized_cnt_motion_data})
    ### optimize motion
    trsf_motion = sess.run(H_decoder_motion)
    trsfvel = np.mean(np.sqrt(trsf_motion[:, -7:-6] ** 2 + trsf_motion[:, -6:-5] ** 2), axis=2)[:, :, np.newaxis]
    cntvel = np.mean(np.sqrt(cnt_motion_data[:, -7:-6] ** 2 + cnt_motion_data[:, -6:-5] ** 2), axis=2)[:, :, np.newaxis]
    labels = trsf_motion[:, -4:, :]
    traj_value = trsfvel * (cnt_motion_data[:, -7:-4, :]/cntvel)
    traj_value[:, -1, :] = cnt_motion_data[:, -5, :]
    # constrain_op = multiconstraint(foot_sliding(labels), joint_lengths(), trajectory(traj_value))(H_decoder_motion)
    constrain_op = multiconstraint(joint_lengths(), foot_sliding(labels))(H_decoder_motion)

    constrain_train_op = optimizer.minimize(constrain_op, var_list=[H])
    for epoch in range(n_epoches):
        sess.run(constrain_train_op)
        # print(sess.run(constrain_op, feed_dict={traj: traj_value}))
    reconstructed_motion = sess.run(H_decoder)
    reconstructed_motion = (reconstructed_motion * preprocess['Xstd']) + preprocess['Xmean']
    reconstructed_motion[:, -7:-4, :] = traj_value
    sigma = 2.5
    for i in range(63):
        reconstructed_motion[0, i, :] = gaussian_filter1d(reconstructed_motion[0, i, :], sigma)
    return reconstructed_motion


def style_transfer_optimization(content_mv, style_mv):
    preprocess = np.load(r'E:\workspace\tensorflow_results\data\preprocessed_core_channel_first.npz')

    # cnt_motion_data = process_file(content_file, sliding_window=False)
    cnt_motion_data = process_motion_vector(content_mv, sliding_window=False)
    ### if the frame number is an odd value, remove last frame for pooling
    if len(cnt_motion_data) % 2 != 0:
        cnt_motion_data = cnt_motion_data[:-1]
    n_cnt_frames, n_dims = cnt_motion_data.shape

    cnt_motion_data = np.swapaxes(cnt_motion_data, 0, 1)[np.newaxis, :, :]

    normalized_cnt_motion_data = (cnt_motion_data - preprocess['Xmean']) / preprocess['Xstd']

    style_motion_data = process_motion_vector(style_mv, sliding_window=False)
    if len(style_motion_data) % 2 != 0:
        style_motion_data = style_motion_data[:-1]
    n_style_frames, n_dims = style_motion_data.shape
    style_motion_data = np.swapaxes(style_motion_data, 0, 1)[np.newaxis, :, :]

    normalized_style_motion_data = (style_motion_data - preprocess['Xmean']) / preprocess['Xstd']

    batchsize = 1
    ## create content encoder
    content_input = tf.placeholder(tf.float32, shape=[batchsize, n_dims, n_cnt_frames])
    content_encoder_op = motion_encoder_channel_first(content_input, name='encoder', hidden_units=256,
                                                      pooling='average')
    ## create style encdoer
    style_input = tf.placeholder(tf.float32, shape=[batchsize, n_dims, n_style_frames])
    style_encoder_op = motion_encoder_channel_first(style_input, name='encoder', hidden_units=256, pooling='average')
    ## create style gram matrix
    style_gram_op = gram_matrix(style_encoder_op)

    ## initilize new motion
    n_hidden_units = 256
    H = tf.Variable(initial_value=tf.random_normal(shape=[batchsize, n_hidden_units, int(n_cnt_frames/2)]),
                    dtype=tf.float32)
    H_decoder = motion_decoder_channel_first(H, n_dims, name='decoder')
    H_decoder_motion = (H_decoder * preprocess['Xstd']) + preprocess['Xmean']
    # input = tf.placeholder(tf.float32, shape=[batchsize, n_dims, n_frames])
    # motion_encoder_op = motion_encoder_channel_first(input, name='encoder', hidden_units=256, pooling='average')
    # motion_decoder_op = motion_decoder_channel_first(motion_encoder_op, n_dims, name='decoder')
    encoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='encoder')
    decoder_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='decoder')
    encoder_saver = tf.train.Saver(encoder_params + decoder_params)
    ## create loss function

    s = 100.0
    c = 1.0
    loss_op = c * tf.reduce_mean(tf.pow(H - content_encoder_op, 2)) + s * tf.reduce_mean(
        tf.pow(gram_matrix(H) - style_gram_op, 2))
    optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
    train_op = optimizer.minimize(loss_op, var_list=[H])

    ## initialize contraints
    traj = tf.placeholder(dtype=tf.float32, shape=(1, 3, n_cnt_frames))
    # labels = cnt_motion_data[:, -4:, :]

    n_epoches = 250
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())

        encoder_saver.restore(sess, r'E:\workspace\tensorflow_results\data\core_network_average_pooling_500.ckpt')

        content_encoded_value = sess.run(content_encoder_op, feed_dict={
            content_input: normalized_cnt_motion_data})
        assign_op = H.assign(content_encoded_value)
        sess.run(assign_op)

        for epoch in range(n_epoches):
            sess.run(train_op, feed_dict={style_input: normalized_style_motion_data,
                                          content_input: normalized_cnt_motion_data})
            # print(sess.run(loss_op, feed_dict={style_input: normalized_style_motion_data,
            #                                    content_input: normalized_cnt_motion_data}))

        ### optimize motion
        trsf_motion = sess.run(H_decoder_motion)
        trsfvel = np.mean(np.sqrt(trsf_motion[:, -7:-6] ** 2 + trsf_motion[:, -6:-5] ** 2), axis=2)[:, :, np.newaxis]
        cntvel = np.mean(np.sqrt(cnt_motion_data[:, -7:-6] ** 2 + cnt_motion_data[:, -6:-5] ** 2), axis=2)[:, :, np.newaxis]
        labels = trsf_motion[:, -4:, :]
        traj_value = trsfvel * (cnt_motion_data[:, -7:-4, :]/cntvel)
        traj_value[:, -1, :] = cnt_motion_data[:, -5, :]
        # constrain_op = multiconstraint(foot_sliding(labels), joint_lengths(), trajectory(traj_value))(H_decoder_motion)
        constrain_op = multiconstraint(joint_lengths(), foot_sliding(labels))(H_decoder_motion)

        constrain_train_op = optimizer.minimize(constrain_op, var_list=[H])
        for epoch in range(n_epoches):
            sess.run(constrain_train_op)
            # print(sess.run(constrain_op, feed_dict={traj: traj_value}))
        reconstructed_motion = sess.run(H_decoder)
        reconstructed_motion = (reconstructed_motion * preprocess['Xstd']) + preprocess['Xmean']
        reconstructed_motion[:, -7:-4, :] = traj_value
        sigma = 2.5
        for i in range(63):
            reconstructed_motion[0, i, :] = gaussian_filter1d(reconstructed_motion[0, i, :], sigma)
        return reconstructed_motion


def create_mv_from_bvhfile(bvhfile):
    bvhreader = BVHReader(bvhfile)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader)
    mv = MotionVector(skeleton)
    # mv.from_bvh_reader(bvhreader, animated_joints=GAME_ENGINE_ANIMATED_JOINTS)
    mv.from_bvh_reader(bvhreader)
    return mv



if __name__ == "__main__":
    content_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\neutral\neutral_normalwalking_28.bvh'
    # content_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\ulm_locomotion\Take_walk\walk_003_1.bvh'
    # content_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\edin\edin_locomotion\locomotion_walk_001_001.bvh'
    # content_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\ulm_locomotion\Take_walk_s\walk_s_003.bvh'
    # content_file = r'E:\workspace\tmp\walk_s_003_3.bvh'
    # content_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\edin\edin_locomotion\locomotion_transitions_000_002.bvh'
    # content_file = r'C:\repo\data\1 - MoCap\4 - Alignment\elementary_action_walk\endLeftStance_game_engine_skeleton_smoothed_grounded\walk_001_2_endleftStance_559_604.bvh'
    # style_file = r'E:\workspace\tensorflow_results\style_examples\depressed\depressed_normalwalking_10.bvh'
    # style_file = r'E:\workspace\tensorflow_results\style_examples\angry\angry_normalwalking_0.bvh'
    # style_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\childlike\childlike_normalwalking_4.bvh'
    # style_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\depressed\depressed_normalwalking_8.bvh'
    # style_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\angry\angry_normalwalking_0.bvh'
    # style_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\old\old_normalwalking_20.bvh'
    # style_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\angry\angry_normalwalking_0.bvh'
    # style_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\proud\proud_normalwalking_23.bvh'
    style_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\sexy\sexy_normalwalking_16.bvh'
    # content_file = r'E:\workspace\tmp\style_samples\neutral_game.bvh'
    content_mv = create_mv_from_bvhfile(content_file)
    origin_frame_length = len(content_mv.frames)
    if len(content_mv.frames) < 240:
        content_mv = adjust_mv_framelegnth(content_mv, 240)
    print(content_mv.n_frames)
    style_mv = create_mv_from_bvhfile(style_file)
    if len(style_mv.frames) < 240:
        style_mv = adjust_mv_framelegnth(style_mv, 240)

    bvhreader = BVHReader(content_file)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader)
    # convert_motion_into_clips(content_file)
    # panim_data = style_transfer_piece_wise(content_mv, style_mv)

    panim_data = style_transfer_optimization(content_mv, style_mv)

    panim_data = np.swapaxes(panim_data[0], 0, 1)

    export_point_cloud_data(panim_data, r'E:\workspace\tmp\style_samples\childlike.panim')
    # export_point_cloud_data(panim_data, r'E:\workspace\tmp\3.panim')
    # export_joint_position_to_cloud_data(panim_data, r'E:\workspace\tmp\style_samples\childlike.panim')
    # new_mv = adjust_mv_framelegnth(new_mv, origin_frame_length)
    # new_mv.export(skeleton, r'E:\workspace\tmp\style_samples\childlike.bvh')