# encoding: UTF-8

import tensorflow as tf
from deepMotionSim.models.test_models import motion_encoder_channel_first as motion_encoder
from deepMotionSim.models.test_models import motion_decoder_channel_first as motion_decoder
from preprocessing import process_file
import numpy as np
from deepMotionSim.utils import export_point_cloud_data



def joint_lengths(
        parents=np.array([-1, 0, 1, 2, 3, 4, 5, 2, 7, 8, 9, 2, 11, 1, 13, 14, 15, 1, 17, 18, 19]),
        lengths=np.array([
            21.89, 26.90, 16.45, 25.38, 26.40, 26.90, 16.45, 25.38,
            26.40, 33.81, 9.40, 11.10, 41.76, 45.32, 14.38, 11.10,
            41.76, 45.32, 14.38])):
    def joint_lengths_constraint(V):
        '''
        
        :param V: n_samples * n_dims * n_frames 
        :return: 
        '''
        input_shape = V.get_shape().as_list()
        J = tf.reshape(V[:, :-7], (input_shape[0], len(parents), 3, input_shape[2]))
        return tf.reduce_mean((tf.sqrt(tf.reduce_sum((J[:, 2:] - tf.gather(J, parents[2:], axis=1)) ** 2, axis=2)) -
                          lengths[np.newaxis, ..., np.newaxis]) ** 2)    #### reshape the lengths vector to 3d array

    return joint_lengths_constraint


def pose_consistence(example_pose):
        ### example_pose: 1 * 1 * 66 suppose one example pose
    def pose_constraint(V):
        '''
        
        :param V: n_samples * n_frames * n_dims, the first 63 dims are corresponding to pose information, we want to const
        :return: 
        '''
        return tf.reduce_mean((V[:, 0:1, :-3] - example_pose[:, :, :-3])**2)
    return pose_constraint


def pose_consistence_minimum(example_pose):
    ### example_pose: 1 * 1 * 66 suppose one example pose
    def pose_constraint(V):
        '''

        :param V: n_samples * n_frames * n_dims, the first 63 dims are corresponding to pose information, we want to const
        :return: 
        '''
        return tf.reduce_min(tf.reduce_mean((V[:, 0, :-3] - example_pose[:, 0, :-3]) ** 2, axis=-1))

    return pose_constraint

def joint_lengths1(
        parents=np.array([-1, 0, 1, 2, 3, 4, 5, 2, 7, 8, 9, 2, 11, 1, 13, 14, 15, 1, 17, 18, 19]),
        lengths=np.array([
            21.89, 26.90, 16.45, 25.38, 26.40, 26.90, 16.45, 25.38,
            26.40, 33.81, 9.40, 11.10, 41.76, 45.32, 14.38, 11.10,
            41.76, 45.32, 14.38])):
    def joint_lengths_constraint(V):
        input_shape = V.get_shape().as_list()
        J = tf.reshape(V, (input_shape[0], len(parents), 3, input_shape[2]))
        return tf.reduce_mean((tf.sqrt(tf.reduce_sum((J[:, 2:] - tf.gather(J, parents[2:], axis=1)) ** 2, axis=2)) -
                          lengths[np.newaxis, ..., np.newaxis]) ** 2)

    return joint_lengths_constraint


def joint_lengths_channel_last(
        parents=np.array([-1, 0, 1, 2, 3, 4, 5, 2, 7, 8, 9, 2, 11, 1, 13, 14, 15, 1, 17, 18, 19]),
        lengths=np.array([
            21.89, 26.90, 16.45, 25.38, 26.40, 26.90, 16.45, 25.38,
            26.40, 33.81, 9.40, 11.10, 41.76, 45.32, 14.38, 11.10,
            41.76, 45.32, 14.38])):
    def joint_lengths_constraint(V):
        input_shape = V.get_shape().as_list()

        J = tf.reshape(V[:, :, :-3], (input_shape[0], input_shape[1], len(parents), 3))
        return tf.reduce_mean((tf.sqrt(tf.reduce_sum((J[:, :, 2:] - tf.gather(J, parents[2:], axis=2)) ** 2, axis=-1)) -
                          lengths[np.newaxis, np.newaxis, ...]) ** 2)

    return joint_lengths_constraint


def traj_channel_last(traj):
    '''
    
    :param traj: 1 * n_frames * 3
    :return: 
    '''
    def trajectory_constraint(V):
        '''
        
        :param V: n_samples * n_frames * n_dims 
        :return: 
        '''
        # tmp = tf.reduce_mean(tf.reduce_mean((V[:, :, -3:] - traj) ** 2, axis=-1), axis=-1)
        return tf.reduce_min(tf.reduce_mean(tf.reduce_mean((V[:, :, -3:] - traj) ** 2, axis=-1), axis=-1))
    
    return trajectory_constraint


def straight_line_constraint():
    def straight_constraint(V):
        '''

        :param V: n_samples * n_frames * n_dims
        :return:
        '''
        return 10 * tf.reduce_min(tf.reduce_sum(tf.abs(V[:, :, -1]), axis=-1))
    return straight_constraint


def multiconstraint(*fs):
    return lambda V: (sum(map(lambda f: f(V), fs)) / len(fs))


def trajectory(traj):
    def trajectory_constraint(V):
        velocity_scale = 10
        return velocity_scale * tf.reduce_mean((V[:, -7:-4] - traj) ** 2)

    return trajectory_constraint


# def foot_contact_for_walking(contact_foot):
#     def

def foot_sliding(labels):
    def foot_sliding_constraint(V):
        feet = np.array(
            [[45, 46, 47], [48, 49, 50], [57, 58, 59], [60, 61, 62]])  ## LeftFoot, LeftToeBase, RightFoot, RightToeBase
        contact = (labels > 0.5)
        offsets = tf.concat(
            values=[
            tf.gather(V, feet[:, 0:1], axis=1),
            tf.zeros((V.shape[0], len(feet), 1, V.shape[2])),
            tf.gather(V, feet[:, 2:3], axis=1)],
        axis=2)

        def cross(A, B):
            return tf.concat(
                values=[
                A[:, :, 1:2] * B[:, :, 2:3] - A[:, :, 2:3] * B[:, :, 1:2],
                A[:, :, 2:3] * B[:, :, 0:1] - A[:, :, 0:1] * B[:, :, 2:3],
                A[:, :, 0:1] * B[:, :, 1:2] - A[:, :, 1:2] * B[:, :, 0:1]
            ],
            axis=2)
        rotation_value = tf.expand_dims(tf.expand_dims(V[:, -5], 1), 1)
        rotation = -rotation_value * cross(np.array([[[0, 1, 0]]]), offsets)

        velocity_scale = 1

        cost_feet_x = velocity_scale * tf.reduce_mean(contact[:, :, :-1] * (((tf.gather(V, feet[:, 0], axis=1)[:, :, 1:] - tf.gather(V, feet[:, 0], axis=1)[:, :, :-1]) \
                                                                             + tf.expand_dims(V[:, -7, :-1], 1) + rotation[:, :, 0, :-1]) ** 2))
        cost_feet_z = velocity_scale * tf.reduce_mean(contact[:, :, :-1] * (((tf.gather(V, feet[:, 2], axis=1)[:, :, 1:] - tf.gather(V, feet[:, 2], axis=1)[:, :, :-1]) \
                                                                             + tf.expand_dims(V[:, -6, :-1], 1) + rotation[:, :, 2, :-1]) ** 2))
        cost_feet_y = velocity_scale * tf.reduce_mean(
            contact[:, :, :-1] * ((tf.gather(V, feet[:, 1], axis=1)[:, :, 1:] - tf.gather(V, feet[:, 1], axis=1)[:, :, :-1]) ** 2))
        cost_feet_h = 1.0 * tf.reduce_mean(tf.minimum(tf.gather(V, feet[:, 1])[:, :, 1:], 0.0) ** 2)

        return (cost_feet_x + cost_feet_z + cost_feet_y + cost_feet_h)

    return foot_sliding_constraint


def test_optimization():

    # content_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\edin\edin_locomotion\locomotion_walk_001_000.bvh'
    content_file = r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data\old\old_normalwalking_20.bvh'
    motion_data = process_file(content_file, sliding_window=False)

    n_frames, n_dims = motion_data.shape
    ### input motion length must be an even value

    motion_data = np.swapaxes(motion_data, 0, 1)[np.newaxis, :, :]
    if motion_data.shape[2] % 2 != 0:
        motion_data = motion_data[:, :, :-1]
    labels = motion_data[:, -4:, :]
    traj = motion_data[:, -7: -4, :]
    preprocess = np.load(r'preprocessed_core_channel_first.npz')
    normalized_data = (motion_data - preprocess['Xmean']) / preprocess['Xstd']
    input = tf.placeholder(dtype=tf.float32, shape=(1, n_dims, n_frames))
    H = tf.Variable(initial_value=np.zeros((1, 256, int(n_frames/2)), dtype=np.float32))
    encoder_op = motion_encoder(input, name='encoder', hidden_units=256, pooling='max')

    decoder_op = motion_decoder(H, n_dims, name='decoder')
    encoder_op_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='encoder')
    decoder_op_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='decoder')
    saver = tf.train.Saver(encoder_op_params + decoder_op_params)
    constrain_op = constrain(preprocess, decoder_op, labels, traj)
    # reconstructed_motion = (decoder_op * preprocess['Xstd']) + preprocess['Xmean']
    # model_file = r'data/core_network_average_pooling.ckpt'
    model_file = r'data/core_network_max_pooling.ckpt'
    # loss_op = joint_lengths()(reconstructed_motion)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    learning_rate = 0.001
    n_epoches = 250
    optimizer = tf.train.AdamOptimizer(learning_rate)
    train_op = optimizer.minimize(constrain_op, var_list=[H])
    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        saver.restore(sess, model_file)
        latent_data = sess.run(encoder_op, feed_dict={input: normalized_data})
        ## assign latent data to H (the value is assigned after encoding)
        assign_op = H.assign(latent_data)
        sess.run(assign_op)

        for epoch in range(n_epoches):
            sess.run(train_op)
            print(sess.run(constrain_op))

        decodered_motion = sess.run(decoder_op)
        decodered_motion = (decodered_motion * preprocess['Xstd']) + preprocess['Xmean']
        export_point_cloud_data(decodered_motion, r'E:\tmp\optimized_motion.panim')


def constrain(preprocess, decoder_op, labels, traj):
    reconstructed_motion = (decoder_op * preprocess['Xstd']) + preprocess['Xmean']
    # constrain_op = joint_lengths()(reconstructed_motion)
    constraints_op = multiconstraint(foot_sliding(labels), trajectory(traj), joint_lengths())(reconstructed_motion)
    return constraints_op


def autoencoder_joint_constraints_evaluation():
    ## original motion reconstruction error
    content_file = r'C:\repo\data\1 - MoCap\4 - Alignment\elementary_action_walk\leftStance_game_engine_skeleton_smoothed_grounded\walk_001_3_leftStance_398_446.bvh'
    motion_data = process_file(content_file, sliding_window=False)

    n_frames, n_dims = motion_data.shape
    motion_data = np.swapaxes(motion_data, 0, 1)[np.newaxis, :, :]

    input = tf.placeholder(dtype=tf.float32, shape=(1, n_dims, n_frames))
    print(input.shape)
    constrain_op = joint_lengths()(input)
    with tf.Session() as sess:
        res = sess.run(constrain_op, feed_dict={input: motion_data})
        print(res)




if __name__ == "__main__":
    autoencoder_joint_constraints_evaluation()
