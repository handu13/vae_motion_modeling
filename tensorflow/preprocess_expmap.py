# encoding: UTF-8
from morphablegraphs.python_src.morphablegraphs.animation_data import BVHReader, BVHWriter, SkeletonBuilder, MotionVector
from morphablegraphs.python_src.morphablegraphs.animation_data.AnimationData import AnimationData
from morphablegraphs.python_src.morphablegraphs.animation_data.utils import pose_orientation_game_engine_skeleton, get_rotation_to_ref_direction, \
pose_orientation_general
import numpy as np
from morphablegraphs.python_src.morphablegraphs.animation_data.quaternion import Quaternion
import os
from morphablegraphs.python_src.morphablegraphs.utilities import write_to_json_file
import asyncio
from multiprocessing import cpu_count
from concurrent.futures import ProcessPoolExecutor


GAME_ENGINE_ANIMATED_JOINTS = ['Game_engine', 'Root', 'pelvis', 'spine_03', 'clavicle_l', 'upperarm_l', 'lowerarm_l',
                               'hand_l', 'clavicle_r',
                               'upperarm_r', 'lowerarm_r', 'hand_r', 'neck_01', 'head', 'thigh_l', 'calf_l', 'foot_l',
                               'ball_l', 'thigh_r', 'calf_r', 'foot_r', 'ball_r']


def process_file(filename, window=180, window_step=90, sliding_window=True):
    print(filename)
    ## 1. convert motion data to exponential map
    bvhreader = BVHReader(filename)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader, animated_joints=GAME_ENGINE_ANIMATED_JOINTS)

    skeleton_model = 'GAME_ENGINE_SKELETON'
    euler_frames = skeleton.get_reduced_euler_frames(bvhreader.frames)
    n_frames = len(euler_frames)
    anim_data = AnimationData.load_from_euler_frames(euler_frames)


    ## 2. compute forward direction

    ref_dir = np.array([0, 0, 1])
    forward = np.zeros([n_frames, 3])
    body_plane = [] ## a set of joints defining body plane, it should be right-hand order
    for i in range(n_frames):
        if skeleton_model == 'GAME_ENGINE_SKELETON':
            forward[i] = pose_orientation_game_engine_skeleton(bvhreader.frames[i], skeleton)
        else:
            forward[i] = pose_orientation_general(bvhreader.frames[i], body_plane, skeleton)

    #% 3. remove rotation about Y axis, frames are aligned to reference directionl. Compute rotation velocity (the
    # rotation velocity is the relative difference between forward vector and reference orientation)
    rotations = get_rotation_to_ref_direction(forward, ref_dir) ## list of Quaternion, using these rotaiton to rotate velocity, and compute the change of rotation

    # remove rotation about Y axis, all frames looks at reference direction
    for i in range(n_frames):
        anim_data.frames[i].frame_data[1] = anim_data.frames[i].frame_data[1] * rotations[i]

    # compute rotation speed
    r_v = np.zeros(n_frames)
    for i in range(n_frames - 1):
        q = rotations[i+1] * (-rotations[i])
        r_v[i+1] = Quaternion.get_angle_from_quaternion(q, ref_dir)

    ## 4. convert global root position to velocity
    # becuase the frame rotation is done in euler space, that's why the root speed is calculated after rotation speed
    expmap_data = anim_data.convert_to_Expmap()  ## n_frames * n_dims
    print(expmap_data.shape)

    velocity = (expmap_data[1:, :3] - expmap_data[:-1, :3]).copy()
    # """remove translation"""
    expmap_data[1:, :3] = velocity
    expmap_data[0, :3] = 0
    ## 5. rotate velocity based on rotation velocity
    for i in range(n_frames):
        expmap_data[i, :3] = rotations[i] * expmap_data[i, :3]
    expmap_data = np.concatenate([expmap_data, r_v[:, np.newaxis]], axis=-1)

    if sliding_window:
        """ Slide Over Windows """
        windows = []
        # windows_classes = []
        if len(expmap_data) % window_step == 0:
            n_clips = (len(expmap_data) - len(expmap_data) % window_step) // window_step
        else:
            n_clips = (len(expmap_data) - len(expmap_data) % window_step) // window_step + 1
        for j in range(0, n_clips):
            """ If slice too small pad out by repeating start and end poses """
            slice = expmap_data[j * window_step: j * window_step + window]
            if len(slice) < window:
                left = slice[:1].repeat((window - len(slice)) // 2 + (window - len(slice)) % 2, axis=0)
                left[:, :3] = 0.0
                left[:, -1] = 0.0
                right = slice[-1:].repeat((window - len(slice)) // 2, axis=0)
                right[:, :3] = 0.0
                right[:, -1] = 0.0
                slice = np.concatenate([left, slice, right], axis=0)
            if len(slice) != window: raise Exception()

            windows.append(slice)
        return np.asarray(windows)

    return expmap_data



def reconstruct_motion_from_processed_data(process_data, skeleton, save_filename):
    '''
    Steps:
    1. restore rotations from rotation velocity
    2. restore root velocity using rotation
    3. restore root position using root velocity
    4. rotate each frame in quaternion space
    5. get the motion from exponential map representation
    :param process_data:
    :return:
    '''
    n_frames, n_dims = process_data.shape
    rotation = Quaternion.identity()
    rotations = [rotation] * n_frames
    r_v = process_data[:, -1]
    for i in range(n_frames - 1):
        rotations[i+1] = Quaternion.fromAngleAxis(r_v[i+1], np.array([0, 1, 0])) * rotations[i]
    for i in range(n_frames):
        process_data[i, :3] = -rotations[i] * process_data[i, :3]

    ## restore the global translation
    for i in range(n_frames - 1):
        process_data[i+1, :3] += process_data[i, :3]

    expmap_data = process_data[:, :-1]
    anim_data = AnimationData.load_from_Expmap(expmap_data)
    for i in range(n_frames):
        anim_data.frames[i].frame_data[1] = anim_data.frames[i].frame_data[1] * -rotations[i]
    quat_frames = anim_data.convert_to_ndarray()
    complete_quat_frames = skeleton.add_fixed_joint_parameters_to_motion(quat_frames)
    BVHWriter(save_filename, skeleton, complete_quat_frames, skeleton.frame_time, is_quaternion=True)


def get_files(path):
    bvhfiles = []
    for root, dirs, files in os.walk(path):
        for filemane in [f for f in files if f.endswith(".bvh") and f != 'rest.bvh']:
            bvhfiles.append(os.path.join(root, filemane))
    return bvhfiles


def process_edin_data(sliding_window=False):
    save_data = {}
    edin_files = get_files(r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\edin')
    for bvhfile in edin_files:
        filename = os.path.split(bvhfile)[-1]
        print(filename)
        save_data[filename] = process_file(bvhfile).tolist()
    save_dir = r'data\training_data'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    write_to_json_file(os.path.join(save_dir, 'processed_edin_data.json'), save_data)


def process_edin_data_fixed_length():
    save_data = {}
    edin_files = get_files(r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\edin')
    for bvhfile in edin_files:
        filename = os.path.split(bvhfile)[-1]
        print(filename)
        save_data[filename] = process_file(bvhfile).tolist()
    save_dir = r'data\training_data'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    write_to_json_file(os.path.join(save_dir, 'processed_edin_data.json'), save_data)


def process_ulm_data():
    save_data = {}
    edin_files = get_files(r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\ulm')
    for bvhfile in edin_files:
        filename = os.path.split(bvhfile)[-1]
        print(filename)
        save_data[filename] = process_file(bvhfile).tolist()
    save_dir = r'data\training_data'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    write_to_json_file(os.path.join(save_dir, 'processed_ulm_data.json'), save_data)


def process_cmu_data():
    save_data = {}
    cmu_files = get_files(r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\cmu')
    for bvhfile in cmu_files:
        filename = os.path.split(bvhfile)[-1]
        print(filename)
        save_data[filename] = process_file(bvhfile).tolist()
    save_dir = r'data\training_data'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    write_to_json_file(os.path.join(save_dir, 'processed_cmu_data.json'), save_data)



def process_ACCAD_data():
    save_data = {}
    ACCAD_files = get_files(r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\ACCAD')
    for bvhfile in ACCAD_files:
        filename = os.path.split(bvhfile)[-1]
        print(filename)
        save_data[filename] = process_file(bvhfile).tolist()
    save_dir = r'data\training_data'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    write_to_json_file(os.path.join(save_dir, 'processed_accad_data.json'), save_data)


def process_stylized_walking_data():
    save_data = {}
    stylized_walking_files = get_files(r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data')
    for bvhfile in stylized_walking_files:
        filename = os.path.split(bvhfile)[-1]
        print(filename)
        save_data[filename] = process_file(bvhfile).tolist()
    save_dir = r'data\training_data'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    write_to_json_file(os.path.join(save_dir, 'processed_stylized_data_expmap.json'), save_data)


def process_edin_data_coroutine():
    n_workers = max(cpu_count()-1, 1)
    pool = ProcessPoolExecutor(max_workers=n_workers)
    results = dict()
    edit_files = get_files(r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\edin')
    tasks = []
    for bvhfile in edit_files:
        t = process_file_coroutine(pool, bvhfile, results)
        tasks.append(t)
    asyncio.get_event_loop().run_until_complete(asyncio.gather(*tasks))
    save_dir = r'data\training_data'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    np.savez_compressed(os.path.join(save_dir, 'processed_edin_data_expmap.npz'), **results)


def process_ulm_data_coroutine():
    n_workers = max(cpu_count()-1, 1)
    pool = ProcessPoolExecutor(max_workers=n_workers)
    results = dict()
    ulm_files = get_files(r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\ulm')
    tasks = []
    for bvhfile in ulm_files:
        t = process_file_coroutine(pool, bvhfile, results)
        tasks.append(t)
    asyncio.get_event_loop().run_until_complete(asyncio.gather(*tasks))
    save_dir = r'data\training_data'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    np.savez_compressed(os.path.join(save_dir, 'processed_ulm_data_expmap.npz'), **results)


def process_cmu_data_coroutine():
    n_workers = max(cpu_count()-1, 1)
    pool = ProcessPoolExecutor(max_workers=n_workers)
    results = dict()
    cmu_files = get_files(r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\cmu')
    tasks = []
    for bvhfile in cmu_files:
        t = process_file_coroutine(pool, bvhfile, results)
        tasks.append(t)
    asyncio.get_event_loop().run_until_complete(asyncio.gather(*tasks))
    save_dir = r'data\training_data'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    np.savez_compressed(os.path.join(save_dir, 'processed_cmu_data_expmap.npz'), **results)


def process_ACCAD_data_coroutine():
    n_workers = max(cpu_count()-1, 1)
    pool = ProcessPoolExecutor(max_workers=n_workers)
    results = dict()
    accad_files = get_files(r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\ACCAD')
    tasks = []
    for bvhfile in accad_files:
        t = process_file_coroutine(pool, bvhfile, results)
        tasks.append(t)
    asyncio.get_event_loop().run_until_complete(asyncio.gather(*tasks))
    save_dir = r'data\training_data'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    np.savez_compressed(os.path.join(save_dir, 'processed_accad_data_expmap.npz'), **results)


def process_stylized_walking_data_coroutine():
    n_workers = max(cpu_count()-1, 1)
    pool = ProcessPoolExecutor(max_workers=n_workers)
    results = dict()
    ulm_files = get_files(r'C:\repo\data\1 - MoCap\2.1 - GameSkeleton retargeting\stylized_data')
    tasks = []
    for bvhfile in ulm_files:
        t = process_file_coroutine(pool, bvhfile, results)
        tasks.append(t)
    asyncio.get_event_loop().run_until_complete(asyncio.gather(*tasks))
    save_dir = r'data\training_data'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    np.savez_compressed(os.path.join(save_dir, 'processed_stylized_data_expmap.npz'), **results)


@asyncio.coroutine
def process_file_coroutine(pool, filepath, results):
    filename = os.path.split(filepath)[-1]
    fut = pool.submit(process_file, filepath)
    while not fut.done() and not fut.cancelled():
        yield from asyncio.sleep(0.1)
    results[filename] = fut.result()


def process_data():
    # process_edin_data_coroutine()
    # process_ulm_data_coroutine()
    # process_cmu_data_coroutine()
    # process_ACCAD_data_coroutine()
    process_stylized_walking_data_coroutine()



if __name__ == "__main__":
    # filename = r'C:\repo\data\1 - MoCap\4 - Alignment\elementary_action_walk\rightStance_game_engine_skeleton_new_grounded\walk_s_016_rightStance_512_564.bvh'
    # expmap_data = process_file(filename)
    # bvhreader = BVHReader(filename)
    # skeleton = SkeletonBuilder().load_from_bvh(bvhreader, animated_joints=GAME_ENGINE_ANIMATED_JOINTS)
    # # print(expmap_data.shape)
    # reconstruct_motion_from_processed_data(expmap_data, skeleton, save_filename=r'E:\tmp\reconstructed.bvh')
    process_data()
    # process_stylized_walking_data_coroutine()