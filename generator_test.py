import os
import shutil
import numpy as np
from Config import Config
import tensorflow as tf
import collections
from mosi_dev_deepmotionmodeling.mosi_utils_anim import load_json_file, write_to_json_file
from mosi_dev_deepmotionmodeling.preprocessing.preprocessing import process_file
from mosi_dev_deepmotionmodeling.utilities.utils import get_files, export_point_cloud_data_without_foot_contact, write_to_json_file
from mosi_dev_deepmotionmodeling.mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder, panim
from src_final.network.Discriminator import HPDiscriminator, Bihmp_Discriminator, CNN_Discriminator
from src_final.network.Generator import HPGenerator,Bihmp_Generator, TCN_RNN_Generator, TCN_RNN_init_Generator
from src_final.model.HPGAN import HPGAN
from src_final.model.BIHMPGAN import BIHMPGAN
from src_final.model.motionGAN_recon import motionGAN_recon
from src_final.model.motionGAN_norecon import motionGAN_norecon
from src_final.utils import CMU_SKELETON, CMU_ANIMATED_JOINTS, CMU_JOINT_LEVELS
from src_final.network.PoseEmbedding import SpatialEncoder, SpatialDecoder
import itertools
import copy
mse = tf.keras.losses.MeanSquaredError()
EPS = 1e-6

training_data = np.load('data/training_data/training_clips.npy')
mean_pose = training_data.mean(axis=(0, 1)) 
std_pose = training_data.std(axis=(0, 1))
std_pose[std_pose<EPS] = EPS
training_data = (training_data - mean_pose) / std_pose 
n_samples, sequence_length, dims = training_data.shape
input_length = 32
output_length = 32
train_set = training_data[:int(0.9*n_samples)]
test_set = training_data[int(0.9*n_samples):]
train_x = train_set[:,:input_length,:]
train_y = train_set[:,input_length:,:]
test_x = test_set[:, :input_length, :]
test_y = test_set[:, input_length:, :]
g_optimizer = tf.keras.optimizers.Adam(1e-3)

def train(g):
    batchsize = 32
    n_batches = len(train_x) // batchsize
    g_losses = []
    for epoch in range(300):
        for i in range(n_batches):
            input_batch = train_x[i*batchsize:(i+1)*batchsize]
            output_batch = train_y[i*batchsize:(i+1)*batchsize]
            random_noise = tf.random.normal([len(input_batch), g.z_dims], dtype=tf.float32)
            with tf.GradientTape() as gen_tape:
                prediction = g(input_batch, random_noise)
                g_loss = mse(prediction, output_batch)# + 10*mse(prediction[:,0,:], input_batch[:,-1,:])
            grads_g = gen_tape.gradient(g_loss, g.trainable_variables)
            g_optimizer.apply_gradients(zip(grads_g, g.trainable_variables))
            g_losses.append(g_loss.numpy())
        print("epoch {}: {}".format(epoch, np.mean(g_losses)))


def predict(model, output_dir):
    random_noise = tf.random.normal([len(test_x), model.z_dims], dtype=tf.float32)
    test_output = model(test_x, random_noise)
    test_output = np.concatenate([test_x, test_output], axis=1)
    test_output = test_output * std_pose + mean_pose
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    skeleton_bvh = BVHReader('data/training_data/preprocessed/input_data/preprocessed/Vicon_6kmh.bvh')
    skeleton = SkeletonBuilder().load_from_bvh(skeleton_bvh)
    animated_joints = skeleton.generate_bone_list_description() 
    if isinstance(animated_joints, list):
        animated_joints = collections.OrderedDict([(d['name'], {'parent':d['parent'], 'index':d['index']}) for d in animated_joints])
    for i in range(len(test_output)):
        filename = os.path.join(output_dir, 'gen_' + str(i) + '.panim')
        export_point_cloud_data_without_foot_contact(test_output[i], filename, skeleton=animated_joints)

if __name__ == "__main__":
    ###motionGAN###
    # f = 'config/generator_config/config_mg.json'
    # config = load_json_file(f)
    # output_dir = os.path.join('gentest', 'motionGAN_vicon')
    # model = TCN_RNN_Generator(config["generator"])
    # train(model)
    # predict(model, output_dir)

    # ###HPGAN####
    # f = 'config/generator_config/config_hp.json'
    # config = load_json_file(f)
    # output_dir = os.path.join('gentest', 'HPGAN_vicon')
    # model = HPGenerator(config["generator"])
    # train(model)
    # predict(model, output_dir)

    # ###BihmpGAN###
    # f = 'config/geneartor_config/config_bi.json'
    # config = load_json_file(f)
    # output_dir = os.path.join('gentest', 'BihmpGAN_vicon')
    # model = Bihmp_Generator(config["generator"])
    # train(model)
    # predict(model, output_dir)

    ##tcn_rnn_init###
    f = 'config/generator_config/config_mg_nospa.json'
    config = load_json_file(f)
    output_dir = os.path.join('gentest', 'motionGANinitnospa_vicon')
    model = TCN_RNN_init_Generator(config["generator"])
    train(model)
    predict(model, output_dir)
