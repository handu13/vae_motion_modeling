import os
import shutil
import numpy as np
from Config import Config
from mosi_dev_deepmotionmodeling.mosi_utils_anim import load_json_file, write_to_json_file
from mosi_dev_deepmotionmodeling.preprocessing.preprocessing import process_file
from src_final.network.Discriminator import HPDiscriminator, Bihmp_Discriminator, CNN_Discriminator, CNN_Discriminator_test1, CNN_Discriminator_test2, CNN_Discriminator_test3
from src_final.network.Generator import HPGenerator,Bihmp_Generator, TCN_RNN_Generator, TCN_RNN_init_Generator
from src_final.model.HPGAN import HPGAN
from src_final.model.BIHMPGAN import BIHMPGAN
from src_final.model.motionGAN_recon import motionGAN_recon
from src_final.model.motionGAN_norecon import motionGAN_norecon
from src_final.utils import CMU_SKELETON, CMU_ANIMATED_JOINTS
import itertools
import copy
EPS = 1e-6

def main():
    model_to_be_trained = ["exp1"]
    ###testCONFIGS for motionGAN_norecon##########
    '''experiment1: discriminator, trained with WGAN-GP'''
    if "exp1" in model_to_be_trained:
        filename = 'config/testconfig/config_recon.json'
        config = load_json_file(filename)
        name = "original_recon"
        output_dir = os.path.join('output', name)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        ## load training data
        training_data = np.load(config['data']['data_path'])
        training_data = np.asarray(training_data, dtype=np.float32)
        mean_pose = training_data.mean(axis=(0, 1))
        std_pose = training_data.std(axis=(0, 1))
        std_pose[std_pose<EPS] = EPS
        training_data = (training_data - mean_pose) / std_pose   
        n_samples, sequence_length, dims = training_data.shape
        train_set = training_data[:int(0.9*n_samples)] 
        test_set = training_data[int(0.9*n_samples):]      

        input_sequence_length = config['data']['input_sequence_length']
        output_sequence_length = config['data']['output_sequence_length']
        input_sequence = train_set[:, :input_sequence_length, :]
        output_sequence = train_set[:, input_sequence_length:, :]    
        test_sequence = test_set[:, :input_sequence_length, :]        

        ## model initialization
        g = TCN_RNN_init_Generator(config["generator"])
        d = CNN_Discriminator(config["discriminator"])
        model = motionGAN_recon(g, d, name, debug=True, output_dir=output_dir)
        model.train(input_sequence, output_sequence, test_sequence, test_set, std_pose, mean_pose, config["train"])
            

    '''experiment2: discriminator+sigmoid, trained with WGAN-GP / GAN'''
    if "exp2" in model_to_be_trained:
        filename = 'config/testconfig/config_2.json'
        config = load_json_file(filename)
        name = "sigmoid_128_vicon"
        output_dir = os.path.join('output', name)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        ## load training data
        training_data = np.load(config['data']['data_path'])
        training_data = np.asarray(training_data, dtype=np.float32)
        mean_pose = training_data.mean(axis=(0, 1))
        std_pose = training_data.std(axis=(0, 1))
        std_pose[std_pose<EPS] = EPS
        training_data = (training_data - mean_pose) / std_pose   
        n_samples, sequence_length, dims = training_data.shape
        train_set = training_data[:int(0.9*n_samples)] 
        test_set = training_data[int(0.9*n_samples):]      

        input_sequence_length = config['data']['input_sequence_length']
        output_sequence_length = config['data']['output_sequence_length']
        input_sequence = train_set[:, :input_sequence_length, :]
        output_sequence = train_set[:, input_sequence_length:, :]    
        test_sequence = test_set[:, :input_sequence_length, :]        


        ## model initialization
        g = TCN_RNN_Generator(config["generator"])
        d = CNN_Discriminator(config["discriminator"])
        e = CNN_Discriminator(config["evaluator"])
        model = motionGAN_norecon(g, d, name, e, debug=True, output_dir=output_dir)
        model.train(input_sequence, output_sequence, test_sequence, test_set, std_pose, mean_pose, config["train"])
        

    '''experiment4: outputlayer + batchnormalization + sigmoid, trained with WGAN-GP / GAN'''
    if "exp4" in model_to_be_trained:
        filename = 'config/testconfig/config_2.json'
        config = load_json_file(filename)
        name = "test2_128_vicon"
        output_dir = os.path.join('output', name)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        ## load training data
        training_data = np.load(config['data']['data_path'])
        training_data = np.asarray(training_data, dtype=np.float32)
        mean_pose = training_data.mean(axis=(0, 1))
        std_pose = training_data.std(axis=(0, 1))
        std_pose[std_pose<EPS] = EPS
        training_data = (training_data - mean_pose) / std_pose   
        n_samples, sequence_length, dims = training_data.shape
        train_set = training_data[:int(0.9*n_samples)] 
        test_set = training_data[int(0.9*n_samples):]      

        input_sequence_length = config['data']['input_sequence_length']
        output_sequence_length = config['data']['output_sequence_length']
        input_sequence = train_set[:, :input_sequence_length, :]
        output_sequence = train_set[:, input_sequence_length:, :]    
        test_sequence = test_set[:, :input_sequence_length, :]        


        ## model initialization
        g = TCN_RNN_Generator(config["generator"])
        d = CNN_Discriminator_test2(config["discriminator"])
        e = CNN_Discriminator_test2(config["evaluator"])
        model = motionGAN_norecon(g, d, name, e, debug=True, output_dir=output_dir)
        model.train(input_sequence, output_sequence, test_sequence, test_set, std_pose, mean_pose, config["train"])
        


    '''experiment5: batchnormalization + outputlayer + sigmoid, trained with WGAN-GP / GAN'''
    if "exp5" in model_to_be_trained:
        filename = 'config/testconfig/config_2.json'
        config = load_json_file(filename)
        name = "test3_128_vicon"
        output_dir = os.path.join('output', name)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        ## load training data
        training_data = np.load(config['data']['data_path'])
        training_data = np.asarray(training_data, dtype=np.float32)
        mean_pose = training_data.mean(axis=(0, 1))
        std_pose = training_data.std(axis=(0, 1))
        std_pose[std_pose<EPS] = EPS
        training_data = (training_data - mean_pose) / std_pose   
        n_samples, sequence_length, dims = training_data.shape
        train_set = training_data[:int(0.9*n_samples)] 
        test_set = training_data[int(0.9*n_samples):]      

        input_sequence_length = config['data']['input_sequence_length']
        output_sequence_length = config['data']['output_sequence_length']
        input_sequence = train_set[:, :input_sequence_length, :]
        output_sequence = train_set[:, input_sequence_length:, :]    
        test_sequence = test_set[:, :input_sequence_length, :]        


        ## model initialization
        g = TCN_RNN_Generator(config["generator"])
        d = CNN_Discriminator_test3(config["discriminator"])
        e = CNN_Discriminator_test3(config["evaluator"])
        model = motionGAN_norecon(g, d, name, e, debug=True, output_dir=output_dir)
        model.train(input_sequence, output_sequence, test_sequence, test_set, std_pose, mean_pose, config["train"])
        



            
    

if __name__ == "__main__":
    main()