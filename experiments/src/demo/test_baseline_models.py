import os
import sys
from pathlib import Path
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
sys.path.append("..")
sys.path.insert(0, str(Path(__file__).parent.absolute()) + r'/../../..')
from model.baseline_models import NaiveVAE
import numpy as np 
from mosi_dev_deepmotionmodeling.utilities.utils import get_files, export_point_cloud_data_without_foot_contact
EPS = 1e-6


def test_naive_VAE():
    training_data = np.load(r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\clipwise\h36m.npz')['clips']
    training_data = np.asarray(training_data, dtype=np.float32)
    
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    training_data = (training_data - mean_pose) / std_pose
    n_samples, n_frames, n_dims = training_data.shape
    input_dims = n_frames * n_dims
    reshaped_training_data = np.reshape(training_data, (n_samples, input_dims))
    model = NaiveVAE(output_dim=input_dims, hidden_dim=10)
    model.build((None, input_dims))
    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../data/models/naiveVAE_clip_modeling'
    model.load_weights(os.path.join(save_path, 'cp-{epoch:04d}.ckpt'.format(epoch = 100)))
    res = model(reshaped_training_data[:10])
    print(res.shape)
    res = np.reshape(res, (-1, n_frames, n_dims))
    original_motions = training_data[:10] * std_pose + mean_pose
    reconstructed_motions = res * std_pose + mean_pose
    export_sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../data/models/naiveVAE_clip_modeling/samples/with_encoder'
    if not os.path.exists(export_sample_path):
        os.makedirs(export_sample_path)
    for i in range(10):
        origin_filename = os.path.join(export_sample_path, 'origin_' + str(i) + '.panim')
        recon_filename = os.path.join(export_sample_path, 'recon_' + str(i) + '.panim')
        export_point_cloud_data_without_foot_contact(original_motions[i], origin_filename)
        export_point_cloud_data_without_foot_contact(reconstructed_motions[i], recon_filename)


if __name__ == "__main__":
    test_naive_VAE()