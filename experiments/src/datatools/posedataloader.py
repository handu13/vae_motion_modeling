import numpy as np
import os
data_folder = os.path.abspath('../..') + '/data'
class PoseDataLoader(object):
    '''
    Dataloader used to load single skeleton data to pretrain the spatial autoencoder. Use random data at this moment for testing functionality.
    '''
    def __init__(self, dataset="h36m", mode="train", num_joints=29, num_params_per_joint=3):
        '''
        Initialize the data_loader.
        '''
        self.dataset = dataset
        self.num_joints = num_joints
        self.num_params_per_joint = num_params_per_joint
        dataname = "{}_data/clips.npy".format(dataset)
        datapath = os.path.join(data_folder,dataname)
        data = np.load(datapath)  
        if mode == "train":
            data = data[:int(0.8*data.shape[0]),:-3]
            self.data = np.reshape(data, (len(data),self.num_joints,self.num_params_per_joint))
        if mode == "test":
            data = data[int(0.8*data.shape[0]):,:-3]
            self.data = np.reshape(data, (len(data),self.num_joints,self.num_params_per_joint))
        self.batch_start = 0
    
    def size(self):
        '''
        return the size of the dataset
        '''
        return len(self.data)

    def has_more(self):
        '''
        Return False if reach the end of the dataset
        '''
        return bool(self.batch_start < self.size())
        
    def reset(self,shuffle=True):
        '''
        reset the batch start back to the beginning of the dataset  
        '''
        if shuffle:
            np.random.shuffle(self.data)
        self.batch_start = 0

    def next_batch(self, batch_size):
        '''
        Read the next batch.
        '''
        batch_end = min(self.batch_start+batch_size, self.size())
        current_batch_size = batch_end - self.batch_start
        batch_data = self.data[self.batch_start:batch_end,:]
        self.batch_start = batch_end
        return batch_data, current_batch_size