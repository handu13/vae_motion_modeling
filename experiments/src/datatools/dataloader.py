import sys
sys.path.append("..")
# from nn.Embedding import SpatialEncoder
import numpy as np
import tensorflow as tf
import os
model_folder = os.path.abspath('../..') + '/savemodel'
data_folder = os.path.abspath('../..') + '/data'
file_encoder = os.path.join(model_folder, '...') #fill the pretrained spatial autoencoder path here
file_decoder = os.path.join(model_folder, '...')
import copy

class DataLoader(object):
    '''
    Dataloader used to load motion sequence data to train the generative model. Use 'mnist' or random data at this moment for testing functionality.
    '''
    def __init__(self, dataset, mode="train", divide=4, embedding=False, length=30, num_joints=31, num_zdim=64, num_params_per_joint=3):
        '''
        Initialize the data_loader.
        '''
        self.dataset = dataset
        self.length = length
        self.embedding = embedding
        if dataset == "testencoder":  # when inputs=[batch, length, joints, params_per_joint]
            if mode == "train": #load train dataset
                self.data = np.random.normal(size=[100, self.length, num_joints, num_params_per_joint])
            if mode == "test": #load test dataset
                self.data = np.random.normal(size=[20, self.length, num_joints, num_params_per_joint])
        if dataset == "mnist": # for testing codes
            mnist = tf.keras.datasets.mnist
            (x_train, y_train), (x_test, y_test) = mnist.load_data()
            if mode == "train":
                self.data = x_train
            if mode == "test":
                self.data = x_test
        else:
            #dataname = "processed_mocap_data/{}/clips.npy".format(dataset)
            #datapath = os.path.join(data_folder,dataname)
            #datapath = r'H://experiments//data//processed_mocap_data//Vicon//normalized.npy'
            # datapath = r'H://experiments//data//processed_mocap_data//Vicon//clips.npy'
            datapath = os.path.join(r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\processed_mocap_data', self.dataset, 'clips.npy')
            data = np.load(datapath)

            element_size = data.shape[-1]
            self.mean = []
            self.std = []
            for i in range(element_size):
                mean = np.mean(data[:,:,i])
                std = np.std(data[:,:,i])
                std = max(std, 1e-8)
                self.mean.append(mean)
                self.std.append(std)
            
                data[:,:,i] = (data[:,:,i] - mean) / std
            self.mean = np.asarray(self.mean)
            self.std = np.asarray(self.std)

            if mode == "train":
                self.data = data[:int(0.9*data.shape[0]),:,:].copy()
                # seg_data = []
                # start = 0
                # end = start + sequence_length
                # while end < data.shape[1]-sequence_length:
                #     seg_data.append(data[:,start:end,:])
                #     start += 1
                #     end = start + sequence_length
                # self.data = np.concatenate(seg_data, axis=0)
            if mode == "test":
                self.data = data[int(0.9*data.shape[0]):,:,:].copy()
                # seg_data = []
                # start = 0
                # end = start + sequence_length
                # while end < data.shape[1]-sequence_length:
                #     seg_data.append(data[:,start:end,:])
                #     start += 1
                #     end = start + sequence_length
                # self.data = np.concatenate(seg_data, axis=0)
        self.batch_start = 0
        # if embedding:
        #     self.poseNet = SpatialEncoder(num_joints=num_joints, num_params_per_joint=num_params_per_joint, num_zdim=num_zdim)
        #     self.poseNet.load_model(file_encoder, file_decoder)
    
    def size(self):
        '''
        return the size of the dataset
        '''
        return len(self.data)

    def has_more(self):
        '''
        Return False if reach the end of the dataset
        '''
        return bool(self.batch_start < self.size())
        
    def reset(self,shuffle=False):
        '''
        reset the batch start back to the beginning of the dataset  
        '''
        if shuffle:
            np.random.shuffle(self.data)
        self.batch_start = 0
    
    def preprocessing(self, data):
        '''
        Some preprocessing before feeding data into the model.
        '''
        if self.dataset=='testencoder':# data:[batch_size, length, joints, param_joints]
            if self.embedding:
                data = data.reshape([-1] + list(data.shape[2:]))
                encoded = self.poseNet.encode(data) #encoded:[batch_size*length, embeded_size]
                return encoded.reshape([-1, self.length, encoded.shape[-1]])
        else:

            return data
    
    def postprocessing(self, data):
        '''
        Some postprocessing after fetch model's result.
        '''
        # data = data*max(1e-8, self.std) + self.mean
        data = data * self.std + self.mean
        if self.dataset == 'testencoder':
            if self.embedding:
                data = data.reshape([-1, data.shape[-1]]) #data:[batch_size, length, embeded_size]
                decoded = self.poseNet.decode(data) #decoded: [batch_size*length, joints, param_joints]
                return decoded.reshape([-1, self.length]+list(decoded.shape[1:]))
        return data

    def next_batch(self, batch_size):
        '''
        Read the next batch.
        '''
        batch_end = min(self.batch_start+batch_size, self.size())
        current_batch_size = batch_end - self.batch_start
        if self.dataset == "testencoder":
            batch_data = self.data[self.batch_start:batch_end,:,:,:]
        else:
            batch_data = self.data[self.batch_start:batch_end, :, :]
        input_batch = self.preprocessing(batch_data)
        self.batch_start = batch_end
        return input_batch, current_batch_size