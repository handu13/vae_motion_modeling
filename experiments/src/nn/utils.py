import tensorflow as tf

def create_rnn_cell(cell_type, num_neurons, use_residual=False):
    ''' Create RNN cell depend on the provided type '''
    cell = None
    if cell_type == "lstm":
        cell = tf.nn.rnn_cell.LSTMCell(num_neurons)
    elif cell_type == "lstmp":
        cell = tf.nn.rnn_cell.LSTMCell(num_neurons, use_peepholes=True)        
    elif cell_type == "gru":
        cell = tf.nn.rnn_cell.GRUCell(num_neurons)
    elif cell_type == "norm_lstm":
        cell = tf.nn.rnn_cell.LayerNormBasicLSTMCell(num_neurons)
    else:
        raise Exception("Unsupported cell type.")

    if use_residual:
        cell = tf.nn.rnn_cell.ResidualWrapper(cell)
    return cell

def create_rnn_model(num_layers, cell_type, num_neurons, use_residual=False):
    ''' Create RNN model '''
    if num_layers > 1:
        return tf.nn.rnn_cell.MultiRNNCell([create_rnn_cell(cell_type, num_neurons, use_residual) for _ in range(num_layers)])
    else:
        return create_rnn_cell(cell_type, num_neurons, use_residual)