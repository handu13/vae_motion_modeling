import os, time, sys, traceback
import numpy as np
# import tensorflow as tf
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
from collections import OrderedDict
sys.path.append("..")
from datatools.dataloader import DataLoader
'''
A spatial autoencoder.
If to train this autoencoder individually:
    call 'create_pretrain_model' and then 'train'
If to train the spatial encoder and decoder together with the generative model:
    add 'build_encoder' and 'build_decoder' to model as part of components
if to load the pretrained autoencoder and apply it to new data:
    call 'load_model' and then  'encode' and 'decode' 
'''

class SpatialEncoder(object):
    '''
    SpatialEncoder used to encode a single skeleton into the latent space.
    '''
    def __init__(self,num_joints,num_params_per_joint,num_zdim,scope=""):

        #self.input_x = tf.placeholder(tf.float32, shape=[None, num_joints, num_params_per_joint], name='input_x')
        #self.input_z = tf.placeholder(tf.float32, shape=[None, num_zdim], name='input_z')
        self.num_joints = num_joints
        self.num_zdim = num_zdim
        self.num_params_per_joint = num_params_per_joint

        #skeleton according to cmu dataset
        # self.joint_names = [
        #     'Hips','LeftUpLeg','LeftLeg','LeftFoot','LeftToeBase', #5
        #     'LowerBack','Spine','Spine1', #8
        #     'LeftShoulder','LeftArm','LeftForeArm','LeftHand','LThumb','LeftFingerBase','LeftHandFinger1', #15
        #     'Neck','Neck1','Head', #18
        #     'RightShoulder','RightArm','RightForeArm', 'RightHand','RThumb','RightFingerBase','RightHandFinger1',#25
        #     'RightUpLeg', 'RightLeg','RightFoot','RightToeBase', #29
        # ] #29 joints
        self.joint_names = [
            'Root', 'L_Femur', 'L_Tibia', 'L_Foot', 'L_Toe', 
            'R_Femur', 'R_Tibia', 'R_Foot', 'R_Toe', 
            'LowerBack', 'Thorax', 'Neck', 'Head', 
            'L_Collar', 'L_Humerus', 'L_Elbow', 'L_Wrist', 'L_Thumb', 
            'R_Collar', 'R_Humerus', 'R_Elbow', 'R_Wrist', 'R_Thumb',
            'Rootvel'] #23 joints 
        
        # self.encoder_joints_dict ={
        #     'r_arm': ["RightShoulder", "RightArm", "RightForeArm", "RightHand","RThumb", "RightFingerBase", "RightHandFinger1"],
        #     'l_arm': ["LeftShoulder", "LeftArm", "LeftForeArm", "LeftHand", "LThumb", "LeftFingerBase", "LeftHandFinger1"],

        #     'r_leg': ["RightUpLeg", "RightLeg", "RightFoot", "RightToeBase"],
        #     'l_leg': ["LeftUpLeg", "LeftLeg", "LeftFoot", "LeftToeBase"],

        #     'torso': ["LowerBack", "Spine", "Spine1", "Neck", "Neck1", "Head"],

        #     'torso_r_arm': ['torso_ft', 'r_arm_ft'],
        #     'torso_l_arm': ['torso_ft', 'l_arm_ft'],
        #     'torso_r_leg': ['torso_ft', 'r_leg_ft'],
        #     'torso_l_leg': ['torso_ft', 'l_leg_ft'],

        #     'upper_body': ['torso_r_arm_ft', 'torso_l_arm_ft'],
        #     'lower_body': ['torso_r_leg_ft', 'torso_l_leg_ft'],
            
        #     'full_body': ['Hips_ft','upper_body_ft', 'lower_body_ft']
        # }
        self.encoder_joints_dict = {
            'r_arm': ['R_Collar', 'R_Humerus', 'R_Elbow', 'R_Wrist', 'R_Thumb'],
            'l_arm': ['L_Collar', 'L_Humerus', 'L_Elbow', 'L_Wrist', 'L_Thumb'],
            'r_leg': ['L_Femur', 'L_Tibia', 'L_Foot', 'L_Toe'],
            'l_leg': ['R_Femur', 'R_Tibia', 'R_Foot', 'R_Toe'],
            'torso': ['LowerBack', 'Thorax', 'Neck', 'Head'],
            'theRoot': ['Root', 'Rootvel'],
            'torso_r_arm': ['torso_ft', 'r_arm_ft'],
            'torso_l_arm': ['torso_ft', 'l_arm_ft'],
            'torso_r_leg': ['torso_ft', 'r_leg_ft'],
            'torso_l_leg': ['torso_ft', 'l_leg_ft'],
            'upper_body': ['torso_r_arm_ft', 'torso_l_arm_ft'],
            'lower_body': ['torso_r_leg_ft', 'torso_l_leg_ft'],
            'full_body': ['theRoot_ft','upper_body_ft', 'lower_body_ft']
        }
        
        self.decoder_joints_dict = self._inverse_graph(self.encoder_joints_dict)
        if scope.strip() == "":
            self._encoder_scope = self.__class__.__name__+'/encoder_net'
            self._decoder_scope = self.__class__.__name__+'/decoder_net'
        else:
            self._encoder_scope = scope + "/" + self.__class__.__name__+'/encoder_net'
            self._decoder_scope = scope + "/" + self.__class__.__name__+'/decoder_net'

    def _inverse_graph(self, graph_dict):
        inverse_graph_dict = {joint_name: [] for joint_name in self.joint_names}
        for u_node, edges in graph_dict.items():
            for i, v_node in enumerate(edges):
                inverse_graph_dict.setdefault(v_node, [])
                inverse_graph_dict[v_node].append((i, u_node))
        return inverse_graph_dict

    def _encoderNet(self,encode_data):
        with tf.variable_scope(self._encoder_scope,reuse=tf.AUTO_REUSE):
            encoder_net = OrderedDict()
            ### individual joints ###
            for i,joint_name in enumerate(self.joint_names):
                encoder_net[joint_name] = encode_data[:,i,:]
            ### L1 group ###
            # joint -> concate -> joint_group -> fc layer -> joint_group_ft
            for joint_group in ['l_arm','r_arm','r_leg','l_leg','torso','theRoot']:
                joint_group_ft = joint_group + '_ft'
                encoder_net[joint_group] = tf.concat([encoder_net[sub_part] for sub_part in self.encoder_joints_dict[joint_group]],axis=1)
                encoder_net[joint_group_ft] = tf.layers.dense(inputs=encoder_net[joint_group], units=64, activation=tf.nn.tanh, name=joint_group_ft)
            ### L2 group ###
            for joint_group in ['torso_l_arm','torso_r_arm','torso_l_leg','torso_r_leg']:
                joint_group_ft = joint_group + '_ft'
                encoder_net[joint_group] = tf.concat([encoder_net[sub_part] for sub_part in self.encoder_joints_dict[joint_group]],axis=1)
                encoder_net[joint_group_ft] = tf.layers.dense(inputs=encoder_net[joint_group], units=128, activation=tf.nn.tanh, name=joint_group_ft)
            ### L3 group ###
            for joint_group in ['upper_body','lower_body']:
                joint_group_ft = joint_group + '_ft'
                encoder_net[joint_group] = tf.concat([encoder_net[sub_part] for sub_part in self.encoder_joints_dict[joint_group]],axis=1)
                encoder_net[joint_group_ft] = tf.layers.dense(inputs=encoder_net[joint_group], units=256, activation=tf.nn.tanh, name=joint_group_ft)
            ### L4 group ###
            #encoder_net['Root_ft'] = tf.layers.dense(inputs=encoder_net['Root'], units=64, activation=tf.nn.tanh, name='Root_ft')
            for joint_group in ['full_body']:
                joint_group_ft = joint_group + '_ft'
                encoder_net[joint_group] = tf.concat([encoder_net[sub_part] for sub_part in self.encoder_joints_dict[joint_group]],axis=1)
                encoder_net[joint_group_ft] = tf.layers.dense(inputs=encoder_net[joint_group], units=512, activation=tf.nn.tanh, name=joint_group_ft)
            encoder_net['full_body_ft2'] = tf.layers.dense(inputs=encoder_net['full_body_ft'], units=512, activation=tf.nn.tanh, name='full_body_ft2')
            encoder_net['z_joints'] = tf.layers.dense(inputs=encoder_net['full_body_ft2'], units=self.num_zdim, activation=tf.nn.tanh, name='z_joints')
        return encoder_net

    def _decoderNet(self,decode_data):
        with tf.variable_scope(self._decoder_scope,reuse=tf.AUTO_REUSE):
            decoder_net = OrderedDict()
            decoder_net['z_joints'] = decode_data
            ### inverse L4 group
            decoder_net['full_body_ft2'] = tf.layers.dense(inputs=decoder_net['z_joints'], units=512, activation=tf.nn.leaky_relu, name='full_body_ft2')
            decoder_net['full_body_ft'] = tf.layers.dense(inputs=decoder_net['full_body_ft2'], units=512, activation=tf.nn.leaky_relu, name='full_body_ft')
            decoder_net['full_body'] = tf.layers.dense(inputs=decoder_net['full_body_ft'], units=576, activation=tf.nn.leaky_relu, name='full_body')
            ### inverse L3 group ###
            for joint_group in ['theRoot','upper_body','lower_body']:
                n_units = 256
                joint_group_ft = joint_group + '_ft'
                super_group_layers = [decoder_net[super_part][:, i * n_units: (i+1) * n_units] for i, super_part in self.decoder_joints_dict[joint_group_ft]]
                #decoder_net[joint_group_ft] = tf.nn.relu(tf.add_n(super_group_layers))
                decoder_net[joint_group_ft] = tf.add_n(super_group_layers)
                decoder_net[joint_group] = tf.layers.dense(inputs=decoder_net[joint_group_ft], units=n_units, activation=tf.nn.leaky_relu, name=joint_group)
            ### inverse L2 group ###
            for joint_group in ['torso_l_arm','torso_r_arm','torso_l_leg','torso_r_leg']:
                n_units = 128
                joint_group_ft = joint_group + '_ft'
                super_group_layers = [decoder_net[super_part][:, i * n_units: (i + 1) * n_units] for i, super_part in self.decoder_joints_dict[joint_group_ft]]
                #decoder_net[joint_group_ft] = tf.nn.relu(tf.add_n(super_group_layers))
                decoder_net[joint_group_ft] = tf.add_n(super_group_layers)
                decoder_net[joint_group] = tf.layers.dense(inputs=decoder_net[joint_group_ft], units=n_units, activation=tf.nn.leaky_relu, name=joint_group)
            ### inverse L1 group ###
            for joint_group in ['l_arm','r_arm','r_leg','l_leg','torso']:
                n_units = 64
                joint_group_ft = joint_group + '_ft'
                super_group_layers = [decoder_net[super_part][:, i * n_units: (i + 1) * n_units] for i, super_part in self.decoder_joints_dict[joint_group_ft]]
                #decoder_net[joint_group_ft] = tf.nn.relu(tf.add_n(super_group_layers))
                decoder_net[joint_group_ft] = tf.add_n(super_group_layers)
                joint_group_ft_units = self.num_params_per_joint * len(self.encoder_joints_dict[joint_group])
                decoder_net[joint_group] = tf.layers.dense(inputs=decoder_net[joint_group_ft], units=joint_group_ft_units, activation=None, name=joint_group)
            ### individual joints ###
            for joint in self.joint_names:
                n_units = self.num_params_per_joint
                super_group_layers = [decoder_net[super_part][:, i * n_units: (i + 1) * n_units] for i, super_part in self.decoder_joints_dict[joint]]
                decoder_net[joint] = tf.add_n(super_group_layers)
            #decoder_net['Hips_ft'] = tf.nn.relu(decoder_net['full_body'][:,:64])
            #decoder_net['Root'] = tf.layers.dense(inputs=decoder_net['Hips_ft'], units=self.num_params_per_joint, activation=None, name='Hips')
            ### concate individual joints ###
            full_body_x = tf.concat([tf.expand_dims(decoder_net[joint_name], axis=1) for joint_name in self.joint_names], axis=1)
            #norms = tf.norm(full_body_x, axis=2, keep_dims=True)
            #decoder_net['full_body_x'] = full_body_x / (norms+tf.constant(1e-9, dtype=tf.float32))
            decoder_net['full_body_x'] = full_body_x
        return decoder_net

    def create_pretrain_model(self, input_x, input_z):
        self.input_x = input_x
        self.input_z = input_z
        ### x-z-x cycle ###
        x_real = self.input_x
        encoder_real = self._encoderNet(x_real)
        self.z_real = encoder_real['z_joints']
        decoder_real = self._decoderNet(self.z_real)
        self.x_recon = decoder_real['full_body_x']
        ### z-x-z cycle ###
        z_rand = self.input_z
        decoder_fake = self._decoderNet(z_rand)
        x_fake = decoder_fake['full_body_x']
        encoder_fake = self._encoderNet(x_fake)
        z_recon = encoder_fake['z_joints']
        ### cyclic loss ###
        self.tensor_x_loss = tf.reduce_mean((self.x_recon - x_real)**2,axis=[1,2])
        self.tensor_z_loss = tf.reduce_mean((z_rand - z_recon)**2,axis=1)
        self.loss_cyclic = tf.reduce_mean(self.tensor_x_loss) + tf.reduce_mean(self.tensor_z_loss)
        self._param_encoder = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self._encoder_scope)
        self._param_decoder = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self._decoder_scope)

    def train(self, dataset, output_model_folder, output_tensorboard_folder, output_result_folder, max_epochs,
              lr_encoder=2e-6, lr_decoder=2e-6, batch_size=256, sample_batch_size=10, save_every_epochs=100, sample_every_epochs=5):
        if not os.path.exists(output_model_folder):
            os.makedirs(output_model_folder)
        if not os.path.exists(output_tensorboard_folder):
            os.makedirs(output_tensorboard_folder)
        if not os.path.exists(output_result_folder):
            os.makedirs(output_result_folder)
        tf.summary.scalar("cyclic_loss", self.loss_cyclic)
        summary_op = tf.summary.merge_all()
        encoder_op = tf.train.AdamOptimizer(learning_rate=lr_encoder).minimize(self.loss_cyclic, var_list=self._param_encoder)
        decoder_op = tf.train.AdamOptimizer(learning_rate=lr_decoder).minimize(self.loss_cyclic, var_list=self._param_decoder)
        saver_encoder = tf.train.Saver(self._param_encoder,max_to_keep=15)
        saver_decoder = tf.train.Saver(self._param_decoder,max_to_keep=15)
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            writer = tf.summary.FileWriter(output_tensorboard_folder, graph=sess.graph)
            epoch = 0
            dataloader = DataLoader(dataset=dataset, mode="train")
            enc_losses = []
            dec_losses = []
            best_enc_loss = float('inf')
            best_enc_epoch = -1
            best_dec_loss = float('inf')
            best_dec_epoch = -1
            while epoch < max_epochs:
                print("Training epoch %d" % epoch)
                dataloader.reset()
                k = 0
                enc_training_loss = 0.
                dec_training_loss = 0.
                while dataloader.has_more():
                    x_inputs, current_batch_size = dataloader.next_batch(batch_size)
                    x_inputs = np.reshape(x_inputs, (-1,self.num_joints,self.num_params_per_joint))
                    z_data = np.random.normal(size=[current_batch_size, self.num_zdim])
                    feed_dict = {self.input_x:x_inputs, self.input_z:z_data}
                    enc_loss, _ = sess.run([self.loss_cyclic, encoder_op], feed_dict=feed_dict)
                    dec_loss, _ = sess.run([self.loss_cyclic, decoder_op], feed_dict=feed_dict)
                    print("Minibatch %d training loss----encoder loss:%f, decoder loss:%f" % (k, enc_loss, dec_loss))
                    summary = sess.run(summary_op, feed_dict=feed_dict)
                    writer.add_summary(summary, epoch)
                    enc_training_loss += enc_loss
                    dec_training_loss += dec_loss
                    k += 1
                if k>0:
                    enc_training_loss /= k
                    dec_training_loss /= k
                    enc_losses.append(enc_training_loss)
                    dec_losses.append(dec_training_loss)
                    print("Average training loss of epoch %d----encoder loss:%f, decoder loss:%f" % (epoch, enc_training_loss, dec_training_loss))
                if epoch > 5 or epoch == max_epochs:
                    if enc_training_loss < best_enc_loss:
                        best_enc_loss = enc_training_loss
                        best_enc_epoch = epoch
                        save_path = saver_encoder.save(sess, os.path.join(output_model_folder,"bestenc"), global_step=epoch)
                        print("Save current encoder model in %s" % save_path)
                    if dec_training_loss < best_dec_loss:
                        best_dec_loss = dec_training_loss
                        best_dec_epoch = epoch
                        save_path = saver_decoder.save(sess, os.path.join(output_model_folder,"bestdec"), global_step=epoch)
                        print("Save current best decoder model in %s" % save_path)
                    if epoch % save_every_epochs == 0:
                        save_path = saver_encoder.save(sess, os.path.join(output_model_folder,"iterenc"), global_step=epoch)
                        print("Save encoder model every %d epochs in %s" % (save_every_epochs, save_path))
                        save_path = saver_decoder.save(sess, os.path.join(output_model_folder,"iterdec"), global_step=epoch)
                        print("Save decoder model every %d epochs in %s" % (save_every_epochs, save_path))
                if epoch % sample_every_epochs == 0:
                    testdataloader = DataLoader(dataset=dataset, mode="test")
                    x_inputs, current_batch_size = testdataloader.next_batch(sample_batch_size)
                    n_samples, n_length, n_body = x_inputs.shape
                    x_inputs = np.reshape(x_inputs, (-1,self.num_joints,self.num_params_per_joint))
                    z_data = np.random.normal(size=[current_batch_size, self.num_zdim])
                    feed_dict = {self.input_x:x_inputs, self.input_z:z_data}
                    encoded, decoded, loss = sess.run([self.z_real, self.x_recon, self.loss_cyclic], feed_dict=feed_dict)
                    print("Testing loss of epoch %d----cyclic loss:%f"%(epoch,loss))
                    decoded = testdataloader.postprocessing(np.reshape(decoded, (n_samples, n_length, n_body)))
                    x_inputs = testdataloader.postprocessing(np.reshape(x_inputs, (n_samples, n_length, n_body)))
                    np.save(os.path.join(output_result_folder, "origin_{}".format(epoch)), x_inputs)
                    np.save(os.path.join(output_result_folder, "encoded_{}".format(epoch)), encoded)
                    np.save(os.path.join(output_result_folder, "recon_{}".format(epoch)), decoded)
                epoch += 1
            writer.close()
            return best_enc_epoch, best_dec_epoch

    def encode(self, encode_data):
        encoded = self.applysess.run(self.z_real, feed_dict={self.input_x:encode_data})
        return encoded
        
    def decode(self, decode_data):
        decoded = self.applysess.run(self.x_recon, feed_dict={self.input_z:decode_data})
        return decoded

    def load_model(self, file_encoder, file_decoder):
        self.input_x = tf.placeholder(tf.float32, shape=[None, self.num_joints, self.num_params_per_joint], name='input_x')
        self.input_z = tf.placeholder(tf.float32, shape=[None, self.num_zdim], name='input_z')
        encoder = self._encoderNet(self.input_x)
        self.z_real = encoder['z_joints']
        decoder = self._decoderNet(self.input_z)
        self.x_recon = decoder['full_body_x']
        param_encoder = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self._encoder_scope)
        param_decoder = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self._decoder_scope)
        # print("encoder parameters:",param_encoder)
        saver_encoder = tf.train.Saver(param_encoder)
        saver_decoder = tf.train.Saver(param_decoder)
        self.applysess = tf.Session()
        saver_encoder.restore(self.applysess, file_encoder)
        saver_decoder.restore(self.applysess, file_decoder)
    
    @property
    def z_size(self):
        return self.num_zdim

    @property
    def encoded(self):
        return self._encoded
    
    @property
    def decoded(self):
        return self._decoded
    
    @property
    def param_encoder(self):
        return self._param_encoder
    
    @property
    def param_decoder(self):
        return self._param_decoder
    
    def build_encoder(self, input_x):
        encoder = self._encoderNet(input_x)
        self._encoded = encoder['z_joints']
        self._param_encoder = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self._encoder_scope)
    
    def build_decoder(self, input_z):
        decoder = self._decoderNet(input_z)
        self._decoded = decoder['full_body_x']
        self._param_decoder = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=self._decoder_scope)
    


        
