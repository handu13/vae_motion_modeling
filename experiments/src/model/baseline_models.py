import os
import sys
from pathlib import Path
abspath = os.path.abspath(__file__)
sys.path.insert(0, str(Path(__file__).parent.absolute()) + r'/../../..')
sys.path.insert(0, str(Path(__file__).parent.absolute()) + r'/..')
# from nn.Generator import SequenceToSequenceGenerator_BiHMPGAN
import tensorflow as tf
from tensorflow.keras import layers, Model
import numpy as np
cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)
# rng = np.random.RandomState(23456)
# tf.random.set_seed(rng)
from mosi_dev_deepmotionmodeling.utilities.utils import get_files, export_point_cloud_data_without_foot_contact, write_to_json_file
EPS = 1e-6

# def test_biHMP_geneartor():
#     g = SequenceToSequenceGenerator_BiHMPGAN()




class NaiveVAE(tf.keras.Model):

    def __init__(self, output_dim, hidden_dim, activation=tf.nn.elu):
        super().__init__()
        self.activation = activation
        self.output_dim = output_dim
        self.hidden_dim = hidden_dim
        self.encoder_l1 = layers.Dense(1024, activation=self.activation)
        self.encoder_l2 = layers.Dense(self.hidden_dim, activation=self.activation)
        self.decoder_l1 = layers.Dense(1024, activation=self.activation)
        self.decoder_l2 = layers.Dense(self.output_dim)

    def _encode(self, inputs):
        l1_res = self.encoder_l1(inputs)
        mu = self.encoder_l2(l1_res)
        log_sigma = self.encoder_l2(l1_res)
        return mu, log_sigma

    def _decode(self, inputs):
        l1_res = self.decoder_l1(inputs)
        return self.decoder_l2(l1_res)
    
    def sample(self, mu, log_sigma):
        eps = tf.random.normal(tf.shape(input=mu), dtype=tf.float32, mean=0., stddev=1.0,
                               name='epsilon')
        # return mu + tf.exp(log_sigma / 2) * eps   
        return mu + (log_sigma / 2) * eps     

    def call(self, inputs):
        mu, log_sigma = self._encode(inputs)
        hidden_sample = self.sample(mu, log_sigma)
        return self._decode(hidden_sample)
    
    def sample_new_motions(self, n_samples):
        noise = tf.random.normal([n_samples, self.hidden_dim], dtype=tf.float32)
        return self._decode(noise)


def train_naiveVAE():
    training_data = np.load(r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\clipwise\h36m.npz')['clips']
    training_data = np.asarray(training_data, dtype=np.float32)
    
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    training_data = (training_data - mean_pose) / std_pose
    n_samples, n_frames, n_dims = training_data.shape
    reshaped_training_data = np.reshape(training_data, (n_samples, n_frames * n_dims))
    input_dims = n_frames * n_dims
    epochs = 100
    batchsize = 32
    learning_rate = 1e-4
    model = NaiveVAE(output_dim=input_dims, hidden_dim=10)
    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../data/models/naiveVAE_clip_modeling'
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    file_path = os.path.join(save_path, 'cp-{epoch:04d}.ckpt')
    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=file_path, verbose=1, save_weights_only=True, period=10)
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate),
                  loss='mse')
    model.fit(reshaped_training_data, reshaped_training_data, epochs=epochs, batch_size=batchsize, callbacks=[cp_callback])


class NaiveGan(object):

    def __init__(self, name, generator, discriminator):
        self.name = name
        self.g = generator
        self.d = discriminator

    def build(self, g_input_shape, d_input_shape):
        # self.g.build(g_input_shape)
        # self.d.build(d_input_shape)
        self.input_shape = g_input_shape[-1]
        self.trainable_variables = self.g.trainable_variables + self.d.trainable_variables
    
    def train(self, training_data, epochs, batchsize, save_every_epochs=None, save_path=None):
        self.generator_optimizer = tf.keras.optimizers.Adam(1e-4)
        self.discriminator_optimizer = tf.keras.optimizers.Adam(1e-4)
        num_training_data = len(training_data)
        n_batches = num_training_data // batchsize
        for epoch in range(epochs):
            disc_errors = []
            gen_errors = []
            batchinds = np.arange(n_batches)
            # for i in range(n_batches):
            #     gen_loss, disc_loss = self.train_step(training_data[i*batchsize: (i+1)*batchsize])
            for idx, batch_idx in enumerate(batchinds):
                gen_loss, disc_loss = self.train_step(training_data[batch_idx * batchsize: (batch_idx+1) * batchsize])
                disc_errors.append(disc_loss.numpy())
                gen_errors.append(gen_loss.numpy())    
                sys.stdout.write('\r[Epoch %i] %0.1f%% gen_loss %.5f disc_loss %.5f' % (epoch, 100 * idx / (n_batches - 1), np.mean(gen_errors), np.mean(disc_errors)))        
                sys.stdout.flush()
            print('')
            if save_every_epochs is not None and save_path is not None:
                if (epoch+1) % save_every_epochs == 0:
                    self.save(save_path, suffix=str(epoch+1))
            
    @tf.function
    def train_step(self, batch):

        random_noise = tf.random.normal([len(batch), self.input_shape], dtype=tf.float32)
        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            generated_samples = self.g(random_noise)
            real_output = self.d(batch, training=True)
            fake_output = self.d(generated_samples, training=True)
            gen_loss = self.generator_loss(fake_output)
            disc_loss = self.discriminator_loss(real_output, fake_output)
        gradients_of_generator = gen_tape.gradient(gen_loss, self.g.trainable_variables)
        gradients_of_discriminator = disc_tape.gradient(disc_loss, self.d.trainable_variables)  
        self.generator_optimizer.apply_gradients(zip(gradients_of_generator, self.g.trainable_variables))
        self.discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, self.d.trainable_variables))      
        return gen_loss, disc_loss

    def generator_loss(self, fake_output):
        return cross_entropy(tf.ones_like(fake_output), fake_output)
    
    def discriminator_loss(self, real_output, fake_output):
        real_loss = cross_entropy(tf.ones_like(real_output), real_output)
        fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
        total_loss = real_loss + fake_loss
        return total_loss  

    def save(self, save_path, prefix='', suffix=''):
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        gen_strings = [self.name, prefix, 'generator', suffix]
        disc_strings = [self.name, prefix, 'discriminator', suffix]
        self.g.save_weights(os.path.join(save_path, '_'.join([x for x in gen_strings if x is not '']) + '.ckpt'))
        self.d.save_weights(os.path.join(save_path, '_'.join([x for x in disc_strings if x is not '']) + '.ckpt')) 

    def load(self, path, prefix='', suffix=''):
        gen_strings = [self.name, prefix, 'generator', suffix]
        disc_strings = [self.name, prefix, 'discriminator', suffix]
        self.g.load_weights(os.path.join(path, '_'.join([x for x in gen_strings if x is not '']) + '.ckpt'))
        self.d.load_weights(os.path.join(path, '_'.join([x for x in disc_strings if x is not '']) + '.ckpt')) 

    def sample_new_motions(self, n_samples):
        random_noise = tf.random.normal([n_samples, self.input_shape], dtype=tf.float32)
        return self.g(random_noise)


class Generator(tf.keras.Model):

    def __init__(self, output_dim, dropout_rate):
        super().__init__()
        self.dense_layer1 = layers.Dense(1024, activation=tf.nn.relu)
        self.dropout = layers.Dropout(rate=dropout_rate)
        self.output_layer = layers.Dense(output_dim)
    
    def call(self, inputs, training=None):
        # if training:
        #     inputs = self.dropout(inputs)
        layer1_ouptut = self.dense_layer1(inputs)
        # if training:
        #     layer1_ouptut = self.dropout(layer1_ouptut)
        output = self.output_layer(layer1_ouptut)
        return output


class Discriminator(tf.keras.Model):
    def __init__(self, output_dim, dropout_rate):
        super().__init__()
        self.dropout_rate = dropout_rate
        self.output_dim = output_dim
        self.dense_layer1 = layers.Dense(128, activation=tf.nn.relu)
        self.output_layer = layers.Dense(self.output_dim)
        self.dropout = layers.Dropout(dropout_rate)
    
    def call(self, inputs, training=None):
        assert len(inputs.shape) == 2
        if training:
            inputs = self.dropout(inputs)
        layer1_output = self.dense_layer1(inputs)

        output = self.output_layer(layer1_output)
        return output
      


def test_gan():
    training_data = np.load(r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\clipwise\h36m.npz')['clips']
    training_data = np.asarray(training_data, dtype=np.float32)
    
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)

    n_samples, n_frames, n_dims = training_data.shape
    training_data = (training_data - mean_pose) / std_pose
    g = Generator(n_frames * n_dims, 0.3)
    d = Discriminator(1, 0.3)
    epochs = 1000
    model = NaiveGan('naive_gan_h36m', g, d)
    model.build((None, 10),(None, n_frames * n_dims))
    reshape_training_data = np.reshape(training_data, (n_samples, n_frames * n_dims))

    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../data/models/naive_gan_clip_modeling'
    model.train(reshape_training_data, epochs, 32, save_every_epochs=100, save_path=save_path)
    # model.save(save_path)
    # model.load(save_path)

    # new_motions = model.sample_new_motions(10)
    # print(new_motions.shape)


def test():
    path = os.path.dirname(os.path.abspath(__file__)) + r'/../../data/models/naive_gan_clip_modeling'
    print(path)



def evaluate_naive_gan():
    "load models at different training phase and sample"
    training_data = np.load(r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\clipwise\h36m.npz')['clips']
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    
    g = Generator(5400, 0.3)
    d = Discriminator(1, 0.3)
    epochs = 100
    model = NaiveGan('naive_gan_h36m', g, d)
    model.build((None, 10),(None, 5400))
    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../data/models/naive_gan_clip_modeling'    
    epochs = 1000
    save_every_epochs=100
    n_samples = 10
    export_sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../data/models/naive_gan_clip_modeling/samples'
    for i in range(epochs):
        if (i+1) % save_every_epochs == 0:
            model.load(save_path, suffix=str(i+1))
            new_samples = model.sample_new_motions(n_samples)
            reshape_new_samples = np.reshape(new_samples, (n_samples, 60, 90))
            reshape_new_samples = reshape_new_samples * std_pose + mean_pose
            sample_dir = os.path.join(export_sample_path, str(i+1))
            if not os.path.exists(sample_dir):
                os.makedirs(sample_dir)
            for j in range(n_samples):
                filename = os.path.join(sample_dir, str(j) + '.panim')
                export_point_cloud_data_without_foot_contact(reshape_new_samples[j], filename)


def evaluate_naiveVAE():
    "load models at different training phase and sample"
    training_data = np.load(r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\clipwise\h36m.npz')['clips']
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    n_samples, n_frames, n_dims = training_data.shape
    input_dim = n_frames * n_dims
    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../data/models/naiveVAE_clip_modeling'
    model = NaiveVAE(output_dim=input_dim, hidden_dim=10)
    model.build((None, input_dim))
    epochs = 100
    save_every_epochs=10
    for i in range(epochs):
        if (i+1) % save_every_epochs == 0:
            
            model.load_weights(os.path.join(save_path, 'cp-{epoch:04d}.ckpt'.format(epoch = i+1)))
            res = model.sample_new_motions(10)
            new_motions = np.reshape(res, (10, n_frames, n_dims))
            new_motions = new_motions * std_pose + mean_pose
            sample_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)) + r'/../../data/models/naiveVAE_clip_modeling/samples', str(i+1))  
            if not os.path.exists(sample_dir):
                os.makedirs(sample_dir)
            for j in range(10):
                filename = os.path.join(sample_dir, str(j) + '.panim')
            
                export_point_cloud_data_without_foot_contact(new_motions[j], filename=filename)




def visualize_training_data():
    training_data = np.load(r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\clipwise\h36m.npz')['clips']
    training_data = np.asarray(training_data, dtype=np.float32)




if __name__ == "__main__":
    test_gan()
    # test()
    # evaluate_naive_gan()
    # visualize_training_data()
    # train_naiveVAE()
    # evaluate_naiveVAE()