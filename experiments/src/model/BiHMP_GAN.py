import sys
sys.path.append("..")
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import numpy as np
import os
from datatools.dataloader import DataLoader

class BiHMP_GAN(object):
    def __init__(self, generator, discriminator, g_inputs, d_inputs, g_z, sess=None, train_embedding=False, use_embedding=False, name=None):
        '''
        Initialize the BiHMP_GAN model.
        Args:
            generator: a Generator object that used in this GAN model.
            discriminator: a Discriminator object that used in this GAN model.
            g_inputs: tf.placeholder, the input of generator, in shape [batch_size, input_sequence_length, element_size].
            d_inputs: tf.placeholder, the input of discriminator, in shape [batch_size, sequence_length, element_size].
            g_z: tf.placeholder, the random vectors used as input to the generator, in shape [batch_size, z_size].
            train_embedding: True if the generator and discriminator is trained with a spatial encoder-decoder.
            use_embedding: True to use a pretrained spatial encoder-decoder to encode data when loading data.
        '''
        self._g = generator
        self._d = discriminator
        self._g_inputs = g_inputs
        self._d_inputs = d_inputs
        self._g_z = g_z
        self.name = name
        self._z_dims = g_z.shape[1]
        self._train_embedding = train_embedding
        if (not train_embedding) and use_embedding:
            self._dataembedding = True
        else:
            self._dataembedding = False
        if sess is not None:
            self._sess = sess
        else:
            self._sess = tf.InteractiveSession()
        self._saver = tf.train.Saver(max_to_keep=10)
        self._build_model()
    
    def _build_model(self):
        self._d_real = self._d.outputs
        self._r_real = self._d.R
        self._d_fake_inputs = tf.concat([self._g_inputs, self._g.outputs], axis=1)
        self._d_fake, self._r_fake = self._d.connect(self._d_fake_inputs)
        self._g_hat = self._g.connect(self._g_inputs, self._r_real)
    
    def _gradient_penalty(self, real_inputs, fake_inputs):
        alpha = tf.random_uniform([], 0.0, 1.0)
        d_inputs_hat = alpha * real_inputs + (1 - alpha) * fake_inputs
        d_outputs_hat, _ = self._d.connect(d_inputs_hat)
        gradients = tf.gradients(d_outputs_hat, d_inputs_hat)[0]
        if self._train_embedding:
            gradients_l2 = tf.sqrt(tf.reduce_sum(tf.square(gradients), axis=[2,3]))
        else:
            gradients_l2 = tf.sqrt(tf.reduce_sum(tf.square(gradients), axis=[1,2]))
        return tf.reduce_mean(tf.square(gradients_l2 - 1.))
    
    def _define_loss(self, output_sequence_length, batch_size, lambda_r, lambda_c):
        g_wgan_loss = -tf.reduce_mean(self._d_fake)
        r_rec_loss = tf.maximum(0.0001, tf.norm(self._g_z-self._r_fake, ord=2) / batch_size)
        if self._train_embedding:
            ground_truth = self._d_inputs[:,-(output_sequence_length):, :, :]
            content_loss = tf.maximum(0.0001, tf.norm(ground_truth-self._g_hat, ord=2) / batch_size)
        else:
            ground_truth = self._d_inputs[:,-(output_sequence_length):, :]
            content_loss = tf.maximum(0.0001, tf.norm(ground_truth-self._g_hat, ord=2) / batch_size)
        enc_loss = g_wgan_loss + lambda_c * content_loss
        dec_loss = g_wgan_loss + lambda_r * r_rec_loss + lambda_c * content_loss
        gradient_penalty_loss = 10.0 * self._gradient_penalty(self._d_inputs, self._d_fake_inputs)
        d_wgan_loss = tf.reduce_mean(self._d_fake - self._d_real) + gradient_penalty_loss
        d_loss = d_wgan_loss + lambda_r*10* r_rec_loss + lambda_c * content_loss
        return enc_loss, dec_loss, d_loss, content_loss, r_rec_loss, gradient_penalty_loss
    
    def _generate_random(self, shape, rand_type="normal", rand_params=None):
        if rand_type == 'normal':
            if rand_params:
                return np.random.normal(rand_params['mean'], rand_params['std'], size=shape)
            else:
                return np.random.normal(size=shape)
        if rand_type == 'uniform':
            if rand_params:
                return np.random.uniform(rand_params['low'], rand_params['high'], size=shape)
            else:
                return np.random.uniform(size=shape)
            
    def train(self, dataset, batch_size, input_sequence_length, output_sequence_length, g_lr, d_lr, lambda_r, lambda_c, 
              max_epochs, output_model_folder, output_tensorboard_folder, output_result_folder, sample_batch_size=10,
              rand_type='normal', rand_params=None, d_iterations=5, g_iterations=1, save_every_epochs=10, sample_every_epochs=10):
        '''
        Args:
            dataset: dataset's name, used to creat a dataloader.(TO DO)
            batch_size: size of minibatch used for training.
            input_sequence_length: the length of given previous sequence.
            output_sequence_length: the length of predicted sequence.
            g_lr, d_lr: learning rate of generator and discriminator.
            lambda_r, lambda_c: weights of r reconstruction loss and content loss.
            max_epochs: maximun number of training epochs.
            rand_type: 'normal' or 'uniform', the name of the distribution where random vectors are sampled from.
            rand_params: dict of parameters of the random distribution. If random_type is 'normal', it should be {'mean':, 'std':}
                          if random_type is 'uniform', it should be {'low':,'high:'}. If None, default values in numpy functions will be used.  
            d_iterations: number of training times to train discriminator in a single training iteration, usually more than g's.
            g_iterations: number of training times to train generator in a single training iteration.
            save_every_epochs: default 100, epochs to save model during training.
        '''
        if not os.path.exists(output_model_folder):
            os.mkdir(output_model_folder)
        if not os.path.exists(output_tensorboard_folder):
            os.mkdir(output_tensorboard_folder)
        if not os.path.exists(output_result_folder):
            os.mkdir(output_result_folder)
        #define loss
        self.enc_loss, self.dec_loss, self.d_loss, content_loss, r_rec_loss, gp_loss = self._define_loss(output_sequence_length, batch_size, lambda_r, lambda_c)
        tf.summary.scalar("encoder_loss", self.enc_loss)
        tf.summary.scalar("decoder_loss", self.dec_loss)
        tf.summary.scalar("discriminator_loss", self.d_loss)
        tf.summary.scalar("content_loss", content_loss)
        tf.summary.scalar("R_recon_loss", r_rec_loss)
        summary_op = tf.summary.merge_all()
        #define optimizer
        enc_op = tf.train.AdamOptimizer(learning_rate=g_lr,beta1=0.5,beta2=0.9).minimize(self.enc_loss, var_list=self._g.enc_parameters)
        dec_op = tf.train.AdamOptimizer(learning_rate=g_lr,beta1=0.5,beta2=0.9).minimize(self.dec_loss, var_list=self._g.dec_parameters)
        d_op = tf.train.AdamOptimizer(learning_rate=d_lr,beta1=0.5,beta2=0.9).minimize(self.d_loss, var_list=self._d.parameters)
        #create a data loader
        dataloader = DataLoader(dataset=dataset, mode="train", embedding=self._dataembedding)
        sample_input_data,_ = dataloader.next_batch(sample_batch_size)
        #start training
        init_op = tf.global_variables_initializer()
        writer = tf.summary.FileWriter(output_tensorboard_folder, graph=tf.get_default_graph())
        g_best_loss = float('inf')
        g_best_epoch = -1
        d_losses = []
        enc_losses = []
        dec_losses = []
        self._sess.run(init_op)
        epoch = 0
        while epoch <= max_epochs:
            print("Training epoch %d" % (epoch))
            d_training_loss = 0.
            enc_training_loss = 0.
            dec_training_loss = 0.
            k = 0
            dataloader.reset()
            while dataloader.has_more():
                input_data, current_batch_size = dataloader.next_batch(batch_size)
                if current_batch_size < batch_size:
                    continue
                if self._train_embedding:
                    input_sequence = input_data[:, 0:input_sequence_length, :, :]
                else:
                    input_sequence = input_data[:, 0:input_sequence_length, :]
                z_data = self._generate_random(shape=[batch_size, self._z_dims], rand_type=rand_type, rand_params=rand_params)
                feed_dict = {self._d_inputs:input_data, self._g_inputs:input_sequence, self._g_z:z_data}
                for _ in range(d_iterations):
                    _, d_loss_val, rec_loss_val, gp_loss_val = self._sess.run([d_op, self.d_loss, r_rec_loss, gp_loss], feed_dict=feed_dict)
                print("Minibatch %d discriminator loss-----discriminator_loss:%f, R recon loss:%f, GP loss:%f" % (k, d_loss_val, lambda_r*rec_loss_val, gp_loss_val))
                for _ in range(g_iterations):
                    _, enc_loss_val, enc_content_loss = self._sess.run([enc_op, self.enc_loss, content_loss], feed_dict=feed_dict)
                    _, dec_loss_val, dec_content_loss, dec_recon_loss = self._sess.run([dec_op, self.dec_loss, content_loss, r_rec_loss], feed_dict=feed_dict)
                print("Minibatch %d encoder loss-----encoder loss:%f, content loss:%f" % (k, enc_loss_val, lambda_c*enc_content_loss))
                print("Minibatch %d decoder loss-----decoder loss:%f, content loss:%f, R recon loss:%f" % (k, dec_loss_val, lambda_c*dec_content_loss, lambda_r*dec_recon_loss))
                summary = self._sess.run(summary_op, feed_dict=feed_dict)
                writer.add_summary(summary, epoch)
                d_training_loss += d_loss_val
                enc_training_loss += enc_loss_val
                dec_training_loss += dec_loss_val
                k += 1
            if k>0:
                d_training_loss /= k
                enc_training_loss /= k
                dec_training_loss /= k
                d_losses.append(d_training_loss)
                enc_losses.append(enc_training_loss)
                dec_losses.append(dec_training_loss)
                print("Average training loss of epoch %d-----encoder loss:%f, decoder_loss:%f, discriminator_loss:%f" % (epoch, enc_training_loss, dec_training_loss, d_training_loss))
            if epoch > 5 or epoch==max_epochs:
                save_model = False
                g_training_loss = enc_training_loss + dec_training_loss
                if g_training_loss < g_best_loss:
                    save_model = True
                    g_best_loss = g_training_loss
                    g_best_epoch = epoch
                if epoch % save_every_epochs==0:
                    save_model = True
                if save_model:
                    self.save_model(os.path.join(output_model_folder, "bihmpgan"), global_step=epoch)
            if epoch % sample_every_epochs == 0:
                sample_data = self.sample(10, sample_input_data, input_sequence_length, dataloader)
                np.save(os.path.join(output_result_folder, "sample_{}_bihmpgan".format(epoch)), sample_data)
            epoch += 1
        writer.close()
        return g_best_epoch
    
    def sample(self, num_generate, input_data, input_sequence_length, dataloader):
        '''
        Args:
            num_generate: the number of predicted sequences to generate for each input sequences.
            input_sequence_length: the length of given previous sequence.
        '''
        if self._train_embedding:
            input_sequence = input_data[:, 0:input_sequence_length, :, :]
        else:
            input_sequence = input_data[:, 0:input_sequence_length, :]
        skeleton_data = []
        d_losses = 0.
        enc_losses = 0.
        dec_losses = 0.
        for _ in range(num_generate):
            z_data = self._generate_random(shape=[input_data.shape[0], self._z_dims])
            feed_dict={self._g_inputs:input_sequence, self._d_inputs:input_data, self._g_z:z_data}
            pred, enc_loss, dec_loss, d_loss = self._sess.run([self._g.outputs, self.enc_loss, self.dec_loss, self.d_loss], feed_dict=feed_dict)
            d_losses += d_loss
            enc_losses += enc_loss
            dec_losses += dec_loss
            output_sequence = np.concatenate((input_sequence, pred), axis=1)
            if self._dataembedding:
                output_sequence = dataloader.postprocessing(output_sequence)
            skeleton_data.append(output_sequence)
        d_losses = d_losses / num_generate
        enc_losses = enc_losses / num_generate
        dec_losses = dec_losses / num_generate
        print("Average testing loss-----encoder loss:%f, decoder_loss:%f, descriminator_loss:%f" % (enc_losses, dec_losses, d_losses))
        return skeleton_data

    def save_model(self, model_file, global_step=None):
        save_path = self._saver.save(self._sess, model_file, global_step=global_step)
        print("Model saved in file: %s" % save_path)
        return save_path

    def load_model(self, model_file):
        self._saver.restore(self._sess, model_file)
    
