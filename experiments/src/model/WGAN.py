import tensorflow as tf
import numpy as np
import os
#TO DO:
#  dataloader

class WGAN(object):
    def __init__(self, generator, discriminator, g_inputs, d_inputs, g_z, sess):
        '''
        Initialize the WGAN-GP model(with condiction).
        Args:
            generator: a Generator object that used in this GAN model.
            discriminator: a Discriminator object that used in this GAN model.
            g_inputs: tf.placeholder, the input of generator.(the condition)
            d_inputs: tf.placeholder, the input of discriminator.
            g_z: tf.placeholder, the random vectors used as input to the generator.
        '''
        self._g = generator
        self._d = discriminator
        self._g_inputs = g_inputs
        self._d_inputs = d_inputs
        self._g_z = g_z
        if sess is not None:
            self._sess = sess
        else:
            self._sess = tf.InteractiveSession()
        self._saver = tf.train.Saver()
        self._build_model()

    def _build_model(self):
        '''
        Build the GAN model by connecting generator, discriminator and other components.
        '''
        self._d_real = self._d.outputs
        self._d_fake_inputs = tf.concat([self._g_inputs, self._g.outputs], axis=1)
        self._d_fake = self._d.connect(self._d_fake_inputs)

    def _gradient_penalty(self, real_inputs, fake_inputs):
        alpha = tf.random_uniform([], 0.0, 1.0)
        d_inputs_hat = alpha * real_inputs + (1 - alpha) * fake_inputs
        d_outputs_hat = self._d.connect(d_inputs_hat)
        gradients = tf.gradients(d_outputs_hat, d_inputs_hat)[0]
        gradients_l2 = tf.sqrt(tf.reduce_sum(tf.square(gradients), axis=[2,3]))
        return tf.reduce_mean(tf.square(gradients_l2 - 1.))

    def _generator_loss(self):
        g_gan_loss = -tf.reduce_mean(self._d_fake)
        g_loss = g_gan_loss
        return g_loss
    
    def _discriminator_loss(self):
        gradient_penalty_loss = 10.0 * self._gradient_penalty(self._d_inputs, self._d_fake_inputs)
        d_loss = tf.reduce_mean(self._d_fake - self._d_real) + gradient_penalty_loss
        return d_loss

    def _generate_random(self, shape, rand_type="normal", rand_params=None):
        if rand_type == 'normal':
            if rand_params:
                return np.random.normal(rand_params['mean'], rand_params['std'], size=shape)
            else:
                return np.random.normal(size=shape)
        if rand_type == 'uniform':
            if rand_params:
                return np.random.uniform(rand_params['low'], rand_params['high'], size=shape)
            else:
                return np.random.uniform(size=shape)

    def train(self, dataset, batch_size, z_dims, g_lr, d_lr, 
              max_epochs, output_models_folder, output_tensorboard_folder, 
              rand_type='normal', rand_params=None, d_iterations=10, g_iterations=2):
        '''
        Args:
            dataset: dataset's name, used to creat a dataloader.(TO DO)
            batch_size: size of minibatch used for training. 
            z_dims: size of random vectors. 
            g_lr, d_lr: learning rate of generator and discriminator.
            max_epochs: maximun number of training epochs.
            rand_type: 'normal' or 'uniform', the name of the distribution where random vectors are sampled from.
            rand_params: dict of parameters of the random distribution. If random_type is 'normal', it should be {'mean':, 'std':}
                          if random_type is 'uniform', it should be {'low':,'high:'}. If None, default values in numpy functions will be used.  
            d_iterations: number of training times to train discriminator in a single training iteration, usually more than g's.
            g_iterations: number of training times to train generator in a single training iteration.
        '''
        #define optimizer
        g_loss = self._generator_loss()
        d_loss = self._discriminator_loss()
        g_op = tf.train.AdamOptimizer(learning_rate=g_lr).minimize(g_loss, var_list=self._g.parameters)
        d_op = tf.train.AdamOptimizer(learning_rate=d_lr).minimize(d_loss, var_list=self._d.parameters)
        #create a data loader
        data_loader = data_loader(dataset) #TO DO: data_loader
        #start training
        init_op = tf.global_variables_initializer()
        writer = tf.summary.FileWriter(output_tensorboard_folder, graph=tf.get_default_graph())
        g_best_loss = float('inf')
        g_best_epoch = -1
        d_losses = []
        g_losses = []
        self._sess.run(init_op)
        epoch = 0
        while epoch < max_epochs:
            d_training_loss = 0.
            g_training_loss = 0.
            k = 0
            while data_loader.has_more():
                input_data, condition = data_loader.next_batch(batch_size)
                z_data = self._generate_random(shape=[batch_size, z_dims], rand_type=rand_type, rand_params=rand_params)
                feed_dict = {self._d_inputs:input_data, self._g_inputs:condition, self._g_z:z_data}
                for _ in range(d_iterations):
                    _, d_loss_val = self._sess.run([d_op, d_loss], feed_dict=feed_dict)
                for _ in range(g_iterations):
                    _, g_loss_val = self._sess.run([g_op, g_loss], feed_dict=feed_dict)
                d_training_loss += d_loss_val
                g_training_loss += g_loss_val
                k += 1
            if k>0:
                d_losses.append(d_training_loss / k)
                g_losses.append(g_training_loss / k)
            if epoch > 20 or epoch==max_epochs:
                save_model = False
                if g_training_loss < g_best_loss:
                    save_model = True
                    g_best_loss = g_training_loss
                    g_best_epoch = epoch
                if epoch%100==0:
                    save_model = True
                if save_model:
                    self.save_model(os.path.join(output_models_folder, "models"), global_step=epoch+1)
        writer.close()
                
    def generate(self, num_generate, z_dims, condition):
        '''
        Args:
            num_generate: the number of predicted sequences to generate for each input sequences.
            z_dims: size of random vectors. 
            condition: to generate data under this condition.
        '''
        batch_size = input_sequence.shape[0]
        generate_data = []
        for _ in range(num_generate):
            z_data = self._generate_random(shape=[batch_size, z_dims])
            pred = self._sess.run(self._g.outputs, feed_dict={self._g_inputs:condition, self._g_z:z_data})
            generate_data.append(pred)
        return generate_data

    def save_model(self, model_file, global_step=None):
        save_path = self._saver.save(self._sess, model_file, global_step=global_step)
        print("Model saved in file: %s" % save_path)

    def load_model(self, model_file):
        self._saver.restore(self._sess, model_file)
        

    

        