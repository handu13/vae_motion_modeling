import numpy as np
import tensorflow as tf

def bone_loss(prev_sequence, next_sequence, body):
    ''' Compare the bone size of the predicted sequence from the last skeleton
        in the input sequence. '''
    #prev_sequence: last skeleton in g_input + skeletons in g_output except for the last one
    #next_sequence: g_output
    #both sequences shape: [batch, sequence, 32, 3]
    num_of_skeletons = prev_sequence.shape[1] 
    num_of_joints = prev_sequence.shape[2] 

    loss = 0.
    for skeleton_index in range(num_of_skeletons):
        for bone in body.bones:
            i = bone[0] #bone start
            j = bone[1] #bone end

            x1 = prev_sequence[:, 0, i, 0]
            y1 = prev_sequence[:, 0, i, 1]
            z1 = prev_sequence[:, 0, i, 2]

            x2 = prev_sequence[:, 0, j, 0]
            y2 = prev_sequence[:, 0, j, 1]
            z2 = prev_sequence[:, 0, j, 2]

            bone_length_ref = tf.sqrt(tf.squared_difference(x2, x1) + tf.squared_difference(y2, y1) + tf.squared_difference(z2, z1))

            x1 = next_sequence[:, skeleton_index, i, 0]
            y1 = next_sequence[:, skeleton_index, i, 1]
            z1 = next_sequence[:, skeleton_index, i, 2]

            x2 = next_sequence[:, skeleton_index, j, 0]
            y2 = next_sequence[:, skeleton_index, j, 1]
            z2 = next_sequence[:, skeleton_index, j, 2]

            bone_length = tf.sqrt(tf.squared_difference(x2, x1) + tf.squared_difference(y2, y1) + tf.squared_difference(z2, z1))

            loss += tf.reduce_sum(tf.abs(bone_length_ref - bone_length) / (bone_length_ref + 0.00001))
    return loss