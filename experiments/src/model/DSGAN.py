import sys
sys.path.append("..")
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import numpy as np
import os
from datatools.dataloader import DataLoader
#TO DO:
#  dataloader

class DSGAN(object):
    def __init__(self, generator, discriminator, g_inputs, d_inputs, g_z, sess=None, train_embedding=False, use_embedding=False):
        '''
        Initialize the BiHMP_GAN model.
        Args:
            generator: a Generator object that used in this GAN model.
            discriminator: a Discriminator object that used in this GAN model.
            g_inputs: tf.placeholder, the input of generator, in shape [batch_size, input_sequence_length, element_size].
            d_inputs: tf.placeholder, the input of discriminator, in shape [batch_size, sequence_length, element_size].
            g_z: tf.placeholder, the random vectors used as input to the generator, in shape [batch_size, z_size].
            train_embedding: True if the generator and discriminator is trained with a spatial encoder-decoder.
            use_embedding: True to use a pretrained spatial encoder-decoder to encode data when loading data.
        '''
        self._g = generator
        self._d = discriminator
        self._g_inputs = g_inputs
        self._d_inputs = d_inputs
        self._g_z = g_z
        self._z_dims = g_z.shape[1]
        self._train_embedding = train_embedding
        if (not train_embedding) and use_embedding:
            self._dataembedding = True
        else:
            self._dataembedding = False
        if sess is not None:
            self._sess = sess
        else:
            self._sess = tf.InteractiveSession()
        self._saver = tf.train.Saver(max_to_keep=10)
        self._build_model()
    
    def _build_model(self):
        self._d_real = self._d.outputs
        #self._r_real = self._d.R
        self._d_fake_inputs = tf.concat([self._g_inputs, self._g.outputs], axis=1)
        self._d_fake, _ = self._d.connect(self._d_fake_inputs)
        #self._g_hat = self._g.connect(self._g_inputs, self._r_real)
    
    def _gradient_penalty(self, real_inputs, fake_inputs):
        alpha = tf.random_uniform([], 0.0, 1.0)
        d_inputs_hat = alpha * real_inputs + (1 - alpha) * fake_inputs
        d_outputs_hat, _ = self._d.connect(d_inputs_hat)
        gradients = tf.gradients(d_outputs_hat, d_inputs_hat)[0]
        if self._train_embedding:
            gradients_l2 = tf.sqrt(tf.reduce_sum(tf.square(gradients), axis=[2,3]))
        else:
            gradients_l2 = tf.sqrt(tf.reduce_sum(tf.square(gradients), axis=[1,2]))
        return tf.reduce_mean(tf.square(gradients_l2 - 1.))
    
    def _diversity_sensitive(self):
        g_1, g_2 = tf.split(self._g_inputs, 2, axis=0)
        z_1, z_2 = tf.split(self._g_z, 2, axis=0)
        if self._train_embedding:
            g_diff = tf.reduce_mean(tf.abs(g_1 - g_2), axis=(1,2,3))
        else:
            g_diff = tf.reduce_mean(tf.abs(g_1 - g_2), axis=(1,2))
        z_diff = tf.reduce_mean(tf.abs(z_1 - z_2),axis=1)
        return tf.reduce_mean( g_diff / z_diff )

    def _define_loss(self, input_sequence_length, output_sequence_length, batch_size, lambda_w, lambda_c):
        g_wgan_loss = -tf.reduce_mean(self._d_fake)
        g_ds_loss = self._diversity_sensitive()
        if self._train_embedding:
            g_prev = self._g_inputs[:, input_sequence_length-1:input_sequence_length, :, :]
        else:
            g_prev = self._g_inputs[:, input_sequence_length-1:input_sequence_length, :]
        if output_sequence_length>1:
            if self._train_embedding:
                g_prev = tf.concat([g_prev, self._g.outputs[:, 0:output_sequence_length-1,:,:]], axis=1)
            else:
                g_prev = tf.concat([g_prev, self._g.outputs[:, 0:output_sequence_length-1,:]], axis=1)
        g_next = self._g.outputs
        g_consistency_loss = tf.maximum(0.0001, tf.norm(g_next-g_prev, ord=2) / (batch_size*output_sequence_length))
        g_loss = g_wgan_loss + lambda_w * g_ds_loss + lambda_c * g_consistency_loss
        gradient_penalty_loss = 10.0 * self._gradient_penalty(self._d_inputs, self._d_fake_inputs)
        d_wgan_loss = tf.reduce_mean(self._d_fake - self._d_real) + gradient_penalty_loss
        d_loss = d_wgan_loss
        return g_loss, d_loss, g_ds_loss, g_consistency_loss
    
    def _generate_random(self, shape, rand_type="normal", rand_params=None):
        if rand_type == 'normal':
            if rand_params:
                return np.random.normal(rand_params['mean'], rand_params['std'], size=shape)
            else:
                return np.random.normal(size=shape)
        if rand_type == 'uniform':
            if rand_params:
                return np.random.uniform(rand_params['low'], rand_params['high'], size=shape)
            else:
                return np.random.uniform(size=shape)
            
    def train(self, dataset, batch_size, input_sequence_length, output_sequence_length, g_lr, d_lr, 
              lambda_w, lambda_c, max_epochs, output_model_folder, output_tensorboard_folder, output_result_folder,
              rand_type='normal', rand_params=None, d_iterations=10, g_iterations=2, save_every_epochs=10, sample_every_epochs=10):
        '''
        Args:
            dataset: dataset's name, used to creat a dataloader.(TO DO)
            batch_size: size of minibatch used for training.
            input_sequence_length: the length of given previous sequence.
            output_sequence_length: the length of predicted sequence.
            g_lr, d_lr: learning rate of generator and discriminator.
            lambda_r, lambda_c: weights of r reconstruction loss and content loss.
            max_epochs: maximun number of training epochs.
            rand_type: 'normal' or 'uniform', the name of the distribution where random vectors are sampled from.
            rand_params: dict of parameters of the random distribution. If random_type is 'normal', it should be {'mean':, 'std':}
                          if random_type is 'uniform', it should be {'low':,'high:'}. If None, default values in numpy functions will be used.  
            d_iterations: number of training times to train discriminator in a single training iteration, usually more than g's.
            g_iterations: number of training times to train generator in a single training iteration.
            save_every_epochs: default 100, epochs to save model during training.
        '''
        if not os.path.exists(output_model_folder):
            os.mkdir(output_model_folder)
        if not os.path.exists(output_tensorboard_folder):
            os.mkdir(output_tensorboard_folder)
        if not os.path.exists(output_result_folder):
            os.mkdir(output_result_folder)
        #define loss
        self.g_loss, self.d_loss, ds_loss, consistency_loss = self._define_loss(input_sequence_length, output_sequence_length, batch_size, lambda_w, lambda_c)
        tf.summary.scalar("generator_loss", self.g_loss)
        tf.summary.scalar("discriminator_loss", self.d_loss)
        tf.summary.scalar("diverse_sensitive_loss", ds_loss)
        tf.summary.scalar("consistency_loss", consistency_loss)
        summary_op = tf.summary.merge_all()
        #define optimizer
        g_op = tf.train.AdamOptimizer(learning_rate=g_lr).minimize(self.g_loss, var_list=self._g.parameters)
        d_op = tf.train.AdamOptimizer(learning_rate=d_lr).minimize(self.d_loss, var_list=self._d.parameters)
        #create a data loader
        dataloader = DataLoader(dataset, mode="train", embedding=self._dataembedding) #TO DO: dataloader
        testloader = DataLoader(dataset, mode="test", embedding=self._dataembedding)
        #start training
        init_op = tf.global_variables_initializer()
        writer = tf.summary.FileWriter(output_tensorboard_folder, graph=tf.get_default_graph())
        g_best_loss = float('inf')
        g_best_epoch = -1
        d_losses = []
        g_losses = []
        self._sess.run(init_op)
        epoch = 0
        while epoch < max_epochs:
            print("Training epoch %d" % (epoch+1))
            d_training_loss = 0.
            g_training_loss = 0.
            k = 0
            dataloader.reset()
            while dataloader.has_more():
                input_data, current_batch_size = dataloader.next_batch(batch_size)
                input_data = np.concatenate([input_data]*2)
                if current_batch_size < batch_size:
                    continue
                if self._train_embedding:
                    input_sequence = input_data[:, 0:input_sequence_length, :, :]
                else:
                    input_sequence = input_data[:, 0:input_sequence_length, :]
                z_data = self._generate_random(shape=[batch_size*2, self._z_dims], rand_type=rand_type, rand_params=rand_params)
                feed_dict = {self._d_inputs:input_data, self._g_inputs:input_sequence, self._g_z:z_data}
                for _ in range(d_iterations):
                    _, d_loss_val = self._sess.run([d_op, self.d_loss], feed_dict=feed_dict)
                for _ in range(g_iterations):
                    _, g_loss_val, ds_loss_val, cons_loss_val = self._sess.run([g_op, self.g_loss, ds_loss,consistency_loss], feed_dict=feed_dict)
                print("Minibatch %d training loss-----generator_loss:%f, discriminator_loss:%f, ds loss:%f, consistency_loss:%f" % (k, g_loss_val, d_loss_val, ds_loss_val, cons_loss_val))
                summary = self._sess.run(summary_op, feed_dict=feed_dict)
                d_training_loss += d_loss_val
                g_training_loss += g_loss_val
                k += 1
            if k>0:
                d_training_loss /= k
                g_training_loss /= k
                d_losses.append(d_training_loss)
                g_losses.append(g_training_loss)
                print("Average training loss of epoch %d-----generator_loss:%f, discriminator_loss:%f" % (epoch, g_training_loss, d_training_loss))
            if epoch > 20 or epoch==max_epochs:
                save_model = False
                if g_training_loss < g_best_loss:
                    save_model = True
                    g_best_loss = g_training_loss
                    g_best_epoch = epoch
                if epoch % save_every_epochs==0:
                    save_model = True
                if save_model:
                    self.save_model(os.path.join(output_model_folder, "dsgan"), global_step=epoch)
            if epoch % sample_every_epochs == 0:
                sample_data = self.sample(10, batch_size, input_sequence_length, testloader)
                np.save(os.path.join(output_result_folder, "sample_{}_dsgan".format(epoch)), sample_data)
            writer.add_summary(summary, epoch)
            epoch += 1
        writer.close()
        print("Best epoch: ", g_best_epoch)
    
    def sample(self, num_generate, batch_size, input_sequence_length, dataloader):
        '''
        Args:
            dataset: dataset's name, used to creat a dataloader.(TO DO)
            num_generate: the number of predicted sequences to generate for each input sequences.
            batch_size: size of minibatch used for testing.
            input_sequence_length: the length of given previous sequence.
        '''
        if dataloader.has_more():
            input_data, current_batch_size = dataloader.next_batch(batch_size)
        else:
            dataloader.reset()
            input_data, current_batch_size = dataloader.next_batch(batch_size)
        input_data = np.concatenate([input_data]*2)
        if self._train_embedding:
            input_sequence = input_data[:, 0:input_sequence_length, :, :]
        else:
            input_sequence = input_data[:, 0:input_sequence_length, :]
        skeleton_data = []
        d_losses = 0.
        g_losses = 0.
        for _ in range(num_generate):
            z_data = self._generate_random(shape=[current_batch_size*2, self._z_dims])
            feed_dict={self._g_inputs:input_sequence, self._d_inputs:input_data, self._g_z:z_data}
            pred, g_loss, d_loss = self._sess.run([self._g.outputs, self.g_loss, self.d_loss], feed_dict=feed_dict)
            d_losses += d_loss
            g_losses += g_loss
            output_sequence = np.concatenate((input_sequence, pred), axis=1)
            if self._dataembedding:
                output_sequence = dataloader.postprocessing(output_sequence)
            skeleton_data.append(output_sequence)
        d_losses = d_losses / num_generate
        g_losses = g_losses / num_generate
        print("Average testing loss-----generator loss:%f, descriminator_loss:%f" % (g_losses, d_losses))
        return skeleton_data

    def save_model(self, model_file, global_step=None):
        save_path = self._saver.save(self._sess, model_file, global_step=global_step)
        print("Model saved in file: %s" % save_path)
        return save_path

    def load_model(self, model_file):
        self._saver.restore(self._sess, model_file)
    
