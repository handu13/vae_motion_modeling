import os
import sys
from pathlib import Path
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
sys.path.append("..")
sys.path.insert(0, str(Path(__file__).parent.absolute()) + r'/../../..')
import numpy as np
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
# import tensorflow as tf
from nn.Embedding import SpatialEncoder
from datatools.dataloader import DataLoader
num_joints = 24
num_params_per_joint=3
num_zdim=10
output_tensorboard_folder = os.path.abspath('../..')+'/poseEncoder/spatialEncoder/logs'
output_model_folder = os.path.abspath('../..') + '/poseEncoder/spatialEncoder/savemodel'
output_result_folder = os.path.abspath('../..') + '/poseEncoder/spatialEncoder/output'

'''
tf.reset_default_graph()
input_x = tf.placeholder(tf.float32, shape=[None, num_joints, num_params_per_joint])
input_z = tf.placeholder(tf.float32, shape=[None, num_zdim])
#build SpatialAutoencoder
pose = SpatialEncoder(num_joints=num_joints, num_params_per_joint=num_params_per_joint, num_zdim=num_zdim)
pose.create_pretrain_model(input_x, input_z)
#check autoencoder's parameters
print("encoder's parameters:") 
for param in pose.param_encoder:
    print(param)
print("decoder's parameters:")
for param in pose.param_decoder:
    print(param)
#Train the spatial autoencoder
pose.train(dataset="Vicon", output_model_folder=output_model_folder, output_tensorboard_folder=output_tensorboard_folder, 
           output_result_folder=output_result_folder, max_epochs=30, lr_encoder=6e-5, lr_decoder=6e-5,batch_size=16, save_every_epochs=5)
'''

'''
use pretrained spatial autoencoder to encode data and decode generated data
'''
tf.reset_default_graph()
#build autoencoder
pose = SpatialEncoder(num_joints=num_joints, num_params_per_joint=num_params_per_joint, num_zdim=num_zdim)
file_encoder = os.path.join(output_model_folder, "bestenc-29")
file_decoder = os.path.join(output_model_folder, "bestdec-29")
pose.load_model(file_encoder, file_decoder)

loader = DataLoader(dataset="Vicon", mode="test")
data,_ = loader.next_batch(batch_size=1)
np.save(os.path.join(output_result_folder, "origin_data.npy"),loader.postprocessing(data))
n_sample,n_length = data.shape[0], data.shape[1] 

data = np.reshape(data,(-1,num_joints,num_params_per_joint))

encoded = pose.encode(data)
# print(encoded.shape)
encoded_data = np.reshape(encoded, (n_sample, n_length, -1))
print(encoded_data.shape)
np.save(os.path.join(output_result_folder, "enc_data.npy"),encoded_data)

#decode generated data
decoded = pose.decode(encoded)
print("###############")
print(decoded.shape)
decoded_data = np.reshape(decoded, (n_sample, n_length,-1))
print("decoded_data shape: ", decoded_data.shape)
decoded_data = loader.postprocessing(decoded_data)
print(decoded_data.shape)
np.save(os.path.join(output_result_folder, "dec_data.npy"),decoded_data)

