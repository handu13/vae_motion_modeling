import os
import sys
from pathlib import Path
sys.path.insert(0, str(Path(__file__).parent.absolute()) + r'/..')
import numpy as np
import tensorflow as tf
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
# tf.disable_v2_behavior()
from nn.Embedding import SpatialEncoder


def run():
    training_data = np.load(r'../../../data\training_data\h36m.npz')['clips']
    print(training_data.shape)

    num_joints = 29
    num_params_per_joint = 3 
    num_zdim = 10
    training_data = np.reshape(training_data[:, :-3], (len(training_data), num_joints, num_params_per_joint))
    print(training_data.shape)
    spatial_encoder = SpatialEncoder(num_joints, num_params_per_joint, num_zdim)
    input_x = tf.compat.v1.placeholder(tf.float32, shape=[None, num_joints, num_params_per_joint], name='input_x')
    input_z = tf.compat.v1.placeholder(tf.float32, shape=[None, num_zdim], name='input_z')
    spatial_encoder.create_pretrain_model(input_x, input_z) 


    epochs = 100

    output_model_folder = r'../../../data/models/spatial_encoder'
    output_tensorboard_folder = r'../../../data/models/spatial_encoder'
    output_result_folder = r'../../../data/results/spatial_encoder'
    if not os.path.exists(output_model_folder):
        os.makedirs(output_model_folder)
    if not os.path.exists(output_tensorboard_folder):
        os.makedirs(output_tensorboard_folder)
    if not os.path.exists(output_result_folder):
        os.makedirs(output_result_folder)
    spatial_encoder.train(training_data, output_model_folder, output_tensorboard_folder, 
                          output_result_folder, epochs, lr_encoder=1e-3, lr_decoder=1e-3)


if __name__ == "__main__":
    run()