import os
import sys
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
sys.path.append("..")
import numpy as np
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
from nn.Generator import SequenceToSequenceGenerator_BiHMPGAN
from nn.Discriminator import RNNDiscriminator
from datatools.dataloader import DataLoader

# experiment setting
input_sequence_length = 40
output_sequence_length = 20
sequence_length = input_sequence_length + output_sequence_length
element_size = 72
embeded_size = 128
batch_size = 16
test_batch_size = 32
z_dims = 64
hidden_size = 512
lr = 0.00001
max_epochs=100
save_every_epochs=5
sample_every_epochs=10
output_tensorboard_folder = os.path.abspath('../..')+'/logs'
output_model_folder = os.path.abspath('../..')+'/savemodel'
output_result_folder = os.path.abspath('../..')+'/output'
if not os.path.exists(output_model_folder):
    os.mkdir(output_model_folder)
if not os.path.exists(output_tensorboard_folder):
    os.mkdir(output_tensorboard_folder)
if not os.path.exists(output_result_folder):
    os.mkdir(output_result_folder)

# build inputs
tf.reset_default_graph()
g_inputs = tf.placeholder(tf.float32, shape=[None, input_sequence_length, element_size])
ground_truth = tf.placeholder(tf.float32, shape=[None, output_sequence_length, element_size])
# build a generator
generator = SequenceToSequenceGenerator_BiHMPGAN(inputs=g_inputs, hidden_size=hidden_size, embeded_size=embeded_size,
                                                 input_length=input_sequence_length, output_length=output_sequence_length,
                                                 use_residual=False)
# check the trainable parameters
print("generator's parameters:")
for param in generator.parameters:
    print(param)
print("seq2seq encoder's parameters:")
for param in generator.enc_parameters:
    print(param)
print("seq2seq decoder's parameters:")
for param in generator.dec_parameters:
    print(param)


def train():
    sess = tf.InteractiveSession()
    saver = tf.train.Saver(max_to_keep=10)
    g_loss = tf.reduce_mean(tf.squared_difference(generator.outputs, ground_truth))
    tf.summary.scalar("mse_loss", g_loss)
    summary_op = tf.summary.merge_all()
    g_op = tf.train.AdamOptimizer(learning_rate=lr).minimize(g_loss, var_list=generator.parameters)
    # dataloader = DataLoader(sequence_length, dataset="edin_locomotion", mode="train") #TO DO: dataloader
    dataloader = DataLoader(sequence_length, dataset="h36m", mode="train")
    sample_input_data,_ = dataloader.next_batch(test_batch_size)
    init_op = tf.global_variables_initializer()
    writer = tf.summary.FileWriter(output_tensorboard_folder, graph=tf.get_default_graph())
    g_best_loss = float('inf')
    g_best_epoch = -1
    g_losses = []
    sess.run(init_op)
    epoch = 0
    while epoch < max_epochs:
        print("Training epoch %d" % (epoch+1))
        g_training_loss = 0.
        k = 0
        dataloader.reset()
        while dataloader.has_more():
            input_data, current_batch_size = dataloader.next_batch(batch_size)
            if current_batch_size < batch_size:
                continue
            input_sequence = input_data[:, 0:input_sequence_length, :]
            truth = input_data[:, input_sequence_length:, :]
            feed_dict = {g_inputs:input_sequence, ground_truth:truth}
            _, g_loss_val = sess.run([g_op, g_loss], feed_dict=feed_dict)
            print("Minibatch %d training loss-----generator_loss:%f" % (k, g_loss_val))
            summary = sess.run(summary_op, feed_dict=feed_dict)
            writer.add_summary(summary, epoch)
            g_training_loss += g_loss_val
            k += 1
        if k>0:
            g_training_loss /= k
            g_losses.append(g_training_loss)
            print("Average training loss of epoch %d-----generator_loss:%f" % (epoch, g_training_loss))
        if epoch > 4 or epoch==max_epochs:  ## skip the first few epochs
            save_model = False
            if g_training_loss < g_best_loss:
                save_model = True
                g_best_loss = g_training_loss
                g_best_epoch = epoch
            if epoch % save_every_epochs==0:
                save_model = True
            if save_model:
                save_path = saver.save(sess, os.path.join(output_model_folder, "mse_seq2seq"), global_step=epoch)
                print("Model saved in file: %s" % save_path)
        if epoch % sample_every_epochs == 0:
            input_sequence = sample_input_data[:, 0:input_sequence_length, :]
            truth = sample_input_data[:, input_sequence_length:, :]
            feed_dict = {g_inputs:input_sequence, ground_truth:truth}
            pred,_ = sess.run([generator.outputs, g_loss], feed_dict=feed_dict)
            output_sequence = np.concatenate((input_sequence, pred), axis=1)
            np.save(os.path.join(output_result_folder, "sample_{}_mse_seq2seq".format(epoch)), output_sequence)
        epoch += 1
    writer.close()
    sess.close()
    return g_best_epoch


if __name__ == "__main__":
    # best_epoch = train()
    # generate data with the best model(best_epoch)
    # tf.reset_default_graph()
    g_inputs = tf.placeholder(tf.float32, shape=[None, input_sequence_length, element_size])
    g = SequenceToSequenceGenerator_BiHMPGAN(inputs=g_inputs, hidden_size=hidden_size, embeded_size=embeded_size,
                                            input_length=input_sequence_length, output_length=output_sequence_length,
                                            use_residual=False)
    saver = tf.train.Saver(g.parameters)
    sess = tf.InteractiveSession()
    saver.restore(sess, os.path.join(output_model_folder, "mse_seq2seq-{}".format(str(98)))) #to fill the model file name here

    loader = DataLoader(sequence_length, dataset="edin", mode="test")
    data, _ = loader.next_batch(test_batch_size)
    print(data.shape)
    input_sequence = data[:, 0:input_sequence_length, :]
    generated_data = [data]
    pred = sess.run(g.outputs, feed_dict={g_inputs:input_sequence})
    result = np.concatenate((input_sequence, pred), axis=1)
    generated_data.append(result)
    np.save(os.path.join(output_result_folder, "best_epoch_mse_g"), generated_data)

