import os
import sys
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
sys.path.append("..")
import numpy as np
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

from nn.Generator import SequenceToSequenceGenerator_BiHMPGAN
from nn.Discriminator import RNNDiscriminator
from model.BiHMP_GAN import BiHMP_GAN
from datatools.dataloader import DataLoader
from pathlib import Path
sys.path.insert(0, str(Path(__file__).parent.absolute()) + r'/../../../')
from mosi_dev_deepmotionmodeling.mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder
from mosi_dev_deepmotionmodeling.utilities.utils import get_files, export_point_cloud_data_without_foot_contact, write_to_json_file


capture_sys = 'Vicon'
# experiment setting
input_sequence_length = 30   #### input motion frames
output_sequence_length = 30   #### predicted motion frames
sequence_length = input_sequence_length + output_sequence_length
element_size = 72
# element_size = 66
# embeded_size = 128
batch_size = 10
z_dims = 64
hidden_size = 128
output_tensorboard_folder = os.path.abspath('../..')+'/output/logs'
output_model_folder = os.path.abspath('../..')+'/output/savemodel' + capture_sys
output_result_folder = os.path.abspath('../..')+'/output/' + capture_sys
# output_model_folder = os.path.abspath('../..')+'/savemodel_new'
# output_result_folder = os.path.abspath('../..')+'/output'
# build inputs
tf.reset_default_graph()
g_inputs = tf.placeholder(tf.float32, shape=[None, input_sequence_length, element_size])
d_inputs = tf.placeholder(tf.float32, shape=[None, sequence_length, element_size])
g_z = tf.placeholder(tf.float32, shape=[None, z_dims])

# # build a generator
# generator = SequenceToSequenceGenerator_BiHMPGAN(inputs=g_inputs, z=g_z, hidden_size=hidden_size, embeded_size=embeded_size,
#                                                  input_length=input_sequence_length, output_length=output_sequence_length,
#                                                  use_residual=True)

# build a generator no embedding
generator = SequenceToSequenceGenerator_BiHMPGAN(inputs=g_inputs, z=g_z, hidden_size=hidden_size, 
                                                 input_length=input_sequence_length, output_length=output_sequence_length,
                                                 use_residual=True)                                                 

# # build a discriminator
# discriminator = RNNDiscriminator(inputs=d_inputs, hidden_size=hidden_size, output_z_dims=z_dims, bidirectional=True, embeded_size=embeded_size,
#                                  use_multi_states=True, input_length=input_sequence_length, output_length=output_sequence_length)

# build a discriminator no embedding
discriminator = RNNDiscriminator(inputs=d_inputs, hidden_size=hidden_size, output_z_dims=z_dims, bidirectional=True,
                                 use_multi_states=True, input_length=input_sequence_length, output_length=output_sequence_length)


# build the GAN model
gan = BiHMP_GAN(generator, discriminator, g_inputs, d_inputs, g_z)

##start training
# gan.fit(config)
# config = {}
best_epoch = gan.train(dataset="h36m", batch_size=batch_size, input_sequence_length=input_sequence_length, output_sequence_length=output_sequence_length,
          g_lr=0.0001, d_lr=0.0001, lambda_r=1, lambda_c=0.1, max_epochs=100, 
          output_model_folder=output_model_folder, output_tensorboard_folder=output_tensorboard_folder, 
          output_result_folder=output_result_folder, save_every_epochs=5, sample_every_epochs=10)

print("the best epoch is: ", best_epoch)

tf.reset_default_graph()
g_inputs = tf.placeholder(tf.float32, shape=[None, input_sequence_length, element_size])
g_z = tf.placeholder(tf.float32, shape=[None, z_dims])
# g = SequenceToSequenceGenerator_BiHMPGAN(inputs=g_inputs, z=g_z, hidden_size=hidden_size, embeded_size=embeded_size,
#                                          input_length=input_sequence_length, output_length=output_sequence_length,
#                                          use_residual=True)

### GAN model without embedding
g = SequenceToSequenceGenerator_BiHMPGAN(inputs=g_inputs, z=g_z, hidden_size=hidden_size,
                                         input_length=input_sequence_length, output_length=output_sequence_length,
                                         use_residual=True)                                         
saver = tf.train.Saver(g.parameters)
# print(g.parameters)

sess = tf.InteractiveSession()
saver.restore(sess, os.path.join(output_model_folder, "bihmpgan-{}".format(str(best_epoch)))) #to fill the model file name here

loader = DataLoader(dataset="Vicon", mode="test")
data, _ = loader.next_batch(10)
input_sequence = data[:, 0:30, :]
generated_data = [data]
for _ in range(10):
    z_data = np.random.normal(size=[batch_size, z_dims])
    pred = sess.run(g.outputs, feed_dict={g_inputs:input_sequence, g_z:z_data})

    result = np.concatenate((input_sequence, pred), axis=1)

    generated_data.append(result)
generated_data = generated_data * loader.std + loader.mean
np.save(os.path.join(output_result_folder, "best_epoch_bihmp_vicon"), generated_data)



