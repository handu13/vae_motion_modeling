import os
import sys
import numpy as np
dirname = os.path.dirname(__file__)
sys.path.insert(0, os.path.join(dirname, '../..'))
from mosi_dev_deepmotionmodeling.models.cnn_autoencoder import CNNAutoEncoder
from mosi_dev_deepmotionmodeling.models.conv_autoencoder import ConvAutoEncoder
from mosi_dev_deepmotionmodeling.models.frame_encoder import FrameEncoder
from mosi_dev_deepmotionmodeling.models.fullBody_pose_encoder import FullBodyPoseEncoder
from mosi_dev_deepmotionmodeling.preprocessing.preprocessing import process_file, process_bvhfile
from mosi_dev_deepmotionmodeling.utilities.utils import get_files
from mosi_dev_deepmotionmodeling.mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder
from mosi_dev_deepmotionmodeling.utilities.quaternions import Quaternions
from mosi_dev_deepmotionmodeling.utilities.skeleton_def import MH_CMU_SKELETON
from mosi_dev_deepmotionmodeling.mosi_utils_anim.utilities import write_to_json_file
import tensorflow as tf 
import logging
tf.get_logger().setLevel(logging.ERROR)
import glob
from src_final.network.PoseEmbedding import SpatialEncoder, SpatialDecoder
from src_final.utils import CMU_SKELETON, CMU_JOINT_LEVELS
from mosi_dev_deepmotionmodeling.utilities.utils import reconstruct_global_position, convert_anim_to_point_cloud_tf, export_point_cloud_data_without_foot_contact


EPS = 1e-6


def create_preprocessed_motion_data(data_name = 'h36m', quaternion=False, window_size=64):

    data_path = os.path.join(dirname, '../..', 'data', 'training_data', 'mk_cmu_skeleton', data_name)
    bvhfiles = []
    get_files(data_path, suffix='.bvh', files=bvhfiles)
    motion_data = []
    ### rotation with quaternion
    if quaternion is False:
        
        for bvhfile in bvhfiles:
            motion_data.append(process_bvhfile(bvhfile, window=window_size, window_step=window_size//2))
    else:
        for bvhfile in bvhfiles:
            motion_data.append(process_file(bvhfile, window=window_size, window_step=window_size//2))
    return np.concatenate(motion_data, axis=0)


def get_preprocessed_motion_data(data_name = 'h36m', use_quaternion=False, window_size=64):
    """check if preprocessed data exist, otherwise call create_preprocessed_motion_data

    Args:
        data_name (str, optional): [description]. Defaults to 'h36m'.
    """
    data_path = os.path.join(dirname, '../..', 'data', 'training_data', 'processed_mocap_data', data_name)
    if not os.path.exists(data_path):
        os.makedirs(data_path)
    if use_quaternion is False:
        if not os.path.exists(os.path.join(data_path, data_name + '_angle.npy')):
            motion_data = create_preprocessed_motion_data(data_name, use_quaternion, window_size)
            np.save(os.path.join(data_path, data_name + '_angle.npy'), motion_data)
        else:
            motion_data = np.load(os.path.join(data_path, data_name + '_angle.npy'))
    else:
        if not os.path.exists(os.path.join(data_path, data_name + '_quaternion.npy')):
            motion_data = create_preprocessed_motion_data(data_name, use_quaternion, window_size)
            np.save(os.path.join(data_path, data_name + '_quaternion.npy'), motion_data)
        else:
            motion_data = np.load(os.path.join(data_path, data_name + '_quaternion.npy'))
    return motion_data


def evaluate_poseEmbedding():
    """
    """
    # tf.compat.v1.enable_eager_execution()

    enc_file = os.path.join(dirname, '../..', 'output/spatialEncoder/model/spatialEncoder_SpatialEncoder_150.ckpt')
    dec_file = os.path.join(dirname, '../..', 'output/spatialEncoder/model/spatialEncoder_SpatialDecoder_150.ckpt')

    spatialEncoder = SpatialEncoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
    spatialEncoder.load_weights(enc_file)

    spatialDecoder = SpatialDecoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
    spatialDecoder.load_weights(dec_file)

    #### test 
    test_set, mean_pose, std_pose = get_test_data_h36m()
    test_bvhfile = r'E:\workspace\mocap_data\mk_cmu_retargeting_default_pose\h36m\S1\Walking 1.bvh'
    filename = os.path.split(test_bvhfile)[-1] 
    bvhreader = BVHReader(test_bvhfile)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader)
    animated_joints = skeleton.animated_joints   
    joint_desc = skeleton.generate_bone_list_description() 
    motion_data = process_file(test_bvhfile, sliding_window=False, animated_joints=animated_joints)
    normalized_motion_data = (motion_data - mean_pose) / std_pose
    enc_res = spatialEncoder(normalized_motion_data)
    # enc_res =enc_res.numpy()

    dec_res = spatialDecoder(enc_res)
    # dec_res = dec_res.numpy()
    dec_res = np.asarray(dec_res)
    res = dec_res * std_pose + mean_pose
    export_point_cloud_data_without_foot_contact(res, filename=r'E:\tmp\Walking 1_poseEmbedding.panim', scale_factor=5, skeleton=joint_desc)
    export_point_cloud_data_without_foot_contact(motion_data, filename=r'E:\tmp\Walking 1_gt.panim', scale_factor=5, skeleton=joint_desc)


def reconstruct_global_position(motion_data):
    up_axis = np.array([0, 1, 0])
    forward_axis = np.array([0, 0, 1])
    joint_postions, v_x, v_z, v_r = motion_data[:, :-3], motion_data[:, -3], motion_data[:, -2], motion_data[:, -1]
    v_r = np.cumsum(v_r)
    qs = Quaternions.from_angle_axis(v_r, up_axis)
    n_frames = len(joint_postions)
    joint_postions = joint_postions.reshape((n_frames, -1, 3))
    translated_joints = []
    for i in range(n_frames):
        # rotated_joints.append(qs[i]*joint_postions[i])
        rotated_joints = qs[i] * joint_postions[i]
        translated_joints.append(rotated_joints + qs[i] * np.array([v_x[i], 0, v_z[i]]))
    return np.asarray(translated_joints)


def test_reconstruct_global_position():
    test_bvhfile = r'E:\workspace\mocap_data\mk_cmu_retargeting_default_pose\h36m\S1\Walking 1.bvh'
    motion_vector = process_bvhfile(test_bvhfile, sliding_window=False)
    print(motion_vector.shape)
    motion_vector_tf = tf.convert_to_tensor(motion_vector, dtype=tf.float32)
    global_pos = np.asarray(convert_anim_to_point_cloud_tf(motion_vector_tf[:100]))
    export_data = {"motion_data": global_pos.tolist(), "has_skeleton": True,
                   "skeleton": MH_CMU_SKELETON}
    write_to_json_file(r'E:\tmp\original_global.panim', export_data)     
    reconstructed_motion = reconstruct_global_position(motion_vector[:100])
    
    export_data = {"motion_data": reconstructed_motion.tolist(), "has_skeleton": True,
                   "skeleton": MH_CMU_SKELETON}
    write_to_json_file(r'E:\tmp\reconstructed.panim', export_data)        
    # global_reconstructed_motion = [reconstructed_motion[0]]
    for i in range(100-1):
        reconstructed_motion[i+1][:, 0] = reconstructed_motion[i+1][:, 0] + reconstructed_motion[i][0, 0]
        reconstructed_motion[i+1][:, 2] = reconstructed_motion[i+1][:, 2] + reconstructed_motion[i][0, 2]
    export_data = {"motion_data": reconstructed_motion.tolist(), "has_skeleton": True,
                   "skeleton": MH_CMU_SKELETON}
    write_to_json_file(r'E:\tmp\reconstructed_global.panim', export_data) 


def evaluate_frame_encoder():
    starting_frame = 0
    frame_length = 1000
    input_data_dir = r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data/h36m.npz'
    input_data = np.load(input_data_dir)
    assert 'clips' in input_data.keys(), "cannot find motion data in " + input_data_dir
    motion_data = input_data['clips']
    mean_value = motion_data.mean(axis=0)[np.newaxis, :]
    std_value = motion_data.std(axis=0)[np.newaxis, :]
    std_value[std_value < EPS] = EPS
    # normalized_data = (motion_data - mean_value) / std_value

    ### single file input
    test_bvhfile = r'E:\workspace\mocap_data\mk_cmu_retargeting_default_pose\h36m\S1\Walking 1.bvh'
    filename = os.path.split(test_bvhfile)[-1]
    motion_data = process_file(test_bvhfile, sliding_window=False)
    print(motion_data.shape) 
    with_normalization = True
    dropout_rate = 0.1
    frame_encoder = FrameEncoder(dropout_rate=dropout_rate)
    frame_encoder.load_weights(r'D:\workspace\my_git_repos\vae_motion_modeling\data\models\frame_encoder1\frame_encoder-100-0.0001-0.1-0100.ckpt')    
    
    if with_normalization:

        normalized_motion_data = (motion_data - mean_value) / std_value

        res = np.asarray(frame_encoder(normalized_motion_data[starting_frame : starting_frame + frame_length]))
        res = mean_value + res * std_value
        export_point_cloud_data_without_foot_contact(res, filename=r'E:\tmp\Walking 1.panim', scale_factor=5)
        export_point_cloud_data_without_foot_contact(motion_data[starting_frame : starting_frame + frame_length], 
                                                    filename=r'E:\tmp\Walking 1_gt.panim',
                                                    scale_factor=5)
    else:
        res = np.asarray(frame_encoder(motion_data[starting_frame : starting_frame + frame_length]))
        export_point_cloud_data_without_foot_contact(res, filename=r'E:\tmp\Walking 1_new.panim', scale_factor=5)
        export_point_cloud_data_without_foot_contact(motion_data[starting_frame : starting_frame + frame_length], 
                                                    filename=r'E:\tmp\Walking 1_new_gt.panim',
                                                    scale_factor=5)





def get_test_data_h36m():
    training_data = np.load(r'data/training_data/training_clips_h36m.npy')
    print(training_data.shape)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS   
    test_set = training_data[int(0.9*len(training_data)):]
    return test_set, mean_pose, std_pose     


def evaluate_spatialencoder():
    test_set, mean_pose, std_pose = get_test_data_h36m()
    print(test_set.shape)
    test_bvhfile = r'E:\workspace\mocap_data\mk_cmu_retargeting_default_pose\h36m\S1\Walking 1.bvh'
    filename = os.path.split(test_bvhfile)[-1] 
    bvhreader = BVHReader(test_bvhfile)
    skeleton = SkeletonBuilder().load_from_bvh(bvhreader)
    animated_joints = skeleton.animated_joints  
    motion_data = process_file(test_bvhfile, sliding_window=False, animated_joints=animated_joints)
    print(motion_data.shape)
    encoder = SpatialEncoder(num_params_per_joint=3, num_zdim=32, joint_dict=CMU_SKELETON, levels=CMU_JOINT_LEVELS)
    enc_file = r'D:\workspace\my_git_repos\vae_motion_modeling\output\spatialEncoder\model\spatialEncoder_SpatialEncoder_200.ckpt'
    encoder.load_weights(enc_file)
    # data = (test_set - mean_pose) / std_pose
    # n_samples, len_seq, dims = data.shape
    # print(data.shape)
    # x = np.reshape(data, (n_samples*len_seq, -1))

    # res = encoder(x)
    # print(res.shape)
    normalized_motion_data = (motion_data - mean_pose) / std_pose
    res = encoder(normalized_motion_data)
    res = res * std_pose + mean_pose
    export_point_cloud_data_without_foot_contact(res, filename=r'E:\tmp\Walking 1_spatialencoder.panim', scale_factor=5)
    export_point_cloud_data_without_foot_contact(motion_data, filename=r'E:\tmp\Walking 1_spatialencoder.panim')


def get_training_data():
    data_path = r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\training_clips_h36m.npy'
    return np.load(data_path)



def test():
    motion_data = get_preprocessed_motion_data()
    


def test_CNNAutoEncoder():
    training_data = get_training_data()
    print(training_data.shape)
    n_samples, n_frames, n_dims = training_data.shape
    # flatten_data = np.reshape(training_data, (n_samples * n_frames, n_dims))
    training_data = np.swapaxes(training_data, 1, 2)
    print(training_data.shape)
    cnn_encoder = CNNAutoEncoder('cnn_encoder', n_frames, n_dims, 15,
                                 encode_activation=tf.nn.elu,
                                 decode_activation=tf.nn.elu,
                                 hidden_units=10,
                                 n_epoches=10,
                                 batchsize=64)
    cnn_encoder.create_model()                             
    # res = cnn_encoder(training_data)
    # print(res.shape)                             
    # cnn_encoder.train(training_data)
    res = cnn_encoder(training_data)
    print(res.shape) 

 

if __name__ == "__main__":
    # test_CNNAutoEncoder()
    # test()
    evaluate_poseEmbedding()
    # evaluate_frame_encoder()
    # evaluate_spatialencoder()
    # test_reconstruct_global_position()
