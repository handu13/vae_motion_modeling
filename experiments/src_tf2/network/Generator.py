import tensorflow as tf
import numpy as np

'''BiHMP-GAN Generator'''
class Bihmp_Generator(tf.keras.Model):
    def __init__(self, hidden_units, out_length, output_dims, z_dims=None, cell="lstm", use_residual=True):
        super(Bihmp_Generator, self).__init__()
        self.enc = bihmp_Encoder(hidden_units, cell=cell)
        if z_dims:
            self.dec = bihmp_Decoder(hidden_units + z_dims, output_dims, cell=cell)
        else:
            self.dec = bihmp_Decoder(hidden_units, output_dims, cell=cell)
        #add
        #self.dec = S2S_Decoder(hidden_units, output_dims)
        self.out_length = out_length
        self.cell = cell
        self.use_residual = use_residual
  
    def call(self, x, z=None):
        enc_output, enc_state = self.enc(x)
        dec_hidden = enc_state
        if z is not None:
            last_x = x[:,-1,:]
            if self.cell == "lstm":
                hidden_h, hidden_c = dec_hidden
                # concat
                dec_hidden = [tf.concat([hidden_h, z],axis=-1), tf.concat([hidden_c, z],axis=-1)]
                input_com = tf.concat([hidden_h, hidden_c, z],axis=-1)
                # add
                # dec_hidden = [tf.add_n([hidden_h, z]), tf.add_n([hidden_c, z])]
                # input_com = tf.add_n([hidden_h, hidden_c, z])
            elif self.cell == "gru":
                dec_hidden = tf.concat([dec_hidden, z], axis=-1)
                input_com = dec_hidden
            next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1),axis=1)
            emit_output = []
            for t in range(self.out_length):
                dec_output, dec_hidden = self.dec(next_input, dec_hidden)
                if self.use_residual:
                    last_x = last_x + tf.squeeze(dec_output)
                    emit_output.append(tf.expand_dims(last_x,axis=1))
                    next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1), axis=1)
                else:
                    emit_output.append(dec_output)
                    next_input = tf.expand_dims(tf.concat([tf.squeeze(dec_output),input_com],axis=-1),axis=1)
        else:
            emit_output = []
            next_input = tf.expand_dims(x[:,-1,:], axis=1)
            for t in range(self.out_length):
                dec_output, dec_hidden = self.dec(next_input, dec_hidden)
                if self.use_residual:
                    next_input = next_input + dec_output
                else:
                    next_input = dec_output
                emit_output.append(next_input)
        output = tf.concat(emit_output, axis=1)
        return output

class bihmp_Encoder(tf.keras.Model):
    def __init__(self, enc_units, cell="lstm"):
        super(bihmp_Encoder, self).__init__()
        self.enc_units = enc_units
        self.cell = cell
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            self.rnn = tf.keras.layers.LSTM(self.enc_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_state=True, unroll=True)
        if self.cell == "gru":
            self.rnn = tf.keras.layers.GRU(self.enc_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_state=True, unroll=True)
    def call(self, x):
        if self.cell == "lstm":
            enc_output, enc_h, enc_c = self.rnn(x)
            enc_state = [enc_h, enc_c]
        elif self.cell == "gru":
            enc_output, enc_state = self.rnn(x)
        return enc_output, enc_state

class bihmp_Decoder(tf.keras.Model):
    def __init__(self, dec_units, out_dims, cell="lstm"):
        super(bihmp_Decoder, self).__init__()
        self.dec_units = dec_units
        self.cell = cell
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            self.rnn = tf.keras.layers.LSTM(self.dec_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_sequences=True, return_state=True, unroll=True)
        if self.cell == "gru":
            self.rnn = tf.keras.layers.GRU(self.dec_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_sequences=True, return_state=True, unroll=True)
        self.fc = tf.keras.layers.Dense(out_dims)
    def call(self, x, hidden):
        if self.cell == "lstm":
            dec_output, dec_state_h, dec_state_c = self.rnn(x, hidden)
            dec_state = [dec_state_h, dec_state_c]
        elif self.cell == "gru":
            dec_output, dec_state = self.rnn(x, hidden)
        output = self.fc(dec_output)
        return output, dec_state


'''HPGAN Generator'''
class hp_Generator(tf.keras.Model):
    def __init__(self, hidden_units, out_length, output_dims, z_dims=None, embed_shape=128, num_layers=2, cell="lstm", use_residual=True):
        super(hp_Generator, self).__init__()
        self.hidden_units = hidden_units
        self.embed_shape = embed_shape
        self.num_layers = num_layers
        self.out_length = out_length
        self.cell = cell
        self.use_residual = use_residual
        self.enc = hp_Encoder(hidden_units, embed_shape, num_layers, cell=cell)
        self.dec = hp_Decoder(hidden_units, num_layers, cell=cell)
        self.z_fc = []
        for _ in range(num_layers):
            if self.cell == 'lstm':
                self.z_fc.append([tf.keras.layers.Dense(hidden_units, use_bias=False), tf.keras.layers.Dense(hidden_units, use_bias=False)])
            elif self.cell == 'gru':
                self.z_fc.append(tf.keras.layers.Dense(hidden_units, use_bias=False))
        self.decinp_fc = tf.keras.layers.Dense(embed_shape, use_bias=False)
        self.fc = tf.keras.layers.Dense(output_dims)
    def call(self, x, z=None):
        enc_output, enc_state = self.enc(x)
        dec_hidden = enc_state
        last_x = tf.expand_dims(enc_output[:,-1,:], axis=1)
        if z is not None:
            if self.cell == "lstm":
                for i in range(self.num_layers):
                    dec_hidden[i][0] = tf.add(dec_hidden[i][0], self.z_fc[i][0](z))
                    dec_hidden[i][1] = tf.add(dec_hidden[i][1], self.z_fc[i][1](z))
            elif self.cell == "gru":
                for i in range(self.num_layers):
                    dec_hidden[i] = tf.add(dec_hidden[i], self.z_fc[i](z))
            next_input = self.decinp_fc(last_x)
            emit_output = []
            for t in range(self.out_length):
                dec_output, dec_hidden = self.dec(next_input, dec_hidden)
                dec_output += last_x
                last_x = dec_output
                emit_output.append(dec_output)
                next_input = self.decinp_fc(dec_output)
        else:
            next_input = self.decinp_fc(last_x)
            emit_output = []
            for t in range(self.out_length):
                dec_output, dec_hidden = self.dec(next_input, dec_hidden)
                emit_output.append(dec_output)
                next_input = self.decinp_fc(dec_output)
        output = tf.concat(emit_output, axis=1)
        output = self.fc(output)
        return output

class hp_Encoder(tf.keras.Model):
    def __init__(self, enc_units, embed_shape, num_layers, cell="lstm"):
        super(hp_Encoder, self).__init__()
        self.enc_units = enc_units
        self.embed_shape = embed_shape
        self.num_layers = num_layers
        self.cell = cell
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        self.projection = tf.keras.layers.Dense(embed_shape)
        if self.cell == "lstm":
            rnncell = [tf.keras.layers.LSTMCell(self.enc_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init) for _ in range(self.num_layers)]
        if self.cell == "gru":
            rnncell = [tf.keras.layers.GRUCell(self.enc_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init) for _ in range(self.num_layers)]
        self.rnn = tf.keras.layers.RNN(rnncell, return_sequences=True, return_state=True, unroll=True)
    def call(self, x):
        input_proj = self.projection(x)
        output = self.rnn(input_proj)
        enc_output = output[0]
        enc_state = output[1:]
        return enc_output, enc_state

class hp_Decoder(tf.keras.Model):
    def __init__(self, dec_units, num_layers, cell="lstm"):
        super(hp_Decoder, self).__init__()
        self.dec_units = dec_units
        self.num_layers = num_layers
        self.cell = cell
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            cells = [tf.keras.layers.LSTMCell(self.dec_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init) for _ in range(self.num_layers)]
        if self.cell == "gru":
            cells = [tf.keras.layers.GRUCell(self.dec_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init) for _ in range(self.num_layers)]
        self.rnn = tf.keras.layers.RNN(cells, return_sequences=True, return_state=True, unroll=True)
    def call(self, x, hidden):
        output = self.rnn(x, hidden)
        dec_output = output[0]
        dec_state = output[1:]
        return dec_output, dec_state

'''TCN Generator'''
class TCN_Generator(tf.keras.Model):
    def __init__(self, n_filters, f_sizes, dilation_rates, output_dims, output_length, use_residual=False):
        super(TCN_Generator, self).__init__()
        self.model = tf.keras.Sequential()
        for i in range(len(n_filters)):
            self.model.add(tcn_Block(n_filters[i], f_sizes[i], dilation_rates[i], use_residual))
        self.model.add(tf.keras.layers.Conv1D(output_dims, 1, padding='same'))
        self.output_length = output_length
    def call(self, x, training=False):
        emit_output = []
        for _ in range(self.output_length):
            pred = self.model(x, training=training)
            new = pred[:,-1,:]
            new = tf.keras.layers.Add()([pred[:,-1,:],x[:,-1,:]]) 
            new = tf.expand_dims(new, axis=1)
            emit_output.append(new)
            x = tf.concat([x, new], axis=1)[:,1:,:]
        output = tf.concat(emit_output, axis=1)
        return output

'''TCN_encoder-decoder generator'''
class ED_TCN_Generator(tf.keras.Model):
    def __init__(self, n_filters, f_sizes, dilation_rates, hidden_dims, output_dims, use_residual=False):
        super(ED_TCN_Generator, self).__init__()
        self.encoder = tf.keras.Sequential()
        for i in range(len(n_filters)):
            self.encoder.add(tcn_Enc_Block(n_filters[i], f_sizes[i], dilation_rates[i]))
        self.encoder.add(tf.keras.layers.Dense(hidden_dims, activation='tanh'))
        self.decoder = tf.keras.Sequential()
        for i in range(len(n_filters)):
            self.decoder.add(tcn_Dec_Block(n_filters[-1-i], f_sizes[-1-i], dilation_rates[-1-i]))
        self.decoder.add(tf.keras.layers.Dense(output_dims))
    def call(self, x, training=False):
        x = self.encoder(x, training=training)
        y = self.decoder(x, training=training)
        return y

class tcn_Enc_Block(tf.keras.Model):
    def __init__(self, n_filter, f_size, dilation_rate):
        super(tcn_Enc_Block, self).__init__()
        self.block = tf.keras.Sequential([
            tf.keras.layers.Conv1D(n_filter, f_size, dilation_rate=dilation_rate, padding='causal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.ReLU(),
            tf.keras.layers.MaxPool1D(2)
        ])
    def call(self, x, training=False):
        return self.block(x, training=training)

class tcn_Dec_Block(tf.keras.Model):
    def __init__(self, n_filter, f_size, dilation_rate):
        super(tcn_Dec_Block, self).__init__()
        self.block = tf.keras.Sequential([
            tf.keras.layers.UpSampling1D(2),
            tf.keras.layers.Conv1D(n_filter, f_size, dilation_rate=dilation_rate, padding='causal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.ReLU()
        ])
    def call(self, x, training=False):
        return self.block(x, training=training)

class tcn_Block(tf.keras.Model):
    def __init__(self, n_filter, f_size, dilation_rate, use_residual=False):
        super(tcn_Block, self).__init__()
        self.use_residual = use_residual
        self.block = tf.keras.Sequential([
            tf.keras.layers.Conv1D(n_filter, f_size, dilation_rate=dilation_rate, padding='causal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.ReLU(),
            tf.keras.layers.SpatialDropout1D(0.5)
            ])
        if self.use_residual:
            self.fc = tf.keras.layers.Conv1D(n_filter, 1, padding='same')
            self.add = tf.keras.layers.Add()
    def call(self, x, training=False):
        y = self.block(x, training=training)
        if self.use_residual:
            origin_x = self.fc(x)
            y = self.add([y, origin_x])
        return y

