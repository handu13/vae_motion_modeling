import tensorflow as tf
import tensorflow_addons as tfa

'''BiHMP-GAN Generator'''
class Bihmp_Discriminator(tf.keras.Model):
    def __init__(self, rnn_hidden, fc_hidden, input_length, output_length, z_dims=None, cell="lstm", bidirectional=False, use_multi_states=False, output_activation=None):
        super(Bihmp_Discriminator, self).__init__()
        self.cell = cell
        self.bidirectional = bidirectional
        self.input_length = input_length
        self.output_length = output_length
        self.z_dims = z_dims
        self.use_multi_states = use_multi_states
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            self.rnn = tf.keras.layers.LSTM(rnn_hidden, return_sequences=True, unroll=True)
        elif self.cell == "gru":
            self.rnn = tf.keras.layers.GRU(rnn_hidden, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_sequences=True, unroll=True)
        if self.bidirectional:
            self.disc = tf.keras.layers.Bidirectional(self.rnn, merge_mode=None)
        else:
            self.disc = self.rnn
        self.fc1 = tf.keras.layers.Dense(fc_hidden, activation=tf.nn.leaky_relu)
        self.output_fc = tf.keras.layers.Dense(1, activation=output_activation)
        if self.z_dims:
            self.z_fc = tf.keras.layers.Dense(self.z_dims)
    def call(self, x):
        output = self.disc(x)
        if self.use_multi_states:
            if self.bidirectional:
                states = [output[0][:,self.input_length,:], output[0][:,-1,:], 
                          output[1][:,self.output_length,: ], output[1][:,-1,:]] 
                final_state = tf.concat(states, axis=-1)
            else:
                final_state = tf.concat([output[:,self.input_length,:],output[:,-1,:]], axis=-1)
        else:
            if self.bidirectional:
                final_state = tf.concat([output[0][:,-1,:], output[1][:,-1,:]],axis=-1)
            else:
                final_state = output[:,-1,:]
        base = self.fc1(final_state)
        output = self.output_fc(base)
        if self.z_dims:
            recon_z = self.z_fc(base)
            return [output, recon_z]
        else:
            return [output]

'''HPGAN Discriminator'''
class hp_Discriminator(tf.keras.Model):
    def __init__(self, hidden_size, embed_shape=128, num_layers=3, output_activation=None):
        super(hp_Discriminator, self).__init__()
        self.hidden_size = hidden_size
        self.embed_shape = embed_shape
        self.num_layers = num_layers
        self.regularizer = tf.keras.regularizers.l2(0.001)
        self.projection = tf.keras.layers.Dense(self.embed_shape, use_bias=False, kernel_regularizer=self.regularizer)
        self.flatten = tf.keras.layers.Flatten()
        self.net = []
        for i in range(self.num_layers):
            self.net.append(tf.keras.layers.Dense(self.hidden_size, activation=tf.nn.relu, kernel_regularizer=self.regularizer))
        self.output_fc = tf.keras.layers.Dense(1, activation=output_activation, kernel_regularizer=self.regularizer)
    
    def call(self, x):
        x = self.projection(x)
        x = self.flatten(x)
        for i in range(self.num_layers):
            x = self.net[i](x)
        output = self.output_fc(x)
        return [output]

'''CNN Discriminator'''
class CNN_Discriminator(tf.keras.Model):
    def __init__(self,base_dim, multiscale=False):
        super(CNN_Discriminator, self).__init__()
        self.multiscale = multiscale
        self.layer1 = cnn_Block(128, 3, 1, 4)
        self.layer2 = cnn_Block(256, 3, 2, 4)
        self.layer3 = cnn_Block(512, 3, 4, 4)
        #self.layer4 = cnn_Block(1024, 3, 1, 2)
        if self.multiscale:
            self.scale1 = tf.keras.layers.Dense(8,activation='relu')
            self.scale1d = tf.keras.layers.Dense(base_dim,activation='relu')
            self.scale2 = tf.keras.layers.Dense(base_dim,activation='relu')
        else:
            self.base = tf.keras.layers.Dense(base_dim,activation='relu')
        #self.dropout = tf.keras.layers.Dropout(0.5)
        self.output_layer = tf.keras.layers.Dense(1)
    def call(self, x, training=False):
        l1 = self.layer1(x, training=training)
        l2 = self.layer2(l1, training=training)
        l3 = self.layer3(l2, training=training)
        #l4 = self.layer4(l3, training=training)
        if self.multiscale:
            s1 = self.scale1(l1)
            s1 = tf.keras.layers.Flatten()(s1)
            s1 = self.scale1d(s1)
            l3 = tf.squeeze(l3, axis=1)
            s2 = self.scale2(l3)
            base = tf.concat([s1, s2], axis=-1)
        else:
            l3 = tf.squeeze(l3, axis=1)
            base = self.base(l3)
        y = self.output_layer(base)
        return [y]

class cnn_Block(tf.keras.Model):
    def __init__(self, n_filter, f_size, dilation_rate, pool_size, use_residual=False):
        super(cnn_Block, self).__init__()
        self.use_residual = use_residual
        self.block = tf.keras.Sequential([
            tf.keras.layers.Conv1D(n_filter, f_size, dilation_rate=dilation_rate, padding='same'),
            tfa.layers.InstanceNormalization(axis=-1),
            tf.keras.layers.ReLU(),
            tf.keras.layers.SpatialDropout1D(0.5),
            tf.keras.layers.MaxPool1D(pool_size)
            ])
        if self.use_residual:
            self.fc = tf.keras.layers.Conv1D(n_filter, 1, padding='same')
            self.add = tf.keras.layers.Add()
    def call(self, x, training=False):
        y = self.block(x, training=training)
        if self.use_residual:
            origin_x = self.fc(x)
            y = self.add([y, origin_x])
        return y
