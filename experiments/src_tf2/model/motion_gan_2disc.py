import os
import sys
import numpy as np 
import tensorflow as tf 
rng = np.random.RandomState(1234567)
cross_entropy = tf.keras.losses.BinaryCrossentropy()

class MotionGAN_2Disc(object):
    def __init__(self, name, g, d, e, z_dims=None, lambda_c=1.0, debug=False):
        self.g = g
        self.d = d
        self.e = e
        self.z_dims = z_dims
        self.lambda_c = lambda_c
        self.name = name
        self.debug = debug

    def train(self, input_sequence, output_sequence, epochs, batchsize, learning_rate, g_iter=1, d_iter=5, e_iter=1, save_every_epochs=None, save_path=None, log_path='./log'):
        assert len(input_sequence) == len(output_sequence)
        self.g_optimizer = tf.keras.optimizers.Adam(learning_rate)
        self.d_optimizer = tf.keras.optimizers.Adam(learning_rate)
        self.e_optimizer = tf.keras.optimizers.Adam(learning_rate / 2)
        n_samples = len(input_sequence)
        n_batches = n_samples // batchsize
        batch_indexes = np.arange(n_batches+1)
        # start training
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        train_summary_writer = tf.summary.create_file_writer(log_path)
        for epoch in range(epochs):
            gen_errors = []
            disc_errors = []
            eval_errors = []
            fake_outputs = []
            real_outputs = []
            fake_likelihood = []
            real_likelihood = []
            rng.shuffle(batch_indexes)
            for i, batch_index in enumerate(batch_indexes):
                if input_sequence[batch_index * batchsize : (batch_index+1) * batchsize].size != 0:
                    input_batch = input_sequence[batch_index * batchsize : (batch_index+1) * batchsize]
                    output_batch = output_sequence[batch_index * batchsize : (batch_index+1) * batchsize]
                    for _ in range(d_iter):
                        disc_loss = self.train_d(input_batch, output_batch)
                    for _ in range(e_iter):
                        eval_loss = self.train_e(input_batch, output_batch)
                    for _ in range(g_iter):
                        gen_loss = self.train_g(input_batch, output_batch)

                    gen_errors.append(gen_loss.numpy())
                    disc_errors.append(disc_loss.numpy()) 
                    eval_errors.append(eval_loss.numpy())
                    if self.debug:
                        if self.z_dims:
                            random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
                            predict_batch = self.g(input_batch, random_noise)
                        else:
                            predict_batch = self.g(input_batch)
                        real_input = tf.concat([input_batch, output_batch], axis=1)
                        fake_input = tf.concat([input_batch, predict_batch], axis=1)
                        real_output = self.d(real_input, training=True)
                        fake_output = self.d(fake_input, training=True)
                        fake_outputs.append(np.mean(fake_output[0].numpy()))
                        real_outputs.append(np.mean(real_output[0].numpy()))
                        real_prob = self.e(real_input)
                        fake_prob = self.e(fake_input)
                        real_likelihood.append(np.mean(real_prob[0].numpy()))
                        fake_likelihood.append(np.mean(fake_prob[0].numpy()))
                        with train_summary_writer.as_default():
                            tf.summary.scalar('gen_loss', np.mean(gen_errors), step=epoch)
                            tf.summary.scalar('disc_loss', np.mean(disc_errors), step=epoch)
                            tf.summary.scalar('eval_loss', np.mean(eval_errors), step=epoch)
                            tf.summary.scalar('real_score', np.mean(real_outputs), step=epoch)
                            tf.summary.scalar('fake_score', np.mean(fake_outputs), step=epoch)
                            tf.summary.scalar('real_likelihood', np.mean(real_likelihood), step=epoch)
                            tf.summary.scalar('fake_likelihood', np.mean(fake_likelihood), step=epoch)
                        sys.stdout.write('\r[Epoch {epoch}] {percent:.1%} gen_loss {gen_loss:.5f} disc_loss {disc_loss:.5f} eval_loss {eval_loss:.5f} real_likelihood {real_prob:.5f} fake_likelihood {fake_prob:.5f}'.format(epoch=epoch, percent=i/(n_batches), 
                                                                                    gen_loss=np.mean(gen_errors), disc_loss=np.mean(disc_errors), eval_loss=np.mean(eval_errors), real_prob=np.mean(real_outputs), fake_prob=np.mean(fake_outputs)))        
                        sys.stdout.flush()   
                    else:
                        with train_summary_writer.as_default():
                            tf.summary.scalar('gen_loss', np.mean(gen_errors), step=epoch)
                            tf.summary.scalar('disc_loss', np.mean(disc_errors), step=epoch) 
                            tf.summary.scalar('eval_loss', np.mean(eval_errors), step=epoch)
                        sys.stdout.write('\r[Epoch {epoch}] {percent:.1%} gen_loss {gen_loss:.5f} disc_loss {disc_loss:.5f} eval_loss {eval_loss:.5f}'.format(epoch=epoch, percent=i/(n_batches), 
                                                                                    gen_loss=np.mean(gen_errors), disc_loss=np.mean(disc_errors), eval_loss=np.mean(eval_errors)))        
                        sys.stdout.flush()     
                                       
            print('')
            if save_every_epochs is not None and save_path is not None:
                if (epoch+1) % save_every_epochs == 0:
                    self.save(save_path, suffix=str(epoch+1))         

    @tf.function
    def train_g(self, input_batch, output_batch):
        if self.z_dims:
            random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
        with tf.GradientTape() as gen_tape:
            if self.z_dims:
                prediction = self.g(input_batch, random_noise)
            else:
                prediction = self.g(input_batch)
            fake_input = tf.concat([input_batch, prediction], axis=1)
            fake_output = self.d(fake_input, training=True)
            gen_loss = self.generator_loss(fake_output[0], prediction, output_batch)
        grads_g = gen_tape.gradient(gen_loss, self.g.trainable_variables)
        self.g_optimizer.apply_gradients(zip(grads_g, self.g.trainable_variables))
        return gen_loss
    
    @tf.function
    def train_d(self, input_batch, output_batch):
        if self.z_dims:
            random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
        with tf.GradientTape() as disc_tape:
            if self.z_dims:
                prediction = self.g(input_batch, random_noise)
            else:
                prediction = self.g(input_batch)
            real_input = tf.concat([input_batch, output_batch], axis=1)
            fake_input = tf.concat([input_batch, prediction], axis=1)
            real_output = self.d(real_input, training=True)
            fake_output = self.d(fake_input, training=True)
            disc_loss = self.discriminator_loss(fake_output[0], fake_input, real_output[0], real_input)
        grads_d = disc_tape.gradient(disc_loss, self.d.trainable_variables)    
        self.d_optimizer.apply_gradients(zip(grads_d, self.d.trainable_variables))
        return disc_loss        

    @tf.function
    def train_e(self, input_batch, output_batch):
        if self.z_dims:
            random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
        with tf.GradientTape() as eval_tape:
            if self.z_dims:
                prediction = self.g(input_batch, random_noise)
            else:
                prediction = self.g(input_batch)
            real_input = tf.concat([input_batch, output_batch], axis=1)
            fake_input = tf.concat([input_batch, prediction], axis=1)
            real_output = self.e(real_input)
            fake_output = self.e(fake_input)
            eval_loss = self.evalDiscriminator_loss(real_output[0], fake_output[0])
        grads_eval= eval_tape.gradient(eval_loss, self.e.trainable_variables)
        self.e_optimizer.apply_gradients(zip(grads_eval, self.e.trainable_variables))
        return eval_loss

    def evalDiscriminator_loss(self, real_output, fake_output):
        real_loss = cross_entropy(tf.ones_like(real_output), real_output)
        fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
        eval_loss = real_loss + fake_loss
        return eval_loss

    def generator_loss(self, fake_output, fake_input, real_input):
        g_wgan_loss = -tf.reduce_mean(fake_output)
        content_loss = tf.reduce_mean(tf.keras.losses.MSE(real_input, fake_input))
        g_loss = g_wgan_loss + self.lambda_c * content_loss
        return g_loss
    
    def discriminator_loss(self, fake_output, fake_input, real_output, real_input):
        d_loss = tf.reduce_mean(fake_output - real_output) + 10.0 * self.gradient_penalty(real_input, fake_input)
        return d_loss
    
    def gradient_penalty(self, real, fake):
        alpha = tf.random.uniform([], 0., 1.)
        diff = fake - real
        inter = real + (alpha * diff)
        with tf.GradientTape() as t:
            t.watch(inter)
            pred = self.d(inter)
        if isinstance(pred, tuple):
            grad = t.gradient(pred[0], [inter])[0]
        else:
            grad = t.gradient(pred, [inter])[0]
        slopes = tf.sqrt(tf.reduce_sum(tf.square(grad), axis=[1,2]))
        gp = tf.reduce_mean((slopes - 1.)**2)
        return gp
    
    def save(self, save_path, prefix='', suffix=''):
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        gen_strings = [self.name, prefix, 'generator', suffix]
        disc_strings = [self.name, prefix, 'discriminator', suffix]
        eval_strings = [self.name, prefix, 'evaluator', suffix]
        self.g.save_weights(os.path.join(save_path, '_'.join([x for x in gen_strings if x != '']) + '.ckpt'))
        self.d.save_weights(os.path.join(save_path, '_'.join([x for x in disc_strings if x != '']) + '.ckpt'))
        self.e.save_weights(os.path.join(save_path, '_'.join([x for x in eval_strings if x != '']) + '.ckpt')) 

    def load(self, path, prefix='', suffix=''):
        
        gen_strings = [self.name, prefix, 'generator', suffix]
        disc_strings = [self.name, prefix, 'discriminator', suffix]
        eval_strings = [self.name, prefix, 'evaluator', suffix]
        self.g.load_weights(os.path.join(path, '_'.join([x for x in gen_strings if x != '']) + '.ckpt'))
        self.d.load_weights(os.path.join(path, '_'.join([x for x in disc_strings if x != '']) + '.ckpt')) 
        self.e.load_weights(os.path.join(path, '_'.join([x for x in eval_strings if x != '']) + '.ckpt'))
