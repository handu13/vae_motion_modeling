import os
import sys
import numpy as np 
import tensorflow as tf 
rng = np.random.RandomState(1234567)
cross_entropy = tf.keras.losses.BinaryCrossentropy()


class HPGAN(object):
    def __init__(self, name, g, d, e, z_dims, lambda_c=0.001, lambda_b=0.01, debug=False):
        self.g = g
        self.d = d
        self.e = e
        self.z_dims = z_dims
        self.lambda_c = lambda_c #for consistency loss
        self.lambda_b = lambda_b #for bone loss
        self.name = name
        self.debug = debug

    def train(self, input_sequence, output_sequence, epochs, batchsize, learning_rate, g_iter=1, d_iter=5, e_iter=1, save_every_epochs=None, save_path=None, log_path='./log'):
        assert len(input_sequence) == len(output_sequence)
        self.g_optimizer = tf.keras.optimizers.Adam(learning_rate)
        self.d_optimizer = tf.keras.optimizers.Adam(learning_rate)
        self.e_optimizer = tf.keras.optimizers.Adam(learning_rate / 2)
        n_samples = len(input_sequence)
        n_batches = n_samples // batchsize
        batch_indexes = np.arange(n_batches+1)
        # start training
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        train_summary_writer = tf.summary.create_file_writer(log_path)
        for epoch in range(epochs):
            gen_errors = []
            disc_errors = []
            eval_errors = []
            fake_outputs = []
            real_outputs = []
            consistency_errors = []
            rng.shuffle(batch_indexes)
            for i, batch_index in enumerate(batch_indexes):
                if input_sequence[batch_index * batchsize : (batch_index+1) * batchsize].size != 0:
                    input_batch = input_sequence[batch_index * batchsize : (batch_index+1) * batchsize]
                    output_batch = output_sequence[batch_index * batchsize : (batch_index+1) * batchsize]
                    for _ in range(d_iter):
                        disc_loss = self.train_d(input_batch, output_batch)
                    for _ in range(e_iter):
                        eval_loss = self.train_e(input_batch, output_batch)
                    for _ in range(g_iter):
                        gen_loss, consistency_loss = self.train_g(input_batch, output_batch)

                    gen_errors.append(gen_loss.numpy())
                    disc_errors.append(disc_loss.numpy()) 
                    eval_errors.append(eval_loss.numpy())
                    if self.debug:
                        random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
                        predict_batch = self.g(input_batch, random_noise)
                        real_input = tf.concat([input_batch, output_batch], axis=1)
                        fake_input = tf.concat([input_batch, predict_batch], axis=1)
                        real_prob = self.e(real_input)
                        fake_prob = self.e(fake_input)
                        fake_outputs.append(np.mean(fake_prob.numpy()))
                        real_outputs.append(np.mean(real_prob.numpy()))
                        consistency_errors.append(consistency_loss.numpy())
                        with train_summary_writer.as_default():
                            tf.summary.scalar('gen_loss', np.mean(gen_errors), step=epoch)
                            tf.summary.scalar('disc_loss', np.mean(disc_errors), step=epoch)
                            tf.summary.scalar('eval_loss', np.mean(eval_errors), step=epoch)
                            tf.summary.scalar('real_likelihood', np.mean(real_outputs), step=epoch)
                            tf.summary.scalar('fake_likelihood', np.mean(fake_outputs), step=epoch)
                            tf.summary.scalar('consistency_loss', np.mean(consistency_errors), step=epoch)
                        sys.stdout.write('\r[Epoch {epoch}] {percent:.1%} gen_loss {gen_loss:.5f} disc_loss {disc_loss:.5f} eval_loss {eval_loss:.5f}'.format(epoch=epoch, percent=i/(n_batches), 
                                                                                                                                gen_loss=np.mean(gen_errors), disc_loss=np.mean(disc_errors), eval_loss=np.mean(eval_errors)))        
                        sys.stdout.flush()   
                    else:
                        with train_summary_writer.as_default():
                            tf.summary.scalar('gen_loss', np.mean(gen_errors), step=epoch)
                            tf.summary.scalar('disc_loss', np.mean(disc_errors), step=epoch)   
                            tf.summary.scalar('eval_loss', np.mean(eval_errors), step=epoch)                     
                        sys.stdout.write('\r[Epoch {epoch}] {percent:.1%} gen_loss {gen_loss:.5f} disc_loss {disc_loss:.5f} eval_loss {eval_loss:.5f}'.format(epoch=epoch, percent=i/(n_batches), 
                                                                                                                                gen_loss=np.mean(gen_errors), disc_loss=np.mean(disc_errors), probd_loss=np.mean(eval_errors)))        
                        sys.stdout.flush()     
                                       
            print('')
            if save_every_epochs is not None and save_path is not None:
                if (epoch+1) % save_every_epochs == 0:
                    self.save(save_path, suffix=str(epoch+1))         

    @tf.function
    def train_g(self, input_batch, output_batch):
        random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
        with tf.GradientTape() as gen_tape:
            prediction = self.g(input_batch, random_noise)
            fake_input = tf.concat([input_batch, prediction], axis=1)
            fake_output = self.d(fake_input)
            gen_loss, consistency_loss = self.generator_loss(fake_output, prediction, tf.concat([input_batch[:,-1:,:], output_batch[:,:-1,:]],axis=1))
        grads_g = gen_tape.gradient(gen_loss, self.g.trainable_variables)
        self.g_optimizer.apply_gradients(zip(grads_g, self.g.trainable_variables))
        return gen_loss, consistency_loss
    
    @tf.function
    def train_d(self, input_batch, output_batch):
        random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
        with tf.GradientTape() as disc_tape:
            predict_batch = self.g(input_batch, random_noise)
            real_input = tf.concat([input_batch, output_batch], axis=1)
            fake_input = tf.concat([input_batch, predict_batch], axis=1)
            real_output = self.d(real_input)
            fake_output = self.d(fake_input)
            disc_loss = self.discriminator_loss(fake_output, fake_input, real_output, real_input)
        grads_d = disc_tape.gradient(disc_loss, self.d.trainable_variables)    
        self.d_optimizer.apply_gradients(zip(grads_d, self.d.trainable_variables))
        return disc_loss        
    
    @tf.function
    def train_e(self, input_batch, output_batch):
        random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
        with tf.GradientTape() as eval_tape:
            predict_batch = self.g(input_batch, random_noise)
            real_input = tf.concat([input_batch, output_batch], axis=1)
            fake_input = tf.concat([input_batch, predict_batch], axis=1)
            real_output = self.e(real_input)
            fake_output = self.e(fake_input)
            eval_loss = self.evalDiscriminator_loss(real_output, fake_output)
        grads_eval= eval_tape.gradient(eval_loss, self.e.trainable_variables)
        self.e_optimizer.apply_gradients(zip(grads_eval, self.e.trainable_variables))
        return eval_loss

    def generator_loss(self, fake_output, fake_input, real_input):
        g_wgan_loss = -tf.reduce_mean(fake_output)
        g_next = fake_input
        g_prev = real_input
        consistency_loss = tf.reduce_mean(tf.norm(g_next-g_prev, ord=2))
        #bone_loss = ...
        g_loss = g_wgan_loss + self.lambda_c * consistency_loss #+self.lambda_b *  bone_loss
        return g_loss, consistency_loss

    def evalDiscriminator_loss(self, real_output, fake_output):
        real_loss = cross_entropy(tf.ones_like(real_output), real_output)
        fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
        eval_loss = real_loss + fake_loss + sum(self.e.losses)
        return eval_loss
    
    def discriminator_loss(self, fake_output, fake_input, real_output, real_input):
        d_loss = tf.reduce_mean(fake_output - real_output) + 10.0 * self.gradient_penalty(real_input, fake_input) + sum(self.d.losses)
        return d_loss
    
    def gradient_penalty(self, real, fake):
        alpha = tf.random.uniform([], 0., 1.)
        diff = fake - real
        inter = real + (alpha * diff)
        with tf.GradientTape() as t:
            t.watch(inter)
            pred = self.d(inter)
        grad = t.gradient(pred, [inter])[0]
        slopes = tf.sqrt(tf.reduce_sum(tf.square(grad), axis=[1,2]))
        gp = tf.reduce_mean((slopes - 1.)**2)
        return gp
    
    def save(self, save_path, prefix='', suffix=''):
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        gen_strings = [self.name, prefix, 'generator', suffix]
        disc_strings = [self.name, prefix, 'discriminator', suffix]
        eval_strings = [self.name, prefix, 'evaluator', suffix]
        self.g.save_weights(os.path.join(save_path, '_'.join([x for x in gen_strings if x != '']) + '.ckpt'))
        self.d.save_weights(os.path.join(save_path, '_'.join([x for x in disc_strings if x != '']) + '.ckpt')) 
        self.e.save_weights(os.path.join(save_path, '_'.join([x for x in eval_strings if x != '']) + '.ckpt'))

    def load(self, path, prefix='', suffix=''):
        
        gen_strings = [self.name, prefix, 'generator', suffix]
        disc_strings = [self.name, prefix, 'discriminator', suffix]
        eval_strings = [self.name, prefix, 'evaluator', suffix]
        self.g.load_weights(os.path.join(path, '_'.join([x for x in gen_strings if x != '']) + '.ckpt'))
        self.d.load_weights(os.path.join(path, '_'.join([x for x in disc_strings if x != '']) + '.ckpt')) 
        self.e.load_weights(os.path.join(path, '_'.join([x for x in eval_strings if x != '']) + '.ckpt'))

