import os
import sys
import numpy as np 
import tensorflow as tf 
rng = np.random.RandomState(1234567)
cross_entropy = tf.keras.losses.BinaryCrossentropy()

def save_sample_data(sample_path, new_samples, suffix):
    sample_dir = os.path.join(sample_path, str(suffix))
    if not os.path.exists(sample_dir):
        os.makedirs(sample_dir)
    for j in range(len(new_samples)):
        filename = os.path.join(sample_dir, str(j) + '.npy')
        np.save(filename, new_samples[j])

class MotionBiGAN_2Disc(object):
    def __init__(self, name, g, d, e, z_dims, lambda_c=0.1, lambda_r=1, debug=False):
        self.g = g
        self.d = d
        self.e = e
        self.z_dims = z_dims
        self.lambda_c = lambda_c #for content loss
        self.lambda_r = lambda_r #for random noise loss
        self.name = name
        self.debug = debug

    def train(self, input_sequence, output_sequence, epochs, batchsize, learning_rate, std_pose, mean_pose, g_iter=1, d_iter=5, e_iter=1, sample_batchsize=10, save_every_epochs=20, sample_every_epochs=20, save_path=None, sample_path=None, log_path='./log'):
        assert len(input_sequence) == len(output_sequence)
        self.enc_optimizer = tf.keras.optimizers.Adam(learning_rate)
        self.dec_optimizer = tf.keras.optimizers.Adam(learning_rate)
        self.disc_optimizer = tf.keras.optimizers.Adam(learning_rate)
        self.eval_optimizer = tf.keras.optimizers.Adam(learning_rate / 2)
        n_samples = len(input_sequence)
        n_batches = n_samples // batchsize
        batch_indexes = np.arange(n_batches+1)
        # build model
        sample_batch = input_sequence[:sample_batchsize]
        sample_random_noise = tf.random.normal([sample_batchsize, self.z_dims], dtype=tf.float32)
        new_samples = self.sample_new_motions(sample_batch, sample_random_noise, build=True)
        new_samples = new_samples * std_pose + mean_pose
        save_sample_data(sample_path, new_samples, 0)
        # start training
        if not os.path.exists(log_path):
            os.makedirs(log_path)
        train_summary_writer = tf.summary.create_file_writer(log_path)
        for epoch in range(epochs):
            enc_errors = []
            dec_errors = []
            disc_errors = []
            eval_errors = []
            d_wgan_errors = []
            g_wgan_errors = []
            real_scores = []
            fake_scores = []
            real_likelihood = []
            fake_likelihood = []
            rng.shuffle(batch_indexes)
            for i, batch_index in enumerate(batch_indexes):
                if input_sequence[batch_index * batchsize : (batch_index+1) * batchsize].size != 0:
                    input_batch = input_sequence[batch_index * batchsize : (batch_index+1) * batchsize]
                    output_batch = output_sequence[batch_index * batchsize : (batch_index+1) * batchsize]
                    for _ in range(d_iter):
                        disc_loss, d_wgan_loss = self.train_d(input_batch, output_batch)
                    for _ in range(e_iter):
                        eval_loss = self.train_e(input_batch, output_batch)
                    for _ in range(g_iter):
                        enc_loss, dec_loss, g_wgan_loss = self.train_g(input_batch, output_batch)

                    enc_errors.append(enc_loss.numpy())
                    dec_errors.append(dec_loss.numpy())
                    disc_errors.append(disc_loss.numpy()) 
                    eval_errors.append(eval_loss.numpy())
                    if self.debug:
                        random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
                        predict_batch = self.g(input_batch, random_noise)
                        real_input = tf.concat([input_batch, output_batch], axis=1)
                        fake_input = tf.concat([input_batch, predict_batch], axis=1)
                        real_output = self.d(real_input)
                        fake_output = self.d(fake_input)
                        fake_scores.append(np.mean(fake_output[0].numpy()))
                        real_scores.append(np.mean(real_output[0].numpy()))
                        real_prob = self.e(real_input)
                        fake_prob = self.e(fake_input)
                        fake_likelihood.append(np.mean(fake_prob[0].numpy()))
                        real_likelihood.append(np.mean(real_prob[0].numpy()))
                        d_wgan_errors.append(d_wgan_loss.numpy())
                        g_wgan_errors.append(g_wgan_loss.numpy())
                        with train_summary_writer.as_default():
                            tf.summary.scalar('enc_loss', np.mean(enc_errors), step=epoch)
                            tf.summary.scalar('dec_loss', np.mean(dec_errors), step=epoch)   
                            tf.summary.scalar('disc_loss', np.mean(disc_errors), step=epoch)
                            tf.summary.scalar('eval_loss', np.mean(eval_errors), step=epoch)
                            tf.summary.scalar('real_score', np.mean(real_scores), step=epoch)
                            tf.summary.scalar('fake_score', np.mean(fake_scores), step=epoch) 
                            tf.summary.scalar('real_likelihood', np.mean(real_likelihood), step=epoch)
                            tf.summary.scalar('fake_likelihood', np.mean(fake_likelihood), step=epoch)
                            tf.summary.scalar('d_wgan_loss', np.mean(d_wgan_errors), step=epoch)   
                            tf.summary.scalar('g_wgan_loss', np.mean(g_wgan_errors), step=epoch)
                        sys.stdout.write('\r[Epoch {epoch}] {percent:.1%} enc_loss {enc_loss:.5f} dec_loss {dec_loss:.5f} disc_loss {disc_loss:.5f} eval_loss {eval_loss:.5f}'.format(epoch=epoch, percent=i/(n_batches), 
                                                                                                                                enc_loss=np.mean(enc_errors), dec_loss=np.mean(dec_errors), disc_loss=np.mean(disc_errors), eval_loss=np.mean(eval_loss)))        
                        sys.stdout.flush()   
                    else:
                        with train_summary_writer.as_default():
                            tf.summary.scalar('enc_loss', np.mean(enc_errors), step=epoch)
                            tf.summary.scalar('dec_loss', np.mean(dec_errors), step=epoch)   
                            tf.summary.scalar('disc_loss', np.mean(disc_errors), step=epoch)   
                            tf.summary.scalar('eval_loss', np.mean(enc_errors), step=epoch)                  
                        sys.stdout.write('\r[Epoch {epoch}] {percent:.1%} enc_loss {enc_loss:.5f} dec_loss {dec_loss:.5f} disc_loss {disc_loss:.5f} eval_loss {eval_loss:.5f}'.format(epoch=epoch, percent=i/(n_batches), 
                                                                                                                                enc_loss=np.mean(enc_errors), dec_loss=np.mean(dec_errors), disc_loss=np.mean(disc_errors), eval_loss=np.mean(eval_errors)))        
                        sys.stdout.flush()     
                                       
            print('')
            if save_every_epochs is not None and save_path is not None:
                if (epoch+1) % save_every_epochs == 0:
                    self.save(save_path, suffix=str(epoch+1))        

            if sample_every_epochs is not None and sample_path is not None:
                if (epoch+1) % sample_every_epochs == 0:
                    new_samples = self.sample_new_motions(sample_batch, sample_random_noise)
                    new_samples = new_samples * std_pose + mean_pose
                    save_sample_data(sample_path, new_samples, epoch+1)

    @tf.function
    def train_d(self, input_batch, output_batch):
        random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
        with tf.GradientTape() as t:
            prediction = self.g(input_batch, random_noise)
            fake_input = tf.concat([input_batch, prediction], axis=1)
            real_input = tf.concat([input_batch, output_batch], axis=1)
            real_output = self.d(real_input)
            fake_output = self.d(fake_input)
            if len(real_output)==2:
                real_output, real_z = real_output[0], real_output[1]
                fake_output, recon_z = fake_output[0], fake_output[1]
                disc_loss, d_wgan_loss = self.discriminator_loss(real_input, real_output, fake_input, fake_output, regress_z=True, random_z=random_noise, recon_z=recon_z)
            else:
                disc_loss, d_wgan_loss = self.discriminator_loss(real_input, real_output[0], fake_input, fake_output[0])
        grad = t.gradient(disc_loss, self.d.trainable_variables)
        self.disc_optimizer.apply_gradients(zip(grad, self.d.trainable_variables))
        return disc_loss, d_wgan_loss

    def discriminator_loss(self, real_input, real_output, fake_input, fake_output, regress_z=False, random_z=None, recon_z=None):
        d_wgan_loss = tf.reduce_mean(fake_output - real_output) + 10.0 * self.gradient_penalty(real_input, fake_input)
        if regress_z:
            r_rec_loss = tf.maximum(0.0001, tf.reduce_mean(tf.norm(random_z-recon_z,axis=-1)))
            d_loss = d_wgan_loss + self.lambda_r * r_rec_loss
        else:
            d_loss = d_wgan_loss
        return d_loss, d_wgan_loss
    
    def gradient_penalty(self, real, fake):
        alpha = tf.random.uniform([], 0., 1.)
        diff = fake - real
        inter = real + (alpha * diff)
        with tf.GradientTape() as t:
            t.watch(inter)
            pred = self.d(inter)
        grad = t.gradient(pred[0], [inter])[0]
        slopes = tf.sqrt(tf.reduce_sum(tf.square(grad), axis=[1,2]))
        gp = tf.reduce_mean((slopes - 1.)**2)
        return gp
    
    @tf.function
    def train_g(self, input_batch, output_batch):
        random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
        content_loss = 0.
        r_recon_loss = 0.
        with tf.GradientTape() as enc_tape, tf.GradientTape() as dec_tape:
            prediction = self.g(input_batch, random_noise)
            fake_input = tf.concat([input_batch, prediction], axis=1)
            real_input = tf.concat([input_batch, output_batch], axis=1)
            fake_output = self.d(fake_input)
            if len(fake_output)==2:
                real_output = self.d(real_input)
                real_output, real_z = real_output[0], real_output[1]
                fake_output, recon_z = fake_output[0], fake_output[1]
                content = self.g(input_batch, real_z)
                enc_loss, dec_loss, g_wgan_loss = self.generator_loss(fake_output, regress_z=True, real_input=output_batch, random_z=random_noise, recon_z=recon_z, content=content)
            else:
                enc_loss, dec_loss, g_wgan_loss = self.generator_loss(fake_output[0])
        gradients_of_encoder = enc_tape.gradient(enc_loss, self.enc_trainable_variables)
        gradients_of_decoder = dec_tape.gradient(dec_loss, self.dec_trainable_variables)
        self.enc_optimizer.apply_gradients(zip(gradients_of_encoder, self.enc_trainable_variables))
        self.dec_optimizer.apply_gradients(zip(gradients_of_decoder, self.dec_trainable_variables))
        return enc_loss, dec_loss, g_wgan_loss
        
    def generator_loss(self, fake_output, regress_z=False, real_input=None, random_z=None, recon_z=None, content=None):
        g_wgan_loss = -tf.reduce_mean(fake_output)
        content_loss = 0.
        r_recon_loss = 0.
        if regress_z:
            r_recon_loss = tf.maximum(0.0001, tf.reduce_mean(tf.norm(random_z-recon_z,axis=-1)))
            content_loss = tf.maximum(0.0001, tf.reduce_mean(tf.norm(real_input-content, axis=-1)))
            enc_loss = g_wgan_loss + self.lambda_c * content_loss
            dec_loss = g_wgan_loss + self.lambda_r * r_recon_loss + self.lambda_c * content_loss
        else:
            enc_loss = g_wgan_loss
            dec_loss = g_wgan_loss
        return enc_loss, dec_loss, g_wgan_loss
    
    @tf.function
    def train_e(self, input_batch, output_batch):
        random_noise = tf.random.normal([len(input_batch), self.z_dims], dtype=tf.float32)
        with tf.GradientTape() as eval_tape:
            predict_batch = self.g(input_batch, random_noise)
            real_input = tf.concat([input_batch, output_batch], axis=1)
            fake_input = tf.concat([input_batch, predict_batch], axis=1)
            real_output = self.e(real_input)
            fake_output = self.e(fake_input)
            eval_loss = self.evalDiscriminator_loss(real_output[0], fake_output[0])
        grads_eval= eval_tape.gradient(eval_loss, self.e.trainable_variables)
        self.eval_optimizer.apply_gradients(zip(grads_eval, self.e.trainable_variables))
        return eval_loss

    def evalDiscriminator_loss(self, real_output, fake_output):
        real_loss = cross_entropy(tf.ones_like(real_output), real_output)
        fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
        eval_loss = real_loss + fake_loss
        return eval_loss
    
    def save(self, save_path, prefix='', suffix=''):
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        gen_strings = [self.name, prefix, 'generator', suffix]
        disc_strings = [self.name, prefix, 'discriminator', suffix]
        eval_strings = [self.name, prefix, 'evaluator', suffix]
        self.g.save_weights(os.path.join(save_path, '_'.join([x for x in gen_strings if x != '']) + '.ckpt'))
        self.d.save_weights(os.path.join(save_path, '_'.join([x for x in disc_strings if x != '']) + '.ckpt')) 
        self.e.save_weights(os.path.join(save_path, '_'.join([x for x in eval_strings if x != '']) + '.ckpt'))

    def load(self, path, prefix='', suffix=''):
        gen_strings = [self.name, prefix, 'generator', suffix]
        disc_strings = [self.name, prefix, 'discriminator', suffix]
        eval_strings = [self.name, prefix, 'evaluator', suffix]
        self.g.load_weights(os.path.join(path, '_'.join([x for x in gen_strings if x != '']) + '.ckpt'))
        self.d.load_weights(os.path.join(path, '_'.join([x for x in disc_strings if x != '']) + '.ckpt'))
        self.e.load_weights(os.path.join(path, '_'.join([x for x in eval_strings if x != '']) + '.ckpt'))

    def sample_new_motions(self, input_batch, random_noise, build=False):
        prediction = self.g(input_batch, random_noise)
        generated_sequence = tf.concat([input_batch, prediction], axis=1)
        if build:
            self.d.build((None, generated_sequence.shape[1], generated_sequence.shape[2]))
            self.trainable_variables = self.g.trainable_variables + self.d.trainable_variables
            self.enc_trainable_variables = []
            self.dec_trainable_variables = []
            for v in self.g.trainable_variables:
                if 'encoder' in v.name:
                    self.enc_trainable_variables.append(v)
                if 'decoder' in v.name:
                    self.dec_trainable_variables.append(v)
        return generated_sequence
