import os
import sys
from pathlib import Path
abspath = os.path.abspath(__file__)
sys.path.insert(0, str(Path(__file__).parent.absolute()) + r'/../../..')
sys.path.insert(0, str(Path(__file__).parent.absolute()) + r'/..')

# from nn.Generator import SequenceToSequenceGenerator_BiHMPGAN
import tensorflow as tf
from tensorflow.keras import layers, Model
import numpy as np
cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)
# rng = np.random.RandomState(23456)
# tf.random.set_seed(rng)
from mosi_dev_deepmotionmodeling.utilities.utils import get_files, export_point_cloud_data_without_foot_contact
from mosi_dev_deepmotionmodeling.mosi_utils_anim.utilities.io_helper_functions import write_to_json_file, load_json_file
from mosi_dev_deepmotionmodeling.mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder
EPS = 1e-6              


def save_sample_data(sample_path, new_samples, epoch):
    sample_dir = os.path.join(sample_path, str(epoch))
    if not os.path.exists(sample_dir):
        os.makedirs(sample_dir)
    for j in range(len(new_samples)):
        filename = os.path.join(sample_dir, str(j) + '.npy')
        #export_point_cloud_data_without_foot_contact(new_samples[j], filename)
        np.save(filename, new_samples[j])

class BiHMPGan(object):
    
    def __init__(self, name, generator, discriminator, z_dims, input_length, output_length):
        self.name = name
        self.g = generator
        self.d = discriminator
        self.input_shape = z_dims
        self.input_length = input_length
        self.output_length = output_length
    
    def train(self, training_data, epochs, batchsize, std_pose, mean_pose, lambda_r=1, lambda_c=0.1, d_iterations=5, g_iterations=1, sample_batchsize=10, log_path=None, sample_every_epochs=None, sample_path=None, save_every_epochs=None, save_path=None):
        self.enc_optimizer = tf.keras.optimizers.Adam(1e-4)
        self.dec_optimizer = tf.keras.optimizers.Adam(1e-4)
        self.discriminator_optimizer = tf.keras.optimizers.Adam(1e-4)
        # reserve some sample data for: 
        #   1. sample generated data 'sample_every_epochs' to monitor the training.
        #   2. build the model. Seq2Seq model can not be built because of its structure. Can only 'call' the model on real data before training in order to build the model.    
        sample_data = training_data[:sample_batchsize]
        new_samples = self.sample_new_motions(sample_data, build=True)
        new_samples = new_samples * std_pose + mean_pose
        save_sample_data(sample_path, new_samples, 0)
        # start training
        if log_path and not os.path.exists(log_path):
            os.makedirs(log_path)
        train_summary_writer = tf.summary.create_file_writer(log_path)
        self.lambda_r = lambda_r
        self.lambda_c = lambda_c
        training_data = training_data[sample_batchsize:]
        num_training_data = len(training_data)
        n_batches = num_training_data // batchsize
        for epoch in range(epochs):
            disc_errors = []
            enc_errors = []
            dec_errors = []
            batchinds = np.arange(1, n_batches)
            for idx, batch_idx in enumerate(batchinds):
                batch = training_data[batch_idx * batchsize: (batch_idx+1) * batchsize]
                for _ in range(d_iterations):
                    disc_loss = self.train_d(batch)
                disc_errors.append(disc_loss.numpy())
                for _ in range(g_iterations):
                    enc_loss, dec_loss = self.train_g(batch)
                enc_errors.append(enc_loss.numpy())
                dec_errors.append(dec_loss.numpy())
                with train_summary_writer.as_default():
                    tf.summary.scalar('enc_loss', enc_loss, step=epoch)
                    tf.summary.scalar('dec_loss', dec_loss, step=epoch)  
                    tf.summary.scalar('disc_loss', disc_loss, step=epoch)  
                sys.stdout.write('\r[Epoch %i] %0.1f%% enc_loss %.5f dec_loss %.5f disc_loss %.5f' % (epoch, 100 * idx / (n_batches - 1), np.mean(enc_errors), np.mean(dec_errors), np.mean(disc_errors)))        
                sys.stdout.flush()
            print('')
            if save_every_epochs is not None and save_path is not None:
                if (epoch+1) % save_every_epochs == 0:
                    self.save(save_path, suffix=str(epoch+1))
            if sample_every_epochs is not None and sample_path is not None:
                if (epoch+1) % sample_every_epochs == 0:
                    new_samples = self.sample_new_motions(sample_data, build=True)
                    new_samples = new_samples * std_pose + mean_pose
                    save_sample_data(sample_path, new_samples, epoch+1)
    
    @tf.function
    def train_d(self, batch):
        random_noise = tf.random.normal([len(batch), self.input_shape], dtype=tf.float32)
        with tf.GradientTape() as t:
            input_sequence = batch[:,:self.input_length,:]
            generated_sequence = self.g(input_sequence, random_noise)
            generated_samples = tf.concat([input_sequence, generated_sequence], axis=1)
            real_output = self.d(batch)
            fake_output = self.d(generated_samples)
            if len(real_output)==2:
                real_output, real_z = real_output[0], real_output[1]
                fake_output, recon_z = fake_output[0], fake_output[1]
                disc_loss = self.discriminator_loss(batch, real_output, generated_samples, fake_output, regress_z=True, random_z=random_noise, recon_z=recon_z)
            else:
                disc_loss = self.discriminator_loss(batch, real_output[0], generated_samples, fake_output[0])
        grad = t.gradient(disc_loss, self.d.trainable_variables)
        self.discriminator_optimizer.apply_gradients(zip(grad, self.d.trainable_variables))
        return disc_loss

    def discriminator_loss(self, real_d_input, real_output, fake_d_input, fake_output, regress_z=False, random_z=None, recon_z=None):
        d_wgan_loss = tf.reduce_mean(fake_output - real_output) + 10.0 * self.gradient_penalty(real_d_input, fake_d_input)
        if regress_z:
            r_rec_loss = tf.maximum(0.0001, tf.reduce_mean(tf.norm(random_z-recon_z,axis=-1)))
            d_loss = d_wgan_loss + self.lambda_r * r_rec_loss
        else:
            d_loss = d_wgan_loss
        return d_loss
    
    def gradient_penalty(self, real, fake):
        alpha = tf.random.uniform([], 0., 1.)
        diff = fake - real
        inter = real + (alpha * diff)
        with tf.GradientTape() as t:
            t.watch(inter)
            pred = self.d(inter)
        if isinstance(pred, tuple):
            grad = t.gradient(pred[0], [inter])[0]
        else:
            grad = t.gradient(pred, [inter])[0]
        slopes = tf.sqrt(tf.reduce_sum(tf.square(grad), axis=[1,2]))
        gp = tf.reduce_mean((slopes - 1.)**2)
        return gp
    
    @tf.function
    def train_g(self, batch):
        random_noise = tf.random.normal([len(batch), self.input_shape], dtype=tf.float32)
        with tf.GradientTape() as enc_tape, tf.GradientTape() as dec_tape:
            input_sequence = batch[:,:self.input_length,:]
            generated_sequence = self.g(input_sequence, random_noise)
            generated_samples = tf.concat([input_sequence, generated_sequence], axis=1)
            fake_output = self.d(generated_samples, training=True)
            if len(fake_output)==2:
                real_output = self.d(batch)
                real_output, real_z = real_output[0], real_output[1]
                fake_output, recon_z = fake_output[0], fake_output[1]
                content = self.g(input_sequence, real_z)
                enc_loss, dec_loss = self.generator_loss(fake_output, regress_z=True, real_d_input=batch, random_z=random_noise, recon_z=recon_z, content=content)
            else:
                enc_loss, dec_loss = self.generator_loss(fake_output[0])
        gradients_of_encoder = enc_tape.gradient(enc_loss, self.enc_trainable_variables)
        gradients_of_decoder = dec_tape.gradient(dec_loss, self.dec_trainable_variables)
        self.enc_optimizer.apply_gradients(zip(gradients_of_encoder, self.enc_trainable_variables))
        self.dec_optimizer.apply_gradients(zip(gradients_of_decoder, self.dec_trainable_variables))
        return enc_loss, dec_loss
        
    def generator_loss(self, fake_output, regress_z=False, real_d_input=None, random_z=None, recon_z=None, content=None):
        g_wgan_loss = -tf.reduce_mean(fake_output)
        if regress_z:
            r_rec_loss = tf.maximum(0.0001, tf.reduce_mean(tf.norm(random_z-recon_z,axis=-1)))
            content_loss = tf.maximum(0.0001, tf.reduce_mean(tf.norm(real_d_input[:, -self.output_length:, :]-content, axis=-1)))
            enc_loss = g_wgan_loss + self.lambda_c * content_loss
            dec_loss = g_wgan_loss + self.lambda_r * r_rec_loss + self.lambda_c * content_loss
        else:
            enc_loss = g_wgan_loss
            dec_loss = g_wgan_loss
        return enc_loss, dec_loss
    

    def save(self, save_path, prefix='', suffix=''):
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        gen_strings = [self.name, prefix, 'generator', suffix]
        disc_strings = [self.name, prefix, 'discriminator', suffix]
        self.g.save_weights(os.path.join(save_path, '_'.join([x for x in gen_strings if x != '']) + '.ckpt'))
        self.d.save_weights(os.path.join(save_path, '_'.join([x for x in disc_strings if x != '']) + '.ckpt')) 

    def load(self, path, prefix='', suffix=''):
        gen_strings = [self.name, prefix, 'generator', suffix]
        disc_strings = [self.name, prefix, 'discriminator', suffix]
        self.g.load_weights(os.path.join(path, '_'.join([x for x in gen_strings if x != '']) + '.ckpt'))
        self.d.load_weights(os.path.join(path, '_'.join([x for x in disc_strings if x != '']) + '.ckpt')) 

    def sample_new_motions(self, sample_data, build=False):
        random_noise = tf.random.normal([len(sample_data), self.input_shape], dtype=tf.float32)
        input_sequence = sample_data[:,:self.input_length,:]
        output_sequence = self.g(input_sequence, random_noise)
        generated_sequence = tf.concat([input_sequence, output_sequence], axis=1)
        if build:
            self.d.build((None, generated_sequence.shape[1], generated_sequence.shape[2]))
            self.trainable_variables = self.g.trainable_variables + self.d.trainable_variables
            self.enc_trainable_variables = []
            self.dec_trainable_variables = []
            for v in self.g.trainable_variables:
                if 'encoder' in v.name:
                    self.enc_trainable_variables.append(v)
                if 'decoder' in v.name:
                    self.dec_trainable_variables.append(v)
        return generated_sequence

class Generator(tf.keras.Model):
    def __init__(self, hidden_units, out_length, output_dims, z_dims, cell="lstm", use_residual=True):
        super(Generator, self).__init__()
        self.enc = S2S_Encoder(hidden_units)
        self.dec = S2S_Decoder(hidden_units + z_dims, output_dims)
        self.out_length = out_length
        self.cell = cell
        self.use_residual = use_residual
  
    def call(self, x, z):
        enc_output, enc_state = self.enc(x)
        dec_hidden = enc_state
        last_x = x[:,-1,:]
        if self.cell == "lstm":
            hidden_h, hidden_c = dec_hidden
            dec_hidden = [tf.concat([hidden_h, z],axis=-1), tf.concat([hidden_c, z],axis=-1)]
            input_com = tf.concat([hidden_h, hidden_c, z],axis=-1)
        elif self.cell == "gru":
            dec_hidden = tf.concat([dec_hidden, z], axis=-1)
            input_com = dec_hidden
        next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1),axis=1)
        emit_output = []
        for t in range(self.out_length):
            dec_output, dec_hidden = self.dec(next_input, dec_hidden)
            if self.use_residual:
                last_x = last_x + tf.squeeze(dec_output)
                emit_output.append(tf.expand_dims(last_x,axis=1))
                next_input = tf.expand_dims(tf.concat([last_x, input_com], axis=-1), axis=1)
            else:
                emit_output.append(dec_output)
                next_input = tf.expand_dims(tf.concat([tf.squeeze(dec_output),input_com],axis=-1),axis=1)
        output = tf.concat(emit_output, axis=1)
        return output

class S2S_Encoder(tf.keras.Model):
    def __init__(self, enc_units, cell="lstm"):
        super(S2S_Encoder, self).__init__()
        self.enc_units = enc_units
        self.cell = cell
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            self.rnn = tf.keras.layers.LSTM(self.enc_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_state=True)
        if self.cell == "gru":
            self.rnn = tf.keras.layers.GRU(self.enc_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_state=True)
    def call(self, x):
        if self.cell == "lstm":
            enc_output, enc_h, enc_c = self.rnn(x)
            enc_state = [enc_h, enc_c]
        elif self.cell == "gru":
            ecn_output, enc_state = self.rnn(x)
        return enc_output, enc_state

class S2S_Decoder(tf.keras.Model):
    def __init__(self, dec_units, out_dims, cell="lstm"):
        super().__init__()
        self.dec_units = dec_units
        self.cell = cell
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            self.rnn = tf.keras.layers.LSTM(self.dec_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_sequences=True, return_state=True)
        if self.cell == "gru":
            self.rnn = tf.keras.layers.GRU(self.dec_units, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_sequences=True, return_state=True)
        self.fc = tf.keras.layers.Dense(out_dims, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init)
    def call(self, x, hidden):
        if self.cell == "lstm":
            dec_output, dec_state_h, dec_state_c = self.rnn(x, hidden)
            dec_state = [dec_state_h, dec_state_c]
        elif self.cell == "gru":
            dec_output, dec_state = self.rnn(x, hidden)
        output = self.fc(dec_output)
        return output, dec_state

class Discriminator(tf.keras.Model):
    def __init__(self, rnn_hidden, fc_hidden, input_length, output_length, z_dims=None, cell="lstm", bidirectional=False, use_multi_states=False):
        super(Discriminator, self).__init__()
        self.cell = cell
        self.bidirectional = bidirectional
        self.input_length = input_length
        self.output_length = output_length
        self.z_dims = z_dims
        self.use_multi_states = use_multi_states
        self.kernel_init = tf.keras.initializers.TruncatedNormal(stddev=0.001)
        self.bias_init = tf.constant_initializer(0.)
        if self.cell == "lstm":
            self.rnn = tf.keras.layers.LSTM(rnn_hidden, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_sequences=True, unroll=True)
        elif self.cell == "gru":
            self.rnn = tf.keras.layers.GRU(rnn_hidden, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init, return_sequences=True, unroll=True)
        if self.bidirectional:
            self.disc = tf.keras.layers.Bidirectional(self.rnn, merge_mode=None)
        else:
            self.disc = self.rnn
        self.fc1 = tf.keras.layers.Dense(fc_hidden, activation=tf.nn.leaky_relu, kernel_initializer=self.kernel_init, bias_initializer=self.bias_init)
        self.output_fc = tf.keras.layers.Dense(1)
        if self.z_dims:
            self.z_fc = tf.keras.layers.Dense(self.z_dims)
    def call(self, x):
        output = self.disc(x)
        if self.use_multi_states:
            if self.bidirectional:
                states = [output[0][:,self.input_length,:], output[0][:,-1,:], 
                          output[1][:,self.output_length,: ], output[1][:,-1,:]] 
                final_state = tf.concat(states, axis=-1)
            else:
                final_state = tf.concat([output[:,self.input_length,:],output[:,-1,:]], axis=-1)
        else:
            if self.bidirectional:
                final_state = tf.concat([output[0][:,-1,:], output[1][:,-1,:]],axis=-1)
            else:
                final_state = output[:,-1,:]
        base = self.fc1(final_state)
        output = self.output_fc(base)
        if self.z_dims:
            recon_z = self.z_fc(base)
            return [output, recon_z]
        else:
            return [output]


def test_gan():
    #### load training data 
    training_data = np.load(r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose

    #### 
    hidden_units = 128
    fc_hidden = 64
    input_length = 30
    output_length = 30
    output_dims = 72
    z_dims = 64
    g = Generator(hidden_units, output_length, output_dims, z_dims)
    d = Discriminator(hidden_units, fc_hidden, input_length, output_length, z_dims, cell="lstm", bidirectional=True, use_multi_states=True)
    epochs = 100
    model = BiHMPGan('bihmp_gan_vicon', g, d, z_dims, input_length, output_length)

    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../savedmodel/bihmp_gan_tf2_graph_init'
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/bihmp_gan_tf2_graph_init'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../logs/bihmp_gan_tf2_graph_init'
    model.train(training_data, epochs, 32, std_pose, mean_pose, log_path=log_path, sample_every_epochs=1, sample_path=sample_path, save_every_epochs=1, save_path=save_path)
    model.save(save_path)
    model.load(save_path)


def evaluate_naive_gan():
    "load models at different training phase and sample"
    training_data = np.load(r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    
    hidden_units = 128
    fc_hidden = 64
    input_length = 30
    output_length = 30
    output_dims = 72
    z_dims = 64
    g = Generator(hidden_units, output_length, output_dims, z_dims)
    d = Discriminator(hidden_units, fc_hidden, input_length, output_length, z_dims, cell="lstm", bidirectional=True, use_multi_states=True)
    model = BiHMPGan('bihmp_gan_vicon', g, d, z_dims, input_length, output_length)
    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../savedmodel/bihmp_gan_tf2_graph_init'
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/bihmp_gan_vicon_eval'
    epochs = 100
    save_every_epochs=10
    n_samples = 10
    bvh = BVHReader(r'D:\workspace\projects\DHM2020\src\data\ViCon\4kmh.bvh')
    skeleton = SkeletonBuilder().load_from_bvh(bvh)
    animated_joints = skeleton.generate_bone_list_description()
    sample_data = training_data[:n_samples]
    for i in range(epochs):
        if (i+1) % save_every_epochs == 0:
            model.load(save_path, suffix=str(i+1))
            new_samples = model.sample_new_motions(sample_data).numpy()
            sample_dir = os.path.join(sample_path, str(i+1))
            if not os.path.exists(sample_dir):
                os.makedirs(sample_dir)
            for j in range(n_samples):
                filename = os.path.join(sample_dir, str(j) + '.panim')
                export_point_cloud_data_without_foot_contact(new_samples[j], filename, skeleton=animated_joints)


def visualize_samples():
    input_data = load_json_file(r'D:\workspace\my_git_repos\vae_motion_modeling\experiments\output\bihmp_gan_vicon_eval\100\9.panim')
    motion_data = np.array(input_data['motion_data'])
    print(motion_data.shape)


if __name__ == "__main__":
    test_gan()
    # evaluate_naive_gan()
    # visualize_samples()