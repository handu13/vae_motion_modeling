import os
import sys
# sys.path.append(r'H:\vae\vae_motion_modeling')
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + r'/../../..')
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + r'/..')
sys.path.append("..")
os.chdir(os.path.dirname(os.path.abspath(__file__)))
import tensorflow as tf
import numpy as np
import collections
from mosi_dev_deepmotionmodeling.utilities.utils import get_files, export_point_cloud_data_without_foot_contact, write_to_json_file
from mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder, panim
from mosi_dev_deepmotionmodeling import preprocessing
from preprocessing import preprocessing
from model.BiHMP_GAN_recursive import BiHMPGanRec
from network.Discriminator import Bihmp_Discriminator
from network.Generator import Bihmp_Generator
EPS = 1e-6

def read_data(bvhfiles):
    #Vicon
    clips = []
    for bvhfile in bvhfiles:
        clip = preprocessing.process_file(bvhfile, window=210, window_step=30, sliding_window=True,  
                            body_plane_indices=np.array([9,1,5]), fid_l=np.array([3, 4]), fid_r=np.array([7,8]), animated_joints=None)
        clips.extend(clip)
    data = np.stack(clips,axis=0)
    return data


def train_bihmpgan_recursive():
    #### load training data
    bvhfile1 = os.path.dirname(os.path.abspath(__file__)) + r'/../../../data/MotionCaptureData\data\ViCon\4kmh.bvh'
    bvhfile2 = os.path.dirname(os.path.abspath(__file__)) + r'/../../../data/MotionCaptureData\data\ViCon\6kmh.bvh'
    files = [bvhfile1, bvhfile2]
    training_data = read_data(files)
    #training_data = np.load(os.path.dirname(os.path.abspath(__file__)) + r'/../../data/processed_mocap_data/Vicon/longclips120.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    ### TODO: config model parameters
    ## config = json()
    ## config = load(config.json)

    ## config["hidden_units"]

    hidden_units = 128
    z_dims = 64
    input_sequence_length = 30
    output_sequence_length = 180
    base_length = 30
    epochs = 200
    learning_rate = 1e-4
    batchsize = 16
    input_sequence = training_data[:, :input_sequence_length, :]
    output_sequence = training_data[:, input_sequence_length:, :]
    model_name = 'bihmpgan_recursive'

    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/model'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/log'
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/samples'



    g = Bihmp_Generator(hidden_units, base_length, dims, z_dims=z_dims, cell="lstm", use_residual=True)
    d = Bihmp_Discriminator(hidden_units, hidden_units/2, input_sequence_length, base_length, z_dims=z_dims, cell="lstm", bidirectional=True, use_multi_states=True, output_activation=None)
    bigan = BiHMPGanRec(model_name, g, d, input_sequence_length, output_sequence_length, z_dims=z_dims, lambda_c=0.1, lambda_r=1, debug=True)
    bigan.train(input_sequence, output_sequence, epochs, batchsize, learning_rate, std_pose, mean_pose, base_length=base_length, recursive_loop=6, 
        sample_batchsize=10, save_every_epochs=20, sample_every_epochs=1, save_path=save_path, sample_path=sample_path, log_path=log_path)



def evaluate_bihmpgan_recursive():
    #### load training data
    bvhfile1 = os.path.dirname(os.path.abspath(__file__)) + r'/../../../data/MotionCaptureData\data\ViCon\4kmh.bvh'
    bvhfile2 = os.path.dirname(os.path.abspath(__file__)) + r'/../../../data/MotionCaptureData\data\ViCon\6kmh.bvh'
    files = [bvhfile1, bvhfile2]
    training_data = read_data(files)
    #training_data = np.load(os.path.dirname(os.path.abspath(__file__)) + r'/../../data/processed_mocap_data/Vicon/longclips120.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    ### TODO: config model parameters
    ## config = json()
    ## config = load(config.json)

    ## config["hidden_units"]

    hidden_units = 128
    z_dims = 64
    input_sequence_length = 30
    output_sequence_length = 200
    base_length = 30
    epochs = 200
    learning_rate = 1e-4
    batchsize = 16
    input_sequence = training_data[:, :input_sequence_length, :]
    output_sequence = training_data[:, input_sequence_length:, :]
    model_name = 'bihmpgan_recursive'

    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/model/'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/log'
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/samples'

    #g = Bihmp_Generator(hidden_units, output_sequence_length, dims, z_dims=z_dims, cell="lstm", use_residual=True)
    g = Bihmp_Generator(hidden_units, base_length, dims, z_dims=z_dims, cell="lstm", use_residual=True)
    d = Bihmp_Discriminator(hidden_units, hidden_units/2, input_sequence_length, base_length, z_dims=z_dims, cell="lstm", bidirectional=True, use_multi_states=True, output_activation=None)
    bigan = BiHMPGanRec(model_name, g, d, input_sequence_length, output_sequence_length, z_dims=z_dims, lambda_c=0.1, lambda_r=1, debug=True)
    bigan.load(save_path, suffix=str(epochs))
    ##### export motion samples

    ## test generator
    n_samples = 10
    test_input = training_data[100:100 + n_samples, :input_sequence_length]
    test_output = training_data[100:100 + n_samples, input_sequence_length:]
    next_input = test_input
    emit_output = [test_input]
    for i in range(2):
        random_noise = tf.random.normal([n_samples, z_dims], dtype=tf.float32)
        predict_output = bigan.g(next_input, random_noise).numpy()
        emit_output.append(predict_output)
        next_input = predict_output
    predict_motion = np.concatenate(emit_output, axis=1)


    #predict_output = bigan.g(test_input, random_noise).numpy()
    real_motion = np.concatenate([test_input, test_output], axis=1)
    #predict_motion = np.concatenate([test_input, predict_output], axis=1)
    real_motion = real_motion * std_pose + mean_pose
    predict_motion = predict_motion * std_pose + mean_pose
    print(real_motion.shape)
    #### visualize generate motions
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/samples'
    if not os.path.exists(sample_path):
        os.makedirs(sample_path)
    skeleton_bvh = os.path.dirname(os.path.abspath(__file__)) + r'/../../../data\MotionCaptureData\data\Vicon\4kmh.bvh'  ### provide skeleton example
    skeleton_bvh = BVHReader(skeleton_bvh)
    skeleton = SkeletonBuilder().load_from_bvh(skeleton_bvh)
    animated_joints = skeleton.generate_bone_list_description() 
    if isinstance(animated_joints, list):
        animated_joints = collections.OrderedDict([(d['name'], {'parent':d['parent'], 'index':d['index']}) for d in animated_joints])
    
    for i in range(n_samples):
        origin_filename = os.path.join(sample_path, 'origin_' + str(i) + '.panim')
        export_point_cloud_data_without_foot_contact(real_motion[i], origin_filename, skeleton=animated_joints)
        sample_filename = os.path.join(sample_path, 'generated_epoch_{epoch}_'.format(epoch=epochs) + str(i) + '.panim')
        export_point_cloud_data_without_foot_contact(predict_motion[i], sample_filename, skeleton=animated_joints)





if __name__ == "__main__":
    # train_bihmpgan_recursive()
    evaluate_bihmpgan_recursive()