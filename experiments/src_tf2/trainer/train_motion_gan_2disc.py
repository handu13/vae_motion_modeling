import os
import sys
sys.path.append(r'H:\vae\vae_motion_modeling')
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + r'/../../..')
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + r'/..')
sys.path.append("..")
os.chdir(os.path.dirname(os.path.abspath(__file__)))
import tensorflow as tf
import numpy as np
import collections
from mosi_dev_deepmotionmodeling.utilities.utils import get_files, export_point_cloud_data_without_foot_contact, write_to_json_file
from mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder, panim
from model.motion_gan_2disc import MotionGAN_2Disc
from network.Discriminator import Bihmp_Discriminator
from network.Generator import Bihmp_Generator
EPS = 1e-6



def train_mgan2disc():
    #### load training data
    # training_data = np.load(os.path.dirname(os.path.abspath(__file__)) + r'/../../data/processed_mocap_data/Vicon/clips.npy')
    training_data = np.load(r'../../../data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    ### TODO: config model parameters
    ## config = json()
    ## config = load(config.json)

    ## config["hidden_units"]

    hidden_units = 128
    z_dims = 64
    input_sequence_length = 30
    output_sequence_length = 30
    epochs = 200
    learning_rate = 1e-4
    batchsize = 16
    input_sequence = training_data[:, :input_sequence_length, :]
    output_sequence = training_data[:, input_sequence_length:, :]
    model_name = 'mgan2disc'

    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/model'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/log'
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/samples'



    g = Bihmp_Generator(hidden_units, output_sequence_length, dims, z_dims=z_dims, cell="lstm", use_residual=True)
    d = Bihmp_Discriminator(hidden_units, hidden_units/2, input_sequence_length, output_sequence_length, cell="lstm", output_activation=None)
    e = Bihmp_Discriminator(hidden_units, hidden_units/2, input_sequence_length, output_sequence_length, cell="lstm", output_activation=tf.nn.sigmoid)
    mgan2d = MotionGAN_2Disc(model_name, g, d, e, z_dims=z_dims, lambda_c=0.1, debug=True)
    mgan2d.train(input_sequence, output_sequence, epochs, batchsize, learning_rate,  
        save_every_epochs=20, save_path=save_path, log_path=log_path)



def evaluate_mgan2disc():
    #### load training data
    training_data = np.load(os.path.dirname(os.path.abspath(__file__)) + r'/../../../data/training_data/processed_mocap_data/Vicon/clips.npy')
    # training_data = np.load(r'../../../data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    hidden_units = 128
    z_dims = 64
    input_sequence_length = 30
    output_sequence_length = 360
    epochs = 200
    learning_rate = 1e-4
    batchsize = 16
    input_sequence = training_data[:, :input_sequence_length, :]
    output_sequence = training_data[:, input_sequence_length:, :]
    model_name = 'mgan2disc'

    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/model'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/log'
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/samples'

    g = Bihmp_Generator(hidden_units, output_sequence_length, dims, z_dims=z_dims, cell="lstm", use_residual=True)
    d = Bihmp_Discriminator(hidden_units, hidden_units/2, input_sequence_length, output_sequence_length, cell="lstm", output_activation=None)
    e = Bihmp_Discriminator(hidden_units, hidden_units/2, input_sequence_length, output_sequence_length, cell="lstm", output_activation=tf.nn.sigmoid)
    mgan2d = MotionGAN_2Disc(model_name, g, d, e, z_dims=z_dims, lambda_c=0.1)
    # mgan2d.load(save_path, suffix=str(epochs))
    ##### export motion samples

    ## test generator
    n_samples = 10
    test_input = training_data[100:100 + n_samples, :input_sequence_length]
    test_output = training_data[100:100 + n_samples, input_sequence_length:]
    random_noise = tf.random.normal([n_samples, z_dims], dtype=tf.float32)
    predict_output = mgan2d.g(test_input, random_noise).numpy()
    real_motion = np.concatenate([test_input, test_output], axis=1)
    predict_motion = np.concatenate([test_input, predict_output], axis=1)
    real_motion = real_motion * std_pose + mean_pose
    predict_motion = predict_motion * std_pose + mean_pose
    # print(real_motion.shape)
    #### visualize generate motions
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/samples'
    if not os.path.exists(sample_path):
        os.makedirs(sample_path)
    skeleton_bvh = os.path.dirname(os.path.abspath(__file__)) + r'/../../../data\MotionCaptureData\data\Vicon\4kmh.bvh'  ### provide skeleton example
    skeleton_bvh = BVHReader(skeleton_bvh)
    skeleton = SkeletonBuilder().load_from_bvh(skeleton_bvh)
    animated_joints = skeleton.generate_bone_list_description() 
    if isinstance(animated_joints, list):
        animated_joints = collections.OrderedDict([(d['name'], {'parent':d['parent'], 'index':d['index']}) for d in animated_joints])
    
    for i in range(n_samples):
        origin_filename = os.path.join(sample_path, 'origin_' + str(i) + '.panim')
        export_point_cloud_data_without_foot_contact(real_motion[i], origin_filename, skeleton=animated_joints)
        sample_filename = os.path.join(sample_path, 'generated_epoch_{epoch}_'.format(epoch=epochs) + str(i) + '_360_random' + '.panim')
        export_point_cloud_data_without_foot_contact(predict_motion[i], sample_filename, skeleton=animated_joints)


def evaluate_motions():
    #### load training data
    training_data = np.load(os.path.dirname(os.path.abspath(__file__)) + r'/../../../data/training_data/processed_mocap_data/Vicon/clips.npy')
    # training_data = np.load(r'../../../data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    hidden_units = 128
    z_dims = 64
    input_sequence_length = 30
    output_sequence_length = 30
    epochs = 200
    learning_rate = 1e-4
    batchsize = 16
    input_sequence = training_data[:, :input_sequence_length, :]
    output_sequence = training_data[:, input_sequence_length:, :]
    model_name = 'mgan2disc'

    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/model'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/log'
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/samples'

    g = Bihmp_Generator(hidden_units, output_sequence_length, dims, z_dims=z_dims, cell="lstm", use_residual=True)
    d = Bihmp_Discriminator(hidden_units, hidden_units/2, input_sequence_length, output_sequence_length, cell="lstm", output_activation=None)
    e = Bihmp_Discriminator(hidden_units, hidden_units/2, input_sequence_length, output_sequence_length, cell="lstm", output_activation=tf.nn.sigmoid)
    mgan2d = MotionGAN_2Disc(model_name, g, d, e, z_dims=z_dims, lambda_c=0.1)
    mgan2d.load(save_path, suffix=str(epochs))    

    ## test generator
    n_samples = 10
    test_input = training_data[100:100 + n_samples, :input_sequence_length]
    test_output = training_data[100:100 + n_samples, input_sequence_length:]
    random_noise = tf.random.normal([n_samples, z_dims], dtype=tf.float32)
    predict_output = mgan2d.g(test_input, random_noise).numpy()
    real_motion = np.concatenate([test_input, test_output], axis=1)
    predict_motion = np.concatenate([test_input, predict_output], axis=1)
    real_motion = real_motion * std_pose + mean_pose
    predict_motion = predict_motion * std_pose + mean_pose
    print(real_motion.shape)

    # e_likelihood = mgan2d.e(test_output)
    # print(e_likelihood)



if __name__ == "__main__":
    # train_mgan2disc()
    # evaluate_mgan2disc()
    evaluate_motions()