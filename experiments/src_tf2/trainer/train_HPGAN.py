import os
import sys

sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + r'/../../..')
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + r'/..')
sys.path.append("..")
os.chdir(os.path.dirname(os.path.abspath(__file__)))
import tensorflow as tf
from tensorflow.keras import layers, Model
import numpy as np
from mosi_dev_deepmotionmodeling.utilities.utils import get_files, export_point_cloud_data_without_foot_contact, write_to_json_file
from mosi_dev_deepmotionmodeling.mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder
from model.HPGAN import HPGAN
from network.Discriminator import hp_Discriminator
from network.Generator import hp_Generator
EPS = 1e-6



def train_hpgan():
    #### load training data
     
    training_data = np.load(r'../../../data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    ### config model parameters
    ## config = json()
    ## config = load(config.json)

    ## config["hidden_units"]

    hidden_units_g = 1024
    hidden_units_d = 512
    num_layers_g = 2
    num_layers_d = 3
    z_dims = 128
    embed_shape = 128
    input_sequence_length = 30
    output_sequence_length = 30
    epochs = 200
    learning_rate = 1e-4
    batchsize = 16
    input_sequence = training_data[:, :input_sequence_length, :]
    output_sequence = training_data[:, input_sequence_length:, :]
    model_name = 'HPGAN'

    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/model'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/log'


    g = hp_Generator(hidden_units_g, output_sequence_length, dims, z_dims=z_dims, embed_shape=embed_shape, num_layers=num_layers_g)
    d = hp_Discriminator(hidden_units_d, embed_shape=embed_shape, num_layers=num_layers_d)
    e = hp_Discriminator(hidden_units_d, embed_shape=embed_shape, num_layers=num_layers_d, output_activation=tf.nn.sigmoid)
    hpgan = HPGAN(name=model_name, g=g, d=d, e=e, z_dims=z_dims, lambda_c=0.001, lambda_b=0.01, debug=True)
    hpgan.train(input_sequence, output_sequence, epochs, batchsize, learning_rate, save_every_epochs=20,
                     save_path=save_path, log_path=log_path)



def evaluate_hpgan():
    #### load training data
     
    training_data = np.load(r'../../../data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose   
    n_samples, sequence_length, dims = training_data.shape
    ### config model parameters
    ## config = json()
    ## config = load(config.json)

    ## config["hidden_units"]

    hidden_units_g = 1024
    hidden_units_d = 512
    num_layers_g = 2
    num_layers_d = 3
    z_dims = 128
    embed_shape = 128
    input_sequence_length = 30
    output_sequence_length = 30
    epochs = 200
    learning_rate = 1e-4
    batchsize = 16
    input_sequence = training_data[:, :input_sequence_length, :]
    output_sequence = training_data[:, input_sequence_length:, :]
    model_name = 'HPGAN'

    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/model'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/log'


    g = hp_Generator(hidden_units_g, output_sequence_length, dims, z_dims=z_dims, embed_shape=embed_shape, num_layers=num_layers_g)
    d = hp_Discriminator(hidden_units_d, embed_shape=embed_shape, num_layers=num_layers_d)
    e = hp_Discriminator(hidden_units_d, embed_shape=embed_shape, num_layers=num_layers_d, output_activation=tf.nn.sigmoid)
    hpgan = HPGAN(name=model_name, g=g, d=d, e=e, z_dims=z_dims, lambda_c=0.001, lambda_b=0.01)
    hpgan.load(save_path, suffix=str(epochs))
    ##### export motion samples
    
    ## test generator
    n_samples = 10
    test_input = training_data[100:100 + n_samples, :input_sequence_length]
    test_output = training_data[100:100 + n_samples, input_sequence_length:]
    random_noise = tf.random.normal([n_samples, z_dims], dtype=tf.float32)
    predict_output = hpgan.g(test_input, random_noise).numpy()
    real_motion = np.concatenate([test_input, test_output], axis=1)
    predict_motion = np.concatenate([test_input, predict_output], axis=1)
    real_motion = real_motion * std_pose + mean_pose
    predict_motion = predict_motion * std_pose + mean_pose
    #### visualize generate motions
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/' + model_name + '/samples'
    if not os.path.exists(sample_path):
        os.makedirs(sample_path)
    skeleton_bvh = r'../../../data\MotionCaptureData\data\Vicon\4kmh.bvh'  ### provide skeleton example
    skeleton_bvh = BVHReader(skeleton_bvh)
    skeleton = SkeletonBuilder().load_from_bvh(skeleton_bvh)
    animated_joints = skeleton.generate_bone_list_description() 
    
    for i in range(n_samples):
        origin_filename = os.path.join(sample_path, 'origin_' + str(i) + '.panim')
        export_point_cloud_data_without_foot_contact(real_motion[i], origin_filename, skeleton=animated_joints)
        sample_filename = os.path.join(sample_path, 'generated_epoch_{epoch}_'.format(epoch=epochs) + str(i) + '.panim')
        export_point_cloud_data_without_foot_contact(predict_motion[i], sample_filename, skeleton=animated_joints)

    # ## test discriminator
    # random_noise = np.random.rand(*predict_output.shape)
    # noise_motion = np.concatenate([test_input, random_noise], axis=1)

    # real_probs = motion_gan.d(real_motion).numpy()

    # fake_probs = motion_gan.d(predict_motion).numpy()
    # random_probs = motion_gan.d(noise_motion).numpy()
    # print("real probs: {r_p}  fake_probs: {f_p}  random probs: {n_p}".format(r_p=np.mean(real_probs), f_p=np.mean(fake_probs), n_p=np.mean(random_probs)))


if __name__ == "__main__":
    train_hpgan()
    # test()
    # evaluate_hpgan()