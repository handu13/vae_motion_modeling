import os
import sys
from pathlib import Path
abspath = os.path.abspath(__file__)
sys.path.insert(0, str(Path(__file__).parent.absolute()) + r'/../../..')
sys.path.insert(0, str(Path(__file__).parent.absolute()) + r'/..')
import tensorflow as tf
from tensorflow.keras import layers, Model
from network.Discriminator import RNNDiscriminator
from network.Generator import Seq2seq
from model.BiHMP_GAN import BiHMPGan
import numpy as np
cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)
# rng = np.random.RandomState(23456)
# tf.random.set_seed(rng)
from mosi_dev_deepmotionmodeling.utilities.utils import get_files, export_point_cloud_data_without_foot_contact, write_to_json_file
EPS = 1e-6     



def train_BiHMP_GAN():
    #### load training data 
    training_data = np.load(r'data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose

    ### initial models 
    hidden_units = 128
    fc_hidden = 64
    input_length = 30
    output_length = 30
    output_dims = 72
    z_dims = 64
    g = Seq2seq(hidden_units, output_length, output_dims, z_dims)
    d = RNNDiscriminator(hidden_units, fc_hidden, input_length, output_length, z_dims, cell="lstm", bidirectional=True, use_multi_states=True)

    epochs = 1000
    model = BiHMPGan('bihmp_gan_vicon', g, d, z_dims, input_length, output_length)

    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/savedmodel/bihmp_gan_tf2_graph_init'
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/bihmp_gan_tf2_graph_init'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/logs/bihmp_gan_tf2_graph_init'
    model.train(training_data, epochs, 32, std_pose, mean_pose, log_path=log_path, sample_every_epochs=100, sample_path=sample_path, save_every_epochs=100, save_path=save_path)
    model.save(save_path)
    model.load(save_path)


def test_gan():
    training_data = np.load(r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose

    hidden_units = 128
    fc_hidden = 64
    input_length = 30
    output_length = 30
    output_dims = 72
    z_dims = 64
    g = Seq2seq(hidden_units, output_length, output_dims, z_dims)
    d = RNNDiscriminator(hidden_units, fc_hidden, input_length, output_length, z_dims, cell="lstm", bidirectional=True, use_multi_states=True)
    epochs = 100
    model = BiHMPGan('bihmp_gan_vicon', g, d, z_dims, input_length, output_length)

    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../savedmodel/bihmp_gan_tf2_graph_init'
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/bihmp_gan_tf2_graph_init'
    log_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../logs/bihmp_gan_tf2_graph_init'
    model.train(training_data, epochs, 32, std_pose, mean_pose, log_path=log_path, sample_every_epochs=1, sample_path=sample_path, save_every_epochs=1, save_path=save_path)
    model.save(save_path)
    model.load(save_path)


def evaluate_BiHMP_GAN():
    "load models at different training phase and sample"
    training_data = np.load(r'D:\workspace\my_git_repos\vae_motion_modeling\data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    
    hidden_units = 128
    fc_hidden = 64
    input_length = 30
    output_length = 30
    output_dims = 72
    z_dims = 64
    g = Seq2seq(hidden_units, output_length, output_dims, z_dims)
    d = RNNDiscriminator(hidden_units, fc_hidden, input_length, output_length, z_dims, cell="lstm", bidirectional=True, use_multi_states=True)
    model = BiHMPGan('bihmp_gan_vicon', g, d, z_dims, input_length, output_length)
    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../savedmodel/bihmp_gan_tf2_graph_init'
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../output/bihmp_gan_tf2_graph_init'
    epochs = 200
    save_every_epochs=10
    n_samples = 10
    sample_data = training_data[:n_samples]
    for i in range(epochs):
        if (i+1) % save_every_epochs == 0:
            model.load(save_path, suffix=str(i+1))
            new_samples = model.sample_new_motions(sample_data)
            sample_dir = os.path.join(sample_path, str(i+1))
            if not os.path.exists(sample_dir):
                os.makedirs(sample_dir)
            for j in range(n_samples):
                filename = os.path.join(sample_dir, str(j) + '.panim')
                export_point_cloud_data_without_foot_contact(new_samples[j], filename)

 
def train_BiHMP_content_loss():
    "load models at different training phase and sample"
    training_data = np.load(r'data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS

    


if __name__ == "__main__":
    # train_BiHMP_GAN()
    evaluate_BiHMP_GAN()