import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + r'/..')
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + r'/../../..')
os.chdir(os.path.dirname(os.path.abspath(__file__)))
from network.Generator import Seq2seq
import numpy as np 
import tensorflow as tf 
from mosi_dev_deepmotionmodeling.utilities.utils import get_files, export_point_cloud_data_without_foot_contact
from mosi_dev_deepmotionmodeling.mosi_utils_anim.animation_data import BVHReader, SkeletonBuilder
EPS = 1e-6





def train_seq2seq():
    #### load training data
     
    # training_data = np.load(r'data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.load(r'data\training_data\clipwise\h36m.npz')['clips']
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose   

    ### config model parameters
    hidden_units = 10
    input_sequence_length = 30
    epochs = 100
    learning_rate = 1e-4
    batchsize = 32
    n_samples, sequence_length, dims = training_data.shape
    input_sequence = training_data[:, :input_sequence_length, :]
    output_sequence = training_data[:, input_sequence_length:, :]
    model_name = 'seq2seq_h36m'
    generator = Seq2seq(hidden_units, 30, dims, name=model_name)
    generator.compile(optimizer=tf.keras.optimizers.Adam(learning_rate), loss='mse')
    save_path = os.path.dirname(os.path.abspath(__file__)) + r'/../../../output/' + model_name + r'/model'
    file_path = os.path.join(save_path, 'cp-{epoch:04d}.ckpt')
    log_dir = os.path.dirname(os.path.abspath(__file__)) + r'/../../../output/' + model_name + r'/log'
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=file_path, verbose=1, save_weights_only=True, period=10)
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir)

    generator.fit(input_sequence, output_sequence, epochs=epochs, callbacks=[cp_callback, tensorboard_callback])


def evaluate():
    # training_data = np.load(r'data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.load(r'../../../data\training_data\clipwise\h36m.npz')['clips']
    training_data = np.asarray(training_data, dtype=np.float32)

    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose   

    ### config model parameters
    hidden_units = 10
    input_sequence_length = 30
    epochs = 100
    learning_rate = 1e-4
    batchsize = 32
    n_samples, sequence_length, dims = training_data.shape
    model_name = 'seq2seq_h36m'    
    input_sequence = training_data[:, :input_sequence_length, :]
    output_sequence = training_data[:, input_sequence_length:, :]
    generator = Seq2seq(hidden_units, 30, dims)   
    generator.build(input_shape=input_sequence.shape)
    save_path = os.path.dirname(os.path.abspath(__file__)) + r'../../../output/' + model_name + r'/model'
    generator.load_weights(os.path.join(save_path, 'cp-{epoch:04d}.ckpt'.format(epoch = 100)))

    new_motions = generator(input_sequence[1100:1200]).numpy()
    concatenated_motions = np.concatenate([input_sequence[1100:1200], new_motions], axis=1)
 
    concatenated_motions = concatenated_motions * std_pose + mean_pose
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'../../../output/' + model_name + r'/new_samples'
    if not os.path.exists(sample_path):
        os.makedirs(sample_path)

    skeleton_bvh = r'D:\workspace\projects\DHM2020\MotionCaptureData\data\Vicon\4kmh.bvh'  ### provide skeleton example
    skeleton_bvh = BVHReader(skeleton_bvh)
    skeleton = SkeletonBuilder().load_from_bvh(skeleton_bvh)
    animated_joints = skeleton.generate_bone_list_description()  

    for i in range(100):
        filename = os.path.join(sample_path, str(i) + '.panim')
        export_point_cloud_data_without_foot_contact(concatenated_motions[i], filename=filename)


def compare_random_noise():
    training_data = np.load(r'../../../data\training_data\processed_mocap_data\Vicon\clips.npy')
    training_data = np.asarray(training_data, dtype=np.float32)
    mean_pose = training_data.mean(axis=(0, 1))
    std_pose = training_data.std(axis=(0, 1))
    std_pose[std_pose<EPS] = EPS
    print(training_data.shape)
    training_data = (training_data - mean_pose) / std_pose
    input_sequence_length = 30 
    input_sequence = training_data[:, :input_sequence_length, :] 
    new_motions = np.random.rand(*input_sequence[100:110].shape)
    concatenated_motions = np.concatenate([input_sequence[100:110], new_motions], axis=1)    
    concatenated_motions = concatenated_motions * std_pose + mean_pose
    sample_path = os.path.dirname(os.path.abspath(__file__)) + r'../../../output/seq2seq_generator/noise_samples'
    if not os.path.exists(sample_path):
        os.makedirs(sample_path)
 

    for i in range(10):
        filename = os.path.join(sample_path, str(i) + '.panim')
        export_point_cloud_data_without_foot_contact(concatenated_motions[i], filename=filename)



if __name__ == "__main__":
    # train_seq2seq()
    evaluate()
    # compare_random_noise()
